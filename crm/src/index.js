import React from 'react';
import { render } from 'react-dom';
// import Header from 'webmotors-react-pj/frame/header/logged';
import Menu from 'webmotors-react-pj/frame/menu';
import Footer from 'webmotors-react-pj/frame/footer/logged';

const header = document.getElementById('header');
const menu = document.getElementById('menu');
const footer = document.getElementById('footer');

const htmlRender = [
    /* [<Header />, header], */
    [<Menu />, menu],
    [<Footer />, footer],
];

htmlRender.forEach((item) => {
    const [component, element] = item;
    if (element) {
        render(component, element);
    }
});
