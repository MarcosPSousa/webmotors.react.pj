const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const entry = require('./config/entry');
const resolve = require('./config/resolve');
const target = require('./config/target');
const modules = require('./config/module');

process.noDeprecation = true;

module.exports = {
    entry,
    resolve,
    target,
    module: modules,
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.resolve(__dirname, '../template/index.html'),
            chunks: ['page'],
        }),
    ],
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: './cockpit.js',
    },
};