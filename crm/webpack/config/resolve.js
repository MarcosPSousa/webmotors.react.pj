const path = require('path');

module.exports = {
    extensions: ['.js', '.jsx'],
    alias: {
        /* 'react': path.resolve(__dirname, '../../../node_modules/react'),
        'react-dom': path.resolve(__dirname, '../../../node_modules/react-dom'),
        'styled-components': path.resolve(__dirname, '../../../node_modules/styled-components'), */
        'webmotors-react-pj/config': path.resolve(__dirname, `../../../dist/config/env/${process.env.NODE_ENV || 'production'}`),
        'webmotors-react-pj': path.resolve(__dirname, '../../../dist'),
    },
};
