const environment = process.env.NODE_ENV;
const now = new Date();
const dateStr = `${now.getFullYear()}${now.getMonth() + 1}${now.getDate()}-${now.getHours()}${now.getMinutes()}`;

const gtmScript = (`
    <script>(function (w, d, s, l, i) { w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' }); var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f); })(window, document, 'script', 'dataLayer', 'GTM-58Z5D5K');</script>
`);

const apmScript = (`
    <script src="https://apm.wmaws.com.br/scripts/elastic-apm-rum.umd.min.js?t=${dateStr}" crossorigin></script>
    <script>if (elasticApm) elasticApm.init({ serviceName: "Cockpit UI - ${environment}", serverUrl: "https://apm.wmaws.com.br", pageLoadTransactionName: window.location.pathname });</script>
`);

const launchScript = '<script src="//assets.adobedtm.com/0a9348ad03c2/8552c5c8b699/launch-9b2932c91a4e.min.js"></script>';

const scripts = () => {
    const src = {
        apm: {
            development: '<script data-script-apm></script>',
            homologation: apmScript,
            blue: apmScript,
            production: apmScript,
        },
        gtm: {
            development: '<script data-script-gtm></script>',
            homologation: gtmScript,
            blue: gtmScript,
            production: gtmScript,
        },
        launch: {
            development: '<script data-script-launch></script>',
            homologation: launchScript,
            blue: launchScript,
            production: launchScript,
        },
    };
    const resolve = {};
    Object.entries(src).forEach((item) => { resolve[item[0]] = item[1][environment]; });
    return resolve;
};

module.exports = scripts();
