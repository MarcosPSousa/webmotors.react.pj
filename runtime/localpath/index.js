const path = require('path');

const current = pathFile => path.resolve(__dirname, pathFile);
const base = path.resolve(__dirname, '../..');
const src = current(`${base}/src`);
const dist = current(`${base}/dist`);

module.exports = {
    current: file => current(file),
    base,
    src,
    dist,
};
