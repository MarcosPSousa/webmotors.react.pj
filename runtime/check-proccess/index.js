module.exports = (name, error) => {
    if (error) {
        console.log('\x1b[31m%s\x1b[0m', `✖ ${name}\n---\n`);
        console.log(error);
    } else {
        console.log('\x1b[32m%s\x1b[0m', `✔ ${name}`);
    }
};
