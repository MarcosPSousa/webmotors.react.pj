const fs = require('fs');
const config = require('../../src/config/config');
const localpath = require('../localpath');

const configList = [];

Object.entries(config).forEach((item) => {
    const configObject = Object.keys(item[1]);
    configObject.forEach((env) => {
        if (!configList.includes(env.toLowerCase())) {
            configList.push(env.toLowerCase());
        }
    });
});

configList.forEach((env) => {
    const configEntries = {};
    Object.entries(config).forEach((item) => {
        configEntries[item[0]] = item[1][env] ? item[1][env] : '';
    });
    fs.writeFileSync(`${localpath.src}/config/env/${env}.js`, `module.exports=${JSON.stringify(configEntries)}`);
    console.log('\x1b[32m%s\x1b[0m', `✔ config/env/${env}.js`);
});
