const { exec } = require('child_process');
const checkProccess = require('../check-proccess');

const env = {
    development: 'development',
    homologation: 'homologation',
    blue: 'blue',
    production: 'production',
};

exec(`set NODE_ENV=${env.development}&&webpack -d --config crm/webpack/webpack.config.js`, error => checkProccess('crm bundle', error));
