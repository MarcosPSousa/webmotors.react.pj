const fs = require('fs');
const { exec } = require('child_process');
const checkProccess = require('./check-proccess');
const localpath = require('./localpath');

const isCRM = (process.env.npm_lifecycle_event === 'crm');

if (!fs.existsSync(localpath.dist)) {
    fs.mkdirSync(localpath.dist);
}

require('./menu');
require('./config');

exec(`cd .. && babel ${localpath.src} -d ${localpath.dist} --no-comments`, (error) => {
    checkProccess('babel.js', error);
    // require('./crm');
if (!isCRM) {
    fs.copyFile(`${localpath.base}/package.json`, `${localpath.dist}/package.json`, error => checkProccess('package.json', error));

    // exec(`cd ${localpath.dist} && npm i`, error => checkProccess('cd dist && npm i', error));

    const color1 = '\x1b[91m';
    const color2 = '\x1b[97m';
    const color3 = '\x1b[31m';
    const color4 = '\x1b[37m';

    const logoIcon = `
                      *#######(,
                  (################.
                #####################*
              (########################
             (#####.*####/@@#####.(####
            .#####*@@####*@@####/@@######
            ######*@@####*@@####/@@######.
            ######*@@@@@@@@@@@@@@@@######*
            ######*@@####*@@####/@@######.
            .#####*@@####*@@####/@@######
             (#####@,####/@@#####@*#####
              /########################
                #####################*
                  #################,
                     .(########*
    `;
    console.log(logoIcon
        .replace(/#/g, `${color1}#`)
        .replace(/\*/g, `${color3}*`)
        .replace(/\(/g, `${color3}(`)
        .replace(/,/g, `${color4},`)
        .replace(/\./g, `${color4}.`)
        .replace(/@/g, `${color2}@`));
}
});


if (isCRM) {
    const logoType = `
                                     &@@.                                            .,.                                    
                                     &@@.                                            @@@                                    
    &@@    (@@/   .@@(     #@@@(     &@@./&@@#        .%&%*   #&&*        (&@@%.     @@@@@/    .%@@&*         (&@   *&@@#.  
    @@@.   %@@#   *@@%  /@@@@@@@@@(  &@@@@@@@@@@#   %@@@@@@@@@@@@@@@   .@@@@@@@@@&   @@@@@%  @@@@@@@@@@    /@@@@@./@@@@@@@& 
    @@@.   %@@#   *@@% (@@&     %@@# &@@&     #@@@ /@@&   /@@@   *@@@ .@@@     /@@&  @@@    @@@*     @@@. #@@&    %@@&
    @@@.   %@@#   *@@% @@@@@@@@@@@@& &@@.      @@@ #@@(   .@@@   .@@@ #@@*      @@@  @@@   .@@@      &@@( @@@.     ,@@@@@@&
    &@@&   @@@@   &@@* ,@@@     (    .@@@.    &@@& #@@(   .@@@   .@@@  @@@.    &@@%  @@@    @@@%    /@@@  @@@.      .   &@@%
     &@@@@@@@@@@@@@@%   ,@@@@@@@@@*   .@@@@@@@@@/  #@@(   .@@@   .@@@   @@@@@@@@@#   &@@@@%  &@@@@@@@@&   @@@.    &@@@@@@@@.
       .#%/    (%#.        *%&%,         .#&%*     ,%%.    %%(    %%#     ,%&%(        /%%,     (&.     (%%       ,%&%/
    `;
    console.log(logoType);
}
