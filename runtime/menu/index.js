const fs = require('fs');
const items = require('./items');
const localpath = require('../localpath');

const distFolder = localpath.current(`${localpath.base}/src/frame/menu/env`);

if (!fs.existsSync(distFolder)) {
    fs.mkdirSync(distFolder);
}

const menuHasSubItems = items.filter(item => item.url);
const { url: urlSubItems } = menuHasSubItems[0];
const envs = Object.keys(urlSubItems);

envs.forEach((env) => {
    const menuAdd = itemArray => (
        itemArray.map((itemObj) => {
            const {
                url,
                sub,
                name,
                icon,
                order,
            } = itemObj;
            if (url) {
                const obj = {};
                const objAdd = (key, value) => {
                    if (value) {
                        obj[key] = value;
                    }
                };
                objAdd('name', name);
                objAdd('icon', icon);
                objAdd('url', url[env]);
                objAdd('order', order);
                return obj;
            }

            return {
                name,
                icon,
                sub: menuAdd(sub),
                order,
            };
        })
    );
    const test = menuAdd(items);
    fs.writeFileSync(`${distFolder}/${env}.js`, `// menu/${env}.js generate automatic. run code "npm start"\n\nmodule.exports=${JSON.stringify(test)}`);
    console.log('\x1b[32m%s\x1b[0m', `✔ menu/env/${env}.js`);
});
