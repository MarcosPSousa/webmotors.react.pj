const translate = require('./translate');

const config = require('../../src/config/config');

const urlGet = (urlPath = '/', urlBase = config.UrlCockpit) => {
    const urls = {};
    Object.entries(urlBase).forEach((item) => {
        const [key, value] = item;
        urls[key] = `${value}${urlPath}`;
    });
    return urls;
};

module.exports = [
    {
        name: translate.menu.home,
        icon: '/product-inicio.svg',
        url: urlGet('/', config.UrlCockpit),
        order: 1,
    },
    {
        name: translate.menu.crm,
        icon: '/product-crm.svg',
        sub: [
            {
                name: translate.menu.crmPanel,
                url: urlGet('/dashboard', config.UrlCRM),
            },
            {
                name: translate.menu.crmLeads,
                url: urlGet('/manage-leads', config.UrlCRM),
            },
            {
                name: translate.menu.crmActivities,
                url: urlGet('/schedule', config.UrlCRM),
            },
            {
                name: translate.menu.crmReports,
                url: urlGet('/reports', config.UrlCRM),
            },
            {
                name: translate.menu.crmConfig,
                url: urlGet('/configuration', config.UrlCRM),
            },
            {
                name: translate.menu.crmIntegration,
                url: urlGet('/integration', config.UrlCRM),
            },
        ],
        order: 1,
    },
    {
        name: translate.menu.gearbox,
        icon: '/product-gearbox.svg',
        sub: [
            {
                name: translate.menu.gearboxPanel,
                url: urlGet('/', config.UrlGearbox),
            },
            {
                name: translate.menu.gearboxSite,
                url: urlGet('/site', config.UrlGearbox),
            },
            {
                name: translate.menu.gearboxMedia,
                url: urlGet('/midia', config.UrlGearbox),
            },
        ],
        order: 1,
    },
    {
        name: translate.menu.stock,
        icon: '/product-estoque.svg',
        sub: [
            {
                name: translate.menu.stockConsultVehicle,
                url: urlGet('/gestao-estoque/consultar', config.UrlEstoquePlataforma),
            },
            {
                name: translate.menu.stockIncludeVehicle,
                url: urlGet('/gestao-estoque/incluir-anuncio', config.UrlEstoquePlataforma),
            },
            {
                name: translate.menu.stockBuyVehicle,
                url: urlGet('/', config.UrlComprarVeiculos),
            },
            {
                name: translate.menu.stockSellVehicle,
                url: urlGet('/vender-veiculo', config.UrlEstoquePlataforma),
            },
            {
                name: translate.menu.stockEvaluateVechicle,
                url: urlGet('/avaliar-veiculo', config.UrlEstoque),
            },
        ],
        order: 1,
    },
    {
        name: translate.menu.fidelity,
        icon: '/product-maisfidelidade.svg',
        url: urlGet('/', config.UrlMaisFidelidade),
        order: 1,
    },
    {
        name: translate.menu.university,
        icon: '/product-universidade.svg',
        url: urlGet('/wm-cockpit', config.UrlUniversidade),
        order: 1,
    },
    {
        name: translate.menu.autoguru,
        icon: '/product-autoguru.svg',
        url: urlGet('/', config.UrlAutoguru),
        order: 1,
    },
    {
        name: translate.menu.store,
        icon: '/product-store.svg',
        url: urlGet('/store', config.UrlCockpit),
        order: 1,
    },
    {
        name: translate.menu.account,
        sub: [
            {
                name: translate.menu.accountContractAdhesion,
                url: urlGet('/termos/pendentes', config.UrlCockpit),
            },
            {
                name: translate.menu.accountUsers,
                url: urlGet('/usuario/listagem/ativo', config.UrlCockpit),
            },
            {
                name: translate.menu.accountMyPlan,
                url: urlGet('/meu-plano/fatura', config.UrlCockpit),
            },
        ],
        order: 2,
    },
    {
        name: translate.menu.frequentsDoubts,
        url: {
            development: 'https://webmotors.zendesk.com/hc/pt-br/categories/360002515092-Lojas-Concession%C3%A1rias',
            homologation: 'https://webmotors.zendesk.com/hc/pt-br/categories/360002515092-Lojas-Concession%C3%A1rias',
            blue: 'https://webmotors.zendesk.com/hc/pt-br/categories/360002515092-Lojas-Concession%C3%A1rias',
            production: 'https://webmotors.zendesk.com/hc/pt-br/categories/360002515092-Lojas-Concession%C3%A1rias',
        },
        order: 2,
    },
];
