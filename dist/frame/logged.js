'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Menu = exports.Footer = exports.Header = undefined;

var _logged = require('webmotors-react-pj/frame/header/logged');

var _logged2 = _interopRequireDefault(_logged);

var _logged3 = require('webmotors-react-pj/frame/footer/logged');

var _logged4 = _interopRequireDefault(_logged3);

var _menu = require('webmotors-react-pj/frame/menu');

var _menu2 = _interopRequireDefault(_menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Header = _logged2.default;
exports.Footer = _logged4.default;
exports.Menu = _menu2.default;