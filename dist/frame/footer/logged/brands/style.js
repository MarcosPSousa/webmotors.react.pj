'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleImgSantander = exports.StyleImgWebmotors = exports.StyleSign = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: center;\n'], ['\n    display: flex;\n    align-items: center;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    height: 32px;\n    transform: translateY(4px);\n'], ['\n    height: 32px;\n    transform: translateY(4px);\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    height: 25px;\n    margin-left: 16px;\n'], ['\n    height: 25px;\n    margin-left: 16px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleSign = exports.StyleSign = _styledComponents2.default.div(_templateObject);

var StyleImgWebmotors = exports.StyleImgWebmotors = _styledComponents2.default.img(_templateObject2);

var StyleImgSantander = exports.StyleImgSantander = _styledComponents2.default.img(_templateObject3);