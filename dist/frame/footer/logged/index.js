'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/footer/logged/style');

var _date = require('webmotors-react-pj/frame/footer/logged/date');

var _date2 = _interopRequireDefault(_date);

var _brands = require('webmotors-react-pj/frame/footer/logged/brands');

var _brands2 = _interopRequireDefault(_brands);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.StyleFooter,
        null,
        _react2.default.createElement(
            _style.StyleContainer,
            null,
            _react2.default.createElement(_date2.default, null),
            _react2.default.createElement(_brands2.default, null)
        )
    );
};