'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleContainer = exports.StyleFooter = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: relative;\n    border-top: 1px solid ', ';\n    padding: 16px 32px;\n    background-color: ', ';\n    font-size: 14px;\n'], ['\n    position: relative;\n    border-top: 1px solid ', ';\n    padding: 16px 32px;\n    background-color: ', ';\n    font-size: 14px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n'], ['\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleFooter = exports.StyleFooter = _styledComponents2.default.footer(_templateObject, (0, _color2.default)('gray-4'), (0, _color2.default)('white'));

var StyleContainer = exports.StyleContainer = _styledComponents2.default.div(_templateObject2);