'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/footer/unlogged/brands-webmotors/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(_style.StyleImageWebmotors, { src: _config.UrlCockpit + '/assets/img/brands/webmotors-2-fill.svg' }),
        _react2.default.createElement(_style.StyleImageSantander, { src: _config.UrlCockpit + '/assets/img/brands/santander-financiamento.svg' })
    );
};