'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleImageSantander = exports.StyleImageWebmotors = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: center;\n    transform: translateX(-8px);\n'], ['\n    display: flex;\n    align-items: center;\n    transform: translateX(-8px);\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    height: 34px;\n    .st0, .st1{\n        fill: ', ';\n    }\n    .st2{\n        fill: ', ';\n    }\n'], ['\n    height: 34px;\n    .st0, .st1{\n        fill: ', ';\n    }\n    .st2{\n        fill: ', ';\n    }\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    height: 18px;\n    margin-left: 24px;\n    fill: ', ';\n    transform: translateY(-2px);\n'], ['\n    height: 18px;\n    margin-left: 24px;\n    fill: ', ';\n    transform: translateY(-2px);\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _img = require('webmotors-react-pj/img');

var _img2 = _interopRequireDefault(_img);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleImageWebmotors = exports.StyleImageWebmotors = (0, _styledComponents2.default)(_img2.default)(_templateObject2, (0, _color2.default)('white'), (0, _color2.default)('primary'));

var StyleImageSantander = exports.StyleImageSantander = (0, _styledComponents2.default)(_img2.default)(_templateObject3, (0, _color2.default)('white'));