'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/footer/unlogged/style');

var _appStore = require('webmotors-react-pj/frame/footer/unlogged/app-store');

var _appStore2 = _interopRequireDefault(_appStore);

var _socialMedia = require('webmotors-react-pj/frame/footer/unlogged/social-media');

var _socialMedia2 = _interopRequireDefault(_socialMedia);

var _brandsWebmotors = require('webmotors-react-pj/frame/footer/unlogged/brands-webmotors');

var _brandsWebmotors2 = _interopRequireDefault(_brandsWebmotors);

var _brands = require('webmotors-react-pj/frame/footer/unlogged/brands');

var _brands2 = _interopRequireDefault(_brands);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.Footer,
        null,
        _react2.default.createElement(
            _style.Container,
            null,
            _react2.default.createElement(
                _style.Row1,
                null,
                _react2.default.createElement(_appStore2.default, null),
                _react2.default.createElement(_socialMedia2.default, null)
            ),
            _react2.default.createElement(
                _style.Row2,
                null,
                _react2.default.createElement(_brandsWebmotors2.default, null),
                _react2.default.createElement(_brands2.default, null)
            )
        )
    );
};