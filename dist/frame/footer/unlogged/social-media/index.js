'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/footer/unlogged/social-media/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(
            'div',
            null,
            'Siga a #Webmotors'
        ),
        _react2.default.createElement(
            _style.StyleLink,
            { target: '_blank', rel: 'noopener noreferrer', href: 'https://www.instagram.com/webmotors' },
            _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/icons/instagram.svg' })
        ),
        _react2.default.createElement(
            _style.StyleLink,
            { target: '_blank', rel: 'noopener noreferrer', href: 'https://twitter.com/webmotors' },
            _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/icons/twitter.svg' })
        ),
        _react2.default.createElement(
            _style.StyleLink,
            { target: '_blank', rel: 'noopener noreferrer', href: 'https://facebook.com/webmotors' },
            _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/icons/facebook-like.svg' })
        )
    );
};