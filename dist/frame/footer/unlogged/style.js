'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Row2 = exports.Row1 = exports.Container = exports.Footer = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    background-color: ', ';\n    padding: 32px 22px;\n    font-size: 16px;\n    font-weight: 400;\n    color: ', ';\n'], ['\n    background-color: ', ';\n    padding: 32px 22px;\n    font-size: 16px;\n    font-weight: 400;\n    color: ', ';\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    max-width: 1280px;\n    width: 100%;\n    margin-left: auto;\n    margin-right: auto;\n'], ['\n    max-width: 1280px;\n    width: 100%;\n    margin-left: auto;\n    margin-right: auto;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    margin-bottom: 32px;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n'], ['\n    margin-bottom: 32px;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n'], ['\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Footer = exports.Footer = _styledComponents2.default.footer(_templateObject, (0, _color2.default)('gray'), (0, _color2.default)('white'));

var Container = exports.Container = _styledComponents2.default.div(_templateObject2);

var Row1 = exports.Row1 = _styledComponents2.default.div(_templateObject3);

var Row2 = exports.Row2 = _styledComponents2.default.div(_templateObject4);