'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleImage = exports.StyleTitle = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: center;\n'], ['\n    display: flex;\n    align-items: center;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    margin-right: 40px;\n'], ['\n    margin-right: 40px;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    height: 38px;\n    width: auto;\n    margin-right: 16px;\n'], ['\n    height: 38px;\n    width: auto;\n    margin-right: 16px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _img = require('webmotors-react-pj/img');

var _img2 = _interopRequireDefault(_img);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleTitle = exports.StyleTitle = _styledComponents2.default.div(_templateObject2);

var StyleImage = exports.StyleImage = (0, _styledComponents2.default)(_img2.default)(_templateObject3);