'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/footer/unlogged/app-store/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(
            _style.StyleTitle,
            null,
            'Baixe nosso app:'
        ),
        _react2.default.createElement(
            'a',
            { target: '_blank', rel: 'noopener noreferrer', href: 'https://apps.apple.com/br/app/revendedor-webmotors/id1069917512' },
            _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/static/store-app-store.png', alt: 'App Store' })
        ),
        _react2.default.createElement(
            'a',
            { target: '_blank', rel: 'noopener noreferrer', href: 'https://apps.apple.com/br/app/revendedor-webmotors/id1069917512' },
            _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/static/store-google-play.png', alt: 'Google Play' })
        )
    );
};