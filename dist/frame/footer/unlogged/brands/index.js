'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/footer/unlogged/brands/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(
            _style.StyleTitle,
            null,
            'Marcas Webmotors:'
        ),
        _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/brands/compreauto.svg' }),
        _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/brands/meucarango.svg', size: 'big' }),
        _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/brands/blucarros.svg' }),
        _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/brands/poacarros.svg' }),
        _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/brands/joinvillecarros.svg' }),
        _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/brands/floripacarros.svg' }),
        _react2.default.createElement(_style.StyleImage, { src: _config.UrlCockpit + '/assets/img/brands/loop.svg' })
    );
};