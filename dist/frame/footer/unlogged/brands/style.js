'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleImage = exports.StyleTitle = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: center;\n'], ['\n    display: flex;\n    align-items: center;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    opacity: 0.3;\n    font-weight: 100;\n'], ['\n    opacity: 0.3;\n    font-weight: 100;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    margin-left: 28px;\n    fill: ', ';\n    opacity: 0.2;\n    height: ', '\n'], ['\n    margin-left: 28px;\n    fill: ', ';\n    opacity: 0.2;\n    height: ', '\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _img = require('webmotors-react-pj/img');

var _img2 = _interopRequireDefault(_img);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleTitle = exports.StyleTitle = _styledComponents2.default.div(_templateObject2);

var StyleImage = exports.StyleImage = (0, _styledComponents2.default)(_img2.default)(_templateObject3, (0, _color2.default)('white'), function (props) {
    return props.size === 'big' ? '30px' : '20px';
});