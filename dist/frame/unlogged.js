'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Footer = exports.Header = undefined;

var _unlogged = require('webmotors-react-pj/frame/header/unlogged');

var _unlogged2 = _interopRequireDefault(_unlogged);

var _unlogged3 = require('webmotors-react-pj/frame/footer/unlogged');

var _unlogged4 = _interopRequireDefault(_unlogged3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Header = _unlogged2.default;
exports.Footer = _unlogged4.default;