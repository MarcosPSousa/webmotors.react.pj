'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _style = require('webmotors-react-pj/frame/header/unlogged/logo/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var handleClick = function handleClick() {
        return (0, _utils.scrollSmooth)(0);
    };

    return _react2.default.createElement(
        _style.StyleWrapper,
        { onClick: function onClick() {
                return handleClick();
            }, href: '/' },
        _react2.default.createElement(_style.StyleImg, { src: _config.UrlCockpit + '/assets/img/brands/cockpit.svg', 'aria-label': 'Logo do Cockpit' })
    );
};