'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleImg = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    width: 118px;\n    line-height: 50%;\n'], ['\n    width: 118px;\n    line-height: 50%;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    .st0{\n        fill: ', ';\n    }\n    .st1{\n        fill: ', ';\n    }\n'], ['\n    .st0{\n        fill: ', ';\n    }\n    .st1{\n        fill: ', ';\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _img = require('webmotors-react-pj/img');

var _img2 = _interopRequireDefault(_img);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.a(_templateObject);

var StyleImg = exports.StyleImg = (0, _styledComponents2.default)(_img2.default)(_templateObject2, (0, _color2.default)('#232323'), (0, _color2.default)('primary'));