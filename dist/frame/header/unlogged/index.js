'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/header/unlogged/style');

var _style2 = _interopRequireDefault(_style);

var _logo = require('webmotors-react-pj/frame/header/unlogged/logo');

var _logo2 = _interopRequireDefault(_logo);

var _menu = require('webmotors-react-pj/frame/header/unlogged/menu');

var _menu2 = _interopRequireDefault(_menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style2.default,
        null,
        _react2.default.createElement(_logo2.default, null),
        _react2.default.createElement(_menu2.default, null)
    );
};