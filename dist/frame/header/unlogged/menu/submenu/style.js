'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleSubItem = exports.StyleSubMenu = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    0% {opacity: 0;}\n    100% {opacity: 1;}\n'], ['\n    0% {opacity: 0;}\n    100% {opacity: 1;}\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['', ' ', 's forwards'], ['', ' ', 's forwards']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    opacity: 1;\n    transition: opacity 0.3s;\n    &:first-child a{\n        border-radius: 8px 8px 0 0;\n    }\n    &:last-child a{\n        border-radius: 0 0 8px 8px;\n    }\n'], ['\n    opacity: 1;\n    transition: opacity 0.3s;\n    &:first-child a{\n        border-radius: 8px 8px 0 0;\n    }\n    &:last-child a{\n        border-radius: 0 0 8px 8px;\n    }\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    background-color: ', ';\n    display: block;\n    position: relative;\n    z-index: 1;\n    font-weight: 500;\n    padding: 18px;\n    min-width: 220px;\n    border-bottom: 1px solid ', ';\n    animation: ', ';\n    transition: color 0.3s, transform 0.3s 1s;\n    &:hover{\n        color: ', ';\n    }\n'], ['\n    background-color: ', ';\n    display: block;\n    position: relative;\n    z-index: 1;\n    font-weight: 500;\n    padding: 18px;\n    min-width: 220px;\n    border-bottom: 1px solid ', ';\n    animation: ', ';\n    transition: color 0.3s, transform 0.3s 1s;\n    &:hover{\n        color: ', ';\n    }\n']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    right: 0;\n    top: 100%;\n    min-width: 100%;\n    border-top: 12px solid transparent;\n    box-shadow: 8px 0 12px -20px rgba(35, 35, 35, 0.6), -12px 0 12px -20px rgba(35, 35, 35, 0.6);\n    pointer-events: ', ';\n    opacity: ', ';\n    &:before{\n        content: \'\';\n        width: 19px;\n        height: 19px;\n        transform: ', ';\n        border-radius: 2px;\n        background: ', ';\n        position: absolute;\n        right: 24px;\n        top: -20px;\n        opacity: ', ';\n        transition: opacity 0.3s, transform 0.3s;\n    }\n'], ['\n    position: absolute;\n    right: 0;\n    top: 100%;\n    min-width: 100%;\n    border-top: 12px solid transparent;\n    box-shadow: 8px 0 12px -20px rgba(35, 35, 35, 0.6), -12px 0 12px -20px rgba(35, 35, 35, 0.6);\n    pointer-events: ', ';\n    opacity: ', ';\n    &:before{\n        content: \'\';\n        width: 19px;\n        height: 19px;\n        transform: ', ';\n        border-radius: 2px;\n        background: ', ';\n        position: absolute;\n        right: 24px;\n        top: -20px;\n        opacity: ', ';\n        transition: opacity 0.3s, transform 0.3s;\n    }\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _utils = require('webmotors-react-pj/utils');

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ElementSubLinkKeyframes = (0, _styledComponents.keyframes)(_templateObject);

var ElementSubLinkAnimation = function ElementSubLinkAnimation(_ref) {
    var data = _ref.data;
    return data.active ? (0, _styledComponents.css)(_templateObject2, ElementSubLinkKeyframes, data.index * 0.2) : '';
};

var ElementSubItem = _styledComponents2.default.li(_templateObject3);

var ElementSubLink = _styledComponents2.default.a(_templateObject4, (0, _color2.default)('white'), (0, _color2.default)('gray-4'), ElementSubLinkAnimation, (0, _color2.default)('#43bccd'));

var StyleSubMenu = exports.StyleSubMenu = _styledComponents2.default.ul(_templateObject5, function (_ref2) {
    var active = _ref2.active;
    return active ? 'all' : 'none';
}, function (_ref3) {
    var active = _ref3.active;
    return active ? 1 : 0;
}, function (_ref4) {
    var active = _ref4.active;
    return active ? 'rotate(45deg) translate(10px, 10px)' : 'rotate(45deg) translate(0px, 0px)';
}, (0, _color2.default)('white'), function (_ref5) {
    var active = _ref5.active;
    return active ? 1 : 0;
});

var StyleSubItem = exports.StyleSubItem = function StyleSubItem(_ref6) {
    var data = _ref6.data,
        href = _ref6.href,
        children = _ref6.children;
    return _react2.default.createElement(
        ElementSubItem,
        null,
        _react2.default.createElement(
            ElementSubLink,
            {
                data: data,
                onClick: function onClick() {
                    return (0, _utils.scrollSmooth)(0);
                },
                href: href
            },
            children
        )
    );
};