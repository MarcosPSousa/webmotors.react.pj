'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/header/unlogged/menu/submenu/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (props) {
    return _react2.default.createElement(
        _style.StyleSubMenu,
        { active: props.onOpen },
        _react2.default.createElement(
            _style.StyleSubItem,
            { data: { index: 0, active: props.onOpen }, href: '/solucoes/crm' },
            'CRM'
        ),
        _react2.default.createElement(
            _style.StyleSubItem,
            { data: { index: 1, active: props.onOpen }, href: '/solucoes/estoque' },
            'Estoque'
        ),
        _react2.default.createElement(
            _style.StyleSubItem,
            { data: { index: 2, active: props.onOpen }, href: '/solucoes/autoguru' },
            'Autoguru'
        ),
        _react2.default.createElement(
            _style.StyleSubItem,
            { data: { index: 3, active: props.onOpen }, href: '/solucoes/universidade' },
            'Universidade'
        ),
        _react2.default.createElement(
            _style.StyleSubItem,
            { data: { index: 4, active: props.onOpen }, href: '/solucoes/feiroes' },
            'Feir\xF5es'
        )
    );
};