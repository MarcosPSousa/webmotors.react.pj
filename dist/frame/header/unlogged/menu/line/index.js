'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/header/unlogged/menu/line/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var line = _ref.line;

    var height = 3;
    if (line) {
        return _react2.default.createElement(_style2.default, {
            height: height,
            style: {
                transform: 'translateX(' + line.left + 'px)',
                top: line.top - height + 1,
                width: line.width
            }
        });
    }
    return null;
};