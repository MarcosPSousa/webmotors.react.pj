'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    height: ', ';\n    background-color: ', ';\n    will-change: transform, width;\n    transition: transform 0.3s, width 0.3s;\n'], ['\n    position: absolute;\n    bottom: 0;\n    left: 0;\n    height: ', ';\n    background-color: ', ';\n    will-change: transform, width;\n    transition: transform 0.3s, width 0.3s;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.i(_templateObject, function (_ref) {
    var height = _ref.height;
    return height + 'px';
}, (0, _color2.default)('primary'));