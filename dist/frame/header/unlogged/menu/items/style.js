'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleSubMenu = exports.StyleTabItem = exports.StyleTabLink = exports.StyleItem = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n'], ['\n    display: flex;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    position: relative;\n'], ['\n    position: relative;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    display: block;\n    padding: 22px 32px;\n    cursor: pointer;\n    transition: color 0.3s;\n'], ['\n    display: block;\n    padding: 22px 32px;\n    cursor: pointer;\n    transition: color 0.3s;\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    right: 0;\n    top: 100%;\n    min-width: 100%;\n    border-top: 12px solid transparent;\n    box-shadow: 8px 0 12px -20px rgba(35, 35, 35, 0.6), -12px 0 12px -20px rgba(35, 35, 35, 0.6);\n    &:before{\n        content: \'\';\n        width: 19px;\n        height: 19px;\n        transform: rotate(45deg) translate(10px, 10px);\n        border-radius: 2px;\n        background: ', ';\n        position: absolute;\n        right: 24px;\n        top: -20px;\n    }\n'], ['\n    position: absolute;\n    right: 0;\n    top: 100%;\n    min-width: 100%;\n    border-top: 12px solid transparent;\n    box-shadow: 8px 0 12px -20px rgba(35, 35, 35, 0.6), -12px 0 12px -20px rgba(35, 35, 35, 0.6);\n    &:before{\n        content: \'\';\n        width: 19px;\n        height: 19px;\n        transform: rotate(45deg) translate(10px, 10px);\n        border-radius: 2px;\n        background: ', ';\n        position: absolute;\n        right: 24px;\n        top: -20px;\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.ul(_templateObject);

var StyleItem = exports.StyleItem = _styledComponents2.default.li(_templateObject2);

var StyleTabLink = exports.StyleTabLink = _styledComponents2.default.a(_templateObject3);

var StyleTabItem = exports.StyleTabItem = _styledComponents2.default.span(_templateObject3);

var StyleSubMenu = exports.StyleSubMenu = _styledComponents2.default.ul(_templateObject4, (0, _color2.default)('white'));