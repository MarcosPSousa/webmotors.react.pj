'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

var _submenu = require('webmotors-react-pj/frame/header/unlogged/menu/submenu');

var _submenu2 = _interopRequireDefault(_submenu);

var _style = require('webmotors-react-pj/frame/header/unlogged/menu/items/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var moveLine = _ref.moveLine;

    var firstTab = (0, _react.useRef)();

    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        open = _useState2[0],
        setOpen = _useState2[1];

    var LineMove = function LineMove(tag) {
        var rect = tag.getBoundingClientRect();
        var left = rect.left,
            width = rect.width,
            top = rect.top,
            height = rect.height;

        moveLine({ left: left, width: width, top: top + height });
    };

    var MouseEnter = function MouseEnter(tag) {
        return LineMove(tag);
    };

    (0, _react.useEffect)(function () {
        return LineMove(firstTab.current);
    }, []);

    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(
            _style.StyleItem,
            { ref: firstTab },
            _react2.default.createElement(
                _style.StyleTabLink,
                { href: '/', onClick: function onClick() {
                        return (0, _utils.scrollSmooth)(0);
                    }, onMouseEnter: function onMouseEnter(e) {
                        return MouseEnter(e.target);
                    } },
                'Login'
            )
        ),
        _react2.default.createElement(
            _style.StyleItem,
            { onMouseLeave: function onMouseLeave() {
                    return setOpen(false);
                } },
            _react2.default.createElement(
                _style.StyleTabItem,
                {
                    onMouseEnter: function onMouseEnter(e) {
                        MouseEnter(e.target);
                        setOpen(true);
                    }
                },
                'Solu\xE7\xF5es'
            ),
            _react2.default.createElement(_submenu2.default, { onOpen: open })
        )
    );
};