'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/header/unlogged/menu/style');

var _style2 = _interopRequireDefault(_style);

var _line = require('webmotors-react-pj/frame/header/unlogged/menu/line');

var _line2 = _interopRequireDefault(_line);

var _items = require('webmotors-react-pj/frame/header/unlogged/menu/items');

var _items2 = _interopRequireDefault(_items);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var _useState = (0, _react.useState)(),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        line = _useState2[0],
        setLine = _useState2[1];

    return _react2.default.createElement(
        _style2.default,
        null,
        _react2.default.createElement(_line2.default, { line: line }),
        _react2.default.createElement(_items2.default, { moveLine: function moveLine(e) {
                return setLine(e);
            } })
    );
};