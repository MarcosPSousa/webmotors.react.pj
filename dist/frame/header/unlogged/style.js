'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: fixed;\n    left: 0;\n    top: 0;\n    width: 100%;\n    z-index: 3;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 0px 24px;\n    font-weight: bold;\n    font-size: 12px;\n    background-color: ', ';\n    box-shadow: 0 12px 12px -12px ', ';\n'], ['\n    position: fixed;\n    left: 0;\n    top: 0;\n    width: 100%;\n    z-index: 3;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 0px 24px;\n    font-weight: bold;\n    font-size: 12px;\n    background-color: ', ';\n    box-shadow: 0 12px 12px -12px ', ';\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.header(_templateObject, (0, _color2.default)('white'), (0, _color2.default)('modal'));