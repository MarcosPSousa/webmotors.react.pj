'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/header/logged/style');

var _style2 = _interopRequireDefault(_style);

var _logo = require('webmotors-react-pj/frame/header/logged/logo');

var _logo2 = _interopRequireDefault(_logo);

var _items = require('webmotors-react-pj/frame/header/logged/items');

var _items2 = _interopRequireDefault(_items);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    if (!window.clickHeader) {
        window.clickHeader = true;
        window.addEventListener('click', function (e) {
            var target = e.target;

            var headerPopoverAttribute = 'data-header-popover';
            var popoverAttribute = 'data-popover-open';
            var header = document.querySelector('[data-header]');
            var iconClicked = target.hasAttribute('data-icon-click');
            var iconNextElement = target.nextElementSibling;
            var removePopover = function removePopover() {
                return document.querySelectorAll('[' + popoverAttribute + ']').forEach(function (item) {
                    return item.removeAttribute(popoverAttribute);
                });
            };

            if (!target.closest('.modal')) {
                if (iconClicked && iconNextElement && !iconNextElement.hasAttribute(popoverAttribute) || target.closest('[' + popoverAttribute + ']')) {
                    if (!target.closest('[' + popoverAttribute + ']')) {
                        removePopover();
                    }
                    header.setAttribute(headerPopoverAttribute, '');
                    iconNextElement && iconNextElement.setAttribute(popoverAttribute, '');
                } else {
                    removePopover();
                    header.removeAttribute(headerPopoverAttribute);
                }
            }
        });
    }

    return _react2.default.createElement(
        _style2.default,
        { 'data-header': true },
        _react2.default.createElement(_logo2.default, null),
        _react2.default.createElement(_items2.default, null)
    );
};