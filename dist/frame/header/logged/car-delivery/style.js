'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    background-color: ', ';\n    color: ', ';\n    font-size: 1.2rem;\n    width: 100%;\n    padding: 12px;\n'], ['\n    background-color: ', ';\n    color: ', ';\n    font-size: 1.2rem;\n    width: 100%;\n    padding: 12px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject, (0, _color2.default)('primary'), (0, _color2.default)('white'));