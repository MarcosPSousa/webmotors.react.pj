'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('./style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        'CarDelivery. Sua loja cuida de toda a venda e entrega o carro na casa do comprador. Oferecer agora'
    );
};