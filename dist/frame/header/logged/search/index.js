'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/header/logged/style');

var _style2 = _interopRequireDefault(_style);

var _logo = require('webmotors-react-pj/frame/header/logged/logo');

var _logo2 = _interopRequireDefault(_logo);

var _items = require('webmotors-react-pj/frame/header/logged/items');

var _items2 = _interopRequireDefault(_items);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var show = /crm\.webmotors/.test(window.location.hostname);
    return show ? 'Menu' : _react2.default.createElement('div', { 'data-search': true });
};