'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: center;\n    color: ', ';\n    background-color: ', ';\n    position: relative;\n    box-shadow: 0 0 12px ', ';\n    z-index: ', ';\n'], ['\n    display: flex;\n    align-items: center;\n    color: ', ';\n    background-color: ', ';\n    position: relative;\n    box-shadow: 0 0 12px ', ';\n    z-index: ', ';\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.header(_templateObject, (0, _color2.default)('gray-2'), (0, _color2.default)('white'), (0, _color2.default)('gray-3'), function (_ref) {
    var isOpen = _ref.isOpen;
    return isOpen ? '4' : '0';
});