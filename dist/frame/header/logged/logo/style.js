'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleLogo = exports.StyleLink = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    width: 250px;\n    height: 86px;\n    line-height: 50%;\n    box-shadow: 0 0px 8px 0 rgba(0, 0, 0, 0.1);\n'], ['\n    width: 250px;\n    height: 86px;\n    line-height: 50%;\n    box-shadow: 0 0px 8px 0 rgba(0, 0, 0, 0.1);\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    width: 200px;\n    margin: 20px auto;\n    display: block;\n'], ['\n    width: 200px;\n    margin: 20px auto;\n    display: block;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleLink = exports.StyleLink = _styledComponents2.default.a(_templateObject);

var StyleLogo = exports.StyleLogo = _styledComponents2.default.img(_templateObject2);