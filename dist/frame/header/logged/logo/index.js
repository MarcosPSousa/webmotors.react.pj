'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/header/logged/logo/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.StyleLink,
        { href: _config.UrlCockpit },
        _react2.default.createElement(_style.StyleLogo, { src: _config.UrlCockpit + '/assets/img/brands/cockpit.svg', alt: 'Logo Cockpit' })
    );
};