'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _arguments = arguments;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _style = require('webmotors-react-pj/frame/header/logged/items/intercom/style');

var _dotNotice = require('webmotors-react-pj/frame/header/logged/items/dot-notice');

var _dotNotice2 = _interopRequireDefault(_dotNotice);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var refWrapper = (0, _react.useRef)();

    (0, _react.useEffect)(function () {
        setTimeout(function () {
            var classNameIntercom = [].concat((0, _toConsumableArray3.default)(refWrapper.current.classList))[0];
            var jwt = (0, _utils.jwtGet)();
            var appId = 'skfddf3u';
            var email = jwt.email,
                name = jwt.nameid,
                sid = jwt.sid;

            var w = window;
            var ic = w.Intercom;
            w.intercomSettings = {
                name: name,
                email: email,
                user_id: sid,
                app_id: appId,
                custom_launcher_selector: '.' + classNameIntercom,
                alignment: 'right',
                created_at: Date.now(),
                horizontal_padding: 0,
                vertical_padding: 0
            };
            if (typeof ic === 'function') {
                ic('reattach_activator');
                ic('update', w.intercomSettings);
            } else {
                var d = document;

                var i = function i() {
                    return i.c(_arguments);
                };
                var l = function l() {
                    var s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = 'https://widget.intercom.io/widget/' + appId;
                    var x = d.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                };
                i.q = [];
                i.c = function (args) {
                    return i.q.push(args);
                };
                w.Intercom = i;
                if (document.readyState === 'complete') {
                    l();
                } else if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
            w.Intercom('boot', {
                app_id: appId,
                email: email,
                user_id: sid,
                created_at: Date.now()
            });
            w.intercomTimeout = w.setTimeout(function () {
                return w.clearInterval(w.intercomInterval);
            }, 30000);
            w.intercomInterval = w.setInterval(function () {
                if (!!w.Intercom && w.Intercom.booted) {
                    clearInterval(w.intercomInterval);
                    clearTimeout(w.intercomTimeout);
                    w.Intercom('onUnreadCountChange', function (unreadCount) {
                        var count = unreadCount <= 9 ? unreadCount : '9+';
                        var notify = window.document.getElementById('intercom-notify');
                        if (count && notify) {
                            notify.innerText = count;
                        }
                    });
                }
            }, 100);
        }, 100);
    }, []);

    return _react2.default.createElement(
        _style.StyleWrapper,
        { ref: refWrapper },
        _react2.default.createElement(_dotNotice2.default, { id: 'intercom-notify' }),
        _react2.default.createElement(_style.StyleImg, { alt: '', 'aria-hidden': 'true', src: _config.UrlCockpit + '/assets/img/icons/bell.svg' })
    );
};