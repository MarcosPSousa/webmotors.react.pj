'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleImg = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: relative;\n'], ['\n    position: relative;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    width: 24px;\n    height: 24px;\n    transform: translate(4px, 4px);\n    opacity: 0.9;\n    transition: opacity 0.3s;\n    &:hover{\n        opacity: 1;\n    }\n'], ['\n    width: 24px;\n    height: 24px;\n    transform: translate(4px, 4px);\n    opacity: 0.9;\n    transition: opacity 0.3s;\n    &:hover{\n        opacity: 1;\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleImg = exports.StyleImg = _styledComponents2.default.img(_templateObject2);