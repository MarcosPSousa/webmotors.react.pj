'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleMenu = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 0 32px;\n'], ['\n    flex: 1;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    padding: 0 32px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n'], ['\n    display: flex;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleMenu = exports.StyleMenu = _styledComponents2.default.div(_templateObject2);