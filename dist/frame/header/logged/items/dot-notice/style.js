'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    right: -10px;\n    top: -10px;\n    width: 26px;\n    height: 26px;\n    border-radius: 100%;\n    border: 3px solid ', ';\n    transform: scale(1);\n    transition: transform 0.3s;\n    text-align: center;\n    z-index: 1;\n    font-size: 1rem;\n    line-height: 2em;\n    color: ', ';\n    background-color: ', ';\n    pointer-events: none;\n    &:empty{\n        transform: scale(0);\n    }\n'], ['\n    position: absolute;\n    right: -10px;\n    top: -10px;\n    width: 26px;\n    height: 26px;\n    border-radius: 100%;\n    border: 3px solid ', ';\n    transform: scale(1);\n    transition: transform 0.3s;\n    text-align: center;\n    z-index: 1;\n    font-size: 1rem;\n    line-height: 2em;\n    color: ', ';\n    background-color: ', ';\n    pointer-events: none;\n    &:empty{\n        transform: scale(0);\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.div(_templateObject, (0, _color2.default)('white'), (0, _color2.default)('white'), function (_ref) {
    var token = _ref.token;
    return (0, _color2.default)(token);
});