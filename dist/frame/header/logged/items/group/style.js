'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleIcon = exports.StyleWrapper = exports.StyleButton = exports.StyleInfo = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n    padding-left: 12px;\n'], ['\n    flex: 1;\n    padding-left: 12px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    line-height: 1.4em;\n    transition: color 0.3s;\n    font-size: 1.4rem;\n    position: relative;\n    &:after{\n        content: \'\';\n        position: absolute;\n        right: -24px;\n        top: 50%;\n        width: 10px;\n        height: 10px;\n        border-style: solid;\n        border-color: ', ';\n        border-width: 0 1px 1px 0;\n        transition: border-color 0.4s;\n        transform: rotate(45deg) translateY(-50%);\n    }\n'], ['\n    display: flex;\n    line-height: 1.4em;\n    transition: color 0.3s;\n    font-size: 1.4rem;\n    position: relative;\n    &:after{\n        content: \'\';\n        position: absolute;\n        right: -24px;\n        top: 50%;\n        width: 10px;\n        height: 10px;\n        border-style: solid;\n        border-color: ', ';\n        border-width: 0 1px 1px 0;\n        transition: border-color 0.4s;\n        transform: rotate(45deg) translateY(-50%);\n    }\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    \n    margin-left: 32px;\n    padding-right: 40px;\n    &:hover{\n        color: ', ';\n        .', ':after{\n            border-color: ', ';\n        }\n    }\n'], ['\n    \n    margin-left: 32px;\n    padding-right: 40px;\n    &:hover{\n        color: ', ';\n        .', ':after{\n            border-color: ', ';\n        }\n    }\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    width: 26px;\n    height: 26px;\n    transform: translateY(2px);\n'], ['\n    width: 26px;\n    height: 26px;\n    transform: translateY(2px);\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleInfo = exports.StyleInfo = _styledComponents2.default.div(_templateObject);

var StyleButton = exports.StyleButton = _styledComponents2.default.div(_templateObject2, (0, _color2.default)('gray'));

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject3, (0, _color2.default)('#43bccd'), StyleInfo.styledComponentId, (0, _color2.default)('#43bccd'));

var StyleIcon = exports.StyleIcon = _styledComponents2.default.img(_templateObject4);