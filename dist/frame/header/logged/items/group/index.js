'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _store = require('webmotors-react-pj/frame/modal/store');

var _store2 = _interopRequireDefault(_store);

var _style = require('webmotors-react-pj/frame/header/logged/items/group/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        modalGroup = _useState2[0],
        setModalGroup = _useState2[1];

    var _jwtGet = (0, _utils.jwtGet)(),
        groupName = _jwtGet.nomeGrupo,
        idGrupo = _jwtGet.idGrupo,
        role = _jwtGet.role;

    var groupIs = idGrupo && idGrupo !== '0' && role && role.indexOf('1') !== -1;
    var storeName = (0, _utils.cookieGet)('CockpitStoreName') || 'Todas as lojas';
    return groupIs ? _react2.default.createElement(
        _style.StyleWrapper,
        { role: 'button' },
        _react2.default.createElement(
            _style.StyleButton,
            { 'data-icon-click': true, onClick: function onClick() {
                    return setModalGroup(true);
                } },
            _react2.default.createElement(_style.StyleIcon, { src: _config.UrlCockpit + '/assets/img/icons/pin-map.svg', alt: '' }),
            _react2.default.createElement(
                _style.StyleInfo,
                null,
                _react2.default.createElement(
                    'div',
                    null,
                    groupName
                ),
                _react2.default.createElement(
                    'div',
                    null,
                    storeName
                )
            )
        ),
        modalGroup && _react2.default.createElement(_store2.default, { onClose: function onClose() {
                return setModalGroup(false);
            } })
    ) : false;
};