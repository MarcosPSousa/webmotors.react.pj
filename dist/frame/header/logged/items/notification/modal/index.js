'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _modal = require('webmotors-react-pj/modal');

var _modal2 = _interopRequireDefault(_modal);

var _step = require('webmotors-react-pj/frame/header/logged/items/notification/modal/step1');

var _step2 = _interopRequireDefault(_step);

var _step3 = require('webmotors-react-pj/frame/header/logged/items/notification/modal/step2');

var _step4 = _interopRequireDefault(_step3);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _tagAnalytics = require('webmotors-react-pj/tag-analytics');

var _tagAnalytics2 = _interopRequireDefault(_tagAnalytics);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var onClose = _ref.onClose;

    var _useState = (0, _react.useState)(1),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        step = _useState2[0],
        setStep = _useState2[1];

    var _useState3 = (0, _react.useState)(false),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        disabled = _useState4[0],
        setDisabled = _useState4[1];

    var _useState5 = (0, _react.useState)(''),
        _useState6 = (0, _slicedToArray3.default)(_useState5, 2),
        error = _useState6[0],
        setError = _useState6[1];

    var modalClose = function modalClose(dispatchAnalytics) {
        onClose(dispatchAnalytics);
    };

    return _react2.default.createElement(
        _modal2.default,
        { onClose: function onClose() {
                return modalClose(true);
            } },
        _react2.default.createElement(
            'style',
            null,
            '\n                    [data-modal-notification] .modal__box{max-width: 776px;}\n                    [data-modal-notification] .modal__wrap{margin-top: -60px;padding: 64px 32px 40px 40px;}\n                '
        ),
        _react2.default.createElement(_step2.default, {
            disabled: disabled,
            error: error,
            onAccept: function onAccept() {
                return modalClose(false);
            },
            onDecline: function onDecline() {
                return modalClose(true);
            }
        })
    );
};