'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleLink = exports.StyleButton = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: inline-block;\n    margin-right: 24px;\n    background-color: ', ';\n    color: ', ';\n    border-width: 1px;\n    border-radius: 4px;\n    border-color: ', ';\n    border-style: solid;\n    padding: 8px 18px;\n    font-size: 1.4rem;\n    font-weight: 500;\n    transition: opacity 0.2s, box-shadow 0.3s;\n    &[disabled] {\n        opacity: 0.7;\n    }\n'], ['\n    display: inline-block;\n    margin-right: 24px;\n    background-color: ', ';\n    color: ', ';\n    border-width: 1px;\n    border-radius: 4px;\n    border-color: ', ';\n    border-style: solid;\n    padding: 8px 18px;\n    font-size: 1.4rem;\n    font-weight: 500;\n    transition: opacity 0.2s, box-shadow 0.3s;\n    &[disabled] {\n        opacity: 0.7;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    &[action]{\n        display: inline-block;\n        margin-right: 24px;\n        background-color: ', ';\n        color: ', ';\n        border-width: 1px;\n        border-radius: 4px;\n        border-color: ', ';\n        border-style: solid;\n        padding: 8px 18px;\n        font-size: 1.4rem;\n        font-weight: 500;\n        transition: opacity 0.2s, box-shadow 0.3s;\n        &[disabled] {\n            opacity: 0.7;\n        }\n    }\n'], ['\n    &[action]{\n        display: inline-block;\n        margin-right: 24px;\n        background-color: ', ';\n        color: ', ';\n        border-width: 1px;\n        border-radius: 4px;\n        border-color: ', ';\n        border-style: solid;\n        padding: 8px 18px;\n        font-size: 1.4rem;\n        font-weight: 500;\n        transition: opacity 0.2s, box-shadow 0.3s;\n        &[disabled] {\n            opacity: 0.7;\n        }\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var background = function background(action) {
    return {
        default: 'transparent',
        success: (0, _color2.default)('primary'),
        decline: 'transparent'
    }[action || 'default'];
};

var textColor = function textColor(action) {
    return {
        default: (0, _color2.default)('gray-2'),
        success: (0, _color2.default)('white'),
        decline: (0, _color2.default)('gray-2')
    }[action || 'default'];
};

var borderColor = function borderColor(action) {
    return {
        default: 'transparent',
        success: (0, _color2.default)('primary'),
        decline: (0, _color2.default)('gray-2')
    }[action || 'default'];
};

var StyleButton = exports.StyleButton = _styledComponents2.default.button(_templateObject, function (_ref) {
    var action = _ref.action;
    return background(action);
}, function (_ref2) {
    var action = _ref2.action;
    return textColor(action);
}, function (_ref3) {
    var action = _ref3.action;
    return borderColor(action);
});

var StyleLink = exports.StyleLink = _styledComponents2.default.a(_templateObject2, function (_ref4) {
    var action = _ref4.action;
    return background(action);
}, function (_ref5) {
    var action = _ref5.action;
    return textColor(action);
}, function (_ref6) {
    var action = _ref6.action;
    return borderColor(action);
});