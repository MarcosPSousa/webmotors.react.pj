'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/header/logged/items/notification/modal/step1/style');

var _style2 = require('webmotors-react-pj/frame/header/logged/items/notification/modal/buttons/style');

var _tagAnalytics = require('webmotors-react-pj/tag-analytics');

var _tagAnalytics2 = _interopRequireDefault(_tagAnalytics);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var onAccept = _ref.onAccept,
        onDecline = _ref.onDecline,
        disabled = _ref.disabled,
        error = _ref.error;

    (0, _react.useEffect)(function () {
        var consoleEvent = function consoleEvent(e) {
            return console.log({ event: e.type });
        };
        window.objDataLayer = _tagAnalytics2.default.objDatalayer;
        var _window = window,
            objDataLayer = _window.objDataLayer;

        objDataLayer.site.country = 'brasil', objDataLayer.site.subEnvironment = 'homepage', objDataLayer.page.flowType = 'cockpit-homepage', objDataLayer.page.pageType = 'modal-car-delivery', objDataLayer.page.pageName = '/webmotors/cockpit/homepage/car-delivery', objDataLayer.page['pageName.tier1'] = 'cockpit', objDataLayer.page['pageName.tier2'] = 'homepage', objDataLayer.page['pageName.tier3'] = 'car-delivery', document.addEventListener('customPageView', consoleEvent);
        document.dispatchEvent(new CustomEvent('customPageView', { detail: objDataLayer }));
        setTimeout(function () {
            return document.removeEventListener('customPageView', consoleEvent);
        }, 300);
    }, []);

    return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement(
            _style.StyleWrapper,
            null,
            _react2.default.createElement(_style.StyleImg, { src: _config.UrlCockpit + '/assets/img/icons/placeholder-cardelivery2.svg', alt: '' }),
            _react2.default.createElement(
                _style.StyleContent,
                null,
                _react2.default.createElement(
                    _style.StyleTitle,
                    null,
                    'Venda 100% online com o CarDelivery!'
                ),
                _react2.default.createElement(
                    _style.StylePhrase,
                    null,
                    'Sua loja cuida de toda a venda e seu cliente recebe o carro em casa, com mais praticidade, conforto e seguran\xE7a.'
                ),
                _react2.default.createElement(
                    _style.StyleMessage,
                    null,
                    _react2.default.createElement(
                        'p',
                        null,
                        'Veja como \xE9 f\xE1cil ativar o CarDelivery:'
                    ),
                    _react2.default.createElement(_style.StyleMessageImg, { src: _config.UrlCockpit + '/assets/img/static/notifications-cardelivery.png' }),
                    _react2.default.createElement(
                        'p',
                        null,
                        'Basta ativar a op\xE7\xE3o em CRM > Configura\xE7\xF5es.'
                    ),
                    _react2.default.createElement(
                        _style.StyleActions,
                        null,
                        _react2.default.createElement(
                            _style2.StyleLink,
                            { disabled: disabled, action: 'success', href: _config.UrlCRM + '/configuration/?idcmpint=t1:c17:m07:modal-cardeliverypj-crm:cardelivery::quero-oferecer', onClick: function onClick() {
                                    return onAccept();
                                } },
                            'Quero ativar'
                        ),
                        _react2.default.createElement(
                            _style2.StyleButton,
                            { disabled: disabled, action: 'decline', type: 'button', onClick: function onClick() {
                                    return onDecline();
                                } },
                            'Agora n\xE3o'
                        ),
                        _react2.default.createElement(
                            _style.StyleButtonLink,
                            { href: 'https://www.webmotors.com.br/cardelivery/lojas/?idcmpint=t1:c17:m07:modal-lp-cardeliverypj:cardelivery::detalhes-cardelivery', target: '_blank', className: 'link' },
                            'Mais detalhes'
                        )
                    )
                )
            )
        )
    );
};