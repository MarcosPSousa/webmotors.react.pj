'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleButtonLink = exports.StyleActions = exports.StyleMessageImg = exports.StyleMessage = exports.StylePhrase = exports.StyleTitle = exports.StyleContent = exports.StyleImg = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: flex-start;\n'], ['\n    display: flex;\n    align-items: flex-start;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    width: 96px;\n    height: auto;\n    border-radius: 4px;\n'], ['\n    width: 96px;\n    height: auto;\n    border-radius: 4px;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n    padding-left: 16px;\n    &[disabled] {\n        opacity: 0.7;\n    }\n'], ['\n    flex: 1;\n    padding-left: 16px;\n    &[disabled] {\n        opacity: 0.7;\n    }\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.4rem;\n    font-weight: bold;\n'], ['\n    font-size: 1.4rem;\n    font-weight: bold;\n']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.2rem;\n'], ['\n    font-size: 1.2rem;\n']),
    _templateObject6 = (0, _taggedTemplateLiteral3.default)(['\n    margin-top: 32px;\n    padding: 32px;\n    background-color: ', ';\n    font-size: 1.2rem;\n    border-radius: 4px;\n    \n    > p {\n        font-style: italic;\n        + p{\n            margin-top: 12px;\n        }\n    }\n'], ['\n    margin-top: 32px;\n    padding: 32px;\n    background-color: ', ';\n    font-size: 1.2rem;\n    border-radius: 4px;\n    \n    > p {\n        font-style: italic;\n        + p{\n            margin-top: 12px;\n        }\n    }\n']),
    _templateObject7 = (0, _taggedTemplateLiteral3.default)(['\n    width: 100%;\n    height: auto;\n    margin: 12px 0 8px -6px;\n'], ['\n    width: 100%;\n    height: auto;\n    margin: 12px 0 8px -6px;\n']),
    _templateObject8 = (0, _taggedTemplateLiteral3.default)(['\n    margin-top: 32px;\n'], ['\n    margin-top: 32px;\n']),
    _templateObject9 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.2rem;\n    &.link{\n        color: ', ';\n        text-decoration: none;\n        cursor: pointer;\n        background-color: transparent;\n        &:before{\n            border-bottom-color: ', ';\n        }\n    }\n'], ['\n    font-size: 1.2rem;\n    &.link{\n        color: ', ';\n        text-decoration: none;\n        cursor: pointer;\n        background-color: transparent;\n        &:before{\n            border-bottom-color: ', ';\n        }\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleImg = exports.StyleImg = _styledComponents2.default.img(_templateObject2);

var StyleContent = exports.StyleContent = _styledComponents2.default.div(_templateObject3);

var StyleTitle = exports.StyleTitle = _styledComponents2.default.div(_templateObject4);

var StylePhrase = exports.StylePhrase = _styledComponents2.default.div(_templateObject5);

var StyleMessage = exports.StyleMessage = _styledComponents2.default.div(_templateObject6, (0, _color2.default)('#f9f9f9'));

var StyleMessageImg = exports.StyleMessageImg = _styledComponents2.default.img(_templateObject7);

var StyleActions = exports.StyleActions = _styledComponents2.default.div(_templateObject8);

var StyleButtonLink = exports.StyleButtonLink = _styledComponents2.default.a(_templateObject9, (0, _color2.default)('primary'), (0, _color2.default)('primary'));