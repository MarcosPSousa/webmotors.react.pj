'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleText = exports.StyleActions = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    text-align: center;\n'], ['\n    text-align: center;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    margin-top: 32px;\n'], ['\n    margin-top: 32px;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.6rem;\n    line-height: 1.50em;\n'], ['\n    font-size: 1.6rem;\n    line-height: 1.50em;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleActions = exports.StyleActions = _styledComponents2.default.div(_templateObject2);

var StyleText = exports.StyleText = _styledComponents2.default.div(_templateObject3);