'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/header/logged/items/notification/modal/step2/style');

var _style2 = require('webmotors-react-pj/frame/header/logged/items/notification/modal/buttons/style');

var _style3 = _interopRequireDefault(_style2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var onAccept = _ref.onAccept;

    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(
            _style.StyleText,
            null,
            'Seu interesse na renova\xE7\xE3o do produto Feir\xF5es para o ano de',
            _react2.default.createElement('br', null),
            '2020 foi enviada ao seu gerente de relacionamento,',
            _react2.default.createElement('br', null),
            'que entrar\xE1 em contato em breve.'
        ),
        _react2.default.createElement(
            _style.StyleActions,
            null,
            _react2.default.createElement(
                _style3.default,
                { action: 'success', type: 'button', onClick: function onClick() {
                        return onAccept();
                    } },
                'Continuar'
            )
        )
    );
};