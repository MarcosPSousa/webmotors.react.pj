'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _tagAnalytics = require('webmotors-react-pj/tag-analytics');

var _tagAnalytics2 = _interopRequireDefault(_tagAnalytics);

var _style = require('webmotors-react-pj/frame/header/logged/items/notification/style');

var _modal = require('webmotors-react-pj/frame/header/logged/items/notification/modal');

var _modal2 = _interopRequireDefault(_modal);

var _popover = require('webmotors-react-pj/frame/header/logged/items/notification/popover');

var _popover2 = _interopRequireDefault(_popover);

var _dotNotice = require('webmotors-react-pj/frame/header/logged/items/dot-notice');

var _dotNotice2 = _interopRequireDefault(_dotNotice);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var _useState = (0, _react.useState)(''),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        notice = _useState2[0],
        setNotice = _useState2[1];

    var _useState3 = (0, _react.useState)(false),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        modal = _useState4[0],
        setModal = _useState4[1];

    var elementWrapper = (0, _react.useRef)();
    var cookieName = 'CockpitNotifications';

    var _jwtGet = (0, _utils.jwtGet)(),
        id = _jwtGet.sid,
        role = _jwtGet.role,
        utilizaCrmTerceiro = _jwtGet.utilizaCrmTerceiro;

    var userAdmin = role ? role.indexOf('2') !== -1 && utilizaCrmTerceiro.indexOf('0') !== -1 : false;
    var sid = Number(id);

    var noticeCookieGet = function noticeCookieGet() {
        var cookie = (0, _utils.cookieGet)(cookieName) || '[]';
        return JSON.parse(cookie);
    };

    var handleCloseModal = function handleCloseModal(dispatchAnalytics) {
        setModal(false);
        document.body.click();

        if (dispatchAnalytics) {
            var consoleEvent = function consoleEvent(e) {
                return console.log({ event: e.type });
            };
            window.objDataLayer = _tagAnalytics2.default.objDatalayer;
            var _window = window,
                objDataLayer = _window.objDataLayer;

            objDataLayer.page.flowType = 'cockpit', objDataLayer.page.pageType = 'cockpit homepage', objDataLayer.page.pageName = '/webmotors/cockpit/homepage', document.addEventListener('customPageView', consoleEvent);
            document.dispatchEvent(new CustomEvent('customPageView', { detail: objDataLayer }));
            setTimeout(function () {
                return document.removeEventListener('customPageView', consoleEvent);
            }, 300);
        }
    };

    var handleClosePopover = function handleClosePopover() {
        setModal(true);
    };

    var handlePopoverOpen = function handlePopoverOpen(value) {
        var noticeUserHas = noticeCookieGet().includes(sid);
        if (!noticeUserHas) {
            var noticeCookie = noticeCookieGet();
            noticeCookie.push(sid);
            (0, _utils.cookieSet)(cookieName, (0, _stringify2.default)(noticeCookie));
        }
        var consoleEvent = function consoleEvent() {
            return console.log({ event: value });
        };
        window.objDataLayer = _tagAnalytics2.default.objDatalayer;
        var _window2 = window,
            objDataLayer = _window2.objDataLayer;

        var isOpen = Boolean(document.querySelector('[data-popover-open]'));
        if (!isOpen) {
            document.addEventListener(value, consoleEvent);
            document.dispatchEvent(new CustomEvent(value, { detail: objDataLayer }));
        }
        setTimeout(function () {
            return document.removeEventListener(value, consoleEvent);
        }, 300);
        setNotice('');
    };

    (0, _react.useEffect)(function () {
        var noticeUserHas = noticeCookieGet().includes(sid);
        if (!noticeUserHas) {
            setNotice('1');
        }
    }, []);

    return userAdmin ? _react2.default.createElement(
        _style.StyleWrapper,
        { 'data-icon': '', 'data-modal-notification': true },
        _react2.default.createElement(
            _style.StyleIcon,
            { onClick: function onClick() {
                    return handlePopoverOpen('notificacaoCockpit');
                }, 'data-icon-click': true },
            _react2.default.createElement(
                _dotNotice2.default,
                { token: 'success' },
                notice
            ),
            _react2.default.createElement(_style.StyleImg, { alt: '', 'aria-hidden': 'true', src: _config.UrlCockpit + '/assets/img/icons/letter.svg' })
        ),
        _react2.default.createElement(_popover2.default, { onOpenModal: function onOpenModal() {
                return handleClosePopover();
            } }),
        modal && _react2.default.createElement(_modal2.default, { onClose: function onClose(e) {
                return handleCloseModal(e);
            } })
    ) : null;
};