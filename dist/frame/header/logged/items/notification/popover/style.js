'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleButton = exports.StyleTitle = exports.StyleText = exports.StyleInfo = exports.StyleMessage = exports.StyleThumb = exports.StyleImg = exports.StyleTime = exports.StyleLogo = exports.StyleProduct = exports.StyleContent = exports.StyleBigTitle = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    background-color: ', ';\n    border: 1px solid ', ';\n    width: 400px;\n    right: -20px;\n    top: 100%;\n    box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px 0px;\n    opacity: 0;\n    pointer-events: none;\n    transition: transform 0.3s, opacity 0.3s;\n    transform: translateY(-32px);\n    &:before{\n        content: \'\';\n        position: absolute;\n        top: -8px;\n        right: 27px;\n        width: 16px;\n        height: 16px;\n        background-color: ', ';\n        transform: rotate(45deg);\n        box-shadow: -1px -1px 1px ', ';\n    }\n    &[data-popover-open]{\n        pointer-events: all;\n        opacity: 1;\n        transform: translateY(14px);\n    }\n'], ['\n    position: absolute;\n    background-color: ', ';\n    border: 1px solid ', ';\n    width: 400px;\n    right: -20px;\n    top: 100%;\n    box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px 0px;\n    opacity: 0;\n    pointer-events: none;\n    transition: transform 0.3s, opacity 0.3s;\n    transform: translateY(-32px);\n    &:before{\n        content: \'\';\n        position: absolute;\n        top: -8px;\n        right: 27px;\n        width: 16px;\n        height: 16px;\n        background-color: ', ';\n        transform: rotate(45deg);\n        box-shadow: -1px -1px 1px ', ';\n    }\n    &[data-popover-open]{\n        pointer-events: all;\n        opacity: 1;\n        transform: translateY(14px);\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.4rem;\n    font-weight: bold;\n    padding: 12px 16px;\n    border-bottom: 1px solid ', ';\n'], ['\n    font-size: 1.4rem;\n    font-weight: bold;\n    padding: 12px 16px;\n    border-bottom: 1px solid ', ';\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    padding: 16px;\n    cursor: pointer;\n    opacity: 0.9;\n    &:hover{\n        opacity: 1;\n    }\n'], ['\n    padding: 16px;\n    cursor: pointer;\n    opacity: 0.9;\n    &:hover{\n        opacity: 1;\n    }\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    justify-content: space-between;\n'], ['\n    display: flex;\n    justify-content: space-between;\n']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n    width: 60px;\n'], ['\n    width: 60px;\n']),
    _templateObject6 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1rem;\n    font-weight: normal;\n    color: ', ';\n'], ['\n    font-size: 1rem;\n    font-weight: normal;\n    color: ', ';\n']),
    _templateObject7 = (0, _taggedTemplateLiteral3.default)(['\n    width: 100%;\n    height: auto;\n    border-radius: 4px;\n'], ['\n    width: 100%;\n    height: auto;\n    border-radius: 4px;\n']),
    _templateObject8 = (0, _taggedTemplateLiteral3.default)(['\n    width: 76px;\n    padding-right: 16px;\n'], ['\n    width: 76px;\n    padding-right: 16px;\n']),
    _templateObject9 = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n'], ['\n    flex: 1;\n']),
    _templateObject10 = (0, _taggedTemplateLiteral3.default)(['\n    padding: 16px 24px 0 0;\n    display: flex;\n    font-size: 1.2rem;\n    line-height: 1.167em;\n    position: relative;\n    &:before{\n        content: \'\';\n        position: absolute;\n        right: 0;\n        top: 16px;\n        border-radius: 100%;\n        width: 6px;\n        height: 6px;\n        background-color: ', ';\n    }\n'], ['\n    padding: 16px 24px 0 0;\n    display: flex;\n    font-size: 1.2rem;\n    line-height: 1.167em;\n    position: relative;\n    &:before{\n        content: \'\';\n        position: absolute;\n        right: 0;\n        top: 16px;\n        border-radius: 100%;\n        width: 6px;\n        height: 6px;\n        background-color: ', ';\n    }\n']),
    _templateObject11 = (0, _taggedTemplateLiteral3.default)(['\n    line-height: 1.5em;\n'], ['\n    line-height: 1.5em;\n']),
    _templateObject12 = (0, _taggedTemplateLiteral3.default)(['\n    font-weight: bold;\n    padding-bottom: 4px;\n'], ['\n    font-weight: bold;\n    padding-bottom: 4px;\n']),
    _templateObject13 = (0, _taggedTemplateLiteral3.default)(['\n    margin-top: 16px;\n    color: ', ';\n    font-weight: bold;\n'], ['\n    margin-top: 16px;\n    color: ', ';\n    font-weight: bold;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject, (0, _color2.default)('white'), (0, _color2.default)('gray-4'), (0, _color2.default)('white'), (0, _color2.default)('gray-4'));

var StyleBigTitle = exports.StyleBigTitle = _styledComponents2.default.div(_templateObject2, (0, _color2.default)('gray-4'));

var StyleContent = exports.StyleContent = _styledComponents2.default.div(_templateObject3);

var StyleProduct = exports.StyleProduct = _styledComponents2.default.div(_templateObject4);

var StyleLogo = exports.StyleLogo = _styledComponents2.default.img(_templateObject5);

var StyleTime = exports.StyleTime = _styledComponents2.default.div(_templateObject6, (0, _color2.default)('gray-2'));

var StyleImg = exports.StyleImg = _styledComponents2.default.img(_templateObject7);

var StyleThumb = exports.StyleThumb = _styledComponents2.default.div(_templateObject8);

var StyleMessage = exports.StyleMessage = _styledComponents2.default.div(_templateObject9);

var StyleInfo = exports.StyleInfo = _styledComponents2.default.div(_templateObject10, (0, _color2.default)('primary'));

var StyleText = exports.StyleText = _styledComponents2.default.div(_templateObject11);

var StyleTitle = exports.StyleTitle = _styledComponents2.default.div(_templateObject12);

var StyleButton = exports.StyleButton = _styledComponents2.default.div(_templateObject13, (0, _color2.default)('primary'));