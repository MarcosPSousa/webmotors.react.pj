'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _tagAnalytics = require('webmotors-react-pj/tag-analytics');

var _tagAnalytics2 = _interopRequireDefault(_tagAnalytics);

var _style = require('webmotors-react-pj/frame/header/logged/items/notification/popover/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var onOpenModal = _ref.onOpenModal;

    var handleClick = function handleClick(value) {
        onOpenModal();
    };

    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(
            _style.StyleBigTitle,
            null,
            'Mensagens'
        ),
        _react2.default.createElement(
            _style.StyleContent,
            { onClick: function onClick() {
                    return handleClick('popover');
                } },
            _react2.default.createElement(
                _style.StyleProduct,
                null,
                _react2.default.createElement(_style.StyleLogo, { src: _config.UrlCockpit + '/assets/img/brands/cockpit.svg', alt: 'Logo Cockpit' })
            ),
            _react2.default.createElement(
                _style.StyleInfo,
                null,
                _react2.default.createElement(
                    _style.StyleThumb,
                    null,
                    _react2.default.createElement(_style.StyleImg, { src: _config.UrlCockpit + '/assets/img/icons/placeholder-cardelivery2.svg', alt: '' })
                ),
                _react2.default.createElement(
                    _style.StyleMessage,
                    null,
                    _react2.default.createElement(
                        _style.StyleTitle,
                        null,
                        'Ofere\xE7a o CarDelivery!'
                    ),
                    _react2.default.createElement(
                        _style.StyleText,
                        null,
                        'Seu cliente compra online, sua loja cuida de tudo e voc\xEA turbina suas vendas!'
                    ),
                    _react2.default.createElement(
                        _style.StyleButton,
                        null,
                        'Saiba mais'
                    )
                )
            )
        )
    );
};