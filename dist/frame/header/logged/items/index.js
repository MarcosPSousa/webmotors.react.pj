'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _intercom = require('webmotors-react-pj/frame/header/logged/items/intercom');

var _intercom2 = _interopRequireDefault(_intercom);

var _user = require('webmotors-react-pj/frame/header/logged/items/user');

var _user2 = _interopRequireDefault(_user);

var _group = require('webmotors-react-pj/frame/header/logged/items/group');

var _group2 = _interopRequireDefault(_group);

var _icon = require('webmotors-react-pj/frame/header/logged/items/icon');

var _icon2 = _interopRequireDefault(_icon);

var _style = require('webmotors-react-pj/frame/header/logged/items/style');

var _search = require('webmotors-react-pj/frame/header/logged/search');

var _search2 = _interopRequireDefault(_search);

var _translate = require('webmotors-react-pj/translate');

var _translate2 = _interopRequireDefault(_translate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(_search2.default, null),
        _react2.default.createElement(
            _style.StyleMenu,
            null,
            _react2.default.createElement(
                _icon2.default,
                null,
                _translate2.default.header.notification
            ),
            _react2.default.createElement(
                _icon2.default,
                null,
                _react2.default.createElement(_intercom2.default, null)
            ),
            _react2.default.createElement(_group2.default, null),
            _react2.default.createElement(
                _icon2.default,
                null,
                _react2.default.createElement(_user2.default, null)
            )
        )
    );
};