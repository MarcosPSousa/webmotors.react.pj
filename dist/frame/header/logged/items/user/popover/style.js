'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    top: 100%;\n    right: -16px;\n    background-color: ', ';\n    padding: 16px 24px 8px;\n    box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.2), 0 6px 8px 0 rgba(0, 0, 0, 0.2);\n    border-radius: 4px;\n    width: 280px;\n    font-size: 1.4rem;\n    pointer-events: none;\n    transition: transform 0.3s, opacity 0.3s;\n    will-change: transform, opacity;\n    opacity: 0;\n    transform: translateY(-32px);\n    &:before{\n        content: \'\';\n        position: absolute;\n        right: 12px;\n        top: 0px;\n        box-shadow: rgba(0, 0, 0, 0.5) -6px -6px 12px -3px;\n        transform: rotate(45deg) translate(-13px, 5px);\n        background-color: ', ';\n        width: 14px;\n        height: 14px;\n    }\n    &[data-popover-open]{\n        pointer-events: all;\n        opacity: 1;\n        transform: translateY(12px);\n    }\n'], ['\n    position: absolute;\n    top: 100%;\n    right: -16px;\n    background-color: ', ';\n    padding: 16px 24px 8px;\n    box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.2), 0 6px 8px 0 rgba(0, 0, 0, 0.2);\n    border-radius: 4px;\n    width: 280px;\n    font-size: 1.4rem;\n    pointer-events: none;\n    transition: transform 0.3s, opacity 0.3s;\n    will-change: transform, opacity;\n    opacity: 0;\n    transform: translateY(-32px);\n    &:before{\n        content: \'\';\n        position: absolute;\n        right: 12px;\n        top: 0px;\n        box-shadow: rgba(0, 0, 0, 0.5) -6px -6px 12px -3px;\n        transform: rotate(45deg) translate(-13px, 5px);\n        background-color: ', ';\n        width: 14px;\n        height: 14px;\n    }\n    &[data-popover-open]{\n        pointer-events: all;\n        opacity: 1;\n        transform: translateY(12px);\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.div(_templateObject, (0, _color2.default)('white'), (0, _color2.default)('white'));