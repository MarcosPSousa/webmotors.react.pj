'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

var _style = require('webmotors-react-pj/frame/header/logged/items/user/info/style');

var _icon = require('webmotors-react-pj/frame/header/logged/items/user/icon');

var _icon2 = _interopRequireDefault(_icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var jwt = (0, _utils.jwtGet)();
    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
                _icon2.default,
                { open: true, size: 64 },
                jwt.nameid && jwt.nameid[0] || '🛱'
            )
        ),
        _react2.default.createElement(
            _style.StyleUser,
            null,
            _react2.default.createElement(
                _style.StyleName,
                null,
                jwt.nameid
            ),
            _react2.default.createElement(
                _style.StyleEmail,
                null,
                jwt.email
            )
        )
    );
};