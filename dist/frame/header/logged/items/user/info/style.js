'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleEmail = exports.StyleName = exports.StyleUser = exports.StyleThumb = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: center;\n    padding-bottom: 16px;\n    border-bottom: 1px solid ', ';\n    margin-bottom: 8px;\n'], ['\n    display: flex;\n    align-items: center;\n    padding-bottom: 16px;\n    border-bottom: 1px solid ', ';\n    margin-bottom: 8px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    position: relative;\n'], ['\n    position: relative;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    padding-left: 12px;\n    flex: 1;\n    overflow: hidden;\n'], ['\n    padding-left: 12px;\n    flex: 1;\n    overflow: hidden;\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.6rem;\n'], ['\n    font-size: 1.6rem;\n']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.2rem;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    white-space: nowrap;\n'], ['\n    font-size: 1.2rem;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    white-space: nowrap;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject, (0, _color2.default)('gray-4'));
var StyleThumb = exports.StyleThumb = _styledComponents2.default.div(_templateObject2);
var StyleUser = exports.StyleUser = _styledComponents2.default.div(_templateObject3);
var StyleName = exports.StyleName = _styledComponents2.default.div(_templateObject4);
var StyleEmail = exports.StyleEmail = _styledComponents2.default.div(_templateObject5);