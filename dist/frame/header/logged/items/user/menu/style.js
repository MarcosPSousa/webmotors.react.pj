'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    padding: 8px 0;\n    opacity: 0.7;\n    transition: opacity 0.3s;\n    display: block;\n    line-height: 1em;\n    &:hover{\n        opacity: 1;\n    }\n'], ['\n    padding: 8px 0;\n    opacity: 0.7;\n    transition: opacity 0.3s;\n    display: block;\n    line-height: 1em;\n    &:hover{\n        opacity: 1;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    width: 22px;\n    height: 22px;\n    margin-right: 12px;\n    vertical-align: middle;\n    transform: translateY(-2px);\n'], ['\n    width: 22px;\n    height: 22px;\n    margin-right: 12px;\n    vertical-align: middle;\n    transform: translateY(-2px);\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _config = require('webmotors-react-pj/config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Link = _styledComponents2.default.a(_templateObject);

var Img = _styledComponents2.default.img(_templateObject2);

exports.default = function (_ref) {
    var children = _ref.children,
        icon = _ref.icon,
        href = _ref.href,
        target = _ref.target;
    return _react2.default.createElement(
        'li',
        null,
        _react2.default.createElement(
            Link,
            { target: target, rel: target ? 'noopener noreferrer' : '', href: href },
            _react2.default.createElement(Img, { alt: '', 'aria-hidden': 'true', src: _config.UrlCockpit + '/assets/img/icons/' + icon }),
            children
        )
    );
};