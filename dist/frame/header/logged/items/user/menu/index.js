'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/header/logged/items/user/menu/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var jwt = (0, _utils.jwtGet)();

    return _react2.default.createElement(
        'ul',
        null,
        _react2.default.createElement(
            _style2.default,
            { icon: 'name-tag.svg', href: _config.UrlCockpit + '/usuario?id=' + jwt.sid },
            'Meu perfil'
        ),
        _react2.default.createElement(
            _style2.default,
            { icon: 'marketshare.svg', href: _config.UrlEstoquePlataforma + '/perfil/dados-revenda', target: '_blank' },
            'Dados cadastrais'
        ),
        _react2.default.createElement(
            _style2.default,
            { icon: 'name-tag.svg', href: _config.UrlCockpit + '/logout' },
            'Sair'
        )
    );
};