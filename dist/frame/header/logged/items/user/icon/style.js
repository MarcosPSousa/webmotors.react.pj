'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    width: ', ';\n    height: ', ';\n    line-height: ', ';\n    border-radius: ', ';\n    text-align: center;\n    text-transform: uppercase;\n    color: ', ';\n    background-color: ', ';\n    border: 2px solid transparent;\n    box-shadow: 0 0 1px 1px ', ' inset;\n    font-size: ', ';;\n    font-weight: 500;\n    cursor: ', ';\n    opacity: ', ';\n    &:hover, &[data-opem]{\n        opacity: 1;\n    }\n'], ['\n    width: ', ';\n    height: ', ';\n    line-height: ', ';\n    border-radius: ', ';\n    text-align: center;\n    text-transform: uppercase;\n    color: ', ';\n    background-color: ', ';\n    border: 2px solid transparent;\n    box-shadow: 0 0 1px 1px ', ' inset;\n    font-size: ', ';;\n    font-weight: 500;\n    cursor: ', ';\n    opacity: ', ';\n    &:hover, &[data-opem]{\n        opacity: 1;\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sizeInit = 32;

exports.default = _styledComponents2.default.div(_templateObject, function (_ref) {
    var _ref$size = _ref.size,
        size = _ref$size === undefined ? sizeInit : _ref$size;
    return size + 'px';
}, function (_ref2) {
    var _ref2$size = _ref2.size,
        size = _ref2$size === undefined ? sizeInit : _ref2$size;
    return size + 'px';
}, function (_ref3) {
    var _ref3$size = _ref3.size,
        size = _ref3$size === undefined ? sizeInit : _ref3$size;
    return size / 10 + 'rem';
}, function (_ref4) {
    var _ref4$size = _ref4.size,
        size = _ref4$size === undefined ? sizeInit : _ref4$size;
    return size + 'px';
}, (0, _color2.default)('white'), (0, _color2.default)('gray-2'), (0, _color2.default)('white'), function (_ref5) {
    var _ref5$size = _ref5.size,
        size = _ref5$size === undefined ? sizeInit : _ref5$size;
    return size / 20 + 'rem';
}, function (_ref6) {
    var onClick = _ref6.onClick;
    return onClick ? 'pointer' : 'default';
}, function (_ref7) {
    var open = _ref7.open;
    return open ? 1 : 0.9;
});