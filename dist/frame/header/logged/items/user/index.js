'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

var _style = require('webmotors-react-pj/frame/header/logged/items/user/style');

var _style2 = _interopRequireDefault(_style);

var _icon = require('webmotors-react-pj/frame/header/logged/items/user/icon');

var _icon2 = _interopRequireDefault(_icon);

var _popover = require('webmotors-react-pj/frame/header/logged/items/user/popover');

var _popover2 = _interopRequireDefault(_popover);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var jwt = (0, _utils.jwtGet)();
    return _react2.default.createElement(
        _style2.default,
        null,
        _react2.default.createElement(
            _icon2.default,
            { 'data-icon-click': true },
            jwt.nameid && jwt.nameid[0] || '🛱'
        ),
        _react2.default.createElement(_popover2.default, null)
    );
};