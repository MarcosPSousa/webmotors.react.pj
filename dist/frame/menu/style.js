'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    background-color: ', ';\n    color: ', ';\n    padding: 24px 0 64px;\n    min-width: 250px;\n    max-width: 250px;\n    min-height: 100%;\n'], ['\n    background-color: ', ';\n    color: ', ';\n    padding: 24px 0 64px;\n    min-width: 250px;\n    max-width: 250px;\n    min-height: 100%;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.nav(_templateObject, (0, _color2.default)('gray'), (0, _color2.default)('white'));