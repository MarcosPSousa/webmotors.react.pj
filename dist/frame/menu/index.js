'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _list = require('webmotors-react-pj/frame/menu/list');

var _list2 = _interopRequireDefault(_list);

var _style = require('webmotors-react-pj/frame/menu/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _react2.default.createElement(
        _style2.default,
        null,
        _react2.default.createElement(_list2.default, null)
    );
};