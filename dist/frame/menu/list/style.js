'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleErrorImg = exports.StyleError = exports.StyleLine = exports.StyleList = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    padding: 0 0 32px;\n    font-size: 1.2rem;\n    text-transform: uppercase;\n    + ul {\n        padding-top: 32px;\n        font-size: 1.4rem;\n        text-transform: none;\n        ul a{\n            padding: 0 32px;\n        }\n        &:before{\n            content: \'\';\n            display: block;\n            border-top: 1px solid ', ';\n            margin: 0 32px;\n            transform: translateY(-32px);\n        }\n    }\n    ul a{\n        padding: 0 32px 0 64px;\n    }\n'], ['\n    padding: 0 0 32px;\n    font-size: 1.2rem;\n    text-transform: uppercase;\n    + ul {\n        padding-top: 32px;\n        font-size: 1.4rem;\n        text-transform: none;\n        ul a{\n            padding: 0 32px;\n        }\n        &:before{\n            content: \'\';\n            display: block;\n            border-top: 1px solid ', ';\n            margin: 0 32px;\n            transform: translateY(-32px);\n        }\n    }\n    ul a{\n        padding: 0 32px 0 64px;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    left: 0;\n    top: 0;\n    width: 9px;\n    background-color: ', ';\n    transition: transform 0.2s;\n'], ['\n    position: absolute;\n    left: 0;\n    top: 0;\n    width: 9px;\n    background-color: ', ';\n    transition: transform 0.2s;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    padding: 0 32px;\n    display: flex;\n    align-items: center;\n    font-size: 1.2rem;\n    cursor: ', ';\n'], ['\n    padding: 0 32px;\n    display: flex;\n    align-items: center;\n    font-size: 1.2rem;\n    cursor: ', ';\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    width: 24px;\n    height: 24px;\n    margin-right: 16px;\n'], ['\n    width: 24px;\n    height: 24px;\n    margin-right: 16px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleList = exports.StyleList = _styledComponents2.default.ul(_templateObject, (0, _color2.default)('gray-2'));

var StyleLine = exports.StyleLine = _styledComponents2.default.i(_templateObject2, (0, _color2.default)('primary'));

var StyleError = exports.StyleError = _styledComponents2.default.div(_templateObject3, function (_ref) {
    var onClick = _ref.onClick;
    return onClick ? 'pointer' : 'default';
});

var StyleErrorImg = exports.StyleErrorImg = _styledComponents2.default.img(_templateObject4);