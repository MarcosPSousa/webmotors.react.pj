'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

var _store = require('webmotors-react-pj/frame/modal/store');

var _store2 = _interopRequireDefault(_store);

var _items = require('webmotors-react-pj/frame/menu/items');

var _items2 = _interopRequireDefault(_items);

var _style = require('webmotors-react-pj/frame/menu/list/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var env = 'production';
    var hostname = window.location.hostname;

    var _jwtGet = (0, _utils.jwtGet)(),
        uniqueName = _jwtGet.unique_name,
        idGrupo = _jwtGet.idGrupo;

    var userGroup = uniqueName === '0' && idGrupo !== '0';
    var line = _react2.default.createElement(_style.StyleLine, null);

    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        openModal = _useState2[0],
        setOpenModal = _useState2[1];

    var _useState3 = (0, _react.useState)(false),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        redirect = _useState4[0],
        setRedirect = _useState4[1];

    if (/^local\./.test(hostname)) {
        env = 'development';
    }
    if (/^hk?(cockpit|crm|gb|estoque|comprarveiculos|autoguru)\./.test(hostname)) {
        env = 'homologation';
    }
    if (/^azul/.test(hostname)) {
        env = 'blue';
    }

    var menu = require('../env/' + env);

    var menuProducts = menu.filter(function (item) {
        return item.order === 1;
    });
    var menuInternal = menu.filter(function (item) {
        return item.order === 2;
    });

    var renderMenu = function renderMenu(obj) {
        return obj.map(function (item) {
            return _react2.default.createElement(
                _items2.default,
                {
                    key: (0, _stringify2.default)(item),
                    line: line,
                    onModal: function onModal() {
                        return setOpenModal(true);
                    },
                    onRedirect: function onRedirect(url) {
                        return setRedirect(url);
                    }
                },
                item
            );
        });
    };
    return _react2.default.createElement(
        _react.Fragment,
        null,
        line,
        _react2.default.createElement(
            _style.StyleList,
            null,
            renderMenu(menuProducts)
        ),
        _react2.default.createElement(
            _style.StyleList,
            null,
            renderMenu(menuInternal)
        ),
        openModal && _react2.default.createElement(_store2.default, {
            onClose: function onClose() {
                return setOpenModal(false);
            },
            onRedirect: redirect
        })
    );
};