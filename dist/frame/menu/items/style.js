'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleSublist = exports.StyleImg = exports.StyleLink = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    line-height: 40px;\n    padding: 0 32px;\n    display: block;\n    color: ', ';\n    opacity: 0.7;\n    transition: opacity 0.3s;\n    &:hover{\n        opacity: 1;\n    }\n'], ['\n    line-height: 40px;\n    padding: 0 32px;\n    display: block;\n    color: ', ';\n    opacity: 0.7;\n    transition: opacity 0.3s;\n    &:hover{\n        opacity: 1;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    vertical-align: middle;\n    width: 20px;\n    height: 20px;\n    margin-right: 12px;\n    transform: translateY(-2px);\n'], ['\n    vertical-align: middle;\n    width: 20px;\n    height: 20px;\n    margin-right: 12px;\n    transform: translateY(-2px);\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    padding: 0;\n    background-color: ', ';\n    overflow: hidden;\n    text-transform: none;\n    height: 0;\n    &.cockpit-menu-animation{\n        transition: height 0.3s, padding 0.3s;\n    }\n    &.cockpit-menu-open{\n        height: auto;\n        padding: 8px 0;\n    }\n'], ['\n    padding: 0;\n    background-color: ', ';\n    overflow: hidden;\n    text-transform: none;\n    height: 0;\n    &.cockpit-menu-animation{\n        transition: height 0.3s, padding 0.3s;\n    }\n    &.cockpit-menu-open{\n        height: auto;\n        padding: 8px 0;\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleLink = exports.StyleLink = _styledComponents2.default.a(_templateObject, (0, _color2.default)('white'));

var StyleImg = exports.StyleImg = _styledComponents2.default.img(_templateObject2);

var StyleSublist = exports.StyleSublist = _styledComponents2.default.ul(_templateObject3, (0, _color2.default)('gray-2'));