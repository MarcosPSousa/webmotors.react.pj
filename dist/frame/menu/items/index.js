'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _style = require('webmotors-react-pj/frame/menu/items/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children,
        line = _ref.line,
        needSelectStore = _ref.needSelectStore,
        onModal = _ref.onModal,
        onRedirect = _ref.onRedirect;

    var modalLinkBlock = ['/termos/pendentes', '/meu-plano/fatura'];

    var _jwtGet = (0, _utils.jwtGet)(),
        idGrupo = _jwtGet.idGrupo,
        role = _jwtGet.role,
        uniqueName = _jwtGet.unique_name,
        v2 = _jwtGet.v2;

    var Href = function Href(_ref2) {
        var sub = _ref2.sub,
            url = _ref2.url;
        return sub ? '#' : url.replace('$hashv2', v2);
    };
    var Rel = function Rel(url) {
        return (/cockpit\./.test(url) ? '' : 'noopener noreferrer'
        );
    };
    var Target = function Target(url) {
        return (/(autoguru)\.webmotors|zendesk\.com|\/universidade$/.test(url) || !/(betaestoque|hkestoque)/.test(window.location.origin) && /(\/h?estoque)/.test(url) ? '_blank' : '_self'
        );
    };

    var Icon = function Icon(param) {
        return param && _react2.default.createElement(_style.StyleImg, { src: _config.UrlCockpit + '/assets/img/icons' + param, alt: '', 'aria-hidden': 'true' });
    };

    var Sub = function Sub(sub) {
        return Boolean(sub) && _react2.default.createElement(
            _style.StyleSublist,
            null,
            sub.map(function (item) {
                return _react2.default.createElement(
                    MenuItem,
                    { key: item.name },
                    item
                );
            })
        );
    };

    var handleClick = function handleClick(event) {
        var target = event.target;
        var nextElementSibling = target.nextElementSibling;

        var groupIs = idGrupo && idGrupo !== '0' && role && role.indexOf('1') !== -1;
        var path = target.href.replace(/http(s?):\/\/(h|azul|)(local|cockpit).webmotors.com.br(:(\d+))?/, '');
        var modalShow = uniqueName === '0' && modalLinkBlock.includes(path) && groupIs;
        if (modalShow) {
            onRedirect(target.href);
            event.preventDefault();
            onModal();
        }
        if (nextElementSibling) {
            event.preventDefault();
            var classOpen = 'cockpit-menu-open';
            var classAnimation = 'cockpit-menu-animation';
            var submenuClose = function submenuClose(item) {
                var element = item;
                element.style.height = '0px';
                element.classList.remove(classOpen);
                element.addEventListener('transitionend', function () {
                    element.classList.remove(classAnimation);
                    element.removeAttribute('style');
                }, { once: true });
            };
            var isOpen = nextElementSibling.classList.contains(classOpen);
            [].concat((0, _toConsumableArray3.default)(document.querySelectorAll('.' + classOpen))).forEach(function (item) {
                return submenuClose(item);
            });
            if (!isOpen) {
                nextElementSibling.classList.add(classOpen);
                var clientHeight = nextElementSibling.clientHeight;

                nextElementSibling.classList.remove(classOpen);
                nextElementSibling.classList.add(classAnimation);
                setTimeout(function () {
                    nextElementSibling.style.height = clientHeight + 'px';
                    nextElementSibling.classList.add(classOpen);
                }, 10);
            }
        }
    };

    var handleMouseEnter = function handleMouseEnter(e) {
        var lineElement = document.querySelector('.' + line.type.styledComponentId);
        var target = e.target;

        var _target$getBoundingCl = target.getBoundingClientRect(),
            top = _target$getBoundingCl.top,
            height = _target$getBoundingCl.height;

        lineElement.setAttribute('style', 'transform: translateY(' + (top + window.scrollY) + 'px);height: ' + height + 'px;');
    };

    var MenuItem = function MenuItem(_ref3) {
        var child = _ref3.children;
        var sub = child.sub,
            url = child.url,
            icon = child.icon,
            name = child.name;

        return _react2.default.createElement(
            'li',
            { key: name },
            _react2.default.createElement(
                _style.StyleLink,
                {
                    onClick: function onClick(e) {
                        return handleClick(e);
                    },
                    onMouseEnter: function onMouseEnter(e) {
                        return handleMouseEnter(e);
                    },
                    href: Href({ sub: sub, url: url }),
                    target: Target(url),
                    rel: Rel(url)
                },
                Icon(icon),
                name
            ),
            Sub(sub)
        );
    };

    return _react2.default.createElement(
        MenuItem,
        null,
        children
    );
};