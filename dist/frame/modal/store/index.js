'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _modal = require('webmotors-react-pj/modal');

var _modal2 = _interopRequireDefault(_modal);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _load = require('webmotors-react-pj/frame/load');

var _load2 = _interopRequireDefault(_load);

var _search = require('webmotors-react-pj/frame/modal/store/search');

var _search2 = _interopRequireDefault(_search);

var _list = require('webmotors-react-pj/frame/modal/store/list');

var _list2 = _interopRequireDefault(_list);

var _style = require('webmotors-react-pj/frame/modal/store/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var _onClose = _ref.onClose,
        onRedirect = _ref.onRedirect;

    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        loaded = _useState2[0],
        setLoaded = _useState2[1];

    var _useState3 = (0, _react.useState)(false),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        success = _useState4[0],
        setSuccess = _useState4[1];

    var _useState5 = (0, _react.useState)(),
        _useState6 = (0, _slicedToArray3.default)(_useState5, 2),
        content = _useState6[0],
        setContent = _useState6[1];

    var _useState7 = (0, _react.useState)(''),
        _useState8 = (0, _slicedToArray3.default)(_useState7, 2),
        search = _useState8[0],
        setSearch = _useState8[1];

    var _useState9 = (0, _react.useState)('Carregando...'),
        _useState10 = (0, _slicedToArray3.default)(_useState9, 2),
        title = _useState10[0],
        setTitle = _useState10[1];

    var dataAllStore = [{ items: [{ idLoja: 0, nomeFantasia: 'Todas as lojas' }] }];

    var _jwtGet = (0, _utils.jwtGet)(),
        uniqueName = _jwtGet.unique_name;

    (0, _react.useEffect)(function () {
        (0, _utils.Ajax)({
            url: (/estoque.webmotors/.test(window.location.host) ? '/api' : _config.ApiCockpit) + '/Grupo/Listarlojas',
            method: 'GET'
        }).then(function (e) {
            var success = e.success,
                data = e.data;

            if (success) {
                var dataFormat = data.map(function (item) {
                    return { title: item.estado, items: item.lojas };
                });
                setSuccess(true);
                setTitle('Selecionar loja');
                setContent(dataFormat);
            } else {
                setSuccess(false);
                setTitle('Erro de listagem');
                setContent(data);
            }
            setLoaded(true);
        });
    }, []);

    var renderContent = function renderContent() {
        return success ? _react2.default.createElement(
            _react.Fragment,
            null,
            _react2.default.createElement(_search2.default, { onSearch: function onSearch(term) {
                    return setSearch(term);
                } }),
            uniqueName !== '0' && _react2.default.createElement(_list2.default, { data: dataAllStore, term: '', redirect: onRedirect }),
            _react2.default.createElement(_list2.default, { data: content, term: search, redirect: onRedirect })
        ) : _react2.default.createElement(
            _style2.default,
            null,
            data
        );
    };

    return _react2.default.createElement(
        _modal2.default,
        { icon: _config.UrlCockpit + '/assets/img/icons/pin-map.svg', title: title, onClose: function onClose() {
                return _onClose();
            } },
        loaded ? renderContent() : _react2.default.createElement(_load2.default, null)
    );
};