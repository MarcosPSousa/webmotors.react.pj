'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleInput = exports.StyleInputWrapper = exports.StyleImg = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: center;\n    flex: 1;\n    width: 80%;\n    margin: 0 auto 12px;\n    position: relative;\n    &:before{\n        content: \'\';\n        position: absolute;\n        width: 100%;\n        width: calc(100% + 28px);\n        left: 0;\n        bottom: 0;\n        border-bottom: 2px solid ', ';\n        transform: ', ';\n        transition: transform 0.3s;\n    }\n'], ['\n    display: flex;\n    align-items: center;\n    flex: 1;\n    width: 80%;\n    margin: 0 auto 12px;\n    position: relative;\n    &:before{\n        content: \'\';\n        position: absolute;\n        width: 100%;\n        width: calc(100% + 28px);\n        left: 0;\n        bottom: 0;\n        border-bottom: 2px solid ', ';\n        transform: ', ';\n        transition: transform 0.3s;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    width: 20px;\n    height: 20px;\n'], ['\n    width: 20px;\n    height: 20px;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    padding-left: 8px;\n    flex: 1;\n'], ['\n    padding-left: 8px;\n    flex: 1;\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    width: 100%;\n    border-style: none;\n    border-bottom: 1px solid ', ';\n    padding: 11px 4px;\n    line-height: 2.4rem;\n    font-size: 1.6rem;\n    outline-style: none;\n'], ['\n    width: 100%;\n    border-style: none;\n    border-bottom: 1px solid ', ';\n    padding: 11px 4px;\n    line-height: 2.4rem;\n    font-size: 1.6rem;\n    outline-style: none;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.label(_templateObject, (0, _color2.default)('#43bccd'), function (_ref) {
    var focus = _ref.focus;
    return focus ? 'translate(0px, -1px) rotateY(0deg)' : 'translate(-28px, -1px) rotateY(90deg)';
});

var StyleImg = exports.StyleImg = _styledComponents2.default.img(_templateObject2);
var StyleInputWrapper = exports.StyleInputWrapper = _styledComponents2.default.span(_templateObject3);
var StyleInput = exports.StyleInput = _styledComponents2.default.input(_templateObject4, (0, _color2.default)('gray-4'));