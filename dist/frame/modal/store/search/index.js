'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/modal/store/search/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var onSearch = _ref.onSearch;

    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        focus = _useState2[0],
        setFocus = _useState2[1];

    return _react2.default.createElement(
        _style.StyleWrapper,
        { focus: focus },
        _react2.default.createElement(_style.StyleImg, { src: _config.UrlCockpit + '/assets/img/icons/search.svg', alt: '', 'aria-hidden': 'true' }),
        _react2.default.createElement(
            _style.StyleInputWrapper,
            null,
            _react2.default.createElement(_style.StyleInput, {
                placeholder: 'Buscar por loja',
                onFocus: function onFocus() {
                    return setFocus(true);
                },
                onInput: function onInput(e) {
                    return onSearch(e.target.value);
                },
                onBlur: function onBlur() {
                    return setFocus(false);
                }
            })
        )
    );
};