'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

var _style = require('webmotors-react-pj/frame/modal/store/list/style');

var _style2 = _interopRequireDefault(_style);

var _item = require('webmotors-react-pj/frame/modal/store/item');

var _item2 = _interopRequireDefault(_item);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var data = _ref.data,
        term = _ref.term,
        redirect = _ref.redirect;
    return data.map(function (item, i) {
        var key = i;
        var uniqueName = _utils.jwtGet.unique_name;

        return _react2.default.createElement(
            _react.Fragment,
            { key: 'list-' + key },
            item.title && _react2.default.createElement(
                _style2.default,
                null,
                item.title
            ),
            _react2.default.createElement(
                'ul',
                null,
                item.items.map(function (list) {
                    var nomeFantasia = list.nomeFantasia,
                        idLoja = list.idLoja;

                    var searchTerm = term.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
                    var nameNormalize = nomeFantasia.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
                    var isOpen = nameNormalize.indexOf(searchTerm) !== -1;
                    return _react2.default.createElement(
                        _item2.default,
                        {
                            key: idLoja,
                            id: idLoja,
                            open: isOpen,
                            redirect: redirect
                        },
                        nomeFantasia
                    );
                })
            )
        );
    });
};