'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleButton = exports.StyleImg = exports.StyleInfo = exports.StyleList = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    border-bottom: 1px solid ', ';\n    padding: 8px 16px;\n    font-size: 1.4rem;\n    transition: padding 0.3s, height 0.3s;\n    align-items: center;\n    justify-content: space-between;\n    display: ', ';\n    &:first-child{\n        border-top: 1px solid ', ';\n    }\n'], ['\n    border-bottom: 1px solid ', ';\n    padding: 8px 16px;\n    font-size: 1.4rem;\n    transition: padding 0.3s, height 0.3s;\n    align-items: center;\n    justify-content: space-between;\n    display: ', ';\n    &:first-child{\n        border-top: 1px solid ', ';\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n    display: flex;\n    color: ', ';\n    align-items: center;\n'], ['\n    flex: 1;\n    display: flex;\n    color: ', ';\n    align-items: center;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    width: 16px;\n    height: 16px;\n    margin-right: 8px;\n    transform: translateY(-1px);\n'], ['\n    width: 16px;\n    height: 16px;\n    margin-right: 8px;\n    transform: translateY(-1px);\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    background-color: transparent;\n    color: ', ';\n    border: 1px solid ', ';\n    border-radius: 3px;\n    font-family: \'Poppins\', sans-serif;\n    outline-style: none;\n    padding: 8px 18px;\n    font-weight: 500;\n    transition: opacity 0.2s;\n    &:hover{\n        opacity: 0.7;\n    }\n    &[disabled]{\n        opacity: 0.3;\n    }\n'], ['\n    background-color: transparent;\n    color: ', ';\n    border: 1px solid ', ';\n    border-radius: 3px;\n    font-family: \'Poppins\', sans-serif;\n    outline-style: none;\n    padding: 8px 18px;\n    font-weight: 500;\n    transition: opacity 0.2s;\n    &:hover{\n        opacity: 0.7;\n    }\n    &[disabled]{\n        opacity: 0.3;\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleList = exports.StyleList = _styledComponents2.default.li(_templateObject, (0, _color2.default)('gray-4'), function (_ref) {
    var open = _ref.open;
    return open ? 'flex' : 'none';
}, (0, _color2.default)('gray-4'));

var StyleInfo = exports.StyleInfo = _styledComponents2.default.div(_templateObject2, (0, _color2.default)('#43bccd'));

var StyleImg = exports.StyleImg = _styledComponents2.default.img(_templateObject3);

var StyleButton = exports.StyleButton = _styledComponents2.default.button(_templateObject4, (0, _color2.default)('#43bccd'), (0, _color2.default)('#43bccd'));