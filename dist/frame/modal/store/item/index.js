'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/frame/modal/store/item/style');

var _utils = require('webmotors-react-pj/utils');

var _config = require('webmotors-react-pj/config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var id = _ref.id,
        children = _ref.children,
        open = _ref.open,
        redirect = _ref.redirect;

    var selectStore = function selectStore(e) {
        [].concat((0, _toConsumableArray3.default)(e.target.closest('ul').parentNode.querySelectorAll('button'))).forEach(function (btn) {
            var item = btn;
            item.disabled = 'disabled';
        });
        (0, _utils.Ajax)({
            url: (/estoque.webmotors/.test(window.location.host) ? '/api' : _config.ApiCockpit) + '/Grupo/RetornarLoja',
            data: { idCliente: id },
            method: 'GET'
        }).then(function (f) {
            if (f.success) {
                var reload = function reload() {
                    return window.location.reload();
                };
                (0, _utils.cookieRemove)('CockpitMenuItems');
                (0, _utils.cookieSet)('CockpitLogged', f.data);
                (0, _utils.cookieSet)('CockpitStoreName', children);
                setTimeout(function () {
                    if (redirect) {
                        if (redirect !== window.location.href) {
                            window.location.href = redirect;
                        } else {
                            reload();
                        }
                    } else {
                        reload();
                    }
                }, 100);
            } else {
                console.error(f);
            }
        });
    };

    return _react2.default.createElement(
        _style.StyleList,
        { open: open },
        _react2.default.createElement(
            _style.StyleInfo,
            null,
            _react2.default.createElement(_style.StyleImg, { src: _config.UrlCockpit + '/assets/img/icons/pin-map.svg', alt: '', 'aria-hidden': 'true' }),
            children
        ),
        _react2.default.createElement(
            _style.StyleButton,
            { type: 'button', onClick: function onClick(e) {
                    return selectStore(e);
                } },
            'Selecionar'
        )
    );
};