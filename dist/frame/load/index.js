'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    from {transform: rotate(0deg);}\n    to {transform: rotate(359deg);}\n'], ['\n    from {transform: rotate(0deg);}\n    to {transform: rotate(359deg);}\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    position: relative;\n    width: 24px;\n    height: 24px;\n    margin: 0 auto;\n    padding: 32px 0;\n    &:before{\n        content: \'\';\n        width: inherit;\n        height: inherit;\n        animation: ', ' 1s infinite;\n        border-radius: 100%;\n        border: 3px solid transparent;\n        border-left-color: ', ';\n        border-right-color: ', ';\n        position: absolute;\n        left: 50%;\n        top: 50%;\n        margin-left: -15px;\n        margin-top: -15px;\n    }\n'], ['\n    position: relative;\n    width: 24px;\n    height: 24px;\n    margin: 0 auto;\n    padding: 32px 0;\n    &:before{\n        content: \'\';\n        width: inherit;\n        height: inherit;\n        animation: ', ' 1s infinite;\n        border-radius: 100%;\n        border: 3px solid transparent;\n        border-left-color: ', ';\n        border-right-color: ', ';\n        position: absolute;\n        left: 50%;\n        top: 50%;\n        margin-left: -15px;\n        margin-top: -15px;\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var rotate = (0, _styledComponents.keyframes)(_templateObject);

exports.default = _styledComponents2.default.div(_templateObject2, rotate, (0, _color2.default)('gray-2'), (0, _color2.default)('gray-2'));