'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleButtons = exports.StyleInner = exports.StyleContent = exports.StyleTitle = exports.StyleClose = exports.StyleBox = exports.StyleBackground = exports.StyleWrapper = exports.StyleModalBody = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    body{\n        overflow: hidden;\n    }\n'], ['\n    body{\n        overflow: hidden;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    position: fixed;\n    left: 0;\n    top: 0;\n    z-index: 4;\n    width: 100vw;\n    height: 100vh;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    overflow-y: scroll;\n    *::-webkit-scrollbar {\n        width: 10px;\n    }\n    *::-webkit-scrollbar-thumb {\n        border-radius: 10px;\n        background-color: ', ';\n        border: 2px solid ', ';\n    }\n    *:hover::-webkit-scrollbar-track {\n        border-radius: 10px;\n        background-color: ', ';\n    }\n    *:hover::-webkit-scrollbar-thumb {\n        border-color: ', ';\n    }\n'], ['\n    position: fixed;\n    left: 0;\n    top: 0;\n    z-index: 4;\n    width: 100vw;\n    height: 100vh;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    overflow-y: scroll;\n    *::-webkit-scrollbar {\n        width: 10px;\n    }\n    *::-webkit-scrollbar-thumb {\n        border-radius: 10px;\n        background-color: ', ';\n        border: 2px solid ', ';\n    }\n    *:hover::-webkit-scrollbar-track {\n        border-radius: 10px;\n        background-color: ', ';\n    }\n    *:hover::-webkit-scrollbar-thumb {\n        border-color: ', ';\n    }\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    left: 0;\n    top: 0;\n    width: 100%;\n    height: 100%;\n    background-color: ', ';\n'], ['\n    position: absolute;\n    left: 0;\n    top: 0;\n    width: 100%;\n    height: 100%;\n    background-color: ', ';\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    background-color: ', ';\n    position: relative;\n    padding: 56px;\n    min-width: 312px;\n    max-height: 90vh;\n    border-radius: 8px;\n    box-shadow: 0px 16px 16px -16px ', ';\n    display: flex;\n'], ['\n    background-color: ', ';\n    position: relative;\n    padding: 56px;\n    min-width: 312px;\n    max-height: 90vh;\n    border-radius: 8px;\n    box-shadow: 0px 16px 16px -16px ', ';\n    display: flex;\n']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    right: 32px;\n    top: 32px;\n    cursor: pointer;\n    svg{\n        width: 24px;\n        height: 24px;\n    }\n'], ['\n    position: absolute;\n    right: 32px;\n    top: 32px;\n    cursor: pointer;\n    svg{\n        width: 24px;\n        height: 24px;\n    }\n']),
    _templateObject6 = (0, _taggedTemplateLiteral3.default)(['\n    font-weight: 500;\n    font-size: 2.8rem;\n    line-height: 1.5em;\n    color: ', ';\n    padding-bottom: 36px;\n'], ['\n    font-weight: 500;\n    font-size: 2.8rem;\n    line-height: 1.5em;\n    color: ', ';\n    padding-bottom: 36px;\n']),
    _templateObject7 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    flex-direction: column;\n'], ['\n    display: flex;\n    flex-direction: column;\n']),
    _templateObject8 = (0, _taggedTemplateLiteral3.default)(['\n    padding-bottom: 20px;\n    overflow: auto;\n    flex: 1;\n    overflow-x: hidden;\n'], ['\n    padding-bottom: 20px;\n    overflow: auto;\n    flex: 1;\n    overflow-x: hidden;\n']),
    _templateObject9 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    justify-content: flex-end;\n    border-top: 1px solid ', ';\n    padding: 20px 0 0;\n    button{\n        margin-left: 12px;\n        &:first-child{\n            margin-left: 0;\n        }\n    }\n'], ['\n    display: flex;\n    justify-content: flex-end;\n    border-top: 1px solid ', ';\n    padding: 20px 0 0;\n    button{\n        margin-left: 12px;\n        &:first-child{\n            margin-left: 0;\n        }\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var renderSize = function renderSize(size) {
    return {
        default: '856px',
        large: '1048px'
    }[size || 'default'];
};

var StyleModalBody = exports.StyleModalBody = (0, _styledComponents.createGlobalStyle)(_templateObject);

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject2, (0, _color2.default)('gray-3'), (0, _color2.default)('white'), (0, _color2.default)('gray-4'), (0, _color2.default)('gray-4'));

var StyleBackground = exports.StyleBackground = _styledComponents2.default.div(_templateObject3, (0, _color2.default)('modal'));

var StyleBox = exports.StyleBox = _styledComponents2.default.div(_templateObject4, (0, _color2.default)('white'), (0, _color2.default)('modal'));

var StyleClose = exports.StyleClose = _styledComponents2.default.div(_templateObject5);

var StyleTitle = exports.StyleTitle = _styledComponents2.default.h2(_templateObject6, (0, _color2.default)('gray'));

var StyleContent = exports.StyleContent = _styledComponents2.default.div(_templateObject7);

var StyleInner = exports.StyleInner = _styledComponents2.default.div(_templateObject8);

var StyleButtons = exports.StyleButtons = _styledComponents2.default.div(_templateObject9, (0, _color2.default)('gray-4'));