'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/webmodal/style');

var _utils = require('webmotors-react-pj/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (props) {
    var title = props.title,
        buttons = props.buttons,
        _props$onOpen = props.onOpen,
        onOpen = _props$onOpen === undefined ? false : _props$onOpen,
        onClose = props.onClose,
        children = props.children,
        dontClose = props.dontClose;

    var customProps = (0, _utils.customAttributes)(props, 'title|onOpen|onClose');

    var _useState = (0, _react.useState)(onOpen),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        isOpen = _useState2[0],
        setIsOpen = _useState2[1];

    var handleClose = function handleClose(e) {
        if (onClose) {
            onClose(e);
        } else {
            console.error('set props onClose in WebModal');
        }
        setIsOpen(false);
    };

    var handleClickBackground = function handleClickBackground(e) {
        if (!dontClose) {
            handleClose(e);
        }
    };

    var handleClickClose = function handleClickClose(e) {
        handleClose(e);
    };

    var renderTitle = function renderTitle() {
        return title ? _react2.default.createElement(
            _style.StyleTitle,
            null,
            title
        ) : false;
    };

    var renderButtons = function renderButtons() {
        return buttons ? _react2.default.createElement(
            _style.StyleButtons,
            null,
            buttons
        ) : false;
    };

    var renderClose = function renderClose() {
        return dontClose ? false : _react2.default.createElement(
            _style.StyleClose,
            { onClick: function onClick(e) {
                    return handleClickClose(e);
                } },
            _react2.default.createElement(
                'svg',
                { viewBox: '0 0 24 24', xmlns: 'http://www.w3.org/2000/svg' },
                _react2.default.createElement('path', { d: 'M13.5808 11.95L20.2618 5.26901C20.7128 4.81807 20.7128 4.08919 20.2618 3.63825C19.8109 3.18731 19.082 3.18731 18.6311 3.63825L11.95 10.3193L5.26901 3.63825C4.81807 3.18731 4.08919 3.18731 3.63825 3.63825C3.18731 4.08919 3.18731 4.81807 3.63825 5.26901L10.3193 11.95L3.63825 18.6311C3.18731 19.082 3.18731 19.8109 3.63825 20.2618C3.86315 20.4867 4.15839 20.5998 4.45363 20.5998C4.74888 20.5998 5.04412 20.4867 5.26901 20.2618L11.95 13.5808L18.6311 20.2618C18.856 20.4867 19.1512 20.5998 19.4465 20.5998C19.7417 20.5998 20.037 20.4867 20.2618 20.2618C20.7128 19.8109 20.7128 19.082 20.2618 18.6311L13.5808 11.95Z' })
            )
        );
    };

    (0, _react.useEffect)(function () {
        return setIsOpen(onOpen);
    }, [onOpen]);

    return isOpen && _react2.default.createElement(
        _style.StyleWrapper,
        customProps,
        _react2.default.createElement(_style.StyleModalBody, null),
        _react2.default.createElement(_style.StyleBackground, { onClick: function onClick(e) {
                return handleClickBackground(e);
            } }),
        _react2.default.createElement(
            _style.StyleBox,
            null,
            renderClose(),
            _react2.default.createElement(
                _style.StyleContent,
                null,
                renderTitle(),
                _react2.default.createElement(
                    _style.StyleInner,
                    null,
                    children
                ),
                renderButtons()
            )
        )
    );
};