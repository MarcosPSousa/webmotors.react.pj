'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleSvg = exports.StyleImg = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    width: ', ';\n    height: ', ';\n    border-radius: 100%;\n    overflow: hidden;\n    display: inline-flex;\n    align-items: center;\n    justify-content: center;\n    background-color: ', ';\n    fill: ', ';\n'], ['\n    width: ', ';\n    height: ', ';\n    border-radius: 100%;\n    overflow: hidden;\n    display: inline-flex;\n    align-items: center;\n    justify-content: center;\n    background-color: ', ';\n    fill: ', ';\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    width: auto;\n    height: 100%;\n'], ['\n    width: auto;\n    height: 100%;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    width: 64%;\n'], ['\n    width: 64%;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject, function (_ref) {
    var size = _ref.size;
    return size + 'px';
}, function (_ref2) {
    var size = _ref2.size;
    return size + 'px';
}, (0, _color2.default)('gray-3'), (0, _color2.default)('white'));
var StyleImg = exports.StyleImg = _styledComponents2.default.img(_templateObject2);
var StyleSvg = exports.StyleSvg = _styledComponents2.default.svg(_templateObject3);