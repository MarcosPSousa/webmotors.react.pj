"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _entries = require("babel-runtime/core-js/object/entries");

var _entries2 = _interopRequireDefault(_entries);

var _getPrototypeOf = require("babel-runtime/core-js/object/get-prototype-of");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require("babel-runtime/helpers/possibleConstructorReturn");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require("babel-runtime/helpers/inherits");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PageTitle = function (_Component) {
    (0, _inherits3.default)(PageTitle, _Component);

    function PageTitle(props) {
        (0, _classCallCheck3.default)(this, PageTitle);

        var _this = (0, _possibleConstructorReturn3.default)(this, (PageTitle.__proto__ || (0, _getPrototypeOf2.default)(PageTitle)).call(this, props));

        _this.state = {};
        _this.key = 0;
        return _this;
    }

    (0, _createClass3.default)(PageTitle, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            var _this2 = this;

            setTimeout(function () {
                return _this2.setState({ title: document.title });
            }, 10);
        }
    }, {
        key: "render",
        value: function render() {
            var _this3 = this;

            return _react2.default.createElement(
                "header",
                { className: "pagetitle" },
                _react2.default.createElement(
                    "div",
                    { className: "pagetitle__content" },
                    _react2.default.createElement(
                        "h1",
                        { className: "pagetitle__title" },
                        this.props.title
                    ),
                    _react2.default.createElement(
                        "div",
                        { className: "pagetitle__aside" },
                        this.props.children
                    )
                ),
                _react2.default.createElement(
                    "div",
                    { className: "pagetitle__tablist", role: "tablist", "aria-label": "Listagem de Links da p\xE1gina " + this.state.title },
                    this.props.tabs && (0, _entries2.default)(this.props.tabs).map(function (item) {
                        return item[0] === window.location.pathname ? _react2.default.createElement(
                            "span",
                            { key: ++_this3.key, className: "pagetitle__tab pagetitle__tab--active", role: "tab" },
                            item[1]
                        ) : _react2.default.createElement(
                            "a",
                            {
                                key: ++_this3.key,
                                href: item[0],
                                className: "pagetitle__tab",
                                type: "button",
                                role: "tab",
                                onClick: _this3.props.onClick
                            },
                            item[1]
                        );
                    })
                )
            );
        }
    }]);
    return PageTitle;
}(_react.Component);

exports.default = PageTitle;