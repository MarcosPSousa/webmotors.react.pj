'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/accordion/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children,
        open = _ref.open;

    var accordion = (0, _react.useRef)();

    (0, _react.useEffect)(function () {
        var current = accordion.current;
        var style = current.style;

        current.addEventListener('transitionstart', function () {
            var openState = current.dataset.accordion === 'true';
            if (!openState) {
                current.classList.remove('accordion-open');
            }
        });
        current.addEventListener('transitionend', function () {
            var openState = current.dataset.accordion === 'true';
            if (openState) {
                current.classList.add('accordion-open');
            }
        });
    }, []);

    (0, _react.useEffect)(function () {
        var current = accordion.current;
        var style = current.style;

        if (open) {
            style.height = 'auto';
            var clientHeight = current.clientHeight;

            style.height = '0px';
            setTimeout(function () {
                style.height = clientHeight + 'px';
            }, 10);
        } else {
            style.height = '0px';
        }
    }, [open]);

    return _react2.default.createElement(
        _style2.default,
        { ref: accordion, 'data-accordion': open },
        children
    );
};