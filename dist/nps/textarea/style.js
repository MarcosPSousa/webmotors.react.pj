'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    width: 100%;\n    border: 1px solid ', ';\n    border-radius: 8px;\n    margin-bottom: 16px;\n    padding: 12px;\n    font-family: inherit;\n    background-color: ', ';\n    opacity: ', '\n'], ['\n    width: 100%;\n    border: 1px solid ', ';\n    border-radius: 8px;\n    margin-bottom: 16px;\n    padding: 12px;\n    font-family: inherit;\n    background-color: ', ';\n    opacity: ', '\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.textarea(_templateObject, (0, _color2.default)('gray-3'), (0, _color2.default)('white'), function (props) {
    return props.disabled ? 0.3 : 1;
});