'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/nps/textarea/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var sendHas = _ref.sendHas,
      onComments = _ref.onComments;
  return _react2.default.createElement(_style2.default, { maxlength: '512', onInput: function onInput(e) {
      return onComments(e.target.value);
    }, disabled: !sendHas });
};