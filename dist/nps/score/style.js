'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ScoreSquare = exports.ScoreRadio = exports.ScoreLabel = exports.Score = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    justify-content: center;\n    position: relative;\n    padding-bottom: 24px;\n    margin-bottom: 16px;\n    &:before{\n        content: \'\';\n        position: absolute;\n        left: -16px;\n        right: -16px;\n        border-bottom: 1px solid ', ';\n        bottom: 0;\n    }\n'], ['\n    display: flex;\n    justify-content: center;\n    position: relative;\n    padding-bottom: 24px;\n    margin-bottom: 16px;\n    &:before{\n        content: \'\';\n        position: absolute;\n        left: -16px;\n        right: -16px;\n        border-bottom: 1px solid ', ';\n        bottom: 0;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    margin: 0 3px;\n    cursor: pointer;\n'], ['\n    margin: 0 3px;\n    cursor: pointer;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    opacity: 0;\n    position: absolute;\n    z-index: -1;\n'], ['\n    opacity: 0;\n    position: absolute;\n    z-index: -1;\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    display: block;\n    width: 18px;\n    height: 18px;\n    line-height: 20px;\n    font-size: 10px;\n    border-radius: 4px;\n    color: ', ';\n    opacity: ', ';\n    background-color: ', ';\n    transition: opacity 0.3s;\n'], ['\n    display: block;\n    width: 18px;\n    height: 18px;\n    line-height: 20px;\n    font-size: 10px;\n    border-radius: 4px;\n    color: ', ';\n    opacity: ', ';\n    background-color: ', ';\n    transition: opacity 0.3s;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var backgroundColor = function backgroundColor(props) {
    return {
        0: '#B72025',
        1: '#D62027',
        2: '#F05223',
        3: '#F36F21',
        4: '#FAA823',
        5: '#FFCA27',
        6: '#ECDB12',
        7: '#E8E73D',
        8: '#C5D92D',
        9: '#AFD136',
        10: '#64B64D'
    }[props.children];
};
var opacity = function opacity(props) {
    return props.active ? 1 : 0.4;
};

var Score = exports.Score = _styledComponents2.default.div(_templateObject, (0, _color2.default)('#f9f9f9'));

var ScoreLabel = exports.ScoreLabel = _styledComponents2.default.label(_templateObject2);

var ScoreRadio = exports.ScoreRadio = _styledComponents2.default.input(_templateObject3);

var ScoreSquare = exports.ScoreSquare = _styledComponents2.default.span(_templateObject4, (0, _color2.default)('white'), function (props) {
    return opacity(props);
}, function (props) {
    return backgroundColor(props);
});