'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/nps/score/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var onUnlock = _ref.onUnlock,
        onScore = _ref.onScore;

    var score = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    var _useState = (0, _react.useState)([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        scoreActive = _useState2[0],
        setScoreActive = _useState2[1];

    var setScore = function setScore(point) {
        setScoreActive([point]);
        onScore(point);
        onUnlock();
    };

    return _react2.default.createElement(
        _style.Score,
        null,
        score.map(function (item) {
            return _react2.default.createElement(
                _style.ScoreLabel,
                { key: item },
                _react2.default.createElement(_style.ScoreRadio, { type: 'radio', name: 'nota', onClick: function onClick() {
                        return setScore(item);
                    } }),
                _react2.default.createElement(
                    _style.ScoreSquare,
                    { active: scoreActive.includes(item) },
                    item
                )
            );
        })
    );
};