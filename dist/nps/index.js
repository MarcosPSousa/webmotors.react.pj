'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _show = require('webmotors-react-pj/nps/db/show');

var _show2 = _interopRequireDefault(_show);

var _steps = require('webmotors-react-pj/nps/steps');

var _steps2 = _interopRequireDefault(_steps);

var _wrapper = require('webmotors-react-pj/nps/wrapper');

var _wrapper2 = _interopRequireDefault(_wrapper);

var _form = require('webmotors-react-pj/nps/form');

var _form2 = _interopRequireDefault(_form);

var _toggle = require('webmotors-react-pj/nps/toggle');

var _toggle2 = _interopRequireDefault(_toggle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        open = _useState2[0],
        setOpen = _useState2[1];

    var _useState3 = (0, _react.useState)(false),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        show = _useState4[0],
        setShow = _useState4[1];

    var _useState5 = (0, _react.useState)(true),
        _useState6 = (0, _slicedToArray3.default)(_useState5, 2),
        render = _useState6[0],
        setRender = _useState6[1];

    (0, _react.useEffect)(function () {
        (0, _show2.default)({ onRender: setRender }).then(function (e) {
            setShow(e);
            setOpen(e);
        }).catch(function () {
            setOpen(true);
            setShow(true);
        });
    }, []);

    return _react2.default.createElement(
        _wrapper2.default,
        { render: render },
        _react2.default.createElement(
            _form2.default,
            { open: open, show: show },
            _react2.default.createElement(_toggle2.default, {
                open: open,
                onClick: function onClick() {
                    return setOpen(!open);
                }
            }),
            _react2.default.createElement(_steps2.default, { handleShow: function handleShow(e) {
                    return setShow(e);
                } })
        )
    );
};