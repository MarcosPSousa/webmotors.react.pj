'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _update = require('webmotors-react-pj/nps/db/update');

var _style = require('webmotors-react-pj/nps/reject/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children,
        onClick = _ref.onClick;

    var handleClick = function handleClick() {
        (0, _update.Reject)();
        onClick();
    };
    return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
            _style2.default,
            { onClick: function onClick() {
                    return handleClick();
                } },
            children
        )
    );
};