'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/nps/toggle/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var open = _ref.open,
      _onClick = _ref.onClick;
  return _react2.default.createElement(_style2.default, { open: open, onClick: function onClick() {
      return _onClick();
    } });
};