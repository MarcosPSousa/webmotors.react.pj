'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    right: 12px;\n    top: 0;\n    padding: 6px 12px 4px 10px;\n    box-shadow: 0 -10px 80px -10px rgba(0, 0, 0, 0.5);\n    transform: translateY(-80%);\n    background-color: ', ';\n    border-radius: 4px 4px 0 0;\n    &:before{\n        content: \'\';\n        border: 2px solid ', ';\n        width: 8px;\n        height: 8px;\n        border-left-style: none;\n        border-top-style: none;\n        display: block;\n        transform: ', ';\n        transform-origin: 6px 6px;\n        transition: transform 0.3s;\n    }\n'], ['\n    position: absolute;\n    right: 12px;\n    top: 0;\n    padding: 6px 12px 4px 10px;\n    box-shadow: 0 -10px 80px -10px rgba(0, 0, 0, 0.5);\n    transform: translateY(-80%);\n    background-color: ', ';\n    border-radius: 4px 4px 0 0;\n    &:before{\n        content: \'\';\n        border: 2px solid ', ';\n        width: 8px;\n        height: 8px;\n        border-left-style: none;\n        border-top-style: none;\n        display: block;\n        transform: ', ';\n        transform-origin: 6px 6px;\n        transition: transform 0.3s;\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var open = function open(props) {
    return props.open ? 'rotate(45deg)' : 'rotate(-135deg)';
};

exports.default = _styledComponents2.default.button(_templateObject, (0, _color2.default)('white'), (0, _color2.default)('gray-2'), function (props) {
    return open(props);
});