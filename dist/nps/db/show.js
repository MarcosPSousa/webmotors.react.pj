'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _dexie = require('dexie');

var _dexie2 = _interopRequireDefault(_dexie);

var _utils = require('webmotors-react-pj/utils');

var _request2 = require('webmotors-react-pj/nps/db/request');

var _request3 = _interopRequireDefault(_request2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var onRender = _ref.onRender;

    var _jwtGet = (0, _utils.jwtGet)(),
        sid = _jwtGet.sid;

    var db = new _dexie2.default('cockpit');
    db.version(4).stores({ Nps: 'id,dateShow' });
    var Nps = db.Nps;

    return db.transaction('rw', Nps, (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
        var id, dateNow, get;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        id = Number(sid);
                        dateNow = Date.now();
                        _context2.next = 4;
                        return Nps.get({ id: id }).then(function () {
                            var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(user) {
                                var request, dateShow, _request;

                                return _regenerator2.default.wrap(function _callee$(_context) {
                                    while (1) {
                                        switch (_context.prev = _context.next) {
                                            case 0:
                                                if (user) {
                                                    _context.next = 5;
                                                    break;
                                                }

                                                _context.next = 3;
                                                return (0, _request3.default)();

                                            case 3:
                                                request = _context.sent;
                                                return _context.abrupt('return', request);

                                            case 5:
                                                dateShow = user.dateShow;

                                                if (!(dateNow > dateShow)) {
                                                    _context.next = 11;
                                                    break;
                                                }

                                                _context.next = 9;
                                                return (0, _request3.default)();

                                            case 9:
                                                _request = _context.sent;
                                                return _context.abrupt('return', _request);

                                            case 11:
                                                return _context.abrupt('return', false);

                                            case 12:
                                            case 'end':
                                                return _context.stop();
                                        }
                                    }
                                }, _callee, undefined);
                            }));

                            return function (_x) {
                                return _ref3.apply(this, arguments);
                            };
                        }());

                    case 4:
                        get = _context2.sent;

                        onRender(get);
                        return _context2.abrupt('return', get);

                    case 7:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    })));
};