'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.showAgainDays = exports.Reject = exports.Send = undefined;

var _dexie = require('dexie');

var _dexie2 = _interopRequireDefault(_dexie);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var showAgainDays = function showAgainDays(timestamp) {
    var _jwtGet = (0, _utils.jwtGet)(),
        sid = _jwtGet.sid;

    var db = new _dexie2.default('cockpit');
    db.version(4).stores({ Nps: 'id,dateShow' });
    var Nps = db.Nps;

    db.transaction('rw', Nps, function () {
        var id = Number(sid);
        Nps.get({ id: id }).then(function (user) {
            if (!user) {
                Nps.add({ id: id, dateShow: timestamp });
            } else {
                Nps.update(id, { dateShow: timestamp });
            }
        });
    });
};

var Send = function Send(_ref) {
    var score = _ref.score,
        comments = _ref.comments;

    (0, _utils.Ajax)({
        url: _config.ApiCockpit + '/Nps?' + Math.random() * 100,
        method: 'POST',
        data: {
            nota: score,
            comentario: comments
        }
    }).then(function (e) {
        if (e.success) {
            var days = 90;
            var date = new Date(Date.now() + 1000 * 60 * 60 * 24 * days);
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getTime();
            showAgainDays(firstDay);
        }
    });
};

var Reject = function Reject() {
    var days = 7;
    var date = new Date(Date.now() + 1000 * 60 * 60 * 24 * days).getTime();
    showAgainDays(date);
};

exports.Send = Send;
exports.Reject = Reject;
exports.showAgainDays = showAgainDays;