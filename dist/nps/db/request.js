'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _update = require('webmotors-react-pj/nps/db/update');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
    var request;
    return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    _context.next = 2;
                    return (0, _utils.Ajax)({
                        url: _config.ApiCockpit + '/Nps',
                        method: 'GET'
                    }).then(function (e) {
                        var data = e.data,
                            success = e.success;

                        var response = false;
                        if (success) {
                            var dateShow = new Date(data.lastAnswer + 'T00:00');
                            var dateSave = new Date(dateShow.getFullYear(), dateShow.getMonth() + 3, 1).getTime();
                            response = data.permissionNps;
                            if (!response) {
                                (0, _update.showAgainDays)(dateSave);
                            }
                        }
                        return response;
                    });

                case 2:
                    request = _context.sent;
                    return _context.abrupt('return', request);

                case 4:
                case 'end':
                    return _context.stop();
            }
        }
    }, _callee, undefined);
}));