'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    overflow: hidden;\n    position: fixed;\n    z-index: 4;\n    left: 0;\n    bottom: 0;\n    width: 100%;\n    max-width: 450px;\n    height: 100%;\n    max-height: 460px;\n    pointer-events: none;\n'], ['\n    overflow: hidden;\n    position: fixed;\n    z-index: 4;\n    left: 0;\n    bottom: 0;\n    width: 100%;\n    max-width: 450px;\n    height: 100%;\n    max-height: 460px;\n    pointer-events: none;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.div(_templateObject);