'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: inline-block;\n    padding: 8px 18px;\n    border-radius: 3px;\n    border-style: none;\n    font-family: inherit;\n    font-size: 1.4rem;\n    font-weight: 500;\n    color: ', ';\n    background-color: ', ';\n    cursor: ', ';\n    opacity: ', ';\n    transition: opacity 0.2s, box-shadow 0.3s;\n    margin-bottom: 12px;\n    &:hover{\n        opacity: ', ';\n    }\n'], ['\n    display: inline-block;\n    padding: 8px 18px;\n    border-radius: 3px;\n    border-style: none;\n    font-family: inherit;\n    font-size: 1.4rem;\n    font-weight: 500;\n    color: ', ';\n    background-color: ', ';\n    cursor: ', ';\n    opacity: ', ';\n    transition: opacity 0.2s, box-shadow 0.3s;\n    margin-bottom: 12px;\n    &:hover{\n        opacity: ', ';\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var disabled = function disabled(props) {
    return props.disabled ? 'not-allowed' : 'pointer';
};
var opacityDisabled = function opacityDisabled(props, value) {
    return props.disabled ? 0.2 : value;
};

exports.default = _styledComponents2.default.button(_templateObject, (0, _color2.default)('white'), (0, _color2.default)('primary'), function (props) {
    return disabled(props);
}, function (props) {
    return opacityDisabled(props, 1);
}, function (props) {
    return opacityDisabled(props, 0.7);
});