'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    max-width: 310px;\n    width: 90%;\n    padding: 16px;\n    border-radius: 4px 4px 0 0;\n    box-shadow: 0 2px 80px 0 rgba(0, 0, 0, 0.5);\n    position: absolute;\n    left: 32px;\n    bottom: 0;\n    z-index: 4;\n    text-align: center;\n    background-color: ', ';\n    transform: ', ';\n    transition: transform 0.3s;\n    pointer-events: all;\n'], ['\n    max-width: 310px;\n    width: 90%;\n    padding: 16px;\n    border-radius: 4px 4px 0 0;\n    box-shadow: 0 2px 80px 0 rgba(0, 0, 0, 0.5);\n    position: absolute;\n    left: 32px;\n    bottom: 0;\n    z-index: 4;\n    text-align: center;\n    background-color: ', ';\n    transform: ', ';\n    transition: transform 0.3s;\n    pointer-events: all;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var open = function open(props) {
    return props.open ? 'translateY(0px)' : 'translateY(calc(100% - 16px))';
};
var show = function show(props) {
    return props.show ? open(props) : 'translateY(150%)';
};

exports.default = _styledComponents2.default.form(_templateObject, (0, _color2.default)('white'), function (props) {
    return show(props);
});