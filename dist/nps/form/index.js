'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/nps/form/style');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children,
        open = _ref.open,
        show = _ref.show;
    return _react2.default.createElement(
        _style2.default,
        { open: open, onSubmit: function onSubmit(e) {
                return e.preventDefault();
            }, show: show },
        children
    );
};