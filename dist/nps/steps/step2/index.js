'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _text = require('webmotors-react-pj/nps/text');

var _text2 = _interopRequireDefault(_text);

var _button = require('webmotors-react-pj/nps/button');

var _button2 = _interopRequireDefault(_button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var handleShow = _ref.handleShow;
    return _react2.default.createElement(
        'div',
        { className: 'step-2' },
        _react2.default.createElement(
            _text2.default,
            null,
            'Agradecemos a sua resposta'
        ),
        _react2.default.createElement(
            _button2.default,
            { sendHas: true, onClick: function onClick() {
                    return handleShow(false);
                } },
            'Continuar'
        )
    );
};