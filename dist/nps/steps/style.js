'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ScrollChildStyle = exports.ScrollContentStyle = exports.ScrollStyle = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    overflow: hidden;\n'], ['\n    overflow: hidden;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: flex-start;\n    transform: ', ';\n    width: ', ';\n    transition: transform 0.3s, height 0.3s;\n'], ['\n    display: flex;\n    align-items: flex-start;\n    transform: ', ';\n    width: ', ';\n    transition: transform 0.3s, height 0.3s;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    width: ', ';\n'], ['\n    width: ', ';\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ScrollStyle = _styledComponents2.default.div(_templateObject);

var ScrollContentStyle = _styledComponents2.default.div(_templateObject2, function (props) {
    return 'translateX(-' + props.left + '%)';
}, function (props) {
    return props.width + '%';
});

var ScrollChildStyle = _styledComponents2.default.div(_templateObject3, function (props) {
    return props.width + '%';
});

exports.ScrollStyle = ScrollStyle;
exports.ScrollContentStyle = ScrollContentStyle;
exports.ScrollChildStyle = ScrollChildStyle;