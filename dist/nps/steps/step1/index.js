'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _text = require('webmotors-react-pj/nps/text');

var _text2 = _interopRequireDefault(_text);

var _score = require('webmotors-react-pj/nps/score');

var _score2 = _interopRequireDefault(_score);

var _description = require('webmotors-react-pj/nps/description');

var _description2 = _interopRequireDefault(_description);

var _textarea = require('webmotors-react-pj/nps/textarea');

var _textarea2 = _interopRequireDefault(_textarea);

var _button = require('webmotors-react-pj/nps/button');

var _button2 = _interopRequireDefault(_button);

var _reject = require('webmotors-react-pj/nps/reject');

var _reject2 = _interopRequireDefault(_reject);

var _update = require('webmotors-react-pj/nps/db/update');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var handleShow = _ref.handleShow,
        onNextStep = _ref.onNextStep;

    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        unlock = _useState2[0],
        setUnlock = _useState2[1];

    var _useState3 = (0, _react.useState)(0),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        score = _useState4[0],
        setScore = _useState4[1];

    var _useState5 = (0, _react.useState)(''),
        _useState6 = (0, _slicedToArray3.default)(_useState5, 2),
        comments = _useState6[0],
        setComments = _useState6[1];

    var handleSend = function handleSend() {
        onNextStep();
        (0, _update.Send)({ score: score, comments: comments });
    };
    return _react2.default.createElement(
        'div',
        { className: 'step-1' },
        _react2.default.createElement(
            _text2.default,
            null,
            'Com base na sua experi\xEAncia com a Webmotors, como voc\xEA classifica os servi\xE7os prestados, em uma escala de 0 a 10?'
        ),
        _react2.default.createElement(_score2.default, { onUnlock: function onUnlock() {
                return setUnlock(true);
            }, onScore: function onScore(e) {
                return setScore(e);
            } }),
        _react2.default.createElement(
            _description2.default,
            { sendHas: unlock },
            'Explique o que te motivou a atribuir esta nota'
        ),
        _react2.default.createElement(_textarea2.default, { sendHas: unlock, onComments: function onComments(e) {
                return setComments(e);
            } }),
        _react2.default.createElement(
            _button2.default,
            { onClick: function onClick() {
                    return handleSend();
                }, sendHas: unlock },
            'Enviar'
        ),
        _react2.default.createElement(
            _reject2.default,
            { onClick: function onClick() {
                    return handleShow(false);
                } },
            'Agora n\xE3o'
        )
    );
};