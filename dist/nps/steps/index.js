'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/nps/steps/style');

var _step = require('webmotors-react-pj/nps/steps/step1');

var _step2 = _interopRequireDefault(_step);

var _step3 = require('webmotors-react-pj/nps/steps/step2');

var _step4 = _interopRequireDefault(_step3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var handleOpen = _ref.handleOpen,
        handleShow = _ref.handleShow;

    var _useState = (0, _react.useState)(0),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        step = _useState2[0],
        setStep = _useState2[1];

    var _useState3 = (0, _react.useState)(0),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        widthContent = _useState4[0],
        setWidthContent = _useState4[1];

    var _useState5 = (0, _react.useState)(0),
        _useState6 = (0, _slicedToArray3.default)(_useState5, 2),
        widthChild = _useState6[0],
        setWidthChild = _useState6[1];

    var child = [_react2.default.createElement(_step2.default, {
        handleShow: handleShow,
        handleOpen: handleOpen,
        onNextStep: function onNextStep() {
            setStep(step + 1);
        }
    }), _react2.default.createElement(_step4.default, { handleShow: handleShow })];

    (0, _react.useEffect)(function () {
        setWidthChild(100 / child.length);
        setWidthContent(100 * child.length);
    }, []);

    (0, _react.useEffect)(function () {
        var stepSelector = document.querySelector('.step-' + (step + 1));
        if (stepSelector) {
            document.querySelector('.step-scroll').style.height = stepSelector.clientHeight + 'px';
        }
    });

    return _react2.default.createElement(
        _style.ScrollStyle,
        null,
        _react2.default.createElement(
            _style.ScrollContentStyle,
            { className: 'step-scroll', width: widthContent, left: step * (100 / child.length) },
            child.map(function (item, i) {
                return _react2.default.createElement(
                    _style.ScrollChildStyle,
                    { width: widthChild, key: i },
                    item
                );
            })
        )
    );
};