'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/button/style');

var _utils = require('webmotors-react-pj/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (props) {
    var _props$tag = props.tag,
        tag = _props$tag === undefined ? 'button' : _props$tag,
        _props$type = props.type,
        type = _props$type === undefined ? 'submit' : _props$type,
        _props$size = props.size,
        size = _props$size === undefined ? 'small' : _props$size,
        _props$token = props.token,
        token = _props$token === undefined ? 'primary' : _props$token,
        icon = props.icon,
        children = props.children;

    var propsCustom = (0, _utils.customAttributes)(props, 'tag|token|icon|size');
    return (0, _react.createElement)((0, _style.StyleElement)({ tag: tag, token: token, size: size }), (0, _extends3.default)({}, propsCustom), _react2.default.createElement(
        _react.Fragment,
        null,
        children,
        _react2.default.createElement(
            _style.StyleIcon,
            null,
            icon
        )
    ));
};