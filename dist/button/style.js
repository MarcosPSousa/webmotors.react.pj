'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleIcon = exports.StyleElement = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    border: 1px solid;\n    border-color: ', ';\n    background-color: ', ';\n    color: ', ';\n    font-weight: bold;\n    font-size: ', ';\n    padding: ', ';\n    border-radius: 8px;\n'], ['\n    border: 1px solid;\n    border-color: ', ';\n    background-color: ', ';\n    color: ', ';\n    font-weight: bold;\n    font-size: ', ';\n    padding: ', ';\n    border-radius: 8px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    display: none;\n'], ['\n    display: none;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var borderColor = function borderColor(token) {
    return {
        primary: (0, _color2.default)('primary'),
        secondary: (0, _color2.default)('primary-3'),
        tertiary: (0, _color2.default)('primary'),
        ghost: 'transparent',
        inactive: (0, _color2.default)('gray-3'),
        cancel: (0, _color2.default)('gray-2'),
        danger: (0, _color2.default)('danger')
    }[token || 'primary'];
};

var backgroundColor = function backgroundColor(token) {
    return {
        primary: (0, _color2.default)('primary'),
        secondary: (0, _color2.default)('primary-3'),
        tertiary: 'transparent',
        ghost: 'transparent',
        inactive: (0, _color2.default)('gray-3'),
        cancel: 'transparent',
        danger: (0, _color2.default)('danger')
    }[token || 'primary'];
};

var textColor = function textColor(token) {
    return {
        primary: (0, _color2.default)('white'),
        secondary: (0, _color2.default)('primary'),
        tertiary: (0, _color2.default)('primary'),
        ghost: (0, _color2.default)('primary'),
        inactive: (0, _color2.default)('white'),
        cancel: (0, _color2.default)('gray-2'),
        danger: (0, _color2.default)('gray-1')
    }[token || 'primary'];
};

var StyleElement = exports.StyleElement = function StyleElement(_ref) {
    var tag = _ref.tag,
        token = _ref.token,
        size = _ref.size;
    return (0, _styledComponents2.default)(tag)(_templateObject, borderColor(token), backgroundColor(token), textColor(token), function (_ref2) {
        var size = _ref2.size;
        return size === 'big' ? '1.6rem' : '1.2rem';
    }, function (_ref3) {
        var size = _ref3.size;
        return size === 'big' ? '16px 24px' : '12px 20px';
    });
};

var StyleIcon = exports.StyleIcon = _styledComponents2.default.i(_templateObject2);