'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    color: ', ';\n    font-weight: 600;\n    font-size: 2rem;\n    margin-bottom: 22px;\n'], ['\n    color: ', ';\n    font-weight: 600;\n    font-size: 2rem;\n    margin-bottom: 22px;\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleHeading = function StyleHeading(tag) {
    return (0, _styledComponents2.default)(tag)(_templateObject, (0, _color2.default)('gray-2'));
};

exports.default = function (_ref) {
    var _ref$tag = _ref.tag,
        tag = _ref$tag === undefined ? 'h1' : _ref$tag,
        children = _ref.children,
        className = _ref.className;
    return (0, _react.createElement)(StyleHeading(tag), { className: className }, children);
};