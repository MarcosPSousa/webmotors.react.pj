'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _notification = require('webmotors-react-pj/frame/header/logged/items/notification');

var _notification2 = _interopRequireDefault(_notification);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var translate = {
    pageStatus: {
        load: 'Cargando...'
    },
    webinput: {
        default: '',
        required: 'Llena el campo obligatorio',
        email: 'Incluye un email válido',
        emailLogin: 'Incluye un email válido',
        tel: 'Incluye un teléfono válido',
        date: 'Incluye una fecha válida',
        dateShort: 'Incluye una fecha simplificada válida',
        password: 'Incluye todos los criterios de contraseña válidos',
        passwordWeakened: 'Incluye todos los criterios de contraseña válidos',
        url: 'Incluye una url válida (ej.: https://www.webmotors.com.br)',
        time: 'Incluye un horario válido',
        nameSurname: 'Incluye un nombre y apellido válidos',
        cep: 'Incluye un código postal válido',
        plate: 'Incluye una placa de vehículo válida',
        cpf: 'Incluye un RUC válido',
        cnpj: 'Incluye un RUC de PJ válido',
        renavam: 'Incluye un registro de vehículo válido',
        equal: function equal(input) {
            return 'Digite um valor igual ao campo "' + (input && input.previousElementSibling.textContent) + '"';
        },
        min: function min(_min) {
            return 'Digite um n\xFAmero maior que ' + _min;
        },
        max: function max(_max) {
            return 'Digite um n\xFAmero menor que ' + _max;
        },
        minMax: function minMax(min, max) {
            return 'Digite um n\xFAmero entre ' + min + ' e ' + max;
        }
    },
    nps: {
        Step1Text: '\n            Com base na sua experi\xEAncia com a Webmotors,\n            como voc\xEA classifica os servi\xE7os prestados, em uma escala de 0 a 10?\n        ',
        Step1Description: 'Explica lo que te motivó a atribuir esta nota',
        Step1ButtonSend: 'Enviar',
        Step1ButtonDecline: 'Ahora no',
        Step2Text: 'Agradecemos tu respuesta',
        Step2Button: 'Continuar'
    },
    main: {
        errorTitle: 'Aplicación no disponible',
        reloadPage: 'Recargar página',
        pleaseEnterContact: 'Se ruega entrar en contacto con nosotros.'
    },
    utils: {
        status0: 'Error no parametrizado',
        status400: 'Datos no válidos',
        status401: 'Usuario no autorizado para esta acción',
        status403: 'Usuario no autorizado para esta acción. Verifica con el administrador de la tienda',
        status404: 'No pudimos encontrar lo que buscabas, inténtalo de nuevo en un instante.',
        status405: 'El método HTTP no es compatible',
        status407: 'Credencial de autenticación por proxy server no válida',
        status408: 'El servidor cerró la conexión por desuso',
        status409: 'Conflicto con el recurso que está en el servidor',
        status411: 'Content-Length en el servidor no está definido',
        status412: 'Acceso negado al recurso de destino',
        status413: 'La carga de la solicitud es mayor que los límites establecidos por el servidor',
        status414: 'URI mayor que lo que el servidor acepta interpretar',
        status415: 'Servidor se niega a aceptar este formato de payload (carga)',
        status416: 'Servidor no puede atender al Header Range solicitado',
        status417: 'Expectativa enviada en el encabezado de la solicitud Expect no fue satisfecha',
        status500: '¡Ups! Cockpit está en mantenimiento y nuestro equipo ya está trabajando para resolverlo, quédate tranquilo que volvemos en breve..',
        status501: 'Acción no disponible, intenta de nuevo en instantes.',
        status502: 'Sistema no disponible, por favor contáctenos.',
        status503: 'Sistema no disponible, intenta de nuevo en instantes.',
        status504: 'Acción no disponible, intenta de nuevo.',
        status505: 'La versión HTTP no es compatible con el servidor',
        status507: 'El servidor no admite el almacenamiento de este dato',
        status508: 'El servidor finalizó la operación porque encontró un bucle infinito'
    },
    menu: {
        default: [{
            name: 'Inicio',
            icon: '/product-inicio.svg',
            url: '/',
            order: 1
        }, {
            name: 'CRM',
            icon: '/product-crm.svg',
            sub: [{
                name: 'Panel',
                url: '/crm/dashboard'
            }, {
                name: 'Gestionar leads',
                url: '/crm/manage-leads'
            }, {
                name: 'Mis actividades',
                url: '/crm/schedule'
            }, {
                name: 'Informes',
                url: '/crm/reports'
            }, {
                name: 'Configuraciones',
                url: '/crm/configuration'
            }, {
                name: 'Integraciones',
                url: '/crm/integration'
            }],
            order: 1
        }, {
            name: 'Inventario',
            icon: '/product-estoque.svg',
            sub: [{
                name: 'Consultar vehículo',
                url: '/inventory'
            }, {
                name: 'Incluir vehículo',
                url: '/inventory/list-vehicle'
            }, {
                name: 'Vender vehículo',
                url: '/inventory/channels'
            }],
            order: 1
        }, {
            name: 'Usuarios',
            url: '/user/listing',
            order: 2
        }, {
            name: 'Dudas Frecuentes',
            url: 'https://webmotors.zendesk.com/hc/pt-br/categories/360002515092-Lojas-Concession%C3%A1rias',
            order: 2
        }],
        home: 'Inicio',
        crm: 'CRM',
        crmPanel: 'Panel',
        crmLeads: 'Gestionar leads',
        crmActivities: 'Mis actividades',
        crmReports: 'Informes',
        crmConfig: 'Configuraciones',
        crmIntegration: 'Integraciones',
        gearbox: 'Gearbox',
        gearboxPanel: 'Panel',
        gearboxSite: 'Sitio web',
        gearboxMedia: 'Medios',
        stock: 'Stock',
        stockConsultVehicle: 'Consultar vehículo',
        stockIncludeVehicle: 'Incluir vehículo',
        stockSellVehicle: 'Vender vehículo',
        fidelity: '+ Fidelidad',
        university: 'Universidad',
        autoguru: 'Autogurú',
        store: 'Store',
        account: 'Cuenta',
        accountContractAdhesion: 'Acuerdo de Adhesión',
        accountUsers: 'Usuarios',
        accountMyPlan: 'Mi Plan',
        frequentsDoubts: 'Dudas Frecuentes'
    },
    login: {
        title: 'Toma el control de tu negocio',
        subTitle: 'Conoce todas las soluciones Cockpit y Santander Financiamentos integradas en un único lugar para que vendas mucho más.',
        recoverPass: 'Redefinir contraseña',
        loginButton: 'Acceder al Cockpit',
        secondTitle: 'Tenemos soluciones que te ayudan en el día a día',
        productText1: 'Para vender más y ganar más.',
        productText2: 'Para salir a la delantera y definir el futuro del mercado.',
        productText3: 'Para tener todos los recursos financieros que necesitas.',
        productText4: 'Para comprar con más ventajas.',
        playVideo: 'Ver el video',
        thirdTitle: 'Y también puedes hacer la gestión de tu tienda desde la palma de tu mano',
        thirdSubTitle: 'Gestiona a tus leads y tu stock desde cualquier lugar y a cualquier hora.',
        joinText1: '¿Te interesó?',
        joinText2: 'Entonces, no pierdas esta oportunidad',
        joinText3: 'Toma el control de tu negocio.',
        joinNow: 'Accede ya',
        loginFrom: 'Estás iniciando sesión a través del',
        redirectIn: 'Serás redirigido en',
        seconds: ' segundos',
        password: 'Contraseña',
        email: 'Email',
        keepLogged: 'Quédate conectado',
        urlAppApple: 'https://apps.apple.com/pe/app/revendedor-webmotors/id1069917512',
        urlAppAndroid: 'https://play.google.com/store/apps/details?id=pe.com.webmotors.webmotorsrevendedor.production',
        brandFidelity: '/assets/img/brands/fidelidad.svg',
        brandStock: '/assets/img/brands/inventario.svg',
        brandUniversity: '/assets/img/brands/universidad.svg?v=3',
        brandHotdeals: '/assets/img/brands/hotdeals.svg?v=3'
    },
    header: {
        myProfile: 'Mi perfil',
        registrationData: 'Datos de registro',
        exit: 'Salir',
        allStores: 'Todas las tiendas',
        selectStore: 'Seleccionar tienda',
        errorList: 'Error de listado',
        searchStore: 'Buscar por tienda',
        select: 'Seleccionar',
        login: 'Login',
        solutions: 'Soluciones',
        crm: 'CRM',
        stock: 'Stock',
        autoguru: 'Autogurú',
        university: 'Universidad',
        fairgrounds: 'Hotdeals',
        menuSolutions: [{
            href: '/solucoes/crm',
            name: 'CRM'
        }, {
            href: '/solucoes/estoque',
            name: 'Inventario'
        }],
        notification: null
    },
    footer: {
        copyrights: 'Webmotors S.A. Todos los derechos reservados',
        downloadOurApps: 'Descarga nuestra app:'
    },
    form: {
        buttonWait: "Espera..."
    }
};

exports.default = translate;