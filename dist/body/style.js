'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleUnlogged = exports.StyleLogged = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    padding: 32px;\n'], ['\n    padding: 32px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    padding: 63px 0 0;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    flex: 1;\n'], ['\n    padding: 63px 0 0;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    flex: 1;\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleLogged = exports.StyleLogged = _styledComponents2.default.div(_templateObject);

var StyleUnlogged = exports.StyleUnlogged = _styledComponents2.default.div(_templateObject2);