'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/body/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children,
        _ref$logged = _ref.logged,
        logged = _ref$logged === undefined ? true : _ref$logged;
    return logged ? _react2.default.createElement(
        _style.StyleLogged,
        { role: 'main' },
        children
    ) : _react2.default.createElement(
        _style.StyleUnlogged,
        { role: 'main' },
        children
    );
};