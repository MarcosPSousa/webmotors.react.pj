'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/webinput/radio/style');

var _utils = require('webmotors-react-pj/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _react.forwardRef)(function (props, ref) {
    var checked = props.checked,
        placeholder = props.placeholder,
        onChange = props.onChange;

    var _useState = (0, _react.useState)(checked),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        itemChecked = _useState2[0],
        setItemChecked = _useState2[1];

    var propsCustom = (0, _utils.customAttributes)(props, 'onChange|type|checked');

    var handleChange = function handleChange(e) {
        setItemChecked(function (prevState) {
            return !prevState;
        });
        onChange && onChange(e);
    };

    return _react2.default.createElement(
        _style.StyleLabel,
        null,
        _react2.default.createElement(_style.StyleInput, (0, _extends3.default)({
            ref: ref
        }, propsCustom, {
            type: 'radio',
            defaultChecked: itemChecked,
            onChange: function onChange(e) {
                return handleChange(e);
            }
        })),
        _react2.default.createElement(
            _style.StyleSquare,
            null,
            _react2.default.createElement(
                'svg',
                { xmlns: 'http://www.w3.org/2000/svg', x: '0px', y: '0px', viewBox: '0 0 18 18' },
                _react2.default.createElement('path', { d: 'M12.7,5.3l-5.3,5.3L5.3,8.5c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l2.8,2.8C6.9,12.9,7.2,13,7.4,13s0.5-0.1,0.7-0.3l6-6 c0.4-0.4,0.4-1,0-1.4C13.7,4.9,13.1,4.9,12.7,5.3z' })
            )
        ),
        _react2.default.createElement(
            _style.StylePlaceholder,
            null,
            placeholder
        )
    );
});