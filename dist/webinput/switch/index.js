'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/webinput/switch/style');

var _customAttributes = require('webmotors-react-pj/webinput/custom-attributes');

var _customAttributes2 = _interopRequireDefault(_customAttributes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _react.forwardRef)(function (props, ref) {
    var _props$id = props.id,
        id = _props$id === undefined ? 'webinput_switch_' + Math.floor(Math.random() * 100000) : _props$id,
        name = props.name,
        value = props.value,
        checked = props.checked,
        onChange = props.onChange,
        placeholder = props.placeholder;

    var _useState = (0, _react.useState)(checked),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        itemChecked = _useState2[0],
        setItemChecked = _useState2[1];

    var input = ref || (0, _react.useRef)();
    var propsCustom = (0, _customAttributes2.default)(props, 'onChange|checked');

    var handleChange = function handleChange(e) {
        setItemChecked(function (prevState) {
            return !prevState;
        });
        onChange && onChange(e);
    };

    (0, _react.useEffect)(function () {
        setItemChecked(checked);
        input.current.checked = checked;
    }, [checked]);

    return _react2.default.createElement(
        _style.StyleLabel,
        null,
        _react2.default.createElement(_style.StyleInput, (0, _extends3.default)({
            ref: input
        }, propsCustom, {
            type: 'checkbox',
            defaultChecked: itemChecked,
            id: id,
            onChange: function onChange(e) {
                return handleChange(e);
            }
        })),
        _react2.default.createElement(_style.StyleToggle, null),
        _react2.default.createElement(
            _style.StylePlaceholder,
            null,
            placeholder
        )
    );
});