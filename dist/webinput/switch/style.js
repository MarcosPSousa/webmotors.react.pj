'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StylePlaceholder = exports.StyleInput = exports.StyleToggle = exports.StyleLabel = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: inline-flex;\n    align-items: center;\n    position: relative;\n    margin-top: 2px;\n'], ['\n    display: inline-flex;\n    align-items: center;\n    position: relative;\n    margin-top: 2px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    width: 48px;\n    height: 20px;\n    border-radius: 20px;\n    background-color: ', ';\n    position: relative;\n    &:before{\n        content: \'\';\n        width: 24px;\n        height: 24px;\n        position: absolute;\n        left: 0;\n        top: 50%;\n        margin-top: -12px;\n        background-color: ', ';\n        transform: translateX(0px);\n        border-radius: 100%;\n        transition: background-color 0.3s, transform 0.3s;\n        will-change: background-color, transform;\n    }\n'], ['\n    width: 48px;\n    height: 20px;\n    border-radius: 20px;\n    background-color: ', ';\n    position: relative;\n    &:before{\n        content: \'\';\n        width: 24px;\n        height: 24px;\n        position: absolute;\n        left: 0;\n        top: 50%;\n        margin-top: -12px;\n        background-color: ', ';\n        transform: translateX(0px);\n        border-radius: 100%;\n        transition: background-color 0.3s, transform 0.3s;\n        will-change: background-color, transform;\n    }\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    left: 0;\n    top: 0;\n    opacity: 0;\n    &:checked{\n        + div {\n            background-color: ', ';\n            transition: background-color 0.3s;\n            &:before{\n                transform: translateX(24px);\n                height: 24px;\n                background-color: ', ';\n            }\n        }\n    }\n'], ['\n    position: absolute;\n    left: 0;\n    top: 0;\n    opacity: 0;\n    &:checked{\n        + div {\n            background-color: ', ';\n            transition: background-color 0.3s;\n            &:before{\n                transform: translateX(24px);\n                height: 24px;\n                background-color: ', ';\n            }\n        }\n    }\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.6rem;\n    padding-left: 16px;\n'], ['\n    font-size: 1.6rem;\n    padding-left: 16px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleLabel = exports.StyleLabel = _styledComponents2.default.label(_templateObject);

var StyleToggle = exports.StyleToggle = _styledComponents2.default.div(_templateObject2, (0, _color2.default)('gray-3'), (0, _color2.default)('gray-2'));

var StyleInput = exports.StyleInput = _styledComponents2.default.input(_templateObject3, (0, _color2.default)('primary-3'), (0, _color2.default)('primary'));

var StylePlaceholder = exports.StylePlaceholder = _styledComponents2.default.div(_templateObject4);