'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StylePlaceholder = exports.StyleSquare = exports.StyleInput = exports.StyleLabel = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    align-items: center;\n    position: relative;\n    padding-right: 24px;\n'], ['\n    display: flex;\n    align-items: center;\n    position: relative;\n    padding-right: 24px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    left: 0;\n    top: 0;\n    opacity: 0;\n    &:checked{\n        + div{\n            border-color: transparent;\n            background-color: ', ';\n            svg{\n                fill: ', ';\n            }\n        }\n    }\n'], ['\n    position: absolute;\n    left: 0;\n    top: 0;\n    opacity: 0;\n    &:checked{\n        + div{\n            border-color: transparent;\n            background-color: ', ';\n            svg{\n                fill: ', ';\n            }\n        }\n    }\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    width: 18px;\n    height: 18px;\n    border: 2px solid ', ';\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    border-radius: 4px;\n    transition: border-color 0.3s, background-color 0.3s;\n    svg{\n        flex: 1;\n        fill: transparent;\n        transition: fill 0.3s;\n    }\n'], ['\n    width: 18px;\n    height: 18px;\n    border: 2px solid ', ';\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    border-radius: 4px;\n    transition: border-color 0.3s, background-color 0.3s;\n    svg{\n        flex: 1;\n        fill: transparent;\n        transition: fill 0.3s;\n    }\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    padding-left: 12px;\n    font-size: 1.6rem;\n    color: ', ';\n    font-weight: 500;\n'], ['\n    padding-left: 12px;\n    font-size: 1.6rem;\n    color: ', ';\n    font-weight: 500;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleLabel = exports.StyleLabel = _styledComponents2.default.label(_templateObject);
var StyleInput = exports.StyleInput = _styledComponents2.default.input(_templateObject2, (0, _color2.default)('primary'), (0, _color2.default)('white'));
var StyleSquare = exports.StyleSquare = _styledComponents2.default.div(_templateObject3, (0, _color2.default)('gray'));
var StylePlaceholder = exports.StylePlaceholder = _styledComponents2.default.div(_templateObject4, (0, _color2.default)('gray'));