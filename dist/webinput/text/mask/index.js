'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var getMask = exports.getMask = function getMask(type) {
    return {
        default: undefined,
        tel: '(99) 9999-99999',
        date: '99/99/9999',
        'date-short': '99/9999',
        time: '99:99',
        cep: '99999-999',
        plate: '',
        'new-plate': '',
        cpf: '999.999.999-99',
        cnpj: '99.999.999/9999-99',
        renavam: '99999999999'
    }[type || 'default'];
};

var setMask = exports.setMask = function setMask(_ref) {
    var input = _ref.input,
        mask = _ref.mask,
        initialValue = _ref.initialValue,
        _ref$minSize = _ref.minSize,
        minSize = _ref$minSize === undefined ? false : _ref$minSize,
        _ref$reverse = _ref.reverse,
        reverse = _ref$reverse === undefined ? false : _ref$reverse,
        _ref$letters = _ref.letters,
        letters = _ref$letters === undefined ? false : _ref$letters,
        _ref$after = _ref.after,
        after = _ref$after === undefined ? null : _ref$after;

    var event = function event() {
        if (window.navigator.msPointerEnabled) {
            var eventInput = document.createEvent('Event');
            eventInput.initEvent('input', false, true);
            input.dispatchEvent(eventInput);
        } else {
            input.dispatchEvent(new Event('input'));
        }
    };
    var formatAfter = function formatAfter() {
        (function (e) {
            return after(e);
        });
    };
    var format = function format() {
        var valueCount = 0;
        var valueArray = input.value.replace(/\D/g, '').split('');
        var valueFinal = [];
        var maskArray = mask.split('');
        if (reverse) {
            var maskArrayReverse = maskArray;
            var valueArrayReverse = valueArray;
            maskArrayReverse.reverse();
            valueArrayReverse.reverse();
            maskArrayReverse.forEach(function (item, i) {
                if (valueArrayReverse[i - valueCount]) {
                    if (/\D/.test(item)) {
                        valueFinal.push(item);
                        valueCount++;
                    } else {
                        valueFinal.push(valueArrayReverse[i - valueCount]);
                    }
                }
            });
            valueFinal.reverse();
        } else {
            maskArray.forEach(function (item) {
                if (valueCount < valueArray.length) {
                    if (/\D/.test(item)) {
                        valueFinal.push(item);
                    } else {
                        valueFinal.push(valueArray[valueCount]);
                        valueCount++;
                    }
                }
            });
        }
        input.value = valueFinal.join('');
    };

    var init = function init() {
        input.addEventListener('input', function () {
            return format();
        });
        var _value = input.value.trim();
        if (_value !== '' || initialValue) {
            input.value = input.value || input.dataset.maskfyValue || initialValue;
        }
        event();
    };

    if (!input) {
        return console.error(new Error('Maskfy: Insert tag selector required. Ex.: Maskfy({tag: "[data-maskfy]"})'));
    }

    init();
};