'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _img = require('webmotors-react-pj/img');

var _img2 = _interopRequireDefault(_img);

var _style = require('webmotors-react-pj/webinput/text/style');

var _validation = require('webmotors-react-pj/webinput/text/validation');

var _validation2 = _interopRequireDefault(_validation);

var _mask = require('webmotors-react-pj/webinput/text/mask');

var _customAttributes = require('webmotors-react-pj/webinput/custom-attributes');

var _customAttributes2 = _interopRequireDefault(_customAttributes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _react.forwardRef)(function (props, ref) {
    var _props$type = props.type,
        type = _props$type === undefined ? 'text' : _props$type,
        _props$description = props.description,
        description = _props$description === undefined ? '' : _props$description,
        _props$placeholder = props.placeholder,
        placeholder = _props$placeholder === undefined ? '' : _props$placeholder,
        onInput = props.onInput,
        onFocus = props.onFocus,
        onBlur = props.onBlur,
        messageError = props.messageError,
        messageSuccess = props.messageSuccess,
        _props$value = props.value,
        valueInput = _props$value === undefined ? '' : _props$value,
        required = props.required,
        iconLeft = props.iconLeft,
        iconLeftEvent = props.iconLeftEvent,
        iconRight = props.iconRight,
        iconRightEvent = props.iconRightEvent,
        equal = props.equal,
        min = props.min,
        max = props.max,
        maxLength = props.maxLength,
        minLength = props.minLength,
        innerRef = props.innerRef;


    var inputRef = ref || (0, _react.useRef)(innerRef);
    var iconPasswordToggle = '/assets/img/icons/eye.svg';
    var inputIsPassword = type === 'password' || type === 'password-weakened';

    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        active = _useState2[0],
        setActive = _useState2[1];

    var _useState3 = (0, _react.useState)(false),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        focus = _useState4[0],
        setFocus = _useState4[1];

    var _useState5 = (0, _react.useState)(false),
        _useState6 = (0, _slicedToArray3.default)(_useState5, 2),
        error = _useState6[0],
        setError = _useState6[1];

    var _useState7 = (0, _react.useState)(valueInput),
        _useState8 = (0, _slicedToArray3.default)(_useState7, 2),
        value = _useState8[0],
        setValue = _useState8[1];

    var _useState9 = (0, _react.useState)(true),
        _useState10 = (0, _slicedToArray3.default)(_useState9, 2),
        readonly = _useState10[0],
        setReadonly = _useState10[1];

    var _useState11 = (0, _react.useState)(iconLeft),
        _useState12 = (0, _slicedToArray3.default)(_useState11, 2),
        iconSrcLeft = _useState12[0],
        setIconSrcLeft = _useState12[1];

    var _useState13 = (0, _react.useState)(inputIsPassword ? iconPasswordToggle : iconRight),
        _useState14 = (0, _slicedToArray3.default)(_useState13, 2),
        iconSrcRight = _useState14[0],
        setIconSrcRight = _useState14[1];

    var _useState15 = (0, _react.useState)(description),
        _useState16 = (0, _slicedToArray3.default)(_useState15, 2),
        descript = _useState16[0],
        setDescript = _useState16[1];

    var _useState17 = (0, _react.useState)(inputIsPassword ? 'password' : 'text'),
        _useState18 = (0, _slicedToArray3.default)(_useState17, 2),
        inputType = _useState18[0],
        setInputType = _useState18[1];

    var propsCustom = (0, _customAttributes2.default)(props, 'onBlur|onFocus|onInput|placeholder|value|required');

    var handleMask = function handleMask() {
        var typeMask = (0, _mask.getMask)(type);
        if (typeMask) {
            (0, _mask.setMask)({
                input: inputRef.current,
                mask: typeMask,
                initialValue: value
            });
        }
    };

    var handleFormat = function handleFormat() {
        if (type === 'email') {
            inputRef.current.value = value.toLowerCase();
        }
        if (value.length > maxLength) {
            inputRef.current.value = value.substring(0, maxLength);
        }
    };

    var handleInput = function handleInput(e) {
        var value = e.target.value;

        var valueFormat = void 0;
        if (type === 'email') {
            var input = inputRef.current;
            var _value = input.value;

            input.value = _value.toLowerCase();
        } else {}
        setValue(e.target.value);
        onInput && onInput(e);
    };

    var handleFocus = function handleFocus(e) {
        setActive(true);
        setFocus(true);
        setReadonly(false);
        onFocus && onFocus(e);
    };

    var handleBlur = function handleBlur(e) {
        e.persist();
        var valueEmpty = value.length === 0;
        setActive(!valueEmpty);
        setFocus(false);
        (0, _validation2.default)({
            value: value,
            type: type,
            min: min,
            max: max,
            required: required,
            equal: equal
        }).then(function (f) {
            var success = f.success,
                data = f.data;

            var dataError = !success && (messageError || data);
            var dataSuccess = success && (messageSuccess || description);
            renderIconRightType(success);
            setError(!success);
            setDescript(!success ? dataError : dataSuccess);
            onBlur && onBlur(e);
        });
    };

    var handleClickIconLeft = function handleClickIconLeft() {
        iconLeftEvent && iconLeftEvent();
    };

    var handleClickIconRight = function handleClickIconRight() {
        setInputType(function (prevType) {
            if (inputIsPassword) {
                return prevType === 'password' ? 'text' : 'password';
            }
            return 'text';
        });
        iconRightEvent && iconRightEvent();
    };

    var renderIconError = function renderIconError(success) {
        setIconSrcRight(!success ? '/assets/img/icons/alert-2.svg' : iconRight);
    };

    var renderIconRightType = function renderIconRightType(success) {
        inputIsPassword ? setIconSrcRight(iconPasswordToggle) : renderIconError(success);
    };

    var renderIconLeft = function renderIconLeft() {
        return iconSrcLeft && _react2.default.createElement(_img2.default, { className: 'icon icon-left', src: iconSrcLeft, onClick: function onClick() {
                return handleClickIconLeft();
            } });
    };

    var renderIconRight = function renderIconRight() {
        return iconSrcRight && _react2.default.createElement(_img2.default, { className: 'icon icon-right', src: iconSrcRight, onClick: function onClick() {
                return handleClickIconRight();
            } });
    };

    var handleActive = function handleActive() {
        if (value && value.length !== 0) {
            setActive(true);
        }
    };

    (0, _react.useEffect)(function () {
        handleActive();
        setTimeout(function () {
            handleMask();
        }, 100);
    }, []);

    (0, _react.useEffect)(function () {
        handleActive();
        handleFormat();
        setTimeout(function () {
            handleMask();
        }, 100);
    }, [value]);

    (0, _react.useEffect)(function () {
        setDescript(messageError);
        setError(messageError && messageError.length);
    }, [messageError]);

    (0, _react.useEffect)(function () {
        setDescript(messageSuccess);
    }, [messageSuccess]);

    return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
            _style.StyleLabel,
            { active: active, focus: focus, error: error, 'data-input-error': error },
            renderIconLeft(),
            _react2.default.createElement(
                _style.StylePlaceholder,
                { active: active, focus: focus, error: error, iconLeft: iconSrcLeft },
                placeholder
            ),
            _react2.default.createElement(_style.StyleInput, (0, _extends3.default)({}, propsCustom, {
                error: error,
                active: active,
                type: inputType,
                defaultValue: value,
                ref: inputRef,
                readOnly: readonly,
                onInput: function onInput(e) {
                    return handleInput(e);
                },
                onFocus: function onFocus(e) {
                    return handleFocus(e);
                },
                onBlur: function onBlur(e) {
                    return handleBlur(e);
                }
            })),
            renderIconRight()
        ),
        _react2.default.createElement(
            _style.StyleDescript,
            null,
            descript
        )
    );
});