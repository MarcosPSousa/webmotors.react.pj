'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _entries = require('babel-runtime/core-js/object/entries');

var _entries2 = _interopRequireDefault(_entries);

var _rules = require('webmotors-react-pj/webinput/text/validation/rules');

var _rules2 = _interopRequireDefault(_rules);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var _ref$type = _ref.type,
        type = _ref$type === undefined ? 'default' : _ref$type,
        min = _ref.min,
        max = _ref.max,
        required = _ref.required,
        value = _ref.value,
        equal = _ref.equal;

    var rulesApply = [];
    var validationByAttributes = { required: required, equal: equal, min: min, max: max };
    var rulesType = (0, _rules2.default)({
        type: type,
        min: min,
        max: max,
        value: value,
        equal: equal
    });

    (0, _entries2.default)(validationByAttributes).forEach(function (item) {
        var _item = (0, _slicedToArray3.default)(item, 2),
            key = _item[0],
            rule = _item[1];

        if (rule) {
            rulesApply.push(key);
        }
    });

    if (rulesType[type]) {
        rulesApply.push(type);
    }

    var validationErrors = rulesApply.filter(function (item) {
        return rulesType[item].fn();
    })[0];
    var validationRule = rulesType[validationErrors];
    var validationRequired = validationErrors === 'required';
    var validationShowFn = validationRule && validationRule.fn();
    var validationShow = (!validationRequired && value.length >= 1 || required && validationShowFn) && validationShowFn;
    return _promise2.default.resolve({
        data: validationShow ? validationRule.message : '',
        success: !Boolean(validationShow)
    });
};