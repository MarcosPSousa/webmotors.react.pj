'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (_ref) {
    var type = _ref.type,
        min = _ref.min,
        max = _ref.max,
        value = _ref.value,
        equal = _ref.equal;
    return {
        required: {
            message: 'Preencha o campo obrigatório',
            fn: function fn() {
                return value.trim().length === 0;
            }
        },
        email: {
            message: 'Digite um e-mail válido',
            fn: function fn() {
                return !/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
            }
        },
        'email-login': {
            message: 'Digite um e-mail válido',
            fn: function fn() {
                return !/^([a-zA-Z0-9_\-.]+)@([a-zA-Z-0-9\-.])+\.[a-z]{1,10}(\.[a-z]{1,3})?$/.test(value);
            }
        },
        tel: {
            message: 'Digite um telefone válido',
            fn: function fn() {
                return !/^\(?\d{2,3}\)?\s?\w{4,5}-?\w{4,5}$/.test(value);
            }
        },
        date: {
            message: 'Digite uma data válida',
            fn: function fn() {
                return !/^\w{2}\/\w{2}\/\w{4}$/.test(value);
            }
        },
        'date-short': {
            message: 'Digite uma data simplificada válida',
            fn: function fn() {
                return !/^\d{2}\/\d{2,4}$/.test(value);
            }
        },
        password: {
            message: 'Digite todos critérios de senha válidos',
            fn: function fn() {
                return !/(?=^.{8,}$)((?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*\d{1,})(?=.*\W{1,}))^.*$/.test(value);
            }
        },
        'password-weakened': {
            message: 'Digite todos critérios de senha válidos',
            fn: function fn() {
                return !/.{1,}$/.test(value);
            }
        },
        url: {
            message: 'Digite uma url válida (ex.: https://www.webmotors.com.br)',
            fn: function fn() {
                return !/^https?:\/\/(\w|-)+\.(\w|-)+((\.|\/)[a-z]+(\.[a-z]+(\/\w+)?)?)?.+$/.test(value);
            }
        },
        time: {
            message: 'Digite um horário válido',
            fn: function fn() {
                return !/^[0-2]\d:[0-5]\d$/.test(value);
            }
        },
        equal: {
            message: 'Digite um valor igual ao campo "' + (document.querySelector(equal) && document.querySelector(equal).previousElementSibling.textContent) + '"',
            fn: function fn() {
                var inputEqual = document.querySelector(equal);
                if (inputEqual) {
                    return inputEqual.value !== value;
                }
                return false;
            }
        },
        min: {
            message: 'Digite um n\xFAmero maior que ' + min,
            fn: function fn() {
                return min > Number(value);
            }
        },
        max: {
            message: 'Digite um n\xFAmero menor que ' + max,
            fn: function fn() {
                return max < Number(value);
            }
        },
        'min-max': {
            message: 'Digite um n\xFAmero entre ' + min + ' e ' + max,
            fn: function fn() {
                return min > Number(value) && max < Number(value);
            }
        },
        'name-surname': {
            message: 'Digite um nome e sobrenome válido',
            fn: function fn() {
                return !/^\D{2,}\s\D{2,}$/.test(value);
            }
        },
        cep: {
            message: 'Digite um CEP válido',
            fn: function fn() {
                return !/^\w{5}-\w{3}$/.test(value);
            }
        },
        plate: {
            message: 'Digite uma placa de veículo válida',
            fn: function fn() {
                return !/^\[A-Z]{3}-\d{4}$/.test(value) || !/^\[A-Z]{3}\d[A-J]\d{2}$/.test(value);
            }
        },
        cpf: {
            message: 'Digite um CPF válido',
            fn: function fn() {
                var cpf = value.replace(/[^\d]+/g, '');
                var add = void 0;
                var rev = void 0;
                var i = void 0;
                if (cpf === '') {
                    return true;
                }
                if (/0{11}|1{11}|2{11}|3{11}|4{11}|5{11}|6{11}|7{11}|8{11}|9{11}/.test(cpf)) {
                    return true;
                }

                add = 0;
                for (i = 0; i < 9; i++) {
                    add += parseInt(cpf.charAt(i), 10) * (10 - i);
                }
                rev = 11 - add % 11;
                if (rev === 10 || rev === 11) {
                    rev = 0;
                }
                if (rev !== parseInt(cpf.charAt(9), 10)) {
                    return true;
                }

                add = 0;
                for (i = 0; i < 10; i++) {
                    add += parseInt(cpf.charAt(i), 10) * (11 - i);
                }
                rev = 11 - add % 11;
                if (rev === 10 || rev === 11) {
                    rev = 0;
                }
                if (rev !== parseInt(cpf.charAt(10), 10)) {
                    return true;
                }
                return false;
            }
        },
        cnpj: {
            message: 'Digite um CNPJ válido',
            fn: function fn() {
                var cnpj = value.replace(/\D/g, '');
                var i = void 0;
                var tamanho = void 0;
                var numeros = void 0;
                var soma = void 0;
                var pos = void 0;
                var resultado = void 0;
                if (cnpj === '' || cnpj.length !== 14 || /0{14}|1{14}|2{14}|3{14}|4{14}|5{14}|6{14}|7{14}|8{14}|9{14}/.test(cnpj)) {
                    return true;
                }

                tamanho = cnpj.length - 2;
                numeros = cnpj.substring(0, tamanho);
                var digitos = cnpj.substring(tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2) {
                        pos = 9;
                    }
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado !== parseInt(digitos.charAt(0), 10)) {
                    return true;
                }
                numeros = cnpj.substring(0, ++tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2) {
                        pos = 9;
                    }
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado !== parseInt(digitos.charAt(1), 10)) {
                    return true;
                }
                return false;
            }
        },
        renavam: {
            message: 'Digite um renavam válido',
            fn: function fn() {
                var d = value.split('');
                var soma = 0;
                var valor = 0;
                var digito = 0;
                var x = 0;
                for (var i = 5; i >= 2; i--) {
                    soma += d[x] * i;
                    x++;
                }
                valor = soma % 11;
                if (valor === 11 || valor === 0 || valor >= 10) {
                    digito = 0;
                } else {
                    digito = valor;
                }
                return digito === d[4];
            }
        },
        default: {
            message: '',
            fn: function fn() {
                console.log('Input não validado', type);
                return false;
            }
        }
    };
};