'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleDescript = exports.StyleInput = exports.StylePlaceholder = exports.StyleLabel = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: relative;\n    display: block;\n    border: 2px solid;\n    display: flex;\n    align-items: center;\n    border-color: ', ';\n    border-radius: 8px;\n    background-color: ', ';\n    transition: border-color 0.3s;\n    &.input-lock{\n        opacity: 0.6;\n        pointer-events: none;\n    }\n    > [data-src]{\n        width: 24px;\n        height: 24px;\n        fill: ', ';\n        position: relative;\n        .icon{\n            width: 100%;\n            height: 100%;\n        }\n        &:nth-child(1){\n            margin-left: 16px;\n        }\n        &:last-child{\n            margin-right: 8px;\n        }\n    }\n'], ['\n    position: relative;\n    display: block;\n    border: 2px solid;\n    display: flex;\n    align-items: center;\n    border-color: ', ';\n    border-radius: 8px;\n    background-color: ', ';\n    transition: border-color 0.3s;\n    &.input-lock{\n        opacity: 0.6;\n        pointer-events: none;\n    }\n    > [data-src]{\n        width: 24px;\n        height: 24px;\n        fill: ', ';\n        position: relative;\n        .icon{\n            width: 100%;\n            height: 100%;\n        }\n        &:nth-child(1){\n            margin-left: 16px;\n        }\n        &:last-child{\n            margin-right: 8px;\n        }\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    left: 0;\n    top: ', ';\n    transform: ', ';\n    margin-top: ', ';\n    padding: 0 12px;\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    background-color: ', ';\n    color: ', '\n    transition: top 0.3s, margin-top 0.3s, transform 0.3s, color 0.3s;\n    will-change: top, margin-top, transform;\n    cursor: text;\n'], ['\n    position: absolute;\n    left: 0;\n    top: ', ';\n    transform: ', ';\n    margin-top: ', ';\n    padding: 0 12px;\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    background-color: ', ';\n    color: ', '\n    transition: top 0.3s, margin-top 0.3s, transform 0.3s, color 0.3s;\n    will-change: top, margin-top, transform;\n    cursor: text;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 1.2rem;\n    font-weight: 500;\n    line-height: 1.5em;\n    padding: 10px 16px 8px;\n    border-style: none;\n    outline-style: none;\n    background-color: transparent;\n    width: 100%;\n    color: ', '\n    transition: color 0.3s, width 0.3s;\n'], ['\n    font-size: 1.2rem;\n    font-weight: 500;\n    line-height: 1.5em;\n    padding: 10px 16px 8px;\n    border-style: none;\n    outline-style: none;\n    background-color: transparent;\n    width: 100%;\n    color: ', '\n    transition: color 0.3s, width 0.3s;\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    min-height: 20px;\n    font-size: 1rem;\n    line-height: 1.5em;\n    margin-bottom: 12px;\n    font-weight: 500;\n    padding-top: 4px;\n'], ['\n    min-height: 20px;\n    font-size: 1rem;\n    line-height: 1.5em;\n    margin-bottom: 12px;\n    font-weight: 500;\n    padding-top: 4px;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

var _colors = require('webmotors-react-pj/webinput/text/style/colors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleLabel = exports.StyleLabel = _styledComponents2.default.label(_templateObject, function (props) {
    return (0, _colors.borderColor)(props);
}, (0, _color2.default)('white'), function (props) {
    return (0, _colors.inputColor)(props);
});

var StylePlaceholder = exports.StylePlaceholder = _styledComponents2.default.span(_templateObject2, function (_ref) {
    var active = _ref.active;
    return active ? '0%' : '50%';
}, function (_ref2) {
    var iconLeft = _ref2.iconLeft;
    return iconLeft ? 'translate(41px, -50%)' : 'translate(5px, -50%)';
}, function (_ref3) {
    var active = _ref3.active;
    return active ? '0px' : '1px';
}, (0, _color2.default)('white'), function (props) {
    return (0, _colors.placeholderColor)(props);
});

var StyleInput = exports.StyleInput = _styledComponents2.default.input(_templateObject3, function (props) {
    return (0, _colors.inputColor)(props);
});

var StyleDescript = exports.StyleDescript = _styledComponents2.default.div(_templateObject4);