'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.inputColor = exports.placeholderColor = exports.borderColor = undefined;

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var borderColor = exports.borderColor = function borderColor(_ref) {
    var error = _ref.error,
        focus = _ref.focus;

    if (error) {
        return (0, _color2.default)('error');
    }
    return focus ? (0, _color2.default)('gray') : (0, _color2.default)('gray-4');
};

var placeholderColor = exports.placeholderColor = function placeholderColor(_ref2) {
    var error = _ref2.error,
        focus = _ref2.focus;

    if (error) {
        return (0, _color2.default)('error');
    }
    return focus ? (0, _color2.default)('gray') : (0, _color2.default)('gray-3');
};

var inputColor = exports.inputColor = function inputColor(_ref3) {
    var error = _ref3.error,
        active = _ref3.active;

    if (error) {
        return (0, _color2.default)('error');
    }
    return active ? (0, _color2.default)('gray') : (0, _color2.default)('gray-3');
};