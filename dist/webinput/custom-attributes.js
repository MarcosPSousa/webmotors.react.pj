"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require("babel-runtime/helpers/slicedToArray");

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _entries = require("babel-runtime/core-js/object/entries");

var _entries2 = _interopRequireDefault(_entries);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (props, except) {
    var propsCustom = {};
    (0, _entries2.default)(props).forEach(function (item) {
        var _item = (0, _slicedToArray3.default)(item, 2),
            propKey = _item[0],
            propValue = _item[1];

        var regex = new RegExp("^(?!(" + except + ")).+");
        if (regex.test(propKey)) {
            propsCustom[propKey] = propValue;
        }
    });
    return propsCustom;
};