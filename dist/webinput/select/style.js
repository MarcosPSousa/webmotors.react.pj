'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleOptionsItem = exports.StyleOptionsContent = exports.StyleOptions = exports.StyleItemIcon = exports.StyleItemName = exports.StyleItemCurrent = exports.StylePlaceholder = exports.StyleContent = exports.StyleSelect = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    position: relative;\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    *::-webkit-scrollbar {\n        width: 10px;\n    }\n    *::-webkit-scrollbar-thumb {\n        border-radius: 10px;\n        background-color: ', ';\n        border: 2px solid ', ';\n    }\n    *:hover::-webkit-scrollbar-track {\n        border-radius: 10px;\n        background-color: #ddd;\n    }\n    *:hover::-webkit-scrollbar-thumb {\n        border-color: #ddd;\n    }\n'], ['\n    position: relative;\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    *::-webkit-scrollbar {\n        width: 10px;\n    }\n    *::-webkit-scrollbar-thumb {\n        border-radius: 10px;\n        background-color: ', ';\n        border: 2px solid ', ';\n    }\n    *:hover::-webkit-scrollbar-track {\n        border-radius: 10px;\n        background-color: #ddd;\n    }\n    *:hover::-webkit-scrollbar-thumb {\n        border-color: #ddd;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    opacity: 0;\n    width: 1px;\n    height: 1px;\n'], ['\n    position: absolute;\n    opacity: 0;\n    width: 1px;\n    height: 1px;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    border: 2px solid;\n    padding: 7px 16px 5px;\n    border-color: ', ';\n    border-radius: 8px;\n    background-color: ', ';\n    color: ', ';\n    cursor: default;\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    position: relative;\n    width: ', ';\n    white-space: nowrap;\n'], ['\n    border: 2px solid;\n    padding: 7px 16px 5px;\n    border-color: ', ';\n    border-radius: 8px;\n    background-color: ', ';\n    color: ', ';\n    cursor: default;\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    position: relative;\n    width: ', ';\n    white-space: nowrap;\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    left: 0;\n    top: 50%;\n    transform: translate(5px,-50%);\n    margin-top: 1px;\n    padding: 0 12px;\n    background-color: ', ';\n    color: ', ';\n    transition: top 0.3s, margin-top 0.3s, transform 0.3s,color 0.3s;\n    will-change: top, margin-top, transform;\n'], ['\n    position: absolute;\n    left: 0;\n    top: 50%;\n    transform: translate(5px,-50%);\n    margin-top: 1px;\n    padding: 0 12px;\n    background-color: ', ';\n    color: ', ';\n    transition: top 0.3s, margin-top 0.3s, transform 0.3s,color 0.3s;\n    will-change: top, margin-top, transform;\n']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n    color: ', ';\n    transition: color 0.3s;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    transform: translateY(1px);\n'], ['\n    color: ', ';\n    transition: color 0.3s;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    transform: translateY(1px);\n']),
    _templateObject6 = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n    overflow: hidden;\n    text-overflow: ellipsis;\n'], ['\n    flex: 1;\n    overflow: hidden;\n    text-overflow: ellipsis;\n']),
    _templateObject7 = (0, _taggedTemplateLiteral3.default)(['\n    width: 24px;\n    height: 24px;\n    fill: ', ';\n    transform: translateY(-1px);\n    margin-left: 16px;\n'], ['\n    width: 24px;\n    height: 24px;\n    fill: ', ';\n    transform: translateY(-1px);\n    margin-left: 16px;\n']),
    _templateObject8 = (0, _taggedTemplateLiteral3.default)(['\n    position: absolute;\n    left: -2px;\n    top: -2px;\n    min-width: calc(100% + 4px);\n    background-color: ', ';\n    padding: 12px;\n    border-radius: 0 0 8px 8px;\n    transform-origin: top;\n    transform: ', ';\n    pointer-events: ', ';\n    opacity: ', ';\n    will-change: transform, opacity;\n    transition: transform 0.3s, opacity 0.3s;\n    z-index: 2;\n'], ['\n    position: absolute;\n    left: -2px;\n    top: -2px;\n    min-width: calc(100% + 4px);\n    background-color: ', ';\n    padding: 12px;\n    border-radius: 0 0 8px 8px;\n    transform-origin: top;\n    transform: ', ';\n    pointer-events: ', ';\n    opacity: ', ';\n    will-change: transform, opacity;\n    transition: transform 0.3s, opacity 0.3s;\n    z-index: 2;\n']),
    _templateObject9 = (0, _taggedTemplateLiteral3.default)(['\n    overflow: auto;\n    max-height: ', ';\n'], ['\n    overflow: auto;\n    max-height: ', ';\n']),
    _templateObject10 = (0, _taggedTemplateLiteral3.default)(['\n    color: ', ';\n    border-radius: 8px;\n    padding: 8px 12px;\n    font-weight: 500;\n    transition: background-color 0.3s;\n    &:hover{\n        background-color: ', '\n    }\n'], ['\n    color: ', ';\n    border-radius: 8px;\n    padding: 8px 12px;\n    font-weight: 500;\n    transition: background-color 0.3s;\n    &:hover{\n        background-color: ', '\n    }\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject, (0, _color2.default)('gray-3'), (0, _color2.default)('gray-4'));

var StyleSelect = exports.StyleSelect = _styledComponents2.default.select(_templateObject2);
var StyleContent = exports.StyleContent = _styledComponents2.default.div(_templateObject3, (0, _color2.default)('gray-4'), (0, _color2.default)('white'), (0, _color2.default)('gray-3'), function (_ref) {
    var width = _ref.width;
    return width;
});

var StylePlaceholder = exports.StylePlaceholder = _styledComponents2.default.span(_templateObject4, (0, _color2.default)('white'), (0, _color2.default)('gray-3'));

var StyleItemCurrent = exports.StyleItemCurrent = _styledComponents2.default.div(_templateObject5, (0, _color2.default)('gray-3'));

var StyleItemName = exports.StyleItemName = _styledComponents2.default.div(_templateObject6);

var StyleItemIcon = exports.StyleItemIcon = _styledComponents2.default.svg(_templateObject7, (0, _color2.default)('gray-3'));
var StyleOptions = exports.StyleOptions = _styledComponents2.default.div(_templateObject8, (0, _color2.default)('gray-4'), function (_ref2) {
    var open = _ref2.open;
    return open ? 'rotateX(0deg)' : 'rotateX(-90deg)';
}, function (_ref3) {
    var open = _ref3.open;
    return open ? 'all' : 'none';
}, function (_ref4) {
    var open = _ref4.open;
    return open ? 1 : 0;
});

var StyleOptionsContent = exports.StyleOptionsContent = _styledComponents2.default.div(_templateObject9, function (_ref5) {
    var maxItems = _ref5.maxItems;
    return 2.8 * maxItems + 0.2 + 'em';
});

var StyleOptionsItem = exports.StyleOptionsItem = _styledComponents2.default.div(_templateObject10, (0, _color2.default)('gray'), (0, _color2.default)('white'));