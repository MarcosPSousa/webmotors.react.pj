'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getOwnPropertyDescriptor = require('babel-runtime/core-js/object/get-own-property-descriptor');

var _getOwnPropertyDescriptor2 = _interopRequireDefault(_getOwnPropertyDescriptor);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/webinput/select/style');

var _customAttributes = require('webmotors-react-pj/webinput/custom-attributes');

var _customAttributes2 = _interopRequireDefault(_customAttributes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _react.forwardRef)(function (props, ref) {
    var _props$children = props.children,
        children = _props$children === undefined ? [] : _props$children,
        onChange = props.onChange,
        value = props.value,
        placeholder = props.placeholder,
        _props$maxItems = props.maxItems,
        maxItems = _props$maxItems === undefined ? Math.min(props.children && props.children.length, 5) : _props$maxItems,
        _props$id = props.id,
        id = _props$id === undefined ? 'webinput_select_' + Math.floor(Math.random() * 100000) : _props$id,
        _props$width = props.width,
        width = _props$width === undefined ? '200px' : _props$width;

    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        open = _useState2[0],
        setOpen = _useState2[1];

    var _useState3 = (0, _react.useState)(),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        selectText = _useState4[0],
        setSelectText = _useState4[1];

    var _useState5 = (0, _react.useState)(value),
        _useState6 = (0, _slicedToArray3.default)(_useState5, 2),
        selectValue = _useState6[0],
        setSelectValue = _useState6[1];

    var _useState7 = (0, _react.useState)(children),
        _useState8 = (0, _slicedToArray3.default)(_useState7, 2),
        selectItems = _useState8[0],
        setSelectItems = _useState8[1];

    var selectInput = (0, _react.useRef)() || ref;
    var propsCustom = (0, _customAttributes2.default)(props, 'options|maxItems|value');

    var handleOpen = function handleOpen() {
        setOpen(true);
    };

    var handleChange = function handleChange(e) {
        setSelectValue(e.target.value);
        onChange && onChange(e);
    };

    var handleSelectItem = function handleSelectItem(e) {
        var target = e.target;


        setSelectValue(target.dataset.value || '');
        setSelectText(target.textContent);
        setOpen(false);
    };

    var forceClose = function forceClose() {
        window.addEventListener('click', function (e) {
            if (!e.target.closest('[data-inputselect]')) {
                setOpen(false);
            }
        });
    };

    var renderPlaceholder = function renderPlaceholder() {
        return placeholder && _react2.default.createElement(
            _style.StylePlaceholder,
            null,
            placeholder
        );
    };

    (0, _react.useEffect)(function () {
        forceClose();
        var child = [];
        children.forEach(function (item) {
            if (item && item.props) {
                var optionValue = item.props.value;
                var optionContent = item.props.children;

                child.push(_react2.default.createElement(
                    _style.StyleOptionsItem,
                    {
                        key: optionValue || Math.floor(Math.random() * 100000),
                        'data-value': optionValue,
                        onClick: function onClick(e) {
                            return handleSelectItem(e);
                        } },
                    optionContent
                ));
            }
        });
        var selectedOptions = selectInput.current.selectedOptions;

        var _selectedOptions = (0, _slicedToArray3.default)(selectedOptions, 1),
            selectedOptionsText = _selectedOptions[0];

        setSelectText(selectedOptionsText && selectedOptionsText.textContent);
        setSelectItems(child);
    }, [children]);

    (0, _react.useEffect)(function () {
        setTimeout(function () {
            return setSelectValue(value);
        }, 10);
    }, [value]);

    (0, _react.useEffect)(function () {
        var current = selectInput.current;

        (0, _getOwnPropertyDescriptor2.default)(window.HTMLSelectElement.prototype, 'value').get.call(current);
        var event = new Event('change', { bubbles: true });
        current.dispatchEvent(event);
        setTimeout(function () {
            var selectedOptions = current.selectedOptions;

            var textContent = '';
            if (selectedOptions && selectedOptions[0]) {
                textContent = selectedOptions[0].textContent;
            }
            setSelectText(textContent);
        }, 10);
    }, [selectValue]);

    return _react2.default.createElement(
        _style.StyleWrapper,
        { 'data-inputselect': true },
        _react2.default.createElement(
            _style.StyleSelect,
            (0, _extends3.default)({}, propsCustom, {
                ref: selectInput,
                value: selectValue,
                onChange: function onChange(e) {
                    return handleChange(e);
                }
            }),
            children
        ),
        _react2.default.createElement(
            _style.StyleContent,
            { width: width, 'data-value': selectValue },
            renderPlaceholder(),
            _react2.default.createElement(
                _style.StyleItemCurrent,
                { onClick: function onClick() {
                        return handleOpen();
                    } },
                _react2.default.createElement(
                    _style.StyleItemName,
                    null,
                    selectText
                ),
                _react2.default.createElement(
                    _style.StyleItemIcon,
                    { viewBox: '0 0 24 24', xmlns: 'http://www.w3.org/2000/svg' },
                    _react2.default.createElement('path', { d: 'M21.0004 8.00012C21.0004 8.25612 20.9024 8.51212 20.7074 8.70712L12.7074 16.7071C12.3164 17.0981 11.6844 17.0981 11.2934 16.7071L3.29337 8.70712C2.90237 8.31612 2.90237 7.68412 3.29337 7.29312C3.68437 6.90212 4.31637 6.90212 4.70737 7.29312L12.0004 14.5861L19.2934 7.29312C19.6844 6.90212 20.3164 6.90212 20.7074 7.29312C20.9024 7.48812 21.0004 7.74412 21.0004 8.00012Z' })
                )
            ),
            _react2.default.createElement(
                _style.StyleOptions,
                { open: open },
                _react2.default.createElement(
                    _style.StyleOptionsContent,
                    { maxItems: maxItems },
                    selectItems
                )
            )
        )
    );
});