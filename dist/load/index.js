'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Load = function (_Component) {
    (0, _inherits3.default)(Load, _Component);

    function Load(props) {
        (0, _classCallCheck3.default)(this, Load);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Load.__proto__ || (0, _getPrototypeOf2.default)(Load)).call(this, props));

        _this.state = {};
        return _this;
    }

    (0, _createClass3.default)(Load, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.setState({ className: this.props.className });
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                'div',
                { className: 'cockpit-load ' + this.state.className },
                _react2.default.createElement('img', {
                    className: 'cockpit-load__icon',
                    src: this.props.icon ? this.props.icon : _config.UrlCockpit + '/assets/img/brands/webmotors-2-icon.svg',
                    alt: '',
                    'aria-hidden': 'true'
                }),
                this.props.title && _react2.default.createElement(
                    'div',
                    { className: 'cockpit-load__title' },
                    this.props.title
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'cockpit-load__text' },
                    this.props.text || 'Carregando...'
                )
            );
        }
    }]);
    return Load;
}(_react.Component);

exports.default = Load;