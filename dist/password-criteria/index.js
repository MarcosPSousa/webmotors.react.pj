'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/password-criteria/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var input = _ref.input;

    var _useState = (0, _react.useState)('default'),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        stateLowerCase = _useState2[0],
        setStateLowerCase = _useState2[1];

    var _useState3 = (0, _react.useState)('default'),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        stateUpperCase = _useState4[0],
        setStateUpperCase = _useState4[1];

    var _useState5 = (0, _react.useState)('default'),
        _useState6 = (0, _slicedToArray3.default)(_useState5, 2),
        stateNumber = _useState6[0],
        setStateNumber = _useState6[1];

    var _useState7 = (0, _react.useState)('default'),
        _useState8 = (0, _slicedToArray3.default)(_useState7, 2),
        stateSpecial = _useState8[0],
        setStateSpecial = _useState8[1];

    var _useState9 = (0, _react.useState)('default'),
        _useState10 = (0, _slicedToArray3.default)(_useState9, 2),
        stateMoreThen = _useState10[0],
        setStateMoreThen = _useState10[1];

    var _useState11 = (0, _react.useState)(false),
        _useState12 = (0, _slicedToArray3.default)(_useState11, 2),
        stateChecked = _useState12[0],
        setStateChecked = _useState12[1];

    var handlerInput = function handlerInput() {
        setTimeout(function () {
            var inputPassword = document.querySelector(input);
            var stateChangeSuccess = function stateChangeSuccess(fn, condition) {
                return fn(condition ? 'success' : 'default');
            };
            var stateChangeError = function stateChangeError(fn, condition) {
                return fn(condition ? 'success' : 'error');
            };
            if (inputPassword) {
                inputPassword.addEventListener('input', function (e) {
                    var value = e.target.value;

                    stateChangeSuccess(setStateLowerCase, /[a-z]/g.test(value));
                    stateChangeSuccess(setStateUpperCase, /[A-Z]/g.test(value));
                    stateChangeSuccess(setStateNumber, /\d/g.test(value));
                    stateChangeSuccess(setStateSpecial, /(\W|_)/g.test(value));
                    stateChangeSuccess(setStateMoreThen, value.length >= 8);
                });
                inputPassword.addEventListener('blur', function (e) {
                    var value = e.target.value;

                    stateChangeError(setStateLowerCase, /[a-z]/g.test(value));
                    stateChangeError(setStateUpperCase, /[A-Z]/g.test(value));
                    stateChangeError(setStateNumber, /\d/g.test(value));
                    stateChangeError(setStateSpecial, /(\W|_)/g.test(value));
                    stateChangeError(setStateMoreThen, value.length >= 8);
                });
            } else {
                console.error('[password-criteria] selector ' + input + ' not found');
            }
        }, 10);
    };

    (0, _react.useEffect)(function () {
        return handlerInput();
    }, []);

    (0, _react.useEffect)(function () {
        var stateSuccessComplete = [stateLowerCase, stateUpperCase, stateNumber, stateSpecial, stateMoreThen].includes('default');
        var stateErrorComplete = [stateLowerCase, stateUpperCase, stateNumber, stateSpecial, stateMoreThen].includes('error');

        setStateChecked(!stateSuccessComplete && !stateErrorComplete);
    }, [stateLowerCase, stateUpperCase, stateNumber, stateSpecial, stateMoreThen]);

    return _react2.default.createElement(
        _style.StyleWrapper,
        null,
        _react2.default.createElement(
            _style.StyleItem,
            { status: stateLowerCase },
            'abc'
        ),
        _react2.default.createElement(
            _style.StyleItem,
            { status: stateUpperCase },
            'ABC'
        ),
        _react2.default.createElement(
            _style.StyleItem,
            { status: stateNumber },
            '123'
        ),
        _react2.default.createElement(
            _style.StyleItem,
            { status: stateSpecial },
            '!@#'
        ),
        _react2.default.createElement(
            _style.StyleItem,
            { status: stateMoreThen },
            '8+'
        ),
        _react2.default.createElement(
            _style.StyleCheck,
            { checked: stateChecked },
            _react2.default.createElement(
                'svg',
                { viewBox: '0 0 24 24', xmlns: 'http://www.w3.org/2000/svg' },
                _react2.default.createElement('path', { d: 'M9.2595 15.7403L7.2925 13.7733C6.9025 13.3833 6.9025 12.7502 7.2925 12.3593C7.6835 11.9693 8.3165 11.9693 8.7065 12.3593L9.9665 13.6192L15.2925 8.29325C15.6835 7.90225 16.3165 7.90225 16.7065 8.29325C17.0975 8.68325 17.0975 9.31625 16.7065 9.70725L10.6735 15.7403C10.4785 15.9353 10.2225 16.0332 9.9665 16.0332C9.7105 16.0332 9.4545 15.9353 9.2595 15.7403Z' })
            )
        )
    );
};