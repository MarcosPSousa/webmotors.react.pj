'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleCheck = exports.StyleItem = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n'], ['\n    display: flex;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    width: 40px;\n    margin-right: 8px;\n    border-bottom: 2px solid;\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    text-align: center;\n    transition: border-bottom-color 0.3s, color 0.3s;\n    border-bottom-color: ', ';\n    color: ', ';\n'], ['\n    width: 40px;\n    margin-right: 8px;\n    border-bottom: 2px solid;\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    text-align: center;\n    transition: border-bottom-color 0.3s, color 0.3s;\n    border-bottom-color: ', ';\n    color: ', ';\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    width: 24px;\n    height: 24px;\n    fill: ', ';\n    transition: fill 0.3s;\n'], ['\n    width: 24px;\n    height: 24px;\n    fill: ', ';\n    transition: fill 0.3s;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var statusColor = function statusColor(status) {
    return {
        default: (0, _color2.default)('gray-3'),
        success: (0, _color2.default)('primary'),
        error: (0, _color2.default)('error')
    }[status || 'default'];
};

var textColor = function textColor(status) {};

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleItem = exports.StyleItem = _styledComponents2.default.div(_templateObject2, function (_ref) {
    var status = _ref.status;
    return statusColor(status);
}, function (_ref2) {
    var status = _ref2.status;
    return statusColor(status);
});

var StyleCheck = exports.StyleCheck = _styledComponents2.default.div(_templateObject3, function (_ref3) {
    var checked = _ref3.checked;
    return checked ? (0, _color2.default)('success') : 'transparent';
});