'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _entries2 = require('babel-runtime/core-js/object/entries');

var _entries3 = _interopRequireDefault(_entries2);

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _dexie = require('dexie');

var _dexie2 = _interopRequireDefault(_dexie);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (props) {
    var src = props.src,
        _props$v = props.v,
        v = _props$v === undefined ? 1 : _props$v,
        _props$maxSize = props.maxSize,
        maxSize = _props$maxSize === undefined ? 1024 * 300 : _props$maxSize,
        onClick = props.onClick;

    var _useState = (0, _react.useState)(''),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        img = _useState2[0],
        setImg = _useState2[1];

    var _useState3 = (0, _react.useState)(),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        imgError = _useState4[0],
        setImgError = _useState4[1];

    var idb = new _dexie2.default('img-store');

    var propsImg = function propsImg() {
        return (0, _entries3.default)(props).map(function (entries) {
            var _entries = (0, _slicedToArray3.default)(entries, 2),
                attr = _entries[0],
                value = _entries[1];

            if (!/^(on)|(maxSize|v|src)$/.test(attr)) {
                return (attr === 'className' ? 'class' : attr) + '="' + value + '" ';
            }
            return '';
        }).join('').trim();
    };

    if (!src) {
        console.error('Component <Img />: attribute src undefined');
        return false;
    }

    var blobToBitmap = function blobToBitmap(blob) {
        return setImg('<img src="' + URL.createObjectURL(blob) + '" ' + propsImg() + ' />');
    };

    var blobToSvg = function blobToSvg(e) {
        return e.text().then(function (f) {
            var data = f.replace(/(<\?(\n|.)+?\?>|<!--(\n|.)+?-->|\s(version|x|y|style|(xml:|xmlns:).+?)=".+?")/g, '').replace(/(\s|\n\s)+/g, ' ').replace(/>\s+</g, '><').replace(/\s+<\//g, '</').replace(/<style(\n|.)+?<\/style>/g, '').replace(/<svg/, '<svg ' + propsImg());
            setImg(data);
        });
    };

    var imgDetected = function imgDetected(blob) {
        return blob.type === 'image/svg+xml' ? blobToSvg(blob) : blobToBitmap(blob);
    };

    var imgFetch = function imgFetch(_ref) {
        var table = _ref.table,
            action = _ref.action;

        var url = src.replace(/((\?|#).+|(\?|#|\/)$)/g, '');
        window.fetch(url).then(function (e) {
            return e.blob();
        }).then(function (blob) {
            var type = blob.type,
                size = blob.size;

            if (!/^image/.test(type)) {
                return setImgError('file "' + url + '" isn\'t image/*');
            }
            if (size > maxSize) {
                return setImgError('file size is more then ' + maxSize / 1024 + 'kb');
            }
            imgDetected(blob);
            setTimeout(function () {
                var tableActions = {
                    add: function add() {
                        return table.add({ src: url, v: v, blob: blob });
                    },
                    update: function update() {
                        return table.update(url, { v: v, blob: blob });
                    }
                };
                if (tableActions[action]) {
                    tableActions[action]();
                }
            }, 10);
            return blob;
        }).catch(function (e) {
            return setImgError('file "' + url + '" error: "' + e.message + '"');
        });
    };

    var imgGet = function imgGet(_ref2) {
        var table = _ref2.table,
            data = _ref2.data;
        return data.v !== v ? imgFetch({ table: table, action: 'update' }) : imgDetected(data.blob);
    };

    var imgRender = function imgRender() {
        idb.version(1).stores({ images: 'src,blob,v' });
        var images = idb.images;

        idb.transaction('rw', images, function () {
            return images.get({ src: src }).then(function (data) {
                return !data ? imgFetch({ table: images, action: 'add' }) : imgGet({ table: images, data: data });
            }).catch(function (data) {
                return console.error({ data: data });
            });
        });
    };

    (0, _react.useEffect)(function () {
        return imgRender();
    }, []);

    return _react2.default.createElement('span', {
        'data-src': src,
        'data-error': imgError,
        'data-version': v === 1 ? undefined : v,
        dangerouslySetInnerHTML: { __html: img },
        onClick: onClick
    });
};