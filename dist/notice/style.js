'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleClose = exports.StyleIcon = exports.StyleText = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    padding: ', ';\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    font-weight: 500;\n    color: ', ';\n    display: flex;\n    justify-content: space-between;\n    border-radius: 4px;\n    background-color: ', ';\n    overflow: hidden;\n    margin-bottom: ', ';\n    transition: padding 0.3s, margin-bottom 0.3s;\n'], ['\n    padding: ', ';\n    font-size: 1.2rem;\n    line-height: 1.5em;\n    font-weight: 500;\n    color: ', ';\n    display: flex;\n    justify-content: space-between;\n    border-radius: 4px;\n    background-color: ', ';\n    overflow: hidden;\n    margin-bottom: ', ';\n    transition: padding 0.3s, margin-bottom 0.3s;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    margin-top: ', ';\n    transition: margin-top 0.3s;\n    display: flex;\n    align-items: center;\n'], ['\n    margin-top: ', ';\n    transition: margin-top 0.3s;\n    display: flex;\n    align-items: center;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    width: 24px;\n    height: 24px;\n    margin-right: 16px;\n    fill: ', ';\n'], ['\n    width: 24px;\n    height: 24px;\n    margin-right: 16px;\n    fill: ', ';\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    width: 24px;\n    height: 24px;\n    fill: ', ';\n    background-color: transparent;\n    margin-top: ', ';\n    transition: margin-top 0.3s;\n'], ['\n    width: 24px;\n    height: 24px;\n    fill: ', ';\n    background-color: transparent;\n    margin-top: ', ';\n    transition: margin-top 0.3s;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var backgroundColor = function backgroundColor(type) {
    return {
        error: (0, _color2.default)('error'),
        success: (0, _color2.default)('success'),
        danger: (0, _color2.default)('danger'),
        warning: (0, _color2.default)('warning'),
        wait: (0, _color2.default)('gray-2')
    }[type || 'error'];
};

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject, function (_ref) {
    var open = _ref.open;
    return open ? '12px 32px' : '0px 32px';
}, (0, _color2.default)('white'), function (_ref2) {
    var type = _ref2.type;
    return backgroundColor(type);
}, function (_ref3) {
    var open = _ref3.open;
    return open ? '20px' : '0px';
});

var StyleText = exports.StyleText = _styledComponents2.default.div(_templateObject2, function (_ref4) {
    var open = _ref4.open;
    return open ? '0px' : '-100px';
});

var StyleIcon = exports.StyleIcon = _styledComponents2.default.i(_templateObject3, (0, _color2.default)('white'));

var StyleClose = exports.StyleClose = _styledComponents2.default.button(_templateObject4, (0, _color2.default)('white'), function (_ref5) {
    var open = _ref5.open;
    return open ? '0px' : '-90px';
});