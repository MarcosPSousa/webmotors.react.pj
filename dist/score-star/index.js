'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _webmotorsSvg = require('webmotors-svg');

var _webmotorsSvg2 = _interopRequireDefault(_webmotorsSvg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ScoreStar = function (_Component) {
    (0, _inherits3.default)(ScoreStar, _Component);

    function ScoreStar(props) {
        (0, _classCallCheck3.default)(this, ScoreStar);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ScoreStar.__proto__ || (0, _getPrototypeOf2.default)(ScoreStar)).call(this, props));

        _this.state = {};
        return _this;
    }

    (0, _createClass3.default)(ScoreStar, [{
        key: 'render',
        value: function render() {
            var score = this.props.score;

            var starArray = new Array(5).fill(false);
            var star = starArray.map(function (item, i) {
                return i + 1 <= (score || 0);
            });
            return _react2.default.createElement(
                'div',
                { className: 'scorestar' },
                star.map(function (item, i) {
                    return _react2.default.createElement(_webmotorsSvg2.default, { key: 'star_' + i, className: 'scorestar__icon ' + (item ? 'scorestar__icon--fill' : ''), src: '/shopping/assets/img/icons/star-rounded.svg' });
                })
            );
        }
    }]);
    return ScoreStar;
}(_react.Component);

exports.default = ScoreStar;