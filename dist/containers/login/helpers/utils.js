'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ajax = exports.invoiceFormat = exports.listboxItemAnimation = exports.accordionAnimate = exports.scrollSmooth = exports.formNotice = exports.WINDOW = undefined;

var _values = require('babel-runtime/core-js/object/values');

var _values2 = _interopRequireDefault(_values);

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _isNan = require('babel-runtime/core-js/number/is-nan');

var _isNan2 = _interopRequireDefault(_isNan);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _utils = require('webmotors-react-pj/utils');

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _constants = require('./constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var formNotice = function formNotice(that, text) {
    var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'error';
    var timer = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 8000;

    var t = that;
    t.setState({
        noticeShow: true,
        noticeText: text,
        noticeType: type
    });
    clearTimeout(t.timerClose);
    t.timerClose = setTimeout(function () {
        return t.setState({ noticeText: '' });
    }, timer);
};

var scrollSmooth = function scrollSmooth(to) {
    var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 300;

    if (_utils.WINDOW) {
        clearTimeout(window.scrollSmoothTimerCache);
        if (duration <= 0) {
            return;
        }
        var difference = to - window.scrollY - 100;
        var perTick = difference / duration * 10;
        window.scrollSmoothTimerCache = setTimeout(function () {
            if (!(0, _isNan2.default)(parseInt(perTick, 10))) {
                window.scrollTo(0, window.scrollY + perTick);
                scrollSmooth(to, duration - 10);
            }
        }, 10);
    }
};

var accordionAnimate = function accordionAnimate(testOpen, item) {
    var tag = item;
    var getPadding = function getPadding(padding) {
        return parseFloat(getComputedStyle(tag)[padding]);
    };
    tag.classList.add('accordion--transition');
    if (testOpen) {
        tag.style.height = 'auto';
        var clientHeight = tag.clientHeight;

        var height = clientHeight + getPadding('paddingTop') + getPadding('paddingBottom');
        tag.style.height = '0px';
        setTimeout(function () {
            tag.style.height = height + 'px';
        }, 1);
    } else {
        tag.style.height = '0px';
        tag.addEventListener('transitionEnd', function () {
            return tag.classList.remove('accordion--transition');
        });
    }
};

var listboxItemAnimation = function listboxItemAnimation() {
    setTimeout(function () {
        return [].concat((0, _toConsumableArray3.default)(document.querySelectorAll('.listbox__item'))).forEach(function (item, i) {
            return setTimeout(function () {
                return item.classList.add('listbox__item--active');
            }, Math.min(i * 60, 2000));
        });
    }, 10);
};

var invoiceFormat = function invoiceFormat(obj) {
    return obj.map(function (item) {
        var getTicketStatus = function getTicketStatus(status) {
            var _statusReturn;

            var statusReturn = (_statusReturn = {}, (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.Emitida, { className: 'color-error', text: 'Emitida' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.AguardandoPagamento, { className: 'color-error', text: 'Aguardando pagamento' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.EmAtraso, { className: 'color-error', text: 'Em atraso' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.Expirada, { className: 'color-error', text: 'Expirada' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.Reagendada, { className: 'color-error', text: 'Reagendada' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.Quitada, { className: 'color-success', text: 'Quitada' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.Vencida, { className: 'color-error', text: 'Vencida' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.ErroNaCobranca, { className: 'color-error', text: 'Erro na cobrança' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.Prorrogada, { className: 'color-error', text: 'Prorrogada' }), (0, _defineProperty3.default)(_statusReturn, _constants.ENUM.invoice.EmAcordo, { className: 'color-error', text: 'Em acordo' }), _statusReturn);
            if (statusReturn[status]) {
                return statusReturn[status];
            }
            return { className: 'color-error', text: '-' };
        };
        var getLinkFormat = function getLinkFormat(product) {
            if (product.LinkBoleto) {
                return _react2.default.createElement(
                    'a',
                    { className: 'link', href: product.LinkBoleto, target: '_blank', rel: 'noopener noreferrer' },
                    'Boleto'
                );
            }
            return '-';
        };
        var getDateValid = function getDateValid(date) {
            return (/0001-01-01/.test(date)
            );
        };
        var response = {};
        var invoiceList = item.Lista;
        var invoiceListFirst = invoiceList[0];
        var invoiceProducts = item.produtos;
        var dateIssue = new Date(invoiceListFirst.DataEmissao);
        var invoiceProductsFilter = [];
        var invoiceProductsList = [];
        var invoiceProductsValue = 0;
        response.outsideTitle = 'Mês';
        response.outsideTotal = 'Valor total da fatura';
        response.outsideNfe = item.notasFiscais.map(function (nfe) {
            return _react2.default.createElement(
                'a',
                { key: 'nfe_' + nfe.Codigo + '_' + nfe.CodigoRPS, className: 'listbox__title--space link', href: nfe.Link, target: '_blank', rel: 'noopener noreferrer' },
                nfe.Codigo || nfe.CodigoRPS
            );
        });
        response.infoMonth = new Date(invoiceListFirst.DataEmissao).toLocaleDateString('pt-BR', { month: 'long' });
        response.printButton = '/meu-plano/imprimir/' + dateIssue.getFullYear() + '/' + (dateIssue.getMonth() + 1);
        response.exportButton = _config.UrlEstoque + '/meu-plano/fatura?exportar=1&ano=' + dateIssue.getFullYear() + '&mes=' + (dateIssue.getMonth() + 1) + '&buscarPorVencimento=false';
        response.tickets = {
            header: ['Status', 'Nº da Fatura', 'Via', 'Emissão', 'Vencimento', 'Quitação', 'Cobrança', 'Imprimir boleto'],
            body: invoiceList.map(function (list) {
                return [getTicketStatus(list.StatusCobranca), { className: '', text: list.CobrancaId }, { className: '', text: list.Via }, { className: '', text: getDateValid(list.DataEmissao) ? '-' : (0, _utils.formatDate)(list.DataEmissao) }, { className: '', text: getDateValid(list.DataVencimento) ? '-' : (0, _utils.formatDate)(list.DataVencimento) }, { className: '', text: getDateValid(list.DataPagamento) ? '-' : (0, _utils.formatDate)(list.DataPagamento) }, { className: '', text: getDateValid(list.Valor) ? '-' : (0, _utils.formatMoney)(list.Valor) }, { className: 'listbox__table-cell--big', text: getLinkFormat(list) }];
            })
        };
        (0, _values2.default)(invoiceProducts).forEach(function (product) {
            if (product) {
                invoiceProductsFilter.push(product);
            }
        });
        invoiceProductsFilter.forEach(function (list) {
            return list.forEach(function (product) {
                invoiceProductsValue = product.PrecoFatura + invoiceProductsValue;
                invoiceProductsList.push([{ className: 'listbox__table-cell--strong ta-l', text: product.Produto }, { className: '', text: product.Quantidade }, { className: '', text: product.DataVigencia }, { className: '', text: product.PrecoTotalFormatado }, { className: '', text: product.PrecoDescontoFormatado }, { className: '', text: product.PrecoFaturaFormatado }]);
            });
        });
        response.infoTotal = invoiceProductsValue.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
        response.products = {
            header: ['Produto', 'Quantidade', 'Período de cobrança', 'Valor bruto', 'Desconto comercial', 'Valor líquido'],
            body: invoiceProductsList
        };
        return response;
    });
};

var ajax = function ajax(url, data) {
    var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'POST';
    var dispatch = arguments[3];
    return (0, _axios2.default)({
        method: method,
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + (0, _utils.cookieGet)('CockpitLogged')
        },
        url: url,
        data: data,
        params: method === 'POST' ? '' : data
    }).then(function (res) {
        var resData = res && (res.data.data || res.data.Result);
        var resStatus = res && res.status;
        var returnObj = {
            type: 'AJAX_' + url,
            payload: {
                success: true,
                data: resData,
                status: resStatus
            }
        };
        if (dispatch) {
            return dispatch(returnObj);
        }
        return returnObj;
    }).catch(function (rej) {
        var resData = void 0;
        var resStatus = void 0;
        var resErrorText = {
            400: 'Dados inválidos',
            401: 'Usuário não autorizado para esta ação',
            403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
            500: 'Erro interno da aplicação, tente novamente mais tarde',
            503: 'Servidor fora do ar, tente novamente mais tarde'
        };
        var resErrorTextUndefined = 'Erro não parametrizado pelo servidor';
        console.error(rej);
        if (rej && rej.response) {
            var errorData = rej.response.data;
            resStatus = rej.status || rej.response.status;
            resData = errorData.errors || errorData.Messages && errorData.Messages[0] || errorData || function () {
                resData = resErrorText[resStatus];
                if (!resData) {
                    resData = resErrorTextUndefined;
                }
                return resData;
            }(resStatus);
        } else {
            resStatus = /(\d+|\d+\n)/.exec(rej) ? /(\d+|\d+\n)/.exec(rej)[0].trim() : false;
            resData = resErrorText[resStatus];
            if (!resData) {
                resData = resErrorTextUndefined;
            }
        }
        var returnObj = {
            type: 'AJAX_' + url + '_FAILURE',
            payload: {
                success: false,
                errors: resData,
                status: Number(resStatus)
            }
        };
        if (dispatch) {
            return dispatch(returnObj);
        }
        return returnObj;
    });
};

exports.WINDOW = _utils.WINDOW;
exports.formNotice = formNotice;
exports.scrollSmooth = scrollSmooth;
exports.accordionAnimate = accordionAnimate;
exports.listboxItemAnimation = listboxItemAnimation;
exports.invoiceFormat = invoiceFormat;
exports.ajax = ajax;
exports.default = _utils.WINDOW;