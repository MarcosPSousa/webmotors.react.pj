'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactHelmet = require('react-helmet');

var _reactHelmet2 = _interopRequireDefault(_reactHelmet);

var _utils = require('webmotors-react-pj/utils');

var _config = require('webmotors-react-pj/config');

var _form = require('webmotors-react-pj/form');

var _form2 = _interopRequireDefault(_form);

var _translate = require('webmotors-react-pj/translate');

var _translate2 = _interopRequireDefault(_translate);

var _text = require('webmotors-react-pj/webinput/text');

var _text2 = _interopRequireDefault(_text);

var _img = require('webmotors-react-pj/img');

var _img2 = _interopRequireDefault(_img);

var _WebInput = require('./components/WebInput');

var _WebInput2 = _interopRequireDefault(_WebInput);

var _Notice = require('./components/Notice');

var _Notice2 = _interopRequireDefault(_Notice);

var _Button = require('./components/Button');

var _Button2 = _interopRequireDefault(_Button);

var _utils2 = require('./helpers/utils');

var _style = require('./style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ModalNoFrame = function ModalNoFrame(_ref) {
    var children = _ref.children,
        open = _ref.open,
        onClose = _ref.onClose;
    return open ? _react2.default.createElement(
        _style.StyleModal,
        null,
        _react2.default.createElement(_style.StyleModalBg, null),
        _react2.default.createElement(_style.StyleModalClose, { 'aria-hidden': 'true', onClick: function onClick() {
                return onClose();
            } }),
        _react2.default.createElement(
            _style.StyleModalContent,
            null,
            children
        )
    ) : false;
};

var Login = function (_Component) {
    (0, _inherits3.default)(Login, _Component);

    function Login(props) {
        (0, _classCallCheck3.default)(this, Login);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Login.__proto__ || (0, _getPrototypeOf2.default)(Login)).call(this, props));

        _this.state = {
            backoffice: (0, _utils.paramURL)('bokey'),
            logincookie: (0, _utils.paramURL)('cookie')
        };
        _this.email = (0, _react.createRef)();
        _this.password = (0, _react.createRef)();
        _this.refButtonLoginset = (0, _react.createRef)();
        return _this;
    }

    (0, _createClass3.default)(Login, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            if (this.state.backoffice) {
                this.getBOKey();
            }
        }
    }, {
        key: 'getBOKey',
        value: function getBOKey() {
            var _this2 = this;

            (0, _utils.Ajax)({
                url: _config.ApiLogin + '/access/sso/loginhub',
                data: { Bokey: this.state.backoffice },
                method: 'POST'
            }).then(function (e) {
                if (e.success) {
                    (0, _utils.cookieSet)('CockpitLogged', e.data.jwt);
                    var jwt = (0, _utils.jwtGet)();
                    var secounds = 5;
                    var mensageSuccess = function mensageSuccess() {
                        if (secounds >= 0) {
                            (0, _utils2.formNotice)(_this2, _react2.default.createElement(
                                'div',
                                null,
                                _translate2.default.login.loginFrom,
                                _react2.default.createElement(
                                    'strong',
                                    null,
                                    ' ' + jwt.status
                                ),
                                _react2.default.createElement('br', null),
                                _react2.default.createElement('br', null),
                                _translate2.default.login.redirectIn,
                                ' ' + secounds--,
                                _translate2.default.login.seconds
                            ), 'success');
                        } else {
                            window.clearInterval(window.LoginInterval);
                            window.location.href = '/';
                        }
                    };
                    mensageSuccess();
                    window.LoginInterval = window.setInterval(function () {
                        mensageSuccess();
                    }, 1000);
                } else {
                    (0, _utils2.formNotice)(_this2, e.data);
                }
            }).then(function () {
                _this2.setState({ loading: false });
            });
        }
    }, {
        key: 'getEmail',
        value: function getEmail() {
            return 'false';
        }
    }, {
        key: 'getPassword',
        value: function getPassword() {
            return 'false';
        }
    }, {
        key: 'handleChangeEmail',
        value: function handleChangeEmail(e) {
            var email = e.target.value;

            this.setState({
                email: email
            }, function () {
                window.localStorage.setItem('CokpitLoginError', email);
            });
        }
    }, {
        key: 'handleChangePassword',
        value: function handleChangePassword(e) {
            var password = e.target.value;

            this.setState({
                password: password
            });
        }
    }, {
        key: 'handlePassword',
        value: function handlePassword(e) {
            var target = e.target;
            var input = target.previousElementSibling;
            var type = input.type;

            target.classList.toggle('page__cover-inputbutton--show');
            input.type = type === 'text' ? 'password' : 'text';
        }
    }, {
        key: 'handleSuccess',
        value: function handleSuccess(e) {
            var _this3 = this;

            var data = e.data;
            var jwt = data.jwt;

            var jwtPayload = jwt.replace(/_/g, '/').replace(/-/g, '+');

            var b64DecodeUnicode = function b64DecodeUnicode(str) {
                return decodeURIComponent(Array.prototype.map.call(atob(str), function (c) {
                    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                }).join(''));
            };
            var parseJwt = function parseJwt(token) {
                return JSON.parse(b64DecodeUnicode(token.split('.')[1].replace('-', '+').replace('_', '/')));
            };
            var setCookieLogged = function setCookieLogged() {
                (0, _utils.cookieSet)('CockpitLogged', jwt);
            };
            var result = parseJwt(jwt);
            if (result.role.indexOf('5') !== -1) {
                (0, _utils2.formNotice)(this, 'Usuário e/ou senha inválidos');
            } else {
                switch (data.status) {
                    case 'Nao Migrado':
                        {
                            (0, _utils2.formNotice)(this, 'Usuário e/ou senha inválidos');
                            break;
                        }case 'Pendente':
                        {
                            setCookieLogged();
                            window.location.href = '/usuario/introducao-cockpit';
                            break;
                        }case 'senhaExpirada':
                        {
                            setCookieLogged();
                            window.location.href = '/redefinir-senha/troca-de-seguranca';
                            break;
                        }default:
                        {
                            var logged = function logged() {
                                setCookieLogged();
                            };
                            if (this.state.logincookie) {
                                this.setState({
                                    jwt: jwt
                                }, function () {
                                    logged();
                                    _this3.refButtonLoginset.current.click();
                                });
                            } else {
                                logged();
                                window.location.reload();
                            }
                        }
                }
            }
        }
    }, {
        key: 'handleBeforeSend',
        value: function handleBeforeSend() {
            var _this4 = this;

            (0, _utils2.formNotice)(this, '');
            this.setState({ noticeText: '' }, function () {
                var _ref2 = [_this4.email.current.value, _this4.password.current.value],
                    email = _ref2[0],
                    senha = _ref2[1];

                _this4.setState({ formBeforeSend: email.trim() !== '' && senha.trim() !== '' });
            });
        }
    }, {
        key: 'handleError',
        value: function handleError(e) {
            (0, _utils2.formNotice)(this, e.data);
        }
    }, {
        key: 'handleComplete',
        value: function handleComplete() {
            if (!this.state.formBeforeSend) {
                (0, _utils2.formNotice)(this, 'Preencha o campo de e-mail e senha');
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this5 = this;

            return _react2.default.createElement(
                'div',
                { role: 'main', className: 'page mt-64' },
                _react2.default.createElement(
                    _reactHelmet2.default,
                    null,
                    _react2.default.createElement(
                        'title',
                        null,
                        'Cockpit'
                    )
                ),
                _react2.default.createElement(_style.StyleLoginContainer, null),
                _react2.default.createElement(
                    'div',
                    { className: 'page__cover page__cover--big' },
                    _react2.default.createElement(
                        'div',
                        { className: 'container page__cover-content d-fx' },
                        _react2.default.createElement(
                            'div',
                            { className: 'pc-col-5 tl-col-12' },
                            _react2.default.createElement(
                                'h1',
                                { className: 'page__title page__cover-title mb-40 color-gray-1' },
                                _translate2.default.login.title
                            ),
                            _react2.default.createElement(
                                'p',
                                { className: 'page__text page__cover-text mb-40' },
                                _translate2.default.login.subTitle
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'page__brands page__brands--white mt-40 d-fx' },
                                _react2.default.createElement(
                                    'div',
                                    { className: 'page__brands-webmotors mr-32' },
                                    _react2.default.createElement(_img2.default, { src: '/assets/img/brands/webmotors-2-fill.svg' })
                                ),
                                _react2.default.createElement(_img2.default, { className: 'page__brands-financiamento', src: '/assets/img/brands/santander-financiamento.svg', removeClass: true })
                            )
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'pc-col-6 -pc-col-1 tl-col-10 -tl-col-1' },
                            _react2.default.createElement(
                                _style.StyleFormLoginset,
                                { method: 'POST', action: _config.ApiLoginWebmotors + '/access/sso/loginset' },
                                _react2.default.createElement('input', { type: 'hidden', name: 'Email', value: this.state.email }),
                                _react2.default.createElement('input', { type: 'hidden', name: 'Senha', value: this.state.password }),
                                _react2.default.createElement('input', { type: 'hidden', name: 'jwt', value: this.state.jwt }),
                                _react2.default.createElement('input', { type: 'hidden', name: 'returnPage', value: window.location.href }),
                                _react2.default.createElement(
                                    'button',
                                    { ref: this.refButtonLoginset, type: 'submit' },
                                    'Enviar'
                                )
                            ),
                            _react2.default.createElement(
                                _form2.default,
                                {
                                    action: this.state.formBeforeSend && _config.ApiLogin + '/access/sso/login',
                                    method: 'POST',
                                    className: 'page__cover-form bg-gray-1 color-gray-8',
                                    onBeforeSend: function onBeforeSend() {
                                        return _this5.handleBeforeSend();
                                    },
                                    onSuccess: function onSuccess(e) {
                                        return _this5.handleSuccess(e);
                                    },
                                    onError: function onError(e) {
                                        return _this5.handleError(e);
                                    },
                                    onComplete: function onComplete() {
                                        return _this5.handleComplete();
                                    },
                                    autoComplete: 'on'
                                },
                                _react2.default.createElement(_Notice2.default, {
                                    text: this.state.noticeText,
                                    type: this.state.noticeType
                                }),
                                _react2.default.createElement(
                                    'h3',
                                    { className: 'page__title page__cover-subtitle pb-32' },
                                    _translate2.default.login.joinNow
                                ),
                                _react2.default.createElement(_text2.default, { ref: this.email, placeholder: _translate2.default.login.email, required: true, type: 'email', name: 'Email', id: 'Email', onInput: function onInput(e) {
                                        return _this5.handleChangeEmail(e);
                                    } }),
                                _react2.default.createElement(_text2.default, { ref: this.password, placeholder: _translate2.default.login.password, required: true, type: 'password-weakened', name: 'Senha', id: 'Senha', onInput: function onInput(e) {
                                        return _this5.handleChangePassword(e);
                                    } }),
                                _react2.default.createElement(
                                    'div',
                                    { className: 'ta-r mb-32' },
                                    _react2.default.createElement(
                                        'a',
                                        { className: 'page__cover-recovery link', href: _config.UrlCockpit + '/redefinir-senha' },
                                        _translate2.default.login.recoverPass
                                    )
                                ),
                                _react2.default.createElement(
                                    'div',
                                    { className: 'page__cover-tools d-fx ta-j va-m' },
                                    _react2.default.createElement(
                                        'div',
                                        { className: 'page__cover-logged' },
                                        _react2.default.createElement(_WebInput2.default, { type: 'checkbox', checked: true, value: 'true', name: 'ManterConectado', id: 'ManterConectado', label: _translate2.default.login.keepLogged })
                                    ),
                                    _react2.default.createElement(
                                        _Button2.default,
                                        { type: 'submit', mode: 'danger', disabled: this.state.loading },
                                        _translate2.default.login.loginButton
                                    )
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'container page__cover-bg' },
                        _react2.default.createElement('div', { className: 'page__cover-circle' })
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'page__products' },
                    _react2.default.createElement(
                        'div',
                        { className: 'container' },
                        _react2.default.createElement(
                            'h2',
                            { className: 'page__title pc-col-5 color-gray-8' },
                            _translate2.default.login.secondTitle
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'page__products-circle ta-c' },
                            _react2.default.createElement(
                                'div',
                                { className: 'page__products-list' },
                                _react2.default.createElement(_img2.default, { className: 'page__products-item page__products-item-1', src: '/assets/img/brands/crm.svg' }),
                                _react2.default.createElement(_img2.default, { className: 'page__products-item page__products-item-2', src: '/assets/img/brands/gearbox.svg' }),
                                _react2.default.createElement(_img2.default, { className: 'page__products-item page__products-item-3', src: _translate2.default.login.brandFidelity }),
                                _react2.default.createElement(_img2.default, { className: 'page__products-item page__products-item-4', src: _translate2.default.login.brandStock }),
                                _react2.default.createElement(_img2.default, { className: 'page__products-item page__products-item-5', src: _translate2.default.login.brandUniversity }),
                                _react2.default.createElement(_img2.default, { className: 'page__products-item page__products-item-6', src: '/assets/img/brands/floorplan.svg' }),
                                _react2.default.createElement(_img2.default, { className: 'page__products-item page__products-item-7', src: '/assets/img/brands/autoguru.svg' }),
                                _react2.default.createElement(_img2.default, { className: 'page__products-item page__products-item-8', src: _translate2.default.login.brandHotdeals })
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'page__products-infos' },
                                _react2.default.createElement(
                                    'div',
                                    { className: 'page__text page__products-text-1' },
                                    _translate2.default.login.productText1
                                ),
                                _react2.default.createElement(
                                    'div',
                                    { className: 'page__text page__products-text-2' },
                                    _translate2.default.login.productText2
                                ),
                                _react2.default.createElement(
                                    'div',
                                    { className: 'page__text page__products-text-3' },
                                    _translate2.default.login.productText3
                                ),
                                _react2.default.createElement(
                                    'div',
                                    { className: 'page__text page__products-text-4' },
                                    _translate2.default.login.productText4
                                )
                            ),
                            _react2.default.createElement(_img2.default, { className: 'page__products-logo', src: '/assets/img/brands/cockpit.svg' }),
                            _react2.default.createElement(
                                'div',
                                { className: 'page__products-button' },
                                _react2.default.createElement(
                                    _Button2.default,
                                    { type: 'button', mode: 'danger', onClick: function onClick() {
                                            return _this5.setState({ modalOpen: true });
                                        } },
                                    _react2.default.createElement(_img2.default, { src: '/assets/img/icons/play-2.svg' }),
                                    _translate2.default.login.playVideo
                                )
                            )
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'page__mobile bg-gray-8' },
                    _react2.default.createElement(
                        'div',
                        { className: 'container page__mobile-shadow d-fx' },
                        _react2.default.createElement(
                            'div',
                            { className: 'page__mobile-info pc-col-6' },
                            _react2.default.createElement(
                                'h2',
                                { className: 'page__title color-gray-1 mb-64' },
                                _translate2.default.login.thirdTitle
                            ),
                            _react2.default.createElement(
                                'p',
                                { className: 'page__text mb-64 pb-8' },
                                _translate2.default.login.thirdSubTitle
                            ),
                            _react2.default.createElement(
                                'div',
                                { className: 'page__mobile-store' },
                                _react2.default.createElement(
                                    'a',
                                    { target: '_blank', rel: 'noopener noreferrer', href: _translate2.default.login.urlAppApple },
                                    _react2.default.createElement(_img2.default, { className: 'page__mobile-app page__mobile-app-1 mr-24', src: '/assets/img/static/store-app-store.png', alt: 'App Store' })
                                ),
                                _react2.default.createElement(
                                    'a',
                                    { target: '_blank', rel: 'noopener noreferrer', href: _translate2.default.login.urlAppAndroid },
                                    _react2.default.createElement(_img2.default, { className: 'page__mobile-app page__mobile-app-2', src: '/assets/img/static/store-google-play.png', alt: 'Google Play' })
                                )
                            )
                        )
                    ),
                    _react2.default.createElement('div', { className: 'container page__mobile-photo' })
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'page__join' },
                    _react2.default.createElement(
                        'div',
                        { className: 'container d-fx va-m' },
                        _react2.default.createElement(
                            'div',
                            { className: 'pc-col-8' },
                            _react2.default.createElement(
                                'h2',
                                { className: 'page__title' },
                                _react2.default.createElement(
                                    'span',
                                    { className: 'color-1' },
                                    _translate2.default.login.joinText1
                                ),
                                _react2.default.createElement('br', null),
                                _react2.default.createElement(
                                    'span',
                                    { className: 'color-gray-8' },
                                    _translate2.default.login.joinText2
                                )
                            )
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'pc-col-4 ta-r' },
                            _react2.default.createElement(
                                'div',
                                { className: 'd-b' },
                                _react2.default.createElement(_img2.default, { className: 'page__join-logo', src: '/assets/img/brands/cockpit.svg' })
                            ),
                            _react2.default.createElement(
                                'p',
                                { className: 'page__text page__join-text' },
                                _translate2.default.login.joinText3
                            )
                        )
                    )
                ),
                _react2.default.createElement(
                    ModalNoFrame,
                    {
                        open: this.state.modalOpen,
                        onClose: function onClose() {
                            return _this5.setState({ modalOpen: false });
                        }
                    },
                    _react2.default.createElement(
                        _style.StyleModalLoad,
                        null,
                        _react2.default.createElement('div', { className: 'load load--white' })
                    ),
                    _react2.default.createElement('iframe', {
                        width: '100%',
                        height: '315',
                        className: 'login__video-iframe',
                        src: 'https://www.youtube-nocookie.com/embed/ieMwYid6aAc?rel=0',
                        title: 'V\xEDdeo Cockpit',
                        frameBorder: '0',
                        allow: 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture',
                        allowFullScreen: true
                    })
                )
            );
        }
    }]);
    return Login;
}(_react.Component);

exports.default = Login;