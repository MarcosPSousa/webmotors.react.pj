'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _webmotorsSvg = require('webmotors-svg');

var _webmotorsSvg2 = _interopRequireDefault(_webmotorsSvg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WebInput = function (_Component) {
    (0, _inherits3.default)(WebInput, _Component);

    function WebInput(props) {
        (0, _classCallCheck3.default)(this, WebInput);

        var _this = (0, _possibleConstructorReturn3.default)(this, (WebInput.__proto__ || (0, _getPrototypeOf2.default)(WebInput)).call(this, props));

        _this.state = {
            readOnly: true,
            passwordView: true,
            description: _this.props.description,
            passwordClass: ''
        };
        _this.input = (0, _react.createRef)();
        _this.wrapper = (0, _react.createRef)();
        return _this;
    }

    (0, _createClass3.default)(WebInput, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.getId();
            this.wrapperClassAdd('inputtext--disabled', this.props.disabled);
            this.wrapperClassAdd('inputtext--opened', this.props.value && this.props.value !== '');
            this.handlerAutoMask();
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate() {
            this.wrapperClassRemove('inputtext--disabled', !this.props.disabled);
            this.wrapperClassAdd('inputtext--opened', this.props.value);
            this.handlerUpdateMask();
            this.handlerUpdateChecked();
        }
    }, {
        key: 'getId',
        value: function getId() {
            var id = this.props.id ? this.props.id : 'WebInput_' + Date.now() + '_' + (this.props.placeholder || this.props.label).replace(/\W/g, '').toLowerCase();
            this.setState({ id: id });
        }
    }, {
        key: 'wrapperClassAdd',
        value: function wrapperClassAdd(className, test) {
            var _this2 = this;

            setTimeout(function () {
                var current = _this2.wrapper.current;

                if (test && current) {
                    current.classList.add(className);
                }
            }, 10);
        }
    }, {
        key: 'wrapperClassRemove',
        value: function wrapperClassRemove(className, test) {
            var current = this.wrapper.current;

            if (test && current) {
                setTimeout(function () {
                    return current.classList.remove(className);
                }, 10);
            }
        }
    }, {
        key: 'handlerUpdateMask',
        value: function handlerUpdateMask() {
            if (this.state.mask && this.input.current.getAttribute('value')) {
                this.renderMask(this.input.current);
            }
        }
    }, {
        key: 'handlerUpdateChecked',
        value: function handlerUpdateChecked() {
            if (this.props.type === 'radio' && this.props.checked) {
                this.input.current.click();
            }
        }
    }, {
        key: 'handlerAutoMask',
        value: function handlerAutoMask() {
            var _this3 = this;

            var fn = {
                tel: function tel() {
                    return _this3.setState({ mask: '(99) 9999-99999' });
                },
                'tel-home': function telHome() {
                    return _this3.setState({ mask: '(99) 9999-9999' });
                },
                cpf: function cpf() {
                    return _this3.setState({ mask: '999.999.999-99' });
                },
                cnpj: function cnpj() {
                    return _this3.setState({ mask: '999.999.999/9999-99' });
                },
                cep: function cep() {
                    return _this3.setState({ mask: '99999-999' });
                },
                plate: function plate() {
                    return console.info('mask plate not found');
                },
                date: function date() {
                    return _this3.setState({ mask: '99/99/9999' });
                },
                'date-short': function dateShort() {
                    return _this3.setState({ mask: '99/9999' });
                },
                renavam: function renavam() {
                    return _this3.setState({ mask: '99999999999' });
                },
                price: function price() {
                    return _this3.setState({ mask: '999.999.999.999,99', maskReverse: true });
                },
                time: function time() {
                    return _this3.setState({ mask: '99:99' });
                }
            };
            var type = fn[this.props.type];
            if (type) {
                type();
            }
        }
    }, {
        key: 'handlerHasValue',
        value: function handlerHasValue(input) {
            if (input.value !== '') {
                this.wrapperClassAdd('inputtext--opened', true);
            } else {
                this.wrapperClassRemove('inputtext--opened', true);
            }
        }
    }, {
        key: 'handlerFocus',
        value: function handlerFocus(e) {
            this.setState({ readOnly: false });
            this.handlerHasValue(e.target);
            this.wrapperClassAdd('inputtext--active', true);
        }
    }, {
        key: 'handlerBlur',
        value: function handlerBlur(e) {
            this.handlerHasValue(e.target);
            this.handlerValidate(e.target);
            this.wrapperClassRemove('inputtext--active', true);
        }
    }, {
        key: 'handlerInput',
        value: function handlerInput(e) {
            var target = e.target;

            this.wrapperClassAdd('inputtext--opened', target.value !== '');
            this.renderMask(target);
            if (this.props.type === 'email' || this.props.type === 'email-login') {
                target.value = target.value.toLowerCase();
            }
        }
    }, {
        key: 'handlerError',
        value: function handlerError(error) {
            if (error) {
                this.wrapperClassAdd('inputtext--error', true);
            } else {
                this.setState({ description: this.props.description });
                this.wrapperClassRemove('inputtext--error', true);
            }
        }
    }, {
        key: 'handlerCustomEvent',
        value: function handlerCustomEvent(event, object) {
            this.props[event](object);
        }
    }, {
        key: 'handlerValidate',
        value: function handlerValidate(input) {
            var value = input.value;
            var props = this.props;
            var type = props.type,
                min = props.min,
                max = props.max;

            var error = void 0;
            switch (type) {
                case 'email':
                    {
                        error = !/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
                        this.setState({ description: 'Digite um e-mail válido' });
                        break;
                    }
                case 'email-login':
                    {
                        error = !/^([a-zA-Z0-9_\-.]+)@([a-zA-Z-0-9\-.])+\.[a-z]{1,10}(\.[a-z]{1,3})?$/.test(value);
                        this.setState({ description: 'Digite um e-mail válido' });
                        break;
                    }
                case 'tel':
                    {
                        error = !/^\(\d{2}\)\s?(\d{4}-\d{4,5}|\d{4,5}-\d{4})$/.test(value);
                        this.setState({ description: 'Digite um telefone válido' });
                        break;
                    }
                case 'tel-home':
                    {
                        error = !/^\(\d{2}\)\s?\d{4}-\d{4}$/.test(value);
                        this.setState({ description: 'Digite um telefone fixo válido' });
                        break;
                    }
                case 'cpf':
                    {
                        error = function () {
                            var cpf = value.replace(/[^\d]+/g, '');
                            var add = void 0;
                            var rev = void 0;
                            var i = void 0;
                            if (cpf === '') {
                                return true;
                            }
                            if (/0{11}|1{11}|2{11}|3{11}|4{11}|5{11}|6{11}|7{11}|8{11}|9{11}/.test(cpf)) {
                                return true;
                            }

                            add = 0;
                            for (i = 0; i < 9; i++) {
                                add += parseInt(cpf.charAt(i), 10) * (10 - i);
                            }
                            rev = 11 - add % 11;
                            if (rev === 10 || rev === 11) {
                                rev = 0;
                            }
                            if (rev !== parseInt(cpf.charAt(9), 10)) {
                                return true;
                            }

                            add = 0;
                            for (i = 0; i < 10; i++) {
                                add += parseInt(cpf.charAt(i), 10) * (11 - i);
                            }
                            rev = 11 - add % 11;
                            if (rev === 10 || rev === 11) {
                                rev = 0;
                            }
                            if (rev !== parseInt(cpf.charAt(10), 10)) {
                                return true;
                            }
                            return false;
                        }();
                        this.setState({ description: 'Digite um CPF válido' });
                        break;
                    }
                case 'cnpj':
                    {
                        error = function () {
                            var cnpj = value.replace(/\D/g, '');
                            var i = void 0;
                            var tamanho = void 0;
                            var numeros = void 0;
                            var soma = void 0;
                            var pos = void 0;
                            var resultado = void 0;
                            if (cnpj === '' || cnpj.length !== 14 || /0{14}|1{14}|2{14}|3{14}|4{14}|5{14}|6{14}|7{14}|8{14}|9{14}/.test(cnpj)) {
                                return true;
                            }

                            tamanho = cnpj.length - 2;
                            numeros = cnpj.substring(0, tamanho);
                            var digitos = cnpj.substring(tamanho);
                            soma = 0;
                            pos = tamanho - 7;
                            for (i = tamanho; i >= 1; i--) {
                                soma += numeros.charAt(tamanho - i) * pos--;
                                if (pos < 2) {
                                    pos = 9;
                                }
                            }
                            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                            if (resultado !== parseInt(digitos.charAt(0), 10)) {
                                return true;
                            }
                            numeros = cnpj.substring(0, ++tamanho);
                            soma = 0;
                            pos = tamanho - 7;
                            for (i = tamanho; i >= 1; i--) {
                                soma += numeros.charAt(tamanho - i) * pos--;
                                if (pos < 2) {
                                    pos = 9;
                                }
                            }
                            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                            if (resultado !== parseInt(digitos.charAt(1), 10)) {
                                return true;
                            }
                            return false;
                        }();
                        this.setState({ description: 'Digite um CNPJ válido' });
                        break;
                    }
                case 'name-surname':
                    {
                        error = !/^\D{2,}\s\D{2,}$/.test(value);
                        this.setState({ description: 'Digite um nome e sobrenome válido' });
                        break;
                    }
                case 'cep':
                    {
                        error = !/^\w{5}-\w{3}$/.test(value);
                        this.setState({ description: 'Digite um CEP válido' });
                        break;
                    }
                case 'char-between':
                    {
                        error = value.length < (min || 1) || value.length > (max || 8);
                        this.setState({ description: 'Digite uma palavra com mais de ' + min + ' caracteres e menos de ' + max });
                        break;
                    }
                case 'plate':
                    {
                        error = !/^\[A-Z]{3}-\d{4}$/.test(value) || !/^\[A-Z]{3}\d[A-J]\d{2}$/.test(value);
                        this.setState({ description: 'Digite uma placa de veículo válida' });
                        break;
                    }
                case 'date':
                    {
                        error = !/^\w{2}\/\w{2}\/\w{4}$/.test(value);
                        this.setState({ description: 'Digite uma data válida' });
                        break;
                    }
                case 'date-short':
                    {
                        error = !/^\d{2}\/\d{2,4}$/.test(value);
                        this.setState({ description: 'Digite uma data simplificada válida' });
                        break;
                    }
                case 'renavam':
                    {
                        error = function () {
                            var d = value.split('');
                            var soma = 0;
                            var valor = 0;
                            var digito = 0;
                            var x = 0;
                            for (var i = 5; i >= 2; i--) {
                                soma += d[x] * i;
                                x++;
                            }
                            valor = soma % 11;
                            if (valor === 11 || valor === 0 || valor >= 10) {
                                digito = 0;
                            } else {
                                digito = valor;
                            }
                            return digito === d[4];
                        }();
                        this.setState({ description: 'Digite um renavam válido' });
                        break;
                    }
                case 'password':
                    {
                        if (!this.props.noForce) {
                            error = !/(?=^.{8,}$)((?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*\d{1,})(?=.*\W{1,}))^.*$/.test(value);
                            this.setState({ description: 'Digite todos critérios de senha válidos' });
                        }
                        break;
                    }
                case 'url':
                    {
                        error = !/^https?:\/\/(\w|-)+\.(\w|-)+((\.|\/)[a-z]+(\.[a-z]+(\/\w+)?)?)?.+$/.test(value);
                        this.setState({ description: 'Digite uma url válida (ex.: http://www.webmotors.com.br)' });
                        break;
                    }
                case 'time':
                    {
                        error = !/^[0-2]\d:[0-5]\d$/.test(value);
                        this.setState({ description: 'Digite um horário válido' });
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            if ((max || min) && type !== 'char-between') {
                var valueNumber = Number(value);
                var notNumber = /\D/.test(value);
                if (notNumber) {
                    error = true;
                    this.setState({ description: 'Digite um número válido' });
                }
                if (props.max && max < valueNumber) {
                    error = true;
                    this.setState({ description: 'Digite um n\xFAmero menor que ' + max });
                }
                if (props.min && min > valueNumber) {
                    error = true;
                    this.setState({ description: 'Digite um n\xFAmero maior que ' + min });
                }
            }
            if (props.equal) {
                var inputEqual = document.querySelector(props.equal);
                if (inputEqual && inputEqual.value !== value) {
                    error = true;
                    var inputName = inputEqual.previousElementSibling;
                    this.setState({ description: 'Digite um valor igual ao campo "' + inputName.textContent + '"' });
                }
            }
            if (value.length === 0 && props.required) {
                error = true;
                this.setState({ description: 'Preencha o campo obrigatório' });
            }
            if (value.length === 0 && !props.required) {
                error = false;
                this.setState({ description: props.description });
            }
            this.handlerError(error);
        }
    }, {
        key: 'handlerPasswordToggle',
        value: function handlerPasswordToggle() {
            if (this.state.passwordView) {
                this.setState({ passwordClass: 'inputtext__button--view', passwordView: false });
                this.input.current.type = 'text';
            } else {
                this.setState({ passwordClass: '', passwordView: true });
                this.input.current.type = 'password';
            }
        }
    }, {
        key: 'renderMask',
        value: function renderMask(tag) {
            var mask = this.props.mask || this.state.mask;
            if (mask) {
                var input = tag;
                var valueCount = 0;
                var valueArray = input.value.replace(/\D/g, '').split('');
                var valueFinal = [];
                var maskArray = mask.split('');
                if (this.state.maskReverse) {
                    var maskArrayReverse = maskArray;
                    var valueArrayReverse = valueArray;
                    maskArrayReverse.reverse();
                    valueArrayReverse.reverse();
                    maskArrayReverse.forEach(function (item, i) {
                        if (valueArrayReverse[i - valueCount]) {
                            if (/\D/.test(item)) {
                                valueFinal.push(item);
                                valueCount++;
                            } else {
                                valueFinal.push(valueArrayReverse[i - valueCount]);
                            }
                        }
                    });
                    valueFinal.reverse();
                } else {
                    maskArray.forEach(function (item) {
                        if (valueCount < valueArray.length) {
                            if (/\D/.test(item)) {
                                valueFinal.push(item);
                            } else {
                                valueFinal.push(valueArray[valueCount]);
                                valueCount++;
                            }
                        }
                    });
                }
                input[input.value ? 'value' : 'textContent'] = valueFinal.join('');
            }
        }
    }, {
        key: 'renderPlaceholder',
        value: function renderPlaceholder() {
            return _react2.default.createElement(
                'span',
                { className: 'inputtext__placeholder' },
                this.props.placeholder
            );
        }
    }, {
        key: 'renderDescription',
        value: function renderDescription() {
            return _react2.default.createElement(
                'div',
                { className: 'inputtext__description' },
                this.props.description || this.state.description
            );
        }
    }, {
        key: 'renderType',
        value: function renderType() {
            var _this4 = this;

            var props = this.props;

            var value = this.input.current && this.input.current.value || props.value;
            switch (props.type) {
                case 'checkbox':
                    {
                        return _react2.default.createElement(
                            'label',
                            { className: 'inputcheckbox ' + (props.disabled ? 'inputcheckbox--disabled' : ''), htmlFor: this.state.id },
                            _react2.default.createElement('input', {
                                type: 'checkbox',
                                className: 'inputcheckbox__input',
                                ref: this.input,
                                id: this.state.id,
                                name: props.name,
                                defaultValue: value,
                                defaultChecked: props.checked,
                                disabled: props.disabled,
                                onChange: function onChange(e) {
                                    if (props.onChange && !props.disabled) _this4.handlerCustomEvent('onChange', e);
                                },
                                'data-type': this.props['data-type']
                            }),
                            _react2.default.createElement('span', { className: 'inputcheckbox__check' }),
                            _react2.default.createElement(
                                'div',
                                { className: 'inputcheckbox__label' },
                                props.label
                            )
                        );
                    }
                case 'radio':
                    {
                        return _react2.default.createElement(
                            'label',
                            { className: 'inputradio ' + (props.disabled ? 'inputradio--disabled' : ''), htmlFor: this.state.id },
                            _react2.default.createElement('input', {
                                type: 'radio',
                                className: 'inputradio__input',
                                ref: this.input,
                                id: this.state.id,
                                name: props.name,
                                defaultValue: value,
                                defaultChecked: props.checked,
                                disabled: props.disabled,
                                onChange: function onChange(e) {
                                    if (props.onChange) _this4.handlerCustomEvent('onChange', e);
                                },
                                'data-type': this.props['data-type']
                            }),
                            _react2.default.createElement('span', { className: 'inputradio__check' }),
                            _react2.default.createElement(
                                'div',
                                { className: 'inputradio__label' },
                                props.label
                            )
                        );
                    }
                case 'select':
                    {
                        return props.disabled ? _react2.default.createElement(
                            'div',
                            { ref: this.wrapper, className: 'inputtext ' + (props.error ? 'inputtext--error' : '') + ' ' + (value ? 'inputtext--opened' : '') },
                            this.renderPlaceholder(),
                            _react2.default.createElement(
                                'div',
                                { className: 'inputtext__input', id: this.state.id },
                                value
                            ),
                            this.renderDescription()
                        ) : _react2.default.createElement(
                            'div',
                            { ref: this.wrapper, className: 'inputtext ' + (props.error ? 'inputtext--error' : '') + ' ' + (value ? 'inputtext--opened' : '') },
                            _react2.default.createElement(
                                'span',
                                { className: 'inputtext__select' },
                                this.renderPlaceholder()
                            ),
                            _react2.default.createElement(
                                'select',
                                {
                                    className: 'inputtext__input',
                                    ref: this.input,
                                    id: this.state.id,
                                    name: props.name,
                                    defaultValue: value,
                                    onChange: function onChange(e) {
                                        _this4.wrapperClassAdd('inputtext--opened', e.target.value !== '');
                                        _this4.wrapperClassRemove('inputtext--opened', e.target.value === '');
                                        if (props.onChange) _this4.handlerCustomEvent('onChange', e);
                                    },
                                    onFocus: function onFocus(e) {
                                        _this4.handlerFocus(e);
                                        if (props.onFocus) _this4.handlerCustomEvent('onFocus', e);
                                    },
                                    onBlur: function onBlur(e) {
                                        _this4.handlerBlur(e);
                                        if (props.onBlur) _this4.handlerCustomEvent('onBlur', e);
                                    },
                                    'data-type': this.props['data-type']
                                },
                                value === '' && _react2.default.createElement(
                                    'option',
                                    { value: '' },
                                    ' '
                                ),
                                props.children
                            ),
                            this.renderDescription()
                        );
                    }
                case 'switch':
                    {
                        return _react2.default.createElement(
                            'label',
                            { className: 'inputswitch ' + (props.disabled ? 'inputswitch--disabled' : ''), htmlFor: this.state.id },
                            _react2.default.createElement('input', {
                                className: 'inputswitch__input',
                                type: 'checkbox',
                                ref: this.input,
                                name: props.name,
                                defaultValue: value,
                                id: this.state.id,
                                defaultChecked: props.checked,
                                disabled: props.disabled,
                                onChange: function onChange(e) {
                                    if (props.onChange) _this4.handlerCustomEvent('onChange', e);
                                },
                                tabIndex: '-1',
                                'data-type': this.props['data-type']
                            }),
                            _react2.default.createElement('div', { className: 'inputswitch__scroll' }),
                            _react2.default.createElement(
                                'div',
                                { className: 'inputswitch__label' },
                                props.label
                            )
                        );
                    }
                default:
                    {
                        var inputMaxLength = function inputMaxLength() {
                            if (props.type === 'email' || props.type === 'email-login') {
                                return 150;
                            }
                            if (props.type === 'password') {
                                return 40;
                            }
                            return props.maxlength;
                        };
                        return props.disabled ? _react2.default.createElement(
                            'div',
                            { ref: this.wrapper, className: 'inputtext ' + (props.error ? 'inputtext--error' : '') + ' ' + (value ? 'inputtext--opened' : '') },
                            this.renderPlaceholder(),
                            _react2.default.createElement(
                                'div',
                                { className: 'inputtext__input', ref: this.input, id: this.state.id },
                                value
                            ),
                            this.renderDescription()
                        ) : _react2.default.createElement(
                            'label',
                            {
                                ref: this.wrapper,
                                className: 'inputtext ' + (props.error ? 'inputtext--error' : '') + ' ' + (value ? 'inputtext--opened' : ''),
                                htmlFor: this.state.id
                            },
                            this.renderPlaceholder(),
                            _react2.default.createElement('input', {
                                type: props.type === 'password' ? 'password' : 'text',
                                className: 'inputtext__input',
                                ref: this.input,
                                id: this.state.id,
                                name: props.name,
                                defaultValue: value,
                                readOnly: this.state.readOnly,
                                tabIndex: props.tabIndex ? props.tabIndex : '0',
                                maxLength: inputMaxLength(),
                                onFocus: function onFocus(e) {
                                    _this4.handlerFocus(e);
                                    if (props.onFocus) _this4.handlerCustomEvent('onFocus', e);
                                },
                                onBlur: function onBlur(e) {
                                    _this4.handlerBlur(e);
                                    if (props.onBlur) _this4.handlerCustomEvent('onBlur', e);
                                },
                                onInput: function onInput(e) {
                                    _this4.handlerInput(e);
                                    if (props.onInput) _this4.handlerCustomEvent('onInput', e);
                                },
                                'data-type': this.props['data-type']
                            }),
                            props.type === 'password' && _react2.default.createElement(
                                'button',
                                { tabIndex: '-1', 'aria-hidden': 'true', className: 'inputtext__button ' + this.state.passwordClass, type: 'button', onClick: function onClick() {
                                        return _this4.handlerPasswordToggle();
                                    } },
                                _react2.default.createElement(_webmotorsSvg2.default, { className: 'inputtext__password--show', src: '/assets/img/icons/eye-view.svg' }),
                                _react2.default.createElement(_webmotorsSvg2.default, { className: 'inputtext__password--hide', src: '/assets/img/icons/eye-hide.svg' })
                            ),
                            this.renderDescription()
                        );
                    }
            }
        }
    }, {
        key: 'render',
        value: function render() {
            return this.renderType();
        }
    }]);
    return WebInput;
}(_react.Component);

exports.default = WebInput;