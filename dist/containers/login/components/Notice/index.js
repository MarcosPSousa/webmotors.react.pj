'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Notice = function (_Component) {
    (0, _inherits3.default)(Notice, _Component);

    function Notice(props) {
        (0, _classCallCheck3.default)(this, Notice);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Notice.__proto__ || (0, _getPrototypeOf2.default)(Notice)).call(this, props));

        _this.state = {};
        _this.text = _this.props.text;
        return _this;
    }

    (0, _createClass3.default)(Notice, [{
        key: 'render',
        value: function render() {
            var text = this.props.text;

            var type = this.props.type === undefined ? 'error' : this.props.type;
            return _react2.default.createElement(
                'div',
                { role: 'alert', className: 'notice notice--' + type + ' ' + (text ? 'notice--active' : '') },
                _react2.default.createElement(
                    'div',
                    { className: 'notice__content' },
                    text
                )
            );
        }
    }]);
    return Notice;
}(_react.Component);

exports.default = Notice;