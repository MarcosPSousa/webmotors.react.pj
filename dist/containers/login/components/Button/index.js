'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = function (_Component) {
    (0, _inherits3.default)(Button, _Component);

    function Button() {
        (0, _classCallCheck3.default)(this, Button);
        return (0, _possibleConstructorReturn3.default)(this, (Button.__proto__ || (0, _getPrototypeOf2.default)(Button)).apply(this, arguments));
    }

    (0, _createClass3.default)(Button, [{
        key: 'renderLink',
        value: function renderLink() {
            return this.props.target === '_blank' ? _react2.default.createElement(
                'a',
                { target: '_blank', rel: 'noopener noreferrer', href: this.props.url, className: this.renderClassName() },
                this.props.children
            ) : _react2.default.createElement(
                'a',
                { href: this.props.url, className: this.renderClassName() },
                this.props.children
            );
        }
    }, {
        key: 'renderClassName',
        value: function renderClassName() {
            var name = ['button'];
            name.push(this.props.mode ? 'button__' + this.props.mode : 'button__default');
            if (this.props.outline) {
                name.push('button--outline');
            }
            if (this.props.shadow) {
                name.push('button--shadow');
            }
            if (this.props.disabled) {
                name.push('button--disabled');
            }
            if (this.props.className) {
                name.push(this.props.className);
            }
            return name.join(' ');
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var button = this.props.type || 'button';
            return this.props.url ? this.renderLink() : _react2.default.createElement(
                'button',
                { onClick: function onClick() {
                        return _this2.props.onClick && _this2.props.onClick();
                    }, type: button, disabled: this.props.disabled, className: this.renderClassName() },
                this.props.children
            );
        }
    }]);
    return Button;
}(_react.Component);

exports.default = Button;