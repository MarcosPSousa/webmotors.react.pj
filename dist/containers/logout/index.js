'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

var _config = require('webmotors-react-pj/config');

var _load = require('webmotors-react-pj/load');

var _load2 = _interopRequireDefault(_load);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Logout = function (_Component) {
    (0, _inherits3.default)(Logout, _Component);

    function Logout(props) {
        (0, _classCallCheck3.default)(this, Logout);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Logout.__proto__ || (0, _getPrototypeOf2.default)(Logout)).call(this, props));

        ['AppTokenSSO', 'CockpitChannelsLeads', 'CockpitFilter', 'storeProductHire'].forEach(function (item) {
            return window.localStorage.removeItem(item);
        });
        (0, _utils.cookieRemoveAll)(['CockpitNotifications']);
        return _this;
    }

    (0, _createClass3.default)(Logout, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            setTimeout(function () {
                return window.location.href = '/';
            }, 2000);
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                'div',
                { role: 'main', className: 'content' },
                _react2.default.createElement('iframe', { title: 'Logout Sisense', src: _config.ApiSisense + 'auth/logout', style: { width: 0, height: 0, border: 'none' }, frameBorder: '0' }),
                _react2.default.createElement(_load2.default, { text: 'Aguarde enquando deslogamos com seguran\xE7a' })
            );
        }
    }]);
    return Logout;
}(_react.Component);

exports.default = Logout;