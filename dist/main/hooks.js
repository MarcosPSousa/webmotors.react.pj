'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.unloggedPage = exports.cockpitLogged = exports.bokey = exports.pathname = undefined;

var _utils = require('webmotors-react-pj/utils');

var pathname = window.location.pathname;

var bokey = Boolean((0, _utils.paramURL)('bokey'));
var cockpitLogged = Boolean((0, _utils.cookieGet)('CockpitLogged'));
var unloggedUrl = ['/', '/logout', '/redefinir-senha', '/redefinir-senha/email-enviado', '/redefinir-senha/resetar-senha', '/redefinir-senha/troca-de-seguranca', '/usuario/atualizar', '/usuario/senha-atualizada', '/usuario/introducao-cockpit', '/solucoes/crm', '/solucoes/estoque', '/solucoes/autoguru', '/solucoes/universidade', '/solucoes/feiroes'];
var unloggedPage = unloggedUrl.includes(pathname);

exports.pathname = pathname;
exports.bokey = bokey;
exports.cockpitLogged = cockpitLogged;
exports.unloggedPage = unloggedPage;