'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _logged = require('webmotors-react-pj/main/logged');

var _logged2 = _interopRequireDefault(_logged);

var _unlogged = require('webmotors-react-pj/main/unlogged');

var _unlogged2 = _interopRequireDefault(_unlogged);

var _style = require('webmotors-react-pj/main/content/style');

var _style2 = _interopRequireDefault(_style);

var _nps = require('webmotors-react-pj/nps');

var _nps2 = _interopRequireDefault(_nps);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var logged = _ref.logged,
        unloggedPage = _ref.unloggedPage,
        children = _ref.children;
    return logged ? _react2.default.createElement(
        _logged2.default,
        null,
        children,
        !/\/logout/.test(window.location.pathname) && /(webmotors|cockpit).com.br\/(?!(crm|store|stock|mais-fidelidade))/.test(window.location.href) && _react2.default.createElement(_nps2.default, null)
    ) : _react2.default.createElement(
        _unlogged2.default,
        { unloggedPage: unloggedPage },
        children
    );
};