'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleContent = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n    min-height: calc(100vh - 152px);\n    display: flex;\n'], ['\n    flex: 1;\n    min-height: calc(100vh - 152px);\n    display: flex;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n'], ['\n    flex: 1;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject);

var StyleContent = exports.StyleContent = _styledComponents2.default.div(_templateObject2);