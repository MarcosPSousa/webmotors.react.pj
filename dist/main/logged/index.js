'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _logged = require('webmotors-react-pj/frame/logged');

var _style = require('webmotors-react-pj/main/logged/style');

var _utils = require('webmotors-react-pj/utils');

var _config = require('webmotors-react-pj/config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children,
        env = _ref.env;

    var _jwtGet = (0, _utils.jwtGet)(),
        v2 = _jwtGet.v2;

    var menu = {
        data: [{ menu: 'Início', url: _config.UrlCockpit, icone: '/product-inicio.svg', subMenu: [] }, {
            menu: 'CRM', url: '#', icone: '/product-crm.svg', subMenu: [{ menu: 'Painel', url: _config.UrlCRM + '/dashboard', icone: '', subMenu: [] }, { menu: 'Gerenciar leads', url: _config.UrlCRM + '/manage-leads', icone: '', subMenu: [] }, { menu: 'Minhas atividades', url: _config.UrlCRM + '/schedule', icone: '', subMenu: [] }, { menu: 'Relatórios', url: _config.UrlCRM + '/reports', icone: '', subMenu: [] }, { menu: 'Configurações', url: _config.UrlCRM + '/configuration', icone: '', subMenu: [] }, { menu: 'Integrações', url: _config.UrlCRM + '/integration', icone: '', subMenu: [] }]
        }, {
            menu: 'Gearbox', url: '#', icone: '/product-gearbox.svg', subMenu: [{ menu: 'Painel', url: '' + _config.UrlGearbox, icone: '', subMenu: [] }, { menu: 'Site', url: _config.UrlGearbox + '/site', icone: '', subMenu: [] }, { menu: 'Mídia', url: _config.UrlGearbox + '/midia', icone: '', subMenu: [] }]
        }, {
            menu: 'Estoque', url: '#', icone: '/product-estoque.svg', subMenu: [{ menu: 'Consultar veículo', url: _config.UrlEstoquePlataforma + '/gestao-estoque/consultar', icone: '', subMenu: [] }, { menu: 'Incluir veículo', url: _config.UrlEstoquePlataforma + '/gestao-estoque/incluir-anuncio', icone: '', subMenu: [] }, { menu: 'Comprar veículo', url: '' + _config.UrlComprarVeiculos, icone: '', subMenu: [] }, { menu: 'Vender veículo', url: _config.UrlEstoquePlataforma + '/vender-veiculo', icone: '', subMenu: [] }, { menu: 'Avaliar veículo', url: _config.UrlEstoque + '/avaliar-veiculo', icone: '', subMenu: [] }]
        }, { menu: '+ Fidelidade', url: '' + _config.UrlMaisFidelidade, icone: '/product-maisfidelidade.svg', subMenu: [] }, { menu: 'Universidade', url: _config.UrlUniversidade + '/wm-cockpit', icone: '/product-universidade.svg', subMenu: [] }, { menu: 'Autoguru', url: _config.UrlAutoguru + '/', icone: '/product-autoguru.svg', subMenu: [] }, {
            menu: 'Cockpit', url: '#', icone: '/product-cockpit-1.svg', subMenu: [{
                menu: 'Conta', url: '#', icone: '', subMenu: [{ menu: 'Termo de Adesão', url: _config.UrlCockpit + '/termos/pendentes', icone: '', subMenu: [] }, { menu: 'Usuários', url: _config.UrlCockpit + '/usuario/listagem/ativo', icone: '', subMenu: [] }, { menu: 'Meu Plano', url: _config.UrlCockpit + '/meu-plano/fatura', icone: '', subMenu: [] }]
            }, { menu: 'Dúvidas Frequentes', url: 'https://webmotors.zendesk.com/hc/pt-br/categories/360002515092-Lojas-Concession%C3%A1rias', icone: '', subMenu: [] }]
        }, { menu: 'Store', url: _config.UrlCockpit + '/store', icone: '/product-store.svg', subMenu: [] }],
        status: 200,
        success: true
    };
    !/local\./.test(window.location.hostname) && (0, _utils.cookieSet)('CockpitMenu', (0, _stringify2.default)(menu));
    return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement(_logged.Header, null),
        _react2.default.createElement(
            _style.StyleWrapper,
            null,
            _react2.default.createElement(_logged.Menu, null),
            _react2.default.createElement(
                _style.StyleContent,
                null,
                children
            )
        ),
        _react2.default.createElement(_logged.Footer, null)
    );
};