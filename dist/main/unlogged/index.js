'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

var _unlogged = require('webmotors-react-pj/frame/unlogged');

var _login = require('webmotors-react-pj/containers/login');

var _login2 = _interopRequireDefault(_login);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children,
        unloggedPage = _ref.unloggedPage;

    var child = children;

    var _jwtGet = (0, _utils.jwtGet)(),
        status = _jwtGet.status;

    var pathname = window.location.pathname;

    var urlAllowPendente = ['/usuario/introducao-cockpit', '/usuario/atualizar', '/usuario/senha-atualizada'];
    var urlAllowSenhaExpirada = ['/redefinir-senha/troca-de-seguranca'];
    if (status === 'Pendente' && !urlAllowPendente.includes(pathname) || status === 'senhaExpirada' && !urlAllowSenhaExpirada.includes(pathname)) {
        (0, _utils.cookieRemove)('CockpitLogged');
    }

    if (pathname === '/') {
        child = _react2.default.createElement(_login2.default, null);
    }

    return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement(_unlogged.Header, null),
        unloggedPage ? child : _react2.default.createElement(_login2.default, null),
        _react2.default.createElement(_unlogged.Footer, null)
    );
};