'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    flex: 1;\n    min-height: 80vh;\n    display: flex;\n    flex-direction: column;\n    flex-grow: 1;\n'], ['\n    flex: 1;\n    min-height: 80vh;\n    display: flex;\n    flex-direction: column;\n    flex-grow: 1;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    return _styledComponents2.default.div(_templateObject);
};