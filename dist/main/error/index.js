'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactHelmet = require('react-helmet');

var _reactHelmet2 = _interopRequireDefault(_reactHelmet);

var _config = require('webmotors-react-pj/config');

var _style = require('webmotors-react-pj/frame/header/logged/style');

var _style2 = _interopRequireDefault(_style);

var _logo = require('webmotors-react-pj/frame/header/logged/logo');

var _logo2 = _interopRequireDefault(_logo);

var _style3 = require('webmotors-react-pj/main/logged/style');

var _style4 = _interopRequireDefault(_style3);

var _style5 = require('webmotors-react-pj/frame/menu/style');

var _style6 = _interopRequireDefault(_style5);

var _style7 = require('webmotors-react-pj/main/style');

var _style8 = _interopRequireDefault(_style7);

var _pageStatus = require('webmotors-react-pj/page-status');

var _pageStatus2 = _interopRequireDefault(_pageStatus);

var _logged = require('webmotors-react-pj/frame/footer/logged');

var _logged2 = _interopRequireDefault(_logged);

var _style9 = require('webmotors-react-pj/frame/menu/list/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ErrorBoundary = function (_Component) {
    (0, _inherits3.default)(ErrorBoundary, _Component);

    function ErrorBoundary(props) {
        (0, _classCallCheck3.default)(this, ErrorBoundary);

        var _this = (0, _possibleConstructorReturn3.default)(this, (ErrorBoundary.__proto__ || (0, _getPrototypeOf2.default)(ErrorBoundary)).call(this, props));

        _this.state = {
            error: false
        };
        return _this;
    }

    (0, _createClass3.default)(ErrorBoundary, [{
        key: 'componentDidCatch',
        value: function componentDidCatch(error) {
            console.error(error);
            this.setState({ error: true });
        }
    }, {
        key: 'render',
        value: function render() {
            var title = 'Aplicação indisponível';
            return this.state.error ? _react2.default.createElement(
                _react.Fragment,
                null,
                _react2.default.createElement(
                    _reactHelmet2.default,
                    null,
                    _react2.default.createElement(
                        'title',
                        null,
                        title
                    )
                ),
                _react2.default.createElement(
                    _style2.default,
                    null,
                    _react2.default.createElement(_logo2.default, null)
                ),
                _react2.default.createElement(
                    _style4.default,
                    null,
                    _react2.default.createElement(
                        _style6.default,
                        null,
                        _react2.default.createElement(
                            _style9.StyleError,
                            { onClick: function onClick() {
                                    return window.location.reload();
                                } },
                            _react2.default.createElement(_style9.StyleErrorImg, { alt: '', src: _config.UrlCockpit + '/assets/img/icons/barrier.svg', 'aria-hidden': 'true' }),
                            'Recarregar p\xE1gina'
                        )
                    ),
                    _react2.default.createElement(
                        _style8.default,
                        null,
                        _react2.default.createElement(_pageStatus2.default, { title: title, text: 'Por favor entre em contato conosco.' })
                    )
                ),
                _react2.default.createElement(_logged2.default, null)
            ) : this.props.children;
        }
    }]);
    return ErrorBoundary;
}(_react.Component);

exports.default = ErrorBoundary;