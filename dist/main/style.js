'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleMain = exports.StyleCopi = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    0%{\n        opacity: 0;\n    }\n    100%{\n        opacity: 1;\n    }\n'], ['\n    0%{\n        opacity: 0;\n    }\n    100%{\n        opacity: 1;\n    }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    0%{\n        opacity: 1;\n    }\n    100%{\n        opacity: 0;\n    }\n'], ['\n    0%{\n        opacity: 1;\n    }\n    100%{\n        opacity: 0;\n    }\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    #drz_overlay_chat_external{\n        z-index: 5;\n        #drz_btn_open{\n            width: 40px !important;\n            min-width: 40px;\n            flex-direction: row-reverse;\n            margin: 0;\n            left: 28px;\n            bottom: 80px;\n        }\n        .drz_btn_open_minimized_arrow{\n            margin: 0;\n            display: none;\n        }\n        &:hover{\n            #drz_btn_open{\n                width: auto !important;\n            }\n            #drz_btn_txt{\n                opacity: 1;\n            }\n        }\n    }\n    .drz_main {\n        #drz_btn_txt{\n            width: auto;\n            padding: 11px 20px;\n            opacity: 0;\n            pointer-events: none;\n            transition: opacity 0.3s;\n            position: absolute;\n            white-space: nowrap;\n            left: 100%;\n            transform: translateX(8px);\n            &:after{\n                font-size: 1.2rem;\n                font-weight: bold;\n                font-family: \'Poppins\', sans-serif;\n                line-height: normal;\n            }\n        }\n        #drz_chat_overlay_main{\n            transform: translate(76px, -80px);\n            top: 0px !important;\n            &:not([style*="display: none"]){\n                animation: ', ' 0.4s forwards;\n            }\n            &:not([style*="top: 0px"]){\n                animation: ', ' 0.2s forwards;\n            }\n        }\n        #drz_btn_img{\n            box-shadow: none !important;\n        }\n    }\n'], ['\n    #drz_overlay_chat_external{\n        z-index: 5;\n        #drz_btn_open{\n            width: 40px !important;\n            min-width: 40px;\n            flex-direction: row-reverse;\n            margin: 0;\n            left: 28px;\n            bottom: 80px;\n        }\n        .drz_btn_open_minimized_arrow{\n            margin: 0;\n            display: none;\n        }\n        &:hover{\n            #drz_btn_open{\n                width: auto !important;\n            }\n            #drz_btn_txt{\n                opacity: 1;\n            }\n        }\n    }\n    .drz_main {\n        #drz_btn_txt{\n            width: auto;\n            padding: 11px 20px;\n            opacity: 0;\n            pointer-events: none;\n            transition: opacity 0.3s;\n            position: absolute;\n            white-space: nowrap;\n            left: 100%;\n            transform: translateX(8px);\n            &:after{\n                font-size: 1.2rem;\n                font-weight: bold;\n                font-family: \'Poppins\', sans-serif;\n                line-height: normal;\n            }\n        }\n        #drz_chat_overlay_main{\n            transform: translate(76px, -80px);\n            top: 0px !important;\n            &:not([style*="display: none"]){\n                animation: ', ' 0.4s forwards;\n            }\n            &:not([style*="top: 0px"]){\n                animation: ', ' 0.2s forwards;\n            }\n        }\n        #drz_btn_img{\n            box-shadow: none !important;\n        }\n    }\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    flex-direction: column;\n    position: relative;\n    flex: 1;\n    background-color: ', ';\n    justify-content: ', ';\n    min-height: 100vh;\n'], ['\n    display: flex;\n    flex-direction: column;\n    position: relative;\n    flex: 1;\n    background-color: ', ';\n    justify-content: ', ';\n    min-height: 100vh;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var animateOpen = (0, _styledComponents.keyframes)(_templateObject);

var animateClose = (0, _styledComponents.keyframes)(_templateObject2);

var StyleCopi = exports.StyleCopi = (0, _styledComponents.createGlobalStyle)(_templateObject3, animateOpen, animateClose);

var StyleMain = exports.StyleMain = _styledComponents2.default.div(_templateObject4, (0, _color2.default)('gray-4'), function (props) {
    return props.center ? 'center' : 'flex-start';
});