'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _pageStatus = require('webmotors-react-pj/page-status');

var _pageStatus2 = _interopRequireDefault(_pageStatus);

var _utils = require('webmotors-react-pj/utils');

var _error = require('webmotors-react-pj/main/error');

var _error2 = _interopRequireDefault(_error);

var _content = require('webmotors-react-pj/main/content');

var _content2 = _interopRequireDefault(_content);

var _hooks = require('webmotors-react-pj/main/hooks');

var _style = require('webmotors-react-pj/main/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children;

    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        loaded = _useState2[0],
        setLoaded = _useState2[1];

    var _useState3 = (0, _react.useState)(false),
        _useState4 = (0, _slicedToArray3.default)(_useState3, 2),
        logged = _useState4[0],
        setLogged = _useState4[1];

    var hostname = window.location.hostname;

    var env = 'production';
    if (/^local\./.test(hostname)) {
        env = 'development';
    }
    if (/^(hcockpit|hk|hestoque|hgb)\./.test(hostname)) {
        env = 'homologation';
    }
    if (/^azul\./.test(hostname)) {
        env = 'blue';
    }
    var loggedView = (!_hooks.unloggedPage || _hooks.cockpitLogged && _hooks.pathname === '/') && ((0, _utils.jwtGet)().status === 'Migrado' || (0, _utils.jwtGet)().status === 'Hub') && !_hooks.bokey && _hooks.pathname !== '/logout';

    var Container = function Container() {
        return _react2.default.createElement(
            _content2.default,
            { logged: logged, unloggedPage: _hooks.unloggedPage, env: env },
            children
        );
    };

    (0, _react.useEffect)(function () {
        setLoaded(true);
        setLogged(loggedView);
        if (_hooks.bokey) {
            (0, _utils.cookieRemoveAll)();
        }
    }, []);

    (0, _react.useEffect)(function () {
        return setLogged(loggedView);
    }, [_hooks.pathname]);

    return _react2.default.createElement(
        _error2.default,
        null,
        _react2.default.createElement(
            _style.StyleMain,
            null,
            loaded ? _react2.default.createElement(Container, null) : _react2.default.createElement(_pageStatus2.default, null),
            _react2.default.createElement(_style.StyleCopi, null)
        )
    );
};