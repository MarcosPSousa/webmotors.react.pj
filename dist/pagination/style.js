'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.StyleItemLink = exports.StyleCurrent = exports.StyleItem = exports.StyleWrapper = undefined;

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    display: flex;\n    justify-content: center;\n    white-space: nowrap;\n    padding-top: 32px;\n    font-size: 1.2rem;\n    font-weight: bold;\n    color: ', ';\n'], ['\n    display: flex;\n    justify-content: center;\n    white-space: nowrap;\n    padding-top: 32px;\n    font-size: 1.2rem;\n    font-weight: bold;\n    color: ', ';\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    padding: 0 4px;\n    cursor: default;\n'], ['\n    padding: 0 4px;\n    cursor: default;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n    color: ', ';\n'], ['\n    color: ', ';\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n    padding: 0 4px; \n'], ['\n    padding: 0 4px; \n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _reactRouterDom = require('react-router-dom');

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var StyleWrapper = exports.StyleWrapper = _styledComponents2.default.div(_templateObject, (0, _color2.default)('gray-3'));

var StyleItem = exports.StyleItem = _styledComponents2.default.div(_templateObject2);

var StyleCurrent = exports.StyleCurrent = (0, _styledComponents2.default)(StyleItem)(_templateObject3, (0, _color2.default)('gray'));

var StyleItemLink = exports.StyleItemLink = (0, _styledComponents2.default)(_reactRouterDom.Link)(_templateObject4);