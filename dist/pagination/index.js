'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/pagination/style');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (props) {
    var _props$page = props.page,
        pageInitial = _props$page === undefined ? 1 : _props$page,
        _props$totalPages = props.totalPages,
        totalPages = _props$totalPages === undefined ? 1 : _props$totalPages,
        _props$maxItems = props.maxItems,
        maxItems = _props$maxItems === undefined ? 5 : _props$maxItems,
        _props$queryPage = props.queryPage,
        queryPage = _props$queryPage === undefined ? 'page' : _props$queryPage,
        _props$onClickItem = props.onClickItem,
        onClickItem = _props$onClickItem === undefined ? function () {} : _props$onClickItem;

    var _useState = (0, _react.useState)(pageInitial),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        page = _useState2[0],
        setPage = _useState2[1];

    var urlClear = function urlClear(value) {
        var _window$location = window.location,
            search = _window$location.search,
            pathname = _window$location.pathname;

        var urlParamRegex = new RegExp('((&|\\?)' + queryPage + ')=(\\d+)');
        var replace = search.replace(urlParamRegex, '$1=' + value);
        return '' + pathname + replace;
    };

    var handleClick = function handleClick(event) {
        event.preventDefault();
        var pageCurrent = Number(event.target.textContent);
        setPage(pageCurrent);
        onClickItem({ page: pageCurrent, event: event });
    };

    var renderFirstPages = function renderFirstPages(itemsBefore, itemsAfter) {
        for (var i = 1; i < page; i++) {
            itemsBefore.unshift(_react2.default.createElement(
                _style.StyleItemLink,
                { onClick: function onClick(e) {
                        return handleClick(e);
                    }, to: urlClear('' + (page - i)), key: i },
                page - i
            ));
        }
        for (var _i = page + 1; _i < Math.min(maxItems, totalPages); _i++) {
            itemsAfter.push(_react2.default.createElement(
                _style.StyleItemLink,
                { onClick: function onClick(e) {
                        return handleClick(e);
                    }, to: urlClear('' + _i), key: _i },
                _i
            ));
        }
        if (totalPages > maxItems) {
            itemsAfter.push(_react2.default.createElement(
                _style.StyleItem,
                { key: '...' },
                '...'
            ));
        }
        if (page < totalPages) {
            itemsAfter.push(_react2.default.createElement(
                _style.StyleItemLink,
                { onClick: function onClick(e) {
                        return handleClick(e);
                    }, to: urlClear('' + totalPages), key: 'last' },
                totalPages
            ));
        }
    };

    var renderBetweenPages = function renderBetweenPages(itemsBefore, itemsAfter) {
        for (var i = page; i > page - 1; i--) {
            itemsBefore.unshift(_react2.default.createElement(
                _style.StyleItemLink,
                { onClick: function onClick(e) {
                        return handleClick(e);
                    }, to: urlClear('' + (i - 1)), key: i },
                i - 1
            ));
        }
        for (var _i2 = page; _i2 < page + 1; _i2++) {
            itemsAfter.push(_react2.default.createElement(
                _style.StyleItemLink,
                { onClick: function onClick(e) {
                        return handleClick(e);
                    }, to: urlClear('' + (_i2 + 1)), key: _i2 },
                _i2 + 1
            ));
        }
        itemsBefore.unshift(_react2.default.createElement(
            _style.StyleItemLink,
            { onClick: function onClick(e) {
                    return handleClick(e);
                }, to: urlClear('1'), key: 'last' },
            '1'
        ), _react2.default.createElement(
            _style.StyleItem,
            { key: '...' },
            '...'
        ));
        itemsAfter.push(_react2.default.createElement(
            _style.StyleItem,
            { key: '...' },
            '...'
        ), _react2.default.createElement(
            _style.StyleItemLink,
            { onClick: function onClick(e) {
                    return handleClick(e);
                }, to: urlClear('' + totalPages), key: 'last' },
            totalPages
        ));
    };

    var renderLastPages = function renderLastPages(itemsBefore, itemsAfter) {
        for (var i = page; i < totalPages; i++) {
            itemsAfter.push(_react2.default.createElement(
                _style.StyleItemLink,
                { onClick: function onClick(e) {
                        return handleClick(e);
                    }, to: urlClear('' + (i + 1)), key: i },
                i + 1
            ));
        }
        for (var _i3 = 1; _i3 < maxItems - itemsAfter.length - 1; _i3++) {
            itemsBefore.unshift(_react2.default.createElement(
                _style.StyleItemLink,
                { onClick: function onClick(e) {
                        return handleClick(e);
                    }, to: urlClear('' + (page - _i3)), key: _i3 },
                page - _i3
            ));
        }
        itemsBefore.unshift(_react2.default.createElement(
            _style.StyleItemLink,
            { onClick: function onClick(e) {
                    return handleClick(e);
                }, to: urlClear('1'), key: 'last' },
            '1'
        ), _react2.default.createElement(
            _style.StyleItem,
            { key: '...' },
            '...'
        ));
    };

    var renderPagination = function renderPagination() {
        var itemsBefore = [];
        var itemsAfter = [];
        if (page <= Math.ceil(maxItems / 2)) {
            renderFirstPages(itemsBefore, itemsAfter);
        } else {
            if (page >= totalPages - Math.ceil(maxItems / 2)) {
                renderLastPages(itemsBefore, itemsAfter);
            } else {
                renderBetweenPages(itemsBefore, itemsAfter);
            }
        }
        return _react2.default.createElement(
            _style.StyleWrapper,
            null,
            itemsBefore,
            _react2.default.createElement(
                _style.StyleCurrent,
                null,
                page
            ),
            itemsAfter
        );
    };

    return totalPages > 1 ? renderPagination() : false;
};