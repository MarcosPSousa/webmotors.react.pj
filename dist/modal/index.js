'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _webmotorsSvg = require('webmotors-svg');

var _webmotorsSvg2 = _interopRequireDefault(_webmotorsSvg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WebModal = function (_Component) {
    (0, _inherits3.default)(WebModal, _Component);

    function WebModal(props) {
        (0, _classCallCheck3.default)(this, WebModal);

        var _this = (0, _possibleConstructorReturn3.default)(this, (WebModal.__proto__ || (0, _getPrototypeOf2.default)(WebModal)).call(this, props));

        _this.state = {};
        _this.modal = (0, _react.createRef)();
        _this.wrap = (0, _react.createRef)();
        _this.content = (0, _react.createRef)();
        return _this;
    }

    (0, _createClass3.default)(WebModal, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.handleZIndex();
            if (this.props.onOpen) {
                this.props.onOpen();
            }
        }
    }, {
        key: 'handleZIndex',
        value: function handleZIndex() {
            if (this.props.classZIndex) {
                document.querySelector(this.props.classZIndex).classList[this.state.close ? 'remove' : 'add']('modal__zindex');
            }
        }
    }, {
        key: 'handleClose',
        value: function handleClose() {
            var _this2 = this;

            this.setState({
                close: true
            }, function () {
                return _this2.modal.current.addEventListener('animationend', function () {
                    if (_this2.props.onClose) {
                        _this2.props.onClose();
                    } else {
                        var internal = _this2._reactInternalFiber;
                        var parent = internal._debugOwner;

                        if (parent) {
                            parent.stateNode.setState((0, _defineProperty3.default)({}, _this2.props.stateToggle || 'modal', false));
                        }
                    }
                    _this2.handleZIndex();
                });
            });
        }
    }, {
        key: 'handleScrollEnter',
        value: function handleScrollEnter() {
            var current = this.content.current;
            var offsetHeight = current.offsetHeight;
            var scrollHeight = current.scrollHeight;

            current.classList[offsetHeight === scrollHeight ? 'add' : 'remove']('modal__content--noscroll');
        }
    }, {
        key: 'handleScrollLeave',
        value: function handleScrollLeave() {
            var current = this.content.current;

            current.classList.remove('modal__content--noscroll');
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            return _react2.default.createElement(
                'div',
                { ref: this.modal, className: 'modal ' + (this.state.close ? 'modal--close' : '') },
                _react2.default.createElement('div', { 'aria-hidden': 'true', className: 'modal__bg', onClick: function onClick() {
                        return _this3.handleClose();
                    } }),
                _react2.default.createElement(
                    'div',
                    { className: 'modal__box ' + (this.props.size ? 'modal__box--' + this.props.size : '') },
                    this.props.icon && _react2.default.createElement(
                        'div',
                        { className: 'ta-c' },
                        _react2.default.createElement(_webmotorsSvg2.default, { className: 'modal__icon', src: this.props.icon })
                    ),
                    this.props.title && _react2.default.createElement(
                        'h2',
                        { className: 'modal__bigtitle ta-c' },
                        this.props.title
                    ),
                    _react2.default.createElement(
                        'div',
                        { 'aria-hidden': 'true', className: 'modal__close', onClick: function onClick() {
                                return _this3.handleClose();
                            } },
                        _react2.default.createElement(_webmotorsSvg2.default, { src: '/assets/img/icons/times.svg' })
                    ),
                    this.props.children && _react2.default.createElement(
                        'div',
                        { className: 'modal__wrap ' + (this.props.shadow ? 'modal__wrap--shadow' : '') },
                        _react2.default.createElement(
                            'div',
                            { className: 'modal__content', ref: this.content, onMouseEnter: function onMouseEnter() {
                                    return _this3.handleScrollEnter();
                                }, onMouseLeave: function onMouseLeave() {
                                    return _this3.handleScrollLeave();
                                } },
                            this.props.children
                        )
                    ),
                    this.props.button && _react2.default.createElement(
                        'div',
                        { className: 'modal__buttons' },
                        this.props.button
                    )
                )
            );
        }
    }]);
    return WebModal;
}(_react.Component);

exports.default = WebModal;