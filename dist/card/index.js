'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _style = require('webmotors-react-pj/card/style.js');

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var children = _ref.children,
        _ref$type = _ref.type,
        type = _ref$type === undefined ? 'radius' : _ref$type,
        _ref$padding = _ref.padding,
        padding = _ref$padding === undefined ? 32 : _ref$padding,
        className = _ref.className;
    return _react2.default.createElement(
        _style2.default,
        { className: className, type: type, padding: padding },
        children
    );
};