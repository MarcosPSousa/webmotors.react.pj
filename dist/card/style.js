'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    background-color: ', ';\n    color: ', ';\n    box-shadow: 0px 2px 2px -2px rgba(0, 0, 0, 0.16);\n    border-radius: ', ';\n    padding: ', ';\n'], ['\n    background-color: ', ';\n    color: ', ';\n    box-shadow: 0px 2px 2px -2px rgba(0, 0, 0, 0.16);\n    border-radius: ', ';\n    padding: ', ';\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _color = require('webmotors-react-pj/tokens/color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.div(_templateObject, (0, _color2.default)('white'), (0, _color2.default)('gray-2'), function (_ref) {
    var type = _ref.type;
    return (type === 'radius' ? 8 : 0) + 'px';
}, function (_ref2) {
    var padding = _ref2.padding;
    return padding + 'px';
});