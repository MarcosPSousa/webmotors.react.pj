'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.customAttributes = exports.isUserGroup = exports.listboxItemAnimation = exports.formatCPF = exports.formatCNPJ = exports.formatCEP = exports.formatPhone = exports.formatMoney = exports.formatDate = exports.jwtGet = exports.accordionAnimate = exports.scrollSmooth = exports.storageRemoveAll = exports.storageRemove = exports.storageGet = exports.storageSet = exports.cookieRemoveAll = exports.cookieRemove = exports.cookieGet = exports.cookieSet = exports.paramURL = exports.dispatchGet = exports.reducerCreate = exports.adobeAnalytics = exports.Ajax = exports.ajax = exports.WINDOW = undefined;

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _entries = require('babel-runtime/core-js/object/entries');

var _entries2 = _interopRequireDefault(_entries);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _isNan = require('babel-runtime/core-js/number/is-nan');

var _isNan2 = _interopRequireDefault(_isNan);

var _immutee = require('immutee');

var _immutee2 = _interopRequireDefault(_immutee);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _config = require('webmotors-react-pj/config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WINDOW = typeof window !== 'undefined' && window;

var reducerCreate = function reducerCreate(state, action, name) {
    var success = '' + name;
    var failure = name + '_FAILURE';
    var immutable = (0, _immutee2.default)(state);
    switch (action.type) {
        case success:
            {
                return immutable.set('result', action.payload).set('fetched', true).done();
            }
        case failure:
            {
                return immutable.set('result', []).set('fetched', true).set('error', action.payload).done();
            }
        default:
            {
                return state;
            }
    }
};

var scrollSmooth = function scrollSmooth(to) {
    var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 300;

    if (WINDOW) {
        clearTimeout(window.scrollSmoothTimerCache);
        if (duration <= 0) {
            return;
        }
        var difference = to - window.scrollY - 100;
        var perTick = difference / duration * 10;
        window.scrollSmoothTimerCache = setTimeout(function () {
            if (!(0, _isNan2.default)(parseInt(perTick, 10))) {
                window.scrollTo(0, window.scrollY + perTick);
                scrollSmooth(to, duration - 10);
            }
        }, 10);
    }
};

var accordionAnimate = function accordionAnimate(testOpen, item) {
    var tag = item;
    var getPadding = function getPadding(padding) {
        return parseFloat(getComputedStyle(tag)[padding]);
    };
    tag.classList.add('accordion--transition');
    if (testOpen) {
        tag.style.height = 'auto';
        var clientHeight = tag.clientHeight;

        var height = clientHeight + getPadding('paddingTop') + getPadding('paddingBottom');
        tag.style.height = '0px';
        setTimeout(function () {
            tag.style.height = height + 'px';
        }, 1);
    } else {
        tag.style.height = '0px';
        tag.addEventListener('transitionEnd', function () {
            return tag.classList.remove('accordion--transition');
        });
    }
};

var paramURL = function paramURL(e) {
    var query = new RegExp(e + '=.+&|' + e + '=.+$', 'g');
    var replaceQuery = new RegExp('&|^' + e + '=', 'g');
    var params = query.exec(window.location.search);
    if (params && params[0]) {
        return params[0].replace(replaceQuery, '');
    }
    return false;
};

var cookieSet = function cookieSet(key, value) {
    window.document.cookie = key + '=' + value + ';domain=.webmotors.com.br;path=/';
};

var cookieGet = function cookieGet(key) {
    var cookie = void 0;
    var value = '; ' + window.document.cookie;
    var parts = value.split('; ' + key + '=');
    if (parts.length === 2) {
        cookie = parts.pop().split(';').shift();
    }
    return cookie;
};

var cookieRemove = function cookieRemove(key) {
    if (WINDOW && key) {
        window.document.cookie = key + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT;domain=.webmotors.com.br;path=/';
    }
};
var cookieRemoveAll = function cookieRemoveAll() {
    var exceptArray = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    if (exceptArray.constructor.name === 'Array') {
        var cookies = document.cookie.split('; ');
        cookies.forEach(function (item) {
            var d = window.location.hostname.split('.');

            var _loop = function _loop() {
                var name = item.split(';')[0].split('=')[0];
                var hasException = exceptArray.filter(function (exceptName) {
                    return exceptName === name;
                });
                var cookieBase = window.encodeURIComponent(name) + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=' + d.join('.') + ' ;path=';
                var p = window.location.pathname.split('/');
                if (!hasException.length) {
                    document.cookie = cookieBase + '/';
                }
                while (p.length > 0) {
                    if (!hasException.length) {
                        document.cookie = cookieBase + p.join('/');
                    }
                    p.pop();
                }
                d.shift();
            };

            while (d.length > 0) {
                _loop();
            }
        });
    }
};

var storageSet = function storageSet() {
    var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    return window.localStorage.setItem(key, value);
};

var storageGet = function storageGet() {
    var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return window.localStorage.getItem(key);
};

var storageRemove = function storageRemove() {
    var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    return window.localStorage.removeItem(key);
};

var storageRemoveAll = function storageRemoveAll() {
    var exceptStorage = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    if (exceptStorage.constructor.name === 'Array') {
        var _window = window,
            localStorage = _window.localStorage;

        (0, _keys2.default)(localStorage).forEach(function (key) {
            var exceptItem = exceptStorage.filter(function (item) {
                return item === key;
            });
            if (!exceptItem.length) {
                localStorage.removeItem(key);
            }
        });
    }
};

var jwtGet = function jwtGet() {
    var jwt = cookieGet('CockpitLogged') || cookieGet('AppLogged');
    if (jwt) {
        var jwtPayload = jwt.replace(/_/g, '/').replace(/-/g, '+');
        var result = JSON.parse(decodeURIComponent(escape(atob(jwtPayload.split('.')[1]))));
        return result;
    }
    return {
        sid: '',
        unique_name: '',
        nameid: '',
        email: '',
        status: '',
        idGrupo: '',
        nomeGrupo: '',
        utilizaCrmTerceiro: '',
        nomeFantasia: '',
        tenant: '',
        role: '',
        permissions: [],
        produtos: []
    };
};

var dispatchGet = function dispatchGet(keyword, url, data) {
    var method = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'POST';
    return function (dispatch) {
        return (0, _axios2.default)({
            method: method,
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + cookieGet('CockpitLogged')
            },
            url: url,
            data: data,
            params: method === 'POST' ? '' : data
        }).then(function (res) {
            var resData = res && (res.data.data || res.data.Result);
            var resStatus = res && res.status;
            return dispatch({
                type: keyword,
                payload: {
                    success: true,
                    data: resData,
                    status: resStatus
                }
            });
        }).catch(function (rej) {
            var resData = void 0;
            var resStatus = void 0;
            var resErrorText = {
                400: 'Dados inválidos',
                401: 'Usuário não autorizado para esta ação',
                403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
                500: 'Erro interno da aplicação, tente novamente mais tarde',
                503: 'Servidor fora do ar, tente novamente mais tarde'
            };
            var resErrorTextUndefined = 'Erro não parametrizado pelo servidor';
            console.error(rej);
            if (rej && rej.response) {
                var errorData = rej.response.data;
                resStatus = rej.status || rej.response.status;
                resData = errorData.errors || errorData.Messages && errorData.Messages[0] || errorData || function () {
                    resData = resErrorText[resStatus];
                    if (!resData) {
                        resData = resErrorTextUndefined;
                    }
                    return resData;
                }(resStatus);
            } else {
                resStatus = /(\d+|\d+\n)/.exec(rej) ? /(\d+|\d+\n)/.exec(rej)[0].trim() : false;
                resData = resErrorText[resStatus];
                if (!resData) {
                    resData = resErrorTextUndefined;
                }
            }
            return dispatch({
                type: keyword + '_FAILURE',
                payload: {
                    success: false,
                    errors: resData,
                    status: Number(resStatus)
                }
            });
        });
    };
};

var AjaxAction = function AjaxAction(param) {
    if (param.status === 401 && Boolean(cookieGet('CockpitLogged'))) {
        window.location.href = _config.UrlCockpit + '/logout';
    }
};

var AjaxRefresh = function AjaxRefresh(AjaxFn, props, jwt) {
    AjaxFn({
        url: _config.ApiLogin + '/access/sso/refreshtoken',
        method: 'POST',
        data: { Token: jwt },
        refresh: true
    }).then(function (e) {
        if (e.success) {
            cookieSet('CockpitLogged', e.data.jwt);
            AjaxFn(props);
        }
        AjaxAction(e);
    });
};

var Ajax = function Ajax(props) {
    var json = jwtGet();
    var getStatus = function getStatus(param) {
        return param.response ? param.response.status : param.status || 0;
    };
    var getPromise = function getPromise(param) {
        return _promise2.default.resolve({
            data: param.data,
            status: param.status,
            success: param.success,
            target: param.target
        });
    };
    var msgText = {
        0: 'Erro não parametrizado',
        400: 'Dados inválidos',
        401: 'Usuário não autorizado para esta ação',
        403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
        404: 'Não conseguimos encontrar o que você estava procurando, tente novamente em instantes.',
        405: 'O método HTTP não está suportado',
        407: 'Credencias de autenticação por proxy server inválida',
        408: 'Conexão encerrada pelo servidor, por questão de desuso',
        409: 'Conflito com o recurso que está no servidor',
        411: 'Content-Length no server está indefinida',
        412: 'Acesso negado ao recurso de destino',
        413: 'Carga de requisição mais larga que os limites estabelecidos pelo servidor',
        414: 'URI maior do que o servidor aceita interpretar',
        415: 'Servidor se recusa a aceitar este formato de payload',
        416: 'Servidor não pode atender ao Header Range solicitado',
        417: 'Expectativa enviada no cabeçalho da requisição Expect não foi suprida',
        500: 'Ops! O Cockpit está em manutenção e nossa equipe já está atuando para resolver, fique tranquilo que já já voltamos.',
        501: 'Ação indisponível, tente novamente em instantes.',
        502: 'Sistema indisponível, por favor entre em contato conosco.',
        503: 'Sistema indisponível, tente novamente em instantes.',
        504: 'Ação indisponível, tente novamente.',
        505: 'Versão HTTP não é suportada pelo servidor',
        507: 'Servidor não suporta armazenar este dado',
        508: 'Servidor finalizou a operação porque encontrou um loop infinito'
    };
    var getError = function getError(param) {
        var status = getStatus(param);
        var data = msgText[status];
        var response = param.response;

        var responseHas = Boolean(response);
        console.error('[' + status + '] ' + data);
        AjaxAction(responseHas ? response : param);
        return getPromise({
            data: data,
            status: status,
            target: props.target,
            success: false
        });
    };
    var getSuccess = function getSuccess(param) {
        var data = (param.data.data === true ? param.data : param.data.data) || param.data.Result || param.data;
        var r = getPromise({
            data: data,
            status: getStatus(param),
            target: props.target,
            success: true
        });
        return r;
    };
    var timeExpires = json.exp;

    var timeNow = Date.now();
    var timeExpiresToSecound = new Date(timeExpires * 1000).getTime();
    var jwt = cookieGet('CockpitLogged');
    if (timeExpires && Boolean(timeExpiresToSecound) && timeNow > timeExpiresToSecound && !props.refresh) {
        AjaxRefresh(Ajax, props, jwt);
    }
    var url = props.url,
        _props$method = props.method,
        method = _props$method === undefined ? 'POST' : _props$method,
        headers = props.headers,
        data = props.data;

    var request = _axios2.default.request({
        url: url,
        method: method,
        data: data,
        headers: !headers ? {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + jwt
        } : headers,
        params: method === 'GET' || method === 'HEAD' ? data : ''
    }).then(function (e) {
        return e.status >= 200 && e.status < 300 ? getSuccess(e) : getError(e);
    }).catch(function (e) {
        var response = e.response || {};
        var status = response.status,
            body = response.data;

        var error = body && (body.errors || body.Messages && body.Messages[0] || body.data);
        AjaxAction(response);
        if (status >= 500) {
            return getError(e);
        }
        if (error) {
            return getPromise({
                data: error,
                status: status,
                success: false,
                target: props.target
            });
        }
        return getError(e);
    });
    return request;
};

var ajax = function ajax(url, data) {
    var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'POST';
    var dispatch = arguments[3];
    return (0, _axios2.default)({
        method: method,
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + cookieGet('CockpitLogged')
        },
        url: url,
        data: data,
        params: method === 'POST' ? '' : data
    }).then(function (res) {
        var resData = res && res.data && ((res.data.data === true ? res.data : res.data.data) || res.data.Result) || res.data;
        var resStatus = res && res.status;
        var returnObj = {
            type: 'AJAX_' + url,
            payload: {
                success: true,
                data: resData,
                status: resStatus
            }
        };
        if (dispatch) {
            return dispatch(returnObj);
        }
        return returnObj;
    }).catch(function (rej) {
        var resData = void 0;
        var resStatus = void 0;
        var resErrorText = {
            400: 'Dados inválidos',
            401: 'Usuário não autorizado para esta ação',
            403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
            500: 'Erro interno da aplicação, tente novamente mais tarde',
            503: 'Servidor fora do ar, tente novamente mais tarde'
        };
        var resErrorTextUndefined = 'Erro não parametrizado pelo servidor';
        if (rej && rej.response) {
            var errorData = rej.response.data;
            resStatus = rej.status || rej.response.status;
            resData = errorData.errors || errorData.Messages && errorData.Messages[0] || errorData || function () {
                resData = resErrorText[resStatus];
                if (!resData) {
                    resData = resErrorTextUndefined;
                }
                return resData;
            }(resStatus);
        } else {
            resStatus = /(\d+|\d+\n)/.exec(rej) ? /(\d+|\d+\n)/.exec(rej)[0].trim() : false;
            resData = resErrorText[resStatus];
            if (!resData) {
                resData = resErrorTextUndefined;
            }
        }
        var returnObj = {
            type: 'AJAX_' + url + '_FAILURE',
            payload: {
                success: false,
                errors: resData,
                status: Number(resStatus)
            }
        };
        if (dispatch) {
            return dispatch(returnObj);
        }
        return returnObj;
    });
};

var adobeAnalytics = function adobeAnalytics(params) {};

var listboxItemAnimation = function listboxItemAnimation() {
    setTimeout(function () {
        return [].concat((0, _toConsumableArray3.default)(document.querySelectorAll('.listbox__item'))).forEach(function (item, i) {
            return setTimeout(function () {
                return item.classList.add('listbox__item--active');
            }, Math.min(i * 60, 2000));
        });
    }, 10);
};

var formatDate = function formatDate(dateString) {
    return new Date(dateString).toLocaleDateString('pt-BR', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'
    });
};

var formatMoney = function formatMoney(number) {
    return Number(number).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
};

var formatPhone = function formatPhone(number) {
    return number.replace(/\D/g, '').replace(/(\d{2})((?=\d{9})(\d{5})(\d{4})|(?=\d{8})(\d{4})(\d{4}))/, function () {
        if (arguments.length <= 3 ? undefined : arguments[3]) {
            return '(' + (arguments.length <= 1 ? undefined : arguments[1]) + ') ' + (arguments.length <= 3 ? undefined : arguments[3]) + '-' + (arguments.length <= 4 ? undefined : arguments[4]);
        }
        if (arguments.length <= 5 ? undefined : arguments[5]) {
            return '(' + (arguments.length <= 1 ? undefined : arguments[1]) + ') ' + (arguments.length <= 5 ? undefined : arguments[5]) + '-' + (arguments.length <= 6 ? undefined : arguments[6]);
        }
        console.error('formatPhone: invalid for "' + number + '"');
        return '';
    });
};

var formatCEP = function formatCEP(number) {
    return number.replace(/\D/g, '').replace(/(\d{5})(\d{3})/, '$1-$2');
};

var formatCNPJ = function formatCNPJ(number) {
    return number.replace(/\D/g, '').replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
};

var formatCPF = function formatCPF(number) {
    return number.replace(/\D/g, '').replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
};

var isUserGroup = function isUserGroup() {
    return jwtGet().idGrupo && jwtGet().role && jwtGet().role.indexOf('1') !== -1;
};

if (WINDOW) {
    (function (ElementProto) {
        var item = ElementProto;
        if (typeof ElementProto.matches !== 'function') {
            item.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
                var element = this;
                var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
                var index = 0;
                while (elements[index] && elements[index] !== element) {
                    ++index;
                }
                return Boolean(elements[index]);
            };
        }
        if (typeof ElementProto.closest !== 'function') {
            item.closest = function closest(selector) {
                var element = this;
                while (element && element.nodeType === 1) {
                    if (element.matches(selector)) {
                        return element;
                    }
                    element = element.parentNode;
                }
                return null;
            };
        }
    })(window.Element.prototype);
}

var customAttributes = function customAttributes(props, except) {
    var propsCustom = {};
    (0, _entries2.default)(props).forEach(function (item) {
        var _item = (0, _slicedToArray3.default)(item, 2),
            propKey = _item[0],
            propValue = _item[1];

        var regex = new RegExp('^(?!(' + except + ')).+');
        if (regex.test(propKey)) {
            propsCustom[propKey] = propValue;
        }
    });
    return propsCustom;
};

exports.WINDOW = WINDOW;
exports.ajax = ajax;
exports.Ajax = Ajax;
exports.adobeAnalytics = adobeAnalytics;
exports.reducerCreate = reducerCreate;
exports.dispatchGet = dispatchGet;
exports.paramURL = paramURL;
exports.cookieSet = cookieSet;
exports.cookieGet = cookieGet;
exports.cookieRemove = cookieRemove;
exports.cookieRemoveAll = cookieRemoveAll;
exports.storageSet = storageSet;
exports.storageGet = storageGet;
exports.storageRemove = storageRemove;
exports.storageRemoveAll = storageRemoveAll;
exports.scrollSmooth = scrollSmooth;
exports.accordionAnimate = accordionAnimate;
exports.jwtGet = jwtGet;
exports.formatDate = formatDate;
exports.formatMoney = formatMoney;
exports.formatPhone = formatPhone;
exports.formatCEP = formatCEP;
exports.formatCNPJ = formatCNPJ;
exports.formatCPF = formatCPF;
exports.listboxItemAnimation = listboxItemAnimation;
exports.isUserGroup = isUserGroup;
exports.customAttributes = customAttributes;
exports.default = WINDOW;