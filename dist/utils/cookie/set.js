"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (key, value) {
    window.document.cookie = key + "=" + value + ";domain=.webmotors.com.br;path=/";
};