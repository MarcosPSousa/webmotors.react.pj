'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var remove = function remove(key) {
    if (key) {
        window.document.cookie = key + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT;domain=.webmotors.com.br;path=/';
    }
};

var removeAll = function removeAll() {
    var exceptArray = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    if (exceptArray.constructor.name === 'Array') {
        var cookies = document.cookie.split('; ');
        cookies.forEach(function (item) {
            var d = window.location.hostname.split('.');

            var _loop = function _loop() {
                var name = item.split(';')[0].split('=')[0];
                var hasException = exceptArray.filter(function (exceptName) {
                    return exceptName === name;
                });
                var cookieBase = window.encodeURIComponent(name) + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=' + d.join('.') + ' ;path=';
                var p = window.location.pathname.split('/');
                if (!hasException.length) {
                    document.cookie = cookieBase + '/';
                }
                while (p.length > 0) {
                    if (!hasException.length) {
                        document.cookie = cookieBase + p.join('/');
                    }
                    p.pop();
                }
                d.shift();
            };

            while (d.length > 0) {
                _loop();
            }
        });
    }
};

exports.remove = remove;
exports.removeAll = removeAll;
exports.default = remove;