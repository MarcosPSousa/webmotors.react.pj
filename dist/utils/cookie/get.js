'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (key) {
    var value = '; ' + window.document.cookie;
    var parts = value.split('; ' + key + '=');
    if (parts.length === 2) {
        return parts.pop().split(';').shift();
    }
    return '';
};