'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Ajax = undefined;

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _enum = require('./enum');

var _enum2 = _interopRequireDefault(_enum);

var _get = require('../cookie/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Ajax = function Ajax(props) {
    var getStatus = function getStatus(param) {
        return param.response ? param.response.status : param.status || 0;
    };

    var getPromise = function getPromise(param) {
        return _promise2.default.resolve({
            data: param.data,
            status: param.status,
            success: param.success,
            target: param.target
        });
    };

    var msgError = function msgError(text) {
        return console.error(text);
    };

    var getError = function getError(param) {
        var data = _enum2.default[getStatus(param)];
        msgError('[' + getStatus(param) + '] ' + data);
        return getPromise({
            data: data,
            status: getStatus(param),
            target: props.target,
            success: false
        });
    };

    var getSuccess = function getSuccess(param) {
        var data = (param.data.data === true ? param.data : param.data.data) || param.data.Result || param.data;
        var r = getPromise({
            data: data,
            status: getStatus(param),
            target: props.target,
            success: true
        });
        return r;
    };

    var parseError = function parseError(e) {
        var response = e.response || {};
        var status = response.status,
            body = response.data;

        var error = body && (body.errors || body.Messages && body.Messages[0]);
        if (status >= 500) {
            return getError(e);
        }
        if (error) {
            return getPromise({
                data: error,
                status: status,
                success: false,
                target: props.target
            });
        }
        return getError(e);
    };

    var url = props.url,
        _props$method = props.method,
        method = _props$method === undefined ? 'POST' : _props$method,
        headers = props.headers,
        data = props.data;


    var request = _axios2.default.request({
        url: url,
        method: method,
        data: data,
        headers: !headers ? {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + (0, _get2.default)('CockpitLogged')
        } : headers,
        params: method === 'POST' ? '' : data
    }).then(function (e) {
        return e.status >= 200 && e.status < 300 ? getSuccess(e) : getError(e);
    }).catch(function (e) {
        return parseError(e);
    });
    return request;
};

exports.Ajax = Ajax;
exports.default = Ajax;