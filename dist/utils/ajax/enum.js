'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    0: 'Erro não parametrizado',
    400: 'Dados inválidos',
    401: 'Usuário não autorizado para esta ação',
    403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
    404: 'Não conseguimos encontrar o que você estava procurando, tente novamente em instantes.',
    405: 'O método HTTP não está suportado',
    407: 'Credencias de autenticação por proxy server inválida',
    408: 'Conexão encerrada pelo servidor, por questão de desuso',
    409: 'Conflito com o recurso que está no servidor',
    411: 'Content-Length no server está indefinida',
    412: 'Acesso negado ao recurso de destino',
    413: 'Carga de requisição mais larga que os limites estabelecidos pelo servidor',
    414: 'URI maior do que o servidor aceita interpretar',
    415: 'Servidor se reca a aceitar este formato de payload',
    416: 'Servidor não pode atender ao Header Range solicitado',
    417: 'Expectativa enviada no cabeçalho da requisição Expect não foi suprida',
    500: 'Ops! O Cockpit está em manutenção e nossa equipe já está atuando para resolver, fique tranquilo que já já voltamos.',
    501: 'Ação indisponível, tente novamente em instantes.',
    502: 'Sistema indisponível, por favor entre em contato conosco.',
    503: 'Sistema indisponível, tente novamente em instantes.',
    504: 'Ação indisponível, tente novamente.',
    505: 'Versão HTTP não é suportada pelo servidor',
    507: 'Servidor não suporta armazenar este dado',
    508: 'Servidor finalizou a operação porque encontrou um loop infinito'
};