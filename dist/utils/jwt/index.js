'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _get = require('../cookie/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
    var jwt = (0, _get2.default)('CockpitLogged');
    if (jwt) {
        var jwtPayload = jwt.replace(/_/g, '/').replace(/-/g, '+');
        var result = JSON.parse(decodeURIComponent(escape(atob(jwtPayload.split('.')[1]))));
        return result;
    }
    return {};
};