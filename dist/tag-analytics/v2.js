'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _entries = require('babel-runtime/core-js/object/entries');

var _entries2 = _interopRequireDefault(_entries);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _utils = require('webmotors-react-pj/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AdobeTrackingDefault = {
    site: {
        domain: undefined,
        country: undefined,
        server: undefined,
        environment: undefined,
        subEnvironment: undefined,
        microEnvironment: undefined,
        tmsVersion: undefined,
        clientType: undefined,
        platform: undefined
    },
    page: {
        flowType: undefined,
        pageType: undefined,
        pageName: undefined,
        'pageName.tier1': undefined,
        'pageName.tier2': undefined,
        'pageName.tier3': undefined,
        error: undefined,
        loadTime: undefined,
        url: undefined,
        trackingCodeExternal: undefined,
        trackingCodeInternal: undefined
    },
    user: {
        loginStatus: undefined,
        userID: undefined,
        loginID: undefined,
        cockpitUserID: undefined,
        cdCliente: undefined,
        type: undefined,
        clientType: undefined,
        clientCode: undefined,
        nmCidade: undefined,
        nmEstado: undefined,
        nmFantasia: undefined,
        nmRede: undefined,
        cdRede: undefined,
        nmZona: undefined,
        cdZona: undefined,
        nmFilial: undefined,
        cdFilial: undefined,
        pacoteProduto: undefined,
        cdPacoteProduto: undefined
    }
};

var dispatchEventTagManager = function dispatchEventTagManager(e) {
    return console.log({ event: e.type });
};

exports.default = function (_ref) {
    var _ref$dataLayer = _ref.dataLayer,
        dataLayer = _ref$dataLayer === undefined ? {} : _ref$dataLayer,
        _ref$event = _ref.event,
        event = _ref$event === undefined ? 'customPageView' : _ref$event;
    var _window = window,
        objDataLayer = _window.objDataLayer;

    var AdobeTracking = objDataLayer ? (0, _extends3.default)({}, objDataLayer) : (0, _extends3.default)({}, AdobeTrackingDefault);

    (0, _entries2.default)(dataLayer).map(function (item) {
        var _item = (0, _slicedToArray3.default)(item, 2),
            key = _item[0],
            value = _item[1];

        AdobeTracking[key] = (0, _extends3.default)({}, AdobeTracking[key], value);
    });

    document.addEventListener(event, dispatchEventTagManager);
    document.dispatchEvent(new CustomEvent(event, { detail: AdobeTracking }));
    document.removeEventListener(event, dispatchEventTagManager);
};