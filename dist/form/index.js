'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('webmotors-react-pj/utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Form = function (_Component) {
    (0, _inherits3.default)(Form, _Component);

    function Form(props) {
        (0, _classCallCheck3.default)(this, Form);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Form.__proto__ || (0, _getPrototypeOf2.default)(Form)).call(this, props));

        _this.form = (0, _react.createRef)();
        return _this;
    }

    (0, _createClass3.default)(Form, [{
        key: 'getError',
        value: function getError() {
            var inputsError = Boolean(document.querySelector('.inputtext--error')) || Boolean(document.querySelector('[data-input-error="true"]'));
            return inputsError;
        }
    }, {
        key: 'handlerSubmit',
        value: function handlerSubmit(e) {
            var _this2 = this;

            var target = e.target;
            var _props = this.props,
                url = _props.action,
                method = _props.method,
                _props$onSuccess = _props.onSuccess,
                onSuccess = _props$onSuccess === undefined ? function () {
                return '';
            } : _props$onSuccess,
                _props$onError = _props.onError,
                onError = _props$onError === undefined ? function () {
                return '';
            } : _props$onError;

            if (!url) {
                console.error('[Component] Form: url undefined');
                if (this.props.onComplete) {
                    this.handleFormEvent(this.props.onComplete, e);
                }
                return false;
            }
            if (!this.getError()) {
                var data = {};
                var button = target.querySelector('[type="submit"]');
                var inputs = [].concat((0, _toConsumableArray3.default)(target.querySelectorAll('input, select, textarea')));
                var fomatObject = function fomatObject(objCurrent, path, value) {
                    var current = objCurrent;
                    var limiter = /\[|\./.exec(path) || path && /\s$/.exec(path + ' ');
                    if (limiter) {
                        var key = path.substring(0, limiter.index);
                        var isArray = limiter[0] === '[';
                        if (isArray) {
                            var pathNext = path.replace(new RegExp('^' + key + '.|.$', 'g'), '');
                            if (!current[key]) {
                                current[key] = [];
                            }
                            current[key].push(!pathNext ? value : (0, _defineProperty3.default)({}, pathNext, value));
                        } else {
                            var _pathNext = path.replace(new RegExp(key + '?.'), '');
                            if (!current[key]) {
                                current[key] = !_pathNext ? value : {};
                            }
                            fomatObject(current[key], _pathNext, value);
                        }
                    }
                };
                var formatNumber = function formatNumber(v) {
                    return Number(v);
                };
                var formatString = function formatString(v) {
                    return v.toString();
                };
                var formatBoolean = function formatBoolean(v) {
                    return v === 'true';
                };
                var formatJSON = function formatJSON(v) {
                    return JSON.parse(v);
                };
                inputs.forEach(function (input) {
                    var _input$dataset = input.dataset,
                        type = _input$dataset.type,
                        val = _input$dataset.value,
                        key = _input$dataset.key;

                    var value = val || input.value;
                    var name = key || input.name;
                    var typeInput = input.type;

                    var valueFinal = null;
                    if (!type) {
                        if (value === 'true' || value === 'false') {
                            valueFinal = formatBoolean(value);
                        } else {
                            var valueToNumber = Number(value);
                            valueFinal = window.Number.isNaN(valueToNumber) ? formatString(value) : formatNumber(value);
                        }
                    } else {
                        switch (type) {
                            case 'boolean':
                                {
                                    valueFinal = formatBoolean(value);
                                    break;
                                }
                            case 'number':
                                {
                                    valueFinal = formatNumber(value);
                                    break;
                                }
                            case 'json':
                                {
                                    valueFinal = formatJSON(value);
                                    break;
                                }
                            default:
                                {
                                    valueFinal = formatString(value);
                                }
                        }
                    }
                    if (typeInput === 'radio' && input.checked || typeInput !== 'radio') {
                        fomatObject(data, name, valueFinal);
                    }
                });
                var inputLock = function inputLock(state) {
                    inputs.forEach(function (input) {
                        input[state ? 'setAttribute' : 'removeAttribute']('readonly', '');
                        input.parentNode.classList[state ? 'add' : 'remove']('input-lock');
                    });
                };
                var buttonLock = function buttonLock(element) {
                    var btn = element;
                    btn.disabled = true;
                    btn.setAttribute('data-button', btn.innerHTML);
                    btn.textContent = 'Aguarde...';
                };
                var buttonUnlock = function buttonUnlock(element) {
                    var btn = element;
                    btn.removeAttribute('disabled');
                    btn.innerHTML = btn.getAttribute('data-button').replace('<script', '');
                };
                buttonLock(button);
                inputLock(true);
                (0, _utils.Ajax)({
                    url: url,
                    method: method,
                    data: data,
                    target: target
                }).then(function (f) {
                    _this2.handleFormEvent(f.success ? onSuccess : onError, f);
                    return f;
                }).then(function (f) {
                    buttonUnlock(button);
                    inputLock(false);
                    if (_this2.props.onComplete) {
                        _this2.handleFormEvent(_this2.props.onComplete, f);
                    }
                });
            }
            return true;
        }
    }, {
        key: 'handleBeforeSend',
        value: function handleBeforeSend(e) {
            var target = e.target;

            var inputAll = [].concat((0, _toConsumableArray3.default)(target.querySelectorAll('input, select, textarea')));
            var _window = window,
                scrollX = _window.scrollX,
                scrollY = _window.scrollY;

            inputAll.forEach(function (item) {
                item.focus();
                item.blur();
            });
            window.scrollTo(scrollX, scrollY);
            if (this.getError()) {
                var error = target.querySelector('.inputtext--error') || document.querySelector('[data-input-error="true"]');
                var rect = error.getBoundingClientRect();
                var top = rect.top + window.pageYOffset - 4;
                (0, _utils.scrollSmooth)(top);
            }
        }
    }, {
        key: 'handleFormEvent',
        value: function handleFormEvent(fn, e) {
            fn(e);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var _props2 = this.props,
                onBeforeSend = _props2.onBeforeSend,
                className = _props2.className;

            return _react2.default.createElement(
                'form',
                {
                    className: 'form ' + (!className ? '' : className),
                    ref: this.form,
                    autoComplete: this.props.autoComplete || 'off',
                    onSubmit: function onSubmit(e) {
                        e.preventDefault();
                        e.persist();
                        var before = onBeforeSend && onBeforeSend();
                        if (before || before === undefined) {
                            _this3.handleBeforeSend(e);
                            setTimeout(function () {
                                return _this3.handlerSubmit(e);
                            }, 10);
                        }
                    }
                },
                this.props.children
            );
        }
    }]);
    return Form;
}(_react.Component);

exports.default = Form;