'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    font-size: 3.6rem;\n    font-weight: 900;\n    padding-bottom: 12px;\n    text-transform: uppercase;\n    line-height: 1em;\n'], ['\n    font-size: 3.6rem;\n    font-weight: 900;\n    padding-bottom: 12px;\n    text-transform: uppercase;\n    line-height: 1em;\n']);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _styledComponents2.default.h1(_templateObject);