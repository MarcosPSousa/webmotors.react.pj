'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

var _page = require('webmotors-react-pj/page-status/page');

var _page2 = _interopRequireDefault(_page);

var _wrapper = require('webmotors-react-pj/page-status/wrapper');

var _wrapper2 = _interopRequireDefault(_wrapper);

var _icon = require('webmotors-react-pj/page-status/icon');

var _icon2 = _interopRequireDefault(_icon);

var _title = require('webmotors-react-pj/page-status/title');

var _title2 = _interopRequireDefault(_title);

var _text = require('webmotors-react-pj/page-status/text');

var _text2 = _interopRequireDefault(_text);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var _ref$title = _ref.title,
        title = _ref$title === undefined ? '' : _ref$title,
        _ref$text = _ref.text,
        text = _ref$text === undefined ? 'Carregando...' : _ref$text,
        _ref$icon = _ref.icon,
        icon = _ref$icon === undefined ? _config.UrlCockpit + '/assets/img/brands/webmotors-2-icon.svg' : _ref$icon,
        className = _ref.className;
    return _react2.default.createElement(
        _wrapper2.default,
        { className: className },
        _react2.default.createElement(
            _page2.default,
            null,
            _react2.default.createElement(_icon2.default, { src: icon }),
            _react2.default.createElement(
                _title2.default,
                null,
                title
            ),
            _react2.default.createElement(
                _text2.default,
                null,
                text
            )
        )
    );
};