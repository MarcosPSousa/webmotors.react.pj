'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

exports.default = Modal;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _config = require('webmotors-react-pj/config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Modal(props) {
    var _useState = (0, _react.useState)(false),
        _useState2 = (0, _slicedToArray3.default)(_useState, 2),
        close = _useState2[0],
        setClose = _useState2[1];

    (0, _react.useEffect)(function () {
        document.querySelector('body').classList.add('overflow-hidden');
        return function () {
            return document.querySelector('body').classList.remove('overflow-hidden');
        };
    });

    function handleClose() {
        if (!props.locked) {
            setClose(true);
            props.onClose();
        }
    }

    return _react2.default.createElement(
        'div',
        { className: 'modal-stock ' + (props.forceClose || close ? 'modal-stock--close' : '') },
        _react2.default.createElement('button', { type: 'button', className: 'modal-stock__bg', onClick: !props.noCloseButton ? function () {
                return handleClose();
            } : '' }),
        _react2.default.createElement(
            'div',
            { className: 'modal-stock__box', style: props.maxWidth ? { maxWidth: props.maxWidth } : null },
            !props.noCloseButton ? _react2.default.createElement('img', { src: _config.UrlCockpit + '/assets/img/icons/icon-close-black.svg', className: 'modal-stock__close', onClick: function onClick() {
                    return handleClose();
                } }) : '',
            _react2.default.createElement(
                'div',
                { className: 'modal-stock__content modal-stock__content--flex' },
                props.children
            )
        )
    );
}