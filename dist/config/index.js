'use strict';

var env = require(process.env.NODE_ENV ? './env/' + process.env.NODE_ENV : './env/development');

module.exports = env;