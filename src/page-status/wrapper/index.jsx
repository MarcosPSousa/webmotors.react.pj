import React from 'react';
import WrapperStyle from 'webmotors-react-pj/page-status/wrapper/style';

export default ({ children, className }) => (
    <WrapperStyle className={className}>{children}</WrapperStyle>
);
