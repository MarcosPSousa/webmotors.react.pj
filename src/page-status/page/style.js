import styled from 'styled-components';

export default styled.div`
    padding: 32px 32px;
    display: flex;
    flex-direction: column;
    text-align: center;
    align-items: center;
    justify-content: center;
`;
