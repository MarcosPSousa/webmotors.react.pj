import React from 'react';
import PageStyle from 'webmotors-react-pj/page-status/page/style';

export default ({ children }) => (
    <PageStyle>{children}</PageStyle>
);
