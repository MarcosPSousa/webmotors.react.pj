import React from 'react';
import TextStyle from 'webmotors-react-pj/page-status/text/style';

export default ({ children }) => (
    <TextStyle>{children}</TextStyle>
);
