import React from 'react';
import IconStyle from 'webmotors-react-pj/page-status/icon/style';

export default ({ src }) => (
    <IconStyle src={src} alt="" aria-hidden="true" />
);
