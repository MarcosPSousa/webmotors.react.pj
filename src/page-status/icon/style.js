import styled from 'styled-components';

export default styled.img`
    width: 128px;
    height: 128px;
    margin-bottom: 24px;
`;
