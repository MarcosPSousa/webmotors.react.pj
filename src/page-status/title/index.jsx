import React from 'react';
import TitleStyle from 'webmotors-react-pj/page-status/title/style';

export default ({ children }) => (
    children && <TitleStyle>{children}</TitleStyle>
);
