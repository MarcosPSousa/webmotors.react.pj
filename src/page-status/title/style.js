import styled from 'styled-components';

export default styled.h1`
    font-size: 3.6rem;
    font-weight: 900;
    padding-bottom: 12px;
    text-transform: uppercase;
    line-height: 1em;
`;
