import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import Page from 'webmotors-react-pj/page-status/page';
import Wrapper from 'webmotors-react-pj/page-status/wrapper';
import Icon from 'webmotors-react-pj/page-status/icon';
import Title from 'webmotors-react-pj/page-status/title';
import Text from 'webmotors-react-pj/page-status/text';

export default ({
    title = '',
    text = 'Carregando...',
    icon = `${UrlCockpit}/assets/img/brands/webmotors-2-icon.svg`,
    className,
}) => (
    <Wrapper className={className}>
        <Page>
            <Icon src={icon} />
            <Title>{title}</Title>
            <Text>{text}</Text>
        </Page>
    </Wrapper>
);
