import React, { useState, useEffect } from 'react';
import PageStatus from 'webmotors-react-pj/page-status';
import { jwtGet, cookieRemoveAll } from 'webmotors-react-pj/utils';
import ErrorBoundary from 'webmotors-react-pj/main/error';
import Content from 'webmotors-react-pj/main/content';
import {
    pathname,
    bokey,
    cockpitLogged,
    unloggedPage,
} from 'webmotors-react-pj/main/hooks';
import { StyleMain, StyleCopi } from 'webmotors-react-pj/main/style';

export default ({ children }) => {
    const [loaded, setLoaded] = useState(false);
    const [logged, setLogged] = useState(false);
    const { hostname } = window.location;
    let env = 'production';
    if (/^local\./.test(hostname)) {
        env = 'development';
    }
    if (/^(hcockpit|hk|hestoque|hgb)\./.test(hostname)) {
        env = 'homologation';
    }
    if (/^azul\./.test(hostname)) {
        env = 'blue';
    }
    const loggedView = (
        (
            (!unloggedPage || (cockpitLogged && pathname === '/'))
            && (jwtGet().status === 'Migrado' || jwtGet().status === 'Hub')
            && !bokey
            && pathname !== '/logout'
        )
    );

    const Container = () => (
        <Content logged={logged} unloggedPage={unloggedPage} env={env}>
            {children}
        </Content>
    );

    useEffect(() => {
        setLoaded(true);
        setLogged(loggedView);
        if (bokey) {
            cookieRemoveAll();
        }
    }, []);

    useEffect(() => setLogged(loggedView), [pathname]);

    return (
        <ErrorBoundary>
            <StyleMain>
                {loaded ? <Container /> : <PageStatus />}
                <StyleCopi />
            </StyleMain>
        </ErrorBoundary>
    );
};
