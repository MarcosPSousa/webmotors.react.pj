import React, { Fragment } from 'react';
import { jwtGet, cookieRemove } from 'webmotors-react-pj/utils';
import { Header, Footer } from 'webmotors-react-pj/frame/unlogged';
import Login from 'webmotors-react-pj/containers/login';

export default ({ children, unloggedPage }) => {
    let child = children;
    const { status } = jwtGet();
    const { pathname } = window.location;
    const urlAllowPendente = [
        '/usuario/introducao-cockpit',
        '/usuario/atualizar',
        '/usuario/senha-atualizada',
    ];
    const urlAllowSenhaExpirada = [
        '/redefinir-senha/troca-de-seguranca',
    ];
    if (
        (
            (status === 'Pendente')
            && (!urlAllowPendente.includes(pathname))
        )
        || (
            (status === 'senhaExpirada')
            && (!urlAllowSenhaExpirada.includes(pathname))
        )
    ) {
        cookieRemove('CockpitLogged');
    }

    if (pathname === '/') {
        child = <Login />;
    }

    return (
        <Fragment>
            <Header />
            {
                unloggedPage
                    ? child
                    : <Login />
            }
            <Footer />
        </Fragment>
    );
};
