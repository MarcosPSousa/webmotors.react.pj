import styled from 'styled-components';

export default () => styled.div`
    flex: 1;
    min-height: 80vh;
    display: flex;
    flex-direction: column;
    flex-grow: 1;
`;
