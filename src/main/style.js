import styled, { createGlobalStyle, keyframes } from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const animateOpen = keyframes`
    0%{
        opacity: 0;
    }
    100%{
        opacity: 1;
    }
`;

const animateClose = keyframes`
    0%{
        opacity: 1;
    }
    100%{
        opacity: 0;
    }
`;

export const StyleCopi = createGlobalStyle`
    #drz_overlay_chat_external{
        z-index: 5;
        #drz_btn_open{
            width: 40px !important;
            min-width: 40px;
            flex-direction: row-reverse;
            margin: 0;
            left: 28px;
            bottom: 80px;
        }
        .drz_btn_open_minimized_arrow{
            margin: 0;
            display: none;
        }
        &:hover{
            #drz_btn_open{
                width: auto !important;
            }
            #drz_btn_txt{
                opacity: 1;
            }
        }
    }
    .drz_main {
        #drz_btn_txt{
            width: auto;
            padding: 11px 20px;
            opacity: 0;
            pointer-events: none;
            transition: opacity 0.3s;
            position: absolute;
            white-space: nowrap;
            left: 100%;
            transform: translateX(8px);
            &:after{
                font-size: 1.2rem;
                font-weight: bold;
                font-family: 'Poppins', sans-serif;
                line-height: normal;
            }
        }
        #drz_chat_overlay_main{
            transform: translate(76px, -80px);
            top: 0px !important;
            &:not([style*="display: none"]){
                animation: ${animateOpen} 0.4s forwards;
            }
            &:not([style*="top: 0px"]){
                animation: ${animateClose} 0.2s forwards;
            }
        }
        #drz_btn_img{
            box-shadow: none !important;
        }
    }
`;


export const StyleMain = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    flex: 1;
    background-color: ${color('gray-4')};
    justify-content: ${props => (props.center ? 'center' : 'flex-start')};
    min-height: 100vh;
`;
