import React, { Fragment } from 'react';
import { Header, Menu, Footer } from 'webmotors-react-pj/frame/logged';
import { StyleWrapper, StyleContent } from 'webmotors-react-pj/main/logged/style';
import { cookieSet, jwtGet } from 'webmotors-react-pj/utils';
import {
    UrlCockpit,
    UrlCRM,
    UrlGearbox,
    UrlEstoque,
    UrlEstoquePlataforma,
    UrlMaisFidelidade,
    UrlUniversidade,
    UrlAutoguru,
    UrlComprarVeiculos,
} from 'webmotors-react-pj/config';

export default ({ children, env }) => {
    const { v2 } = jwtGet();
    const menu = {
        data: [
            { menu: 'Início', url: UrlCockpit, icone: '/product-inicio.svg', subMenu: [] },
            {
                menu: 'CRM', url: '#', icone: '/product-crm.svg', subMenu: [
                    { menu: 'Painel', url: `${UrlCRM}/dashboard`, icone: '', subMenu: [] },
                    { menu: 'Gerenciar leads', url: `${UrlCRM}/manage-leads`, icone: '', subMenu: [] },
                    { menu: 'Minhas atividades', url: `${UrlCRM}/schedule`, icone: '', subMenu: [] },
                    { menu: 'Relatórios', url: `${UrlCRM}/reports`, icone: '', subMenu: [] },
                    { menu: 'Configurações', url: `${UrlCRM}/configuration`, icone: '', subMenu: [] },
                    { menu: 'Integrações', url: `${UrlCRM}/integration`, icone: '', subMenu: [] }
                ]
            },
            {
                menu: 'Gearbox', url: '#', icone: '/product-gearbox.svg', subMenu: [
                    { menu: 'Painel', url: `${UrlGearbox}`, icone: '', subMenu: [] },
                    { menu: 'Site', url: `${UrlGearbox}/site`, icone: '', subMenu: [] },
                    { menu: 'Mídia', url: `${UrlGearbox}/midia`, icone: '', subMenu: [] }
                ]
            },
            {
                menu: 'Estoque', url: `#`, icone: '/product-estoque.svg', subMenu: [
                    { menu: 'Consultar veículo', url: `${UrlEstoquePlataforma}/gestao-estoque/consultar`, icone: '', subMenu: [] },
                    { menu: 'Incluir veículo', url: `${UrlEstoquePlataforma}/gestao-estoque/incluir-anuncio`, icone: '', subMenu: [] },
                    { menu: 'Comprar veículo', url: `${UrlComprarVeiculos}`, icone: '', subMenu: [] },
                    { menu: 'Vender veículo', url: `${UrlEstoquePlataforma}/vender-veiculo`, icone: '', subMenu: [] },
                    { menu: 'Avaliar veículo', url: `${UrlEstoque}/avaliar-veiculo`, icone: '', subMenu: [] }
                ]
            },
            { menu: '+ Fidelidade', url: `${UrlMaisFidelidade}`, icone: '/product-maisfidelidade.svg', subMenu: [] },
            { menu: 'Universidade', url: `${UrlUniversidade}/wm-cockpit`, icone: '/product-universidade.svg', subMenu: [] },
            { menu: 'Autoguru', url: `${UrlAutoguru}/`, icone: '/product-autoguru.svg', subMenu: [] },
            {
                menu: 'Cockpit', url: '#', icone: '/product-cockpit-1.svg', subMenu: [
                    {
                        menu: 'Conta', url: `#`, icone: '', subMenu: [
                            { menu: 'Termo de Adesão', url: `${UrlCockpit}/termos/pendentes`, icone: '', subMenu: [] },
                            { menu: 'Usuários', url: `${UrlCockpit}/usuario/listagem/ativo`, icone: '', subMenu: [] },
                            { menu: 'Meu Plano', url: `${UrlCockpit}/meu-plano/fatura`, icone: '', subMenu: [] }
                        ]
                    },
                    { menu: 'Dúvidas Frequentes', url: `https://webmotors.zendesk.com/hc/pt-br/categories/360002515092-Lojas-Concession%C3%A1rias`, icone: '', subMenu: [] }
                ]
            },
            { menu: 'Store', url: `${UrlCockpit}/store`, icone: '/product-store.svg', subMenu: [] }
        ],
        status: 200,
        success: true,
    };
    !/local\./.test(window.location.hostname) && cookieSet('CockpitMenu', JSON.stringify(menu));
    return (
        <Fragment>
            <Header />
            <StyleWrapper>
                <Menu />
                <StyleContent>{children}</StyleContent>
            </StyleWrapper>
            <Footer />
        </Fragment>
    )
};
