import styled from 'styled-components';

export const StyleWrapper = styled.div`
    flex: 1;
    min-height: calc(100vh - 152px);
    display: flex;
`;

export const StyleContent = styled.div`
    flex: 1;
`;
