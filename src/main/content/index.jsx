import React from 'react';
import Logged from 'webmotors-react-pj/main/logged';
import Unlogged from 'webmotors-react-pj/main/unlogged';
import StyleCopi from 'webmotors-react-pj/main/content/style';
import Nps from 'webmotors-react-pj/nps';

export default ({ logged, unloggedPage, children }) => (
    logged
        ? (
            <Logged>
                {children}
                {
                    !/\/logout/.test(window.location.pathname)
                    && (/(webmotors|cockpit).com.br\/(?!(crm|store|stock|mais-fidelidade))/).test(window.location.href)
                    && <Nps />
                }
            </Logged>
        )
        : (
            <Unlogged unloggedPage={unloggedPage}>
                {children}
            </Unlogged>
        )
);
