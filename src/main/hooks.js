
import { cookieGet, paramURL } from 'webmotors-react-pj/utils';

const { pathname } = window.location;
const bokey = Boolean(paramURL('bokey'));
const cockpitLogged = Boolean(cookieGet('CockpitLogged'));
const unloggedUrl = [
    '/',
    '/logout',
    '/redefinir-senha',
    '/redefinir-senha/email-enviado',
    '/redefinir-senha/resetar-senha',
    '/redefinir-senha/troca-de-seguranca',
    '/usuario/atualizar',
    '/usuario/senha-atualizada',
    '/usuario/introducao-cockpit',
    '/solucoes/crm',
    '/solucoes/estoque',
    '/solucoes/autoguru',
    '/solucoes/universidade',
    '/solucoes/feiroes',
];
const unloggedPage = unloggedUrl.includes(pathname);

export {
    pathname,
    bokey,
    cockpitLogged,
    unloggedPage,
};
