import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { UrlCockpit } from 'webmotors-react-pj/config';
import Header from 'webmotors-react-pj/frame/header/logged/style';
import Logo from 'webmotors-react-pj/frame/header/logged/logo';
import Wrapper from 'webmotors-react-pj/main/logged/style';
import Menu from 'webmotors-react-pj/frame/menu/style';
import Container from 'webmotors-react-pj/main/style';
import PageStatus from 'webmotors-react-pj/page-status';
import Footer from 'webmotors-react-pj/frame/footer/logged';
import {
    StyleError,
    StyleErrorImg,
} from 'webmotors-react-pj/frame/menu/list/style';

class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false,
        };
    }

    componentDidCatch(error) {
        console.error(error);
        this.setState({ error: true });
    }

    render() {
        const title = 'Aplicação indisponível';
        return (
            this.state.error
                ? (
                    <Fragment>
                        <Helmet>
                            <title>{title}</title>
                        </Helmet>
                        <Header>
                            <Logo />
                        </Header>
                        <Wrapper>
                            <Menu>
                                <StyleError onClick={() => window.location.reload()}>
                                    <StyleErrorImg alt="" src={`${UrlCockpit}/assets/img/icons/barrier.svg`} aria-hidden="true" />
                                    Recarregar página
                                </StyleError>
                            </Menu>
                            <Container>
                                <PageStatus title={title} text="Por favor entre em contato conosco." />
                            </Container>
                        </Wrapper>
                        <Footer />
                    </Fragment>
                )
                : this.props.children
        );
    }
}

export default ErrorBoundary;
