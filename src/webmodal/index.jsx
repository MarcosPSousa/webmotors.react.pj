import React, { useState, useEffect } from 'react';
import {
    StyleWrapper,
    StyleBackground,
    StyleBox,
    StyleClose,
    StyleTitle,
    StyleContent,
    StyleInner,
    StyleButtons,
    StyleModalBody,
} from 'webmotors-react-pj/webmodal/style';
import { customAttributes } from 'webmotors-react-pj/utils';

export default (props) => {
    const {
        title,
        buttons,
        onOpen = false,
        onClose,
        children,
        dontClose,
    } = props;
    const customProps = customAttributes(props, 'title|onOpen|onClose');

    const [isOpen, setIsOpen] = useState(onOpen);

    const handleClose = (e) => {
        if (onClose) {
            onClose(e);
        } else {
            console.error('set props onClose in WebModal');
        }
        setIsOpen(false);
    };

    const handleClickBackground = (e) => {
        if (!dontClose) {
            handleClose(e)
        }
    };

    const handleClickClose = (e) => {
        handleClose(e)
    };

    const renderTitle = () => title ? <StyleTitle>{title}</StyleTitle> : false;

    const renderButtons = () => buttons ? <StyleButtons>{buttons}</StyleButtons> : false;

    const renderClose = () => (
        dontClose
            ? false
            : (
                <StyleClose onClick={e => handleClickClose(e)}>
                    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.5808 11.95L20.2618 5.26901C20.7128 4.81807 20.7128 4.08919 20.2618 3.63825C19.8109 3.18731 19.082 3.18731 18.6311 3.63825L11.95 10.3193L5.26901 3.63825C4.81807 3.18731 4.08919 3.18731 3.63825 3.63825C3.18731 4.08919 3.18731 4.81807 3.63825 5.26901L10.3193 11.95L3.63825 18.6311C3.18731 19.082 3.18731 19.8109 3.63825 20.2618C3.86315 20.4867 4.15839 20.5998 4.45363 20.5998C4.74888 20.5998 5.04412 20.4867 5.26901 20.2618L11.95 13.5808L18.6311 20.2618C18.856 20.4867 19.1512 20.5998 19.4465 20.5998C19.7417 20.5998 20.037 20.4867 20.2618 20.2618C20.7128 19.8109 20.7128 19.082 20.2618 18.6311L13.5808 11.95Z" />
                    </svg>
                </StyleClose>
            )
    );

    useEffect(() => setIsOpen(onOpen), [onOpen]);

    return (
        isOpen && (
            <StyleWrapper {...customProps}>
                <StyleModalBody />
                <StyleBackground onClick={e => handleClickBackground(e)} />
                <StyleBox>
                    {renderClose()}
                    <StyleContent>
                        {renderTitle()}
                        <StyleInner>{children}</StyleInner>
                        {renderButtons()}
                    </StyleContent>
                </StyleBox>
            </StyleWrapper>
        )
    )
};
