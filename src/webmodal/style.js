import styled, { createGlobalStyle } from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const renderSize = size => ({
    default: '856px',
    large: '1048px',
}[size || 'default']);

export const StyleModalBody = createGlobalStyle`
    body{
        overflow: hidden;
    }
`;

export const StyleWrapper = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    z-index: 4;
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    overflow-y: scroll;
    *::-webkit-scrollbar {
        width: 10px;
    }
    *::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: ${color('gray-3')};
        border: 2px solid ${color('white')};
    }
    *:hover::-webkit-scrollbar-track {
        border-radius: 10px;
        background-color: ${color('gray-4')};
    }
    *:hover::-webkit-scrollbar-thumb {
        border-color: ${color('gray-4')};
    }
`;

export const StyleBackground = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: ${color('modal')};
`;

export const StyleBox = styled.div`
    background-color: ${color('white')};
    position: relative;
    padding: 56px;
    min-width: 312px;
    max-height: 90vh;
    border-radius: 8px;
    box-shadow: 0px 16px 16px -16px ${color('modal')};
    display: flex;
`;

export const StyleClose = styled.div`
    position: absolute;
    right: 32px;
    top: 32px;
    cursor: pointer;
    svg{
        width: 24px;
        height: 24px;
    }
`;

export const StyleTitle = styled.h2`
    font-weight: 500;
    font-size: 2.8rem;
    line-height: 1.5em;
    color: ${color('gray')};
    padding-bottom: 36px;
`;

export const StyleContent = styled.div`
    display: flex;
    flex-direction: column;
`;

export const StyleInner = styled.div`
    padding-bottom: 20px;
    overflow: auto;
    flex: 1;
    overflow-x: hidden;
`;

export const StyleButtons = styled.div`
    display: flex;
    justify-content: flex-end;
    border-top: 1px solid ${color('gray-4')};
    padding: 20px 0 0;
    button{
        margin-left: 12px;
        &:first-child{
            margin-left: 0;
        }
    }
`;


