import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color'

export const StyleWrapper = styled.div`
    width: ${({ size }) => `${size}px`};
    height: ${({ size }) => `${size}px`};
    border-radius: 100%;
    overflow: hidden;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    background-color: ${color('gray-3')};
    fill: ${color('white')};
`;
export const StyleImg = styled.img`
    width: auto;
    height: 100%;
`;
export const StyleSvg = styled.svg`
    width: 64%;
`;