import React from 'react';
import styled from 'styled-components';

export const StyleLogged = styled.div`
    padding: 32px;
`;

export const StyleUnlogged = styled.div`
    padding: 63px 0 0;
    display: flex;
    align-items: center;
    justify-content: center;
    flex: 1;
`;
