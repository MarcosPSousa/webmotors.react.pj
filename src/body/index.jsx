import React from 'react';
import {
    StyleLogged,
    StyleUnlogged,
} from 'webmotors-react-pj/body/style';

export default ({ children, logged = true }) => (
    logged 
        ? <StyleLogged role="main">{children}</StyleLogged>
        : <StyleUnlogged role="main">{children}</StyleUnlogged>
);
