import Axios from 'axios';
import Enum from './enum';
import cookieGet from '../cookie/get';

const Ajax = (props) => {
    const getStatus = param => (param.response ? param.response.status : (param.status || 0));

    const getPromise = param => Promise.resolve({
        data: param.data,
        status: param.status,
        success: param.success,
        target: param.target,
    });

    const msgError = text => console.error(text);

    const getError = (param) => {
        const data = Enum[getStatus(param)];
        msgError(`[${getStatus(param)}] ${data}`);
        return getPromise({
            data,
            status: getStatus(param),
            target: props.target,
            success: false,
        });
    };

    const getSuccess = (param) => {
        const data = (param.data.data === true ? param.data : param.data.data)
        || param.data.Result
        || param.data;
        const r = getPromise({
            data,
            status: getStatus(param),
            target: props.target,
            success: true,
        });
        return r;
    };

    const parseError = (e) => {
        const response = e.response || {};
        const { status, data: body } = response;
        const error = (
            body && (
                body.errors
                || (body.Messages && body.Messages[0])
            )
        );
        if (status >= 500) {
            return getError(e);
        }
        if (error) {
            return getPromise({
                data: error,
                status,
                success: false,
                target: props.target,
            });
        }
        return getError(e);
    };

    const {
        url,
        method = 'POST',
        headers,
        data,
    } = props;

    const request = Axios.request({
        url,
        method,
        data,
        headers: !headers
            ? ({
                'Content-Type': 'application/json',
                Authorization: `Bearer ${cookieGet('CockpitLogged')}`,
            })
            : headers,
        params: method === 'POST' ? '' : data,
    })
        .then(e => (e.status >= 200 && e.status < 300 ? getSuccess(e) : getError(e)))
        .catch(e => parseError(e));
    return request;
};

export {
    Ajax,
};
export default Ajax;
