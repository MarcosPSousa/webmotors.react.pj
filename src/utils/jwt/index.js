import cookieGet from '../cookie/get';

export default () => {
    const jwt = cookieGet('CockpitLogged');
    if (jwt) {
        const jwtPayload = jwt.replace(/_/g, '/').replace(/-/g, '+');
        const result = JSON.parse(decodeURIComponent(escape(atob(jwtPayload.split('.')[1]))));
        return result;
    }
    return {};
};
