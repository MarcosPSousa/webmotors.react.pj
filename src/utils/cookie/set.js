export default (key, value) => {
    window.document.cookie = `${key}=${value};domain=.webmotors.com.br;path=/`;
};
