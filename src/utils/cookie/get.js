export default (key) => {
    const value = `; ${window.document.cookie}`;
    const parts = value.split(`; ${key}=`);
    if (parts.length === 2) {
        return parts.pop().split(';').shift();
    }
    return '';
};
