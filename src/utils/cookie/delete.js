const remove = (key) => {
    if (key) {
        window.document.cookie = `${key}=;expires=Thu, 01 Jan 1970 00:00:00 GMT;domain=.webmotors.com.br;path=/`;
    }
};

const removeAll = (exceptArray = []) => {
    if (exceptArray.constructor.name === 'Array') {
        const cookies = document.cookie.split('; ');
        cookies.forEach((item) => {
            const d = window.location.hostname.split('.');
            while (d.length > 0) {
                const name = item.split(';')[0].split('=')[0];
                const hasException = exceptArray.filter(exceptName => exceptName === name);
                const cookieBase = `${window.encodeURIComponent(name)}=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=${d.join('.')} ;path=`;
                const p = window.location.pathname.split('/');
                if (!hasException.length) {
                    document.cookie = `${cookieBase}/`;
                }
                while (p.length > 0) {
                    if (!hasException.length) {
                        document.cookie = cookieBase + p.join('/');
                    }
                    p.pop();
                }
                d.shift();
            }
        });
    }
};

export {
    remove,
    removeAll,
};

export default remove;
