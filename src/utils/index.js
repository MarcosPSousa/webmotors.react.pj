import Immutable from 'immutee';
import Axios from 'axios';
import { UrlCockpit, ApiLogin } from 'webmotors-react-pj/config';

const WINDOW = typeof window !== 'undefined' && window;

const reducerCreate = (state, action, name) => {
    const success = `${name}`;
    const failure = `${name}_FAILURE`;
    const immutable = Immutable(state);
    switch (action.type) {
        case success: {
            return immutable
                .set('result', action.payload)
                .set('fetched', true)
                .done();
        }
        case failure: {
            return immutable
                .set('result', [])
                .set('fetched', true)
                .set('error', action.payload)
                .done();
        }
        default: {
            return state;
        }
    }
};

const scrollSmooth = (to, duration = 300) => {
    if (WINDOW) {
        clearTimeout(window.scrollSmoothTimerCache);
        if (duration <= 0) {
            return;
        }
        const difference = to - window.scrollY - 100;
        const perTick = (difference / duration * 10);
        window.scrollSmoothTimerCache = setTimeout(() => {
            if (!Number.isNaN(parseInt(perTick, 10))) {
                window.scrollTo(0, window.scrollY + perTick);
                scrollSmooth(to, duration - 10);
            }
        }, 10);
    }
};

const accordionAnimate = (testOpen, item) => {
    const tag = item;
    const getPadding = padding => parseFloat(getComputedStyle(tag)[padding]);
    tag.classList.add('accordion--transition');
    if (testOpen) {
        tag.style.height = 'auto';
        const { clientHeight } = tag;
        const height = (clientHeight + getPadding('paddingTop') + getPadding('paddingBottom'));
        tag.style.height = '0px';
        setTimeout(() => {
            tag.style.height = `${height}px`;
        }, 1);
    } else {
        tag.style.height = '0px';
        tag.addEventListener('transitionEnd', () => tag.classList.remove('accordion--transition'));
    }
};

const paramURL = (e) => {
    const query = new RegExp(`${e}=.+&|${e}=.+$`, 'g');
    const replaceQuery = new RegExp(`&|^${e}=`, 'g');
    const params = query.exec(window.location.search);
    if (params && params[0]) {
        return params[0].replace(replaceQuery, '');
    }
    return false;
};

const cookieSet = (key, value) => {
    window.document.cookie = `${key}=${value};domain=.webmotors.com.br;path=/`;
};

const cookieGet = (key) => {
    let cookie;
    const value = `; ${window.document.cookie}`;
    const parts = value.split(`; ${key}=`);
    if (parts.length === 2) {
        cookie = parts.pop().split(';').shift();
    }
    return cookie;
};

const cookieRemove = (key) => {
    if (WINDOW && key) {
        window.document.cookie = `${key}=;expires=Thu, 01 Jan 1970 00:00:00 GMT;domain=.webmotors.com.br;path=/`;
    }
};
const cookieRemoveAll = (exceptArray = []) => {
    if (exceptArray.constructor.name === 'Array') {
        const cookies = document.cookie.split('; ');
        cookies.forEach((item) => {
            const d = window.location.hostname.split('.');
            while (d.length > 0) {
                const name = item.split(';')[0].split('=')[0];
                const hasException = exceptArray.filter(exceptName => exceptName === name);
                const cookieBase = `${window.encodeURIComponent(name)}=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=${d.join('.')} ;path=`;
                const p = window.location.pathname.split('/');
                if (!hasException.length) {
                    document.cookie = `${cookieBase}/`;
                }
                while (p.length > 0) {
                    if (!hasException.length) {
                        document.cookie = cookieBase + p.join('/');
                    }
                    p.pop();
                }
                d.shift();
            }
        });
    }
};

const storageSet = (key = '', value = '') => window.localStorage.setItem(key, value);

const storageGet = (key = '') => window.localStorage.getItem(key);

const storageRemove = (key = '') => window.localStorage.removeItem(key);

const storageRemoveAll = (exceptStorage = []) => {
    if (exceptStorage.constructor.name === 'Array') {
        const { localStorage } = window;
        Object.keys(localStorage).forEach(key => {
            const exceptItem = exceptStorage.filter(item => item === key);
            if(!exceptItem.length) {
                localStorage.removeItem(key);
            }
        });
    }
}

const jwtGet = () => {
    const jwt = cookieGet('CockpitLogged') || cookieGet('AppLogged');
    if (jwt) {
        const jwtPayload = jwt.replace(/_/g, '/').replace(/-/g, '+');
        const result = JSON.parse(decodeURIComponent(escape(atob(jwtPayload.split('.')[1]))));
        return result;
    }
    return {
        sid: '',
        unique_name: '',
        nameid: '',
        email: '',
        status: '',
        idGrupo: '',
        nomeGrupo: '',
        utilizaCrmTerceiro: '',
        nomeFantasia: '',
        tenant: '',
        role: '',
        permissions: [],
        produtos: [],
    };
};

const dispatchGet = (keyword, url, data, method = 'POST') => (
    dispatch => (
        Axios({
            method,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${cookieGet('CockpitLogged')}`,
            },
            url,
            data,
            params: method === 'POST' ? '' : data,
        }).then((res) => {
            const resData = res && (res.data.data || res.data.Result);
            const resStatus = res && res.status;
            return dispatch({
                type: keyword,
                payload: {
                    success: true,
                    data: resData,
                    status: resStatus,
                },
            });
        }).catch((rej) => {
            let resData;
            let resStatus;
            const resErrorText = {
                400: 'Dados inválidos',
                401: 'Usuário não autorizado para esta ação',
                403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
                500: 'Erro interno da aplicação, tente novamente mais tarde',
                503: 'Servidor fora do ar, tente novamente mais tarde',
            };
            const resErrorTextUndefined = 'Erro não parametrizado pelo servidor';
            console.error(rej);
            if (rej && rej.response) {
                const errorData = rej.response.data;
                resStatus = rej.status || rej.response.status;
                resData = (
                    errorData.errors
                    || (errorData.Messages && errorData.Messages[0])
                    || errorData
                    || (() => {
                        resData = resErrorText[resStatus];
                        if (!resData) {
                            resData = resErrorTextUndefined;
                        }
                        return resData;
                    })(resStatus)
                );
            } else {
                resStatus = /(\d+|\d+\n)/.exec(rej) ? /(\d+|\d+\n)/.exec(rej)[0].trim() : false;
                resData = resErrorText[resStatus];
                if (!resData) {
                    resData = resErrorTextUndefined;
                }
            }
            return dispatch({
                type: `${keyword}_FAILURE`,
                payload: {
                    success: false,
                    errors: resData,
                    status: Number(resStatus),
                },
            });
        })
    )
);

const AjaxAction = (param) => {
    if (param.status === 401 && Boolean(cookieGet('CockpitLogged'))) {
        window.location.href = `${UrlCockpit}/logout`;
    }
};

const AjaxRefresh = (AjaxFn, props, jwt) => {
    AjaxFn({
        url: `${ApiLogin}/access/sso/refreshtoken`,
        method: 'POST',
        data: { Token: jwt },
        refresh: true,
    }).then((e) => {
        if (e.success) {
            cookieSet('CockpitLogged', e.data.jwt);
            AjaxFn(props);
        }
        AjaxAction(e);
    });
};

const Ajax = (props) => {
    const json = jwtGet();
    const getStatus = param => (param.response ? param.response.status : (param.status || 0));
    const getPromise = param => Promise.resolve({
        data: param.data,
        status: param.status,
        success: param.success,
        target: param.target,
    });
    const msgText = {
        0: 'Erro não parametrizado',
        400: 'Dados inválidos',
        401: 'Usuário não autorizado para esta ação',
        403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
        404: 'Não conseguimos encontrar o que você estava procurando, tente novamente em instantes.',
        405: 'O método HTTP não está suportado',
        407: 'Credencias de autenticação por proxy server inválida',
        408: 'Conexão encerrada pelo servidor, por questão de desuso',
        409: 'Conflito com o recurso que está no servidor',
        411: 'Content-Length no server está indefinida',
        412: 'Acesso negado ao recurso de destino',
        413: 'Carga de requisição mais larga que os limites estabelecidos pelo servidor',
        414: 'URI maior do que o servidor aceita interpretar',
        415: 'Servidor se recusa a aceitar este formato de payload',
        416: 'Servidor não pode atender ao Header Range solicitado',
        417: 'Expectativa enviada no cabeçalho da requisição Expect não foi suprida',
        500: 'Ops! O Cockpit está em manutenção e nossa equipe já está atuando para resolver, fique tranquilo que já já voltamos.',
        501: 'Ação indisponível, tente novamente em instantes.',
        502: 'Sistema indisponível, por favor entre em contato conosco.',
        503: 'Sistema indisponível, tente novamente em instantes.',
        504: 'Ação indisponível, tente novamente.',
        505: 'Versão HTTP não é suportada pelo servidor',
        507: 'Servidor não suporta armazenar este dado',
        508: 'Servidor finalizou a operação porque encontrou um loop infinito',
    };
    const getError = (param) => {
        const status = getStatus(param);
        const data = msgText[status];
        const { response } = param;
        const responseHas = Boolean(response);
        console.error(`[${status}] ${data}`);
        AjaxAction(responseHas ? response : param);
        return getPromise({
            data,
            status,
            target: props.target,
            success: false,
        });
    };
    const getSuccess = (param) => {
        const data = (param.data.data === true ? param.data : param.data.data)
        || param.data.Result
        || param.data;
        const r = getPromise({
            data,
            status: getStatus(param),
            target: props.target,
            success: true,
        });
        return r;
    };
    const { exp: timeExpires } = json;
    const timeNow = Date.now();
    const timeExpiresToSecound = new Date(timeExpires * 1000).getTime();
    const jwt = cookieGet('CockpitLogged');
    if (
        timeExpires
        && Boolean(timeExpiresToSecound)
        && timeNow > timeExpiresToSecound
        && !props.refresh
    ) {
        AjaxRefresh(Ajax, props, jwt);
    }
    const {
        url,
        method = 'POST',
        headers,
        data,
    } = props;
    const request = Axios.request({
        url,
        method,
        data,
        headers: !headers
            ? ({
                'Content-Type': 'application/json',
                Authorization: `Bearer ${jwt}`,
            })
            : headers,
        params: (method === 'GET' || method === 'HEAD') ? data : '',
    })
        .then(e => (e.status >= 200 && e.status < 300 ? getSuccess(e) : getError(e)))
        .catch((e) => {
            const response = e.response || {};
            const { status, data: body } = response;
            const error = (
                body && (
                    body.errors
                    || (body.Messages && body.Messages[0])
                    || body.data
                )
            );
            AjaxAction(response);
            if (status >= 500) {
                return getError(e);
            }
            if (error) {
                return getPromise({
                    data: error,
                    status,
                    success: false,
                    target: props.target,
                });
            }
            return getError(e);
        });
    return request;
};

const ajax = (url, data, method = 'POST', dispatch) => (
    Axios({
        method,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${cookieGet('CockpitLogged')}`,
        },
        url,
        data,
        params: method === 'POST' ? '' : data,
    }).then((res) => {
        const resData = (
            res
            && res.data
            && (
                (
                    res.data.data === true
                        ? res.data
                        : res.data.data
                )
                || res.data.Result))
            || res.data;
        const resStatus = res && res.status;
        const returnObj = {
            type: `AJAX_${url}`,
            payload: {
                success: true,
                data: resData,
                status: resStatus,
            },
        };
        if (dispatch) {
            return dispatch(returnObj);
        }
        return returnObj;
    }).catch((rej) => {
        let resData;
        let resStatus;
        const resErrorText = {
            400: 'Dados inválidos',
            401: 'Usuário não autorizado para esta ação',
            403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
            500: 'Erro interno da aplicação, tente novamente mais tarde',
            503: 'Servidor fora do ar, tente novamente mais tarde',
        };
        const resErrorTextUndefined = 'Erro não parametrizado pelo servidor';
        if (rej && rej.response) {
            const errorData = rej.response.data;
            resStatus = rej.status || rej.response.status;
            resData = (
                errorData.errors
                || (errorData.Messages && errorData.Messages[0])
                || errorData
                || (() => {
                    resData = resErrorText[resStatus];
                    if (!resData) {
                        resData = resErrorTextUndefined;
                    }
                    return resData;
                })(resStatus)
            );
        } else {
            resStatus = /(\d+|\d+\n)/.exec(rej) ? /(\d+|\d+\n)/.exec(rej)[0].trim() : false;
            resData = resErrorText[resStatus];
            if (!resData) {
                resData = resErrorTextUndefined;
            }
        }
        const returnObj = {
            type: `AJAX_${url}_FAILURE`,
            payload: {
                success: false,
                errors: resData,
                status: Number(resStatus),
            },
        };
        if (dispatch) {
            return dispatch(returnObj);
        }
        return returnObj;
    })
);

const adobeAnalytics = (params) => {
    /* const {
        event,
        name,
        title,
        flow:
        flowType,
    } = params;
    if (!params.flow) {
        console.error('[Adobe Analytics] flowType não está definido');
    }
    window.objDataLayer.page = {
        pageName: name || window.location.pathname,
        pageType: title || window.document.title,
        flowType,
    };
    window.objDataLayer.user = {
        userID: jwtGet().sid,
    };
    window.objDataLayer.dealer = {
        idAnunciante: jwtGet().unique_name,
    };
    if (event) {
        document.addEventListener(event, () => console.log(event));
        document.dispatchEvent(new CustomEvent(event, {
            detail: document.objDataLayer,
        }));
        console.log(window.objDataLayer);
    } */
};

const listboxItemAnimation = () => {
    setTimeout(() => [...document.querySelectorAll('.listbox__item')].forEach((item, i) => setTimeout(() => item.classList.add('listbox__item--active'), Math.min(i * 60, 2000))), 10);
};

const formatDate = dateString => new Date(dateString).toLocaleDateString('pt-BR', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
});

const formatMoney = number => Number(number).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });

const formatPhone = number => number.replace(/\D/g, '').replace(/(\d{2})((?=\d{9})(\d{5})(\d{4})|(?=\d{8})(\d{4})(\d{4}))/, (...e) => {
    if (e[3]) {
        return `(${e[1]}) ${e[3]}-${e[4]}`;
    }
    if (e[5]) {
        return `(${e[1]}) ${e[5]}-${e[6]}`;
    }
    console.error(`formatPhone: invalid for "${number}"`);
    return '';
});

const formatCEP = number => number.replace(/\D/g, '').replace(/(\d{5})(\d{3})/, '$1-$2');

const formatCNPJ = number => number.replace(/\D/g, '').replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');

const formatCPF = number => number.replace(/\D/g, '').replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');

const isUserGroup = () => jwtGet().idGrupo && jwtGet().role && jwtGet().role.indexOf('1') !== -1;

if (WINDOW) {
    ((ElementProto) => {
        const item = ElementProto;
        if (typeof ElementProto.matches !== 'function') {
            item.matches = ElementProto.msMatchesSelector
                || ElementProto.mozMatchesSelector
                || ElementProto.webkitMatchesSelector
                || function matches(selector) {
                    const element = this;
                    const elements = (
                        element.document
                        || element.ownerDocument
                    ).querySelectorAll(selector);
                    let index = 0;
                    while (elements[index] && elements[index] !== element) {
                        ++index;
                    }
                    return Boolean(elements[index]);
                };
        }
        if (typeof ElementProto.closest !== 'function') {
            item.closest = function closest(selector) {
                let element = this;
                while (element && element.nodeType === 1) {
                    if (element.matches(selector)) {
                        return element;
                    }
                    element = element.parentNode;
                }
                return null;
            };
        }
    })(window.Element.prototype);
}

const customAttributes = (props, except) => {
    const propsCustom = {};
    Object.entries(props).forEach((item) => {
        const [propKey, propValue] = item;
        const regex = new RegExp(`^(?!(${except})).+`);
        if (regex.test(propKey)) {
            propsCustom[propKey] = propValue;
        }
    });
    return propsCustom;
};

export {
    WINDOW,
    ajax,
    Ajax,
    adobeAnalytics,
    reducerCreate,
    dispatchGet,
    paramURL,
    cookieSet,
    cookieGet,
    cookieRemove,
    cookieRemoveAll,
    storageSet,
    storageGet,
    storageRemove,
    storageRemoveAll,
    scrollSmooth,
    accordionAnimate,
    jwtGet,
    formatDate,
    formatMoney,
    formatPhone,
    formatCEP,
    formatCNPJ,
    formatCPF,
    listboxItemAnimation,
    isUserGroup,
    customAttributes,
};
export default WINDOW;
