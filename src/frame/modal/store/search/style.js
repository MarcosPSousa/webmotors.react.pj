import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.label`
    display: flex;
    align-items: center;
    flex: 1;
    width: 80%;
    margin: 0 auto 12px;
    position: relative;
    &:before{
        content: '';
        position: absolute;
        width: 100%;
        width: calc(100% + 28px);
        left: 0;
        bottom: 0;
        border-bottom: 2px solid ${color('#43bccd')};
        transform: ${({ focus }) => (focus ? 'translate(0px, -1px) rotateY(0deg)' : 'translate(-28px, -1px) rotateY(90deg)')};
        transition: transform 0.3s;
    }
`;

export const StyleImg = styled.img`
    width: 20px;
    height: 20px;
`;
export const StyleInputWrapper = styled.span`
    padding-left: 8px;
    flex: 1;
`;
export const StyleInput = styled.input`
    width: 100%;
    border-style: none;
    border-bottom: 1px solid ${color('gray-4')};
    padding: 11px 4px;
    line-height: 2.4rem;
    font-size: 1.6rem;
    outline-style: none;
`;
