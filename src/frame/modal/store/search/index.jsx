import React, { useState } from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import {
    StyleWrapper,
    StyleImg,
    StyleInputWrapper,
    StyleInput,
} from 'webmotors-react-pj/frame/modal/store/search/style';

export default ({ onSearch }) => {
    const [focus, setFocus] = useState(false);

    return (
        <StyleWrapper focus={focus}>
            <StyleImg src={`${UrlCockpit}/assets/img/icons/search.svg`} alt="" aria-hidden="true" />
            <StyleInputWrapper>
                <StyleInput
                    placeholder="Buscar por loja"
                    onFocus={() => setFocus(true)}
                    onInput={e => onSearch(e.target.value)}
                    onBlur={() => setFocus(false)}
                />
            </StyleInputWrapper>
        </StyleWrapper>
    );
};
