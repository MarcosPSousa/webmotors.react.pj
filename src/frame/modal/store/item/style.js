import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleList = styled.li`
    border-bottom: 1px solid ${color('gray-4')};
    padding: 8px 16px;
    font-size: 1.4rem;
    transition: padding 0.3s, height 0.3s;
    align-items: center;
    justify-content: space-between;
    display: ${({ open }) => (open ? 'flex' : 'none')};
    &:first-child{
        border-top: 1px solid ${color('gray-4')};
    }
`;

export const StyleInfo = styled.div`
    flex: 1;
    display: flex;
    color: ${color('#43bccd')};
    align-items: center;
`;

export const StyleImg = styled.img`
    width: 16px;
    height: 16px;
    margin-right: 8px;
    transform: translateY(-1px);
`;

export const StyleButton = styled.button`
    background-color: transparent;
    color: ${color('#43bccd')};
    border: 1px solid ${color('#43bccd')};
    border-radius: 3px;
    font-family: 'Poppins', sans-serif;
    outline-style: none;
    padding: 8px 18px;
    font-weight: 500;
    transition: opacity 0.2s;
    &:hover{
        opacity: 0.7;
    }
    &[disabled]{
        opacity: 0.3;
    }
`;
