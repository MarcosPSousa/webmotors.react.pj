import React from 'react';
import {
    StyleList,
    StyleInfo,
    StyleImg,
    StyleButton,
} from 'webmotors-react-pj/frame/modal/store/item/style';
import { Ajax, cookieRemove, cookieSet } from 'webmotors-react-pj/utils';
import { UrlCockpit, ApiCockpit } from 'webmotors-react-pj/config';

export default ({
    id,
    children,
    open,
    redirect,
}) => {
    const selectStore = (e) => {
        [...e.target.closest('ul').parentNode.querySelectorAll('button')].forEach((btn) => {
            const item = btn;
            item.disabled = 'disabled';
        });
        Ajax({
            url: `${/estoque.webmotors/.test(window.location.host) ? '/api' : ApiCockpit}/Grupo/RetornarLoja`,
            data: { idCliente: id },
            method: 'GET',
        }).then((f) => {
            if (f.success) {
                const reload = () => window.location.reload();
                cookieRemove('CockpitMenuItems');
                cookieSet('CockpitLogged', f.data);
                cookieSet('CockpitStoreName', children);
                setTimeout(() => {
                    if (redirect) {
                        if (redirect !== window.location.href) {
                            window.location.href = redirect;
                        } else {
                            reload();
                        }
                    } else {
                        reload();
                    }
                }, 100);
            } else {
                console.error(f);
            }
        });
    };

    return (
        <StyleList open={open}>
            <StyleInfo>
                <StyleImg src={`${UrlCockpit}/assets/img/icons/pin-map.svg`} alt="" aria-hidden="true" />
                {children}
            </StyleInfo>
            <StyleButton type="button" onClick={e => selectStore(e)}>Selecionar</StyleButton>
        </StyleList>
    );
};
