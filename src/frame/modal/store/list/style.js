import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.h3`
    color: ${color('gray-2')};
    font-size: 1.6rem;
    font-weight: 600;
    padding: 32px 0 12px 16px;
    border-bottom: 1px solid ${color('gray-4')};
`;
