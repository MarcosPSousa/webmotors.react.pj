import React, { Fragment } from 'react';
import { jwtGet } from 'webmotors-react-pj/utils';
import StyleTitle from 'webmotors-react-pj/frame/modal/store/list/style';
import Item from 'webmotors-react-pj/frame/modal/store/item';

export default ({ data, term, redirect }) => (
    data.map((item, i) => {
        const key = i;
        const { unique_name: uniqueName } = jwtGet;
        return (
            <Fragment key={`list-${key}`}>
                {item.title && <StyleTitle>{item.title}</StyleTitle>}
                <ul>
                    {
                        item.items.map((list) => {
                            const { nomeFantasia, idLoja } = list;
                            const searchTerm = term.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
                            const nameNormalize = nomeFantasia.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
                            const isOpen = nameNormalize.indexOf(searchTerm) !== -1;
                            return (
                                <Item
                                    key={idLoja}
                                    id={idLoja}
                                    open={isOpen}
                                    redirect={redirect}
                                >
                                    {nomeFantasia}
                                </Item>
                            );
                        })
                    }
                </ul>
            </Fragment>
        );
    })
);
