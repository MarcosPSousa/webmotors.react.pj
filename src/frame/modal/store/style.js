import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.p`
    color: ${color('gray-2')};
    font-size: 1.4rem;
    text-align: center;
    padding: 0 16px 32px;
`;
