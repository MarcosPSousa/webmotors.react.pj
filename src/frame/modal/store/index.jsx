import React, { useState, useEffect, Fragment } from 'react';
import Modal from 'webmotors-react-pj/modal';
import { ApiCockpit, UrlCockpit } from 'webmotors-react-pj/config';
import { Ajax, jwtGet } from 'webmotors-react-pj/utils';
import StyleLoad from 'webmotors-react-pj/frame/load';
import Search from 'webmotors-react-pj/frame/modal/store/search';
import List from 'webmotors-react-pj/frame/modal/store/list';
import StyleError from 'webmotors-react-pj/frame/modal/store/style';

export default ({ onClose, onRedirect }) => {
    const [loaded, setLoaded] = useState(false);
    const [success, setSuccess] = useState(false);
    const [content, setContent] = useState();
    const [search, setSearch] = useState('');
    const [title, setTitle] = useState('Carregando...');
    const dataAllStore = [{ items: [{ idLoja: 0, nomeFantasia: 'Todas as lojas' }] }];
    const { unique_name: uniqueName } = jwtGet();

    useEffect(() => {
        Ajax({
            url: `${/estoque.webmotors/.test(window.location.host) ? '/api' : ApiCockpit}/Grupo/Listarlojas`,
            method: 'GET',
        }).then((e) => {
            const { success, data } = e;
            if (success) {
                const dataFormat = data.map(item => ({ title: item.estado, items: item.lojas }));
                setSuccess(true);
                setTitle('Selecionar loja');
                setContent(dataFormat);
            } else {
                setSuccess(false);
                setTitle('Erro de listagem');
                setContent(data);
            }
            setLoaded(true);
        });
    }, []);
    
    const renderContent = () => (
        success
            ? (
                <Fragment>
                    <Search onSearch={term => setSearch(term)} />
                    {uniqueName !== '0' && <List data={dataAllStore} term={''} redirect={onRedirect} />}
                    <List data={content} term={search} redirect={onRedirect} />
                </Fragment>
            )
            : <StyleError>{data}</StyleError>
    );

    return (
        <Modal icon={`${UrlCockpit}/assets/img/icons/pin-map.svg`} title={title} onClose={() => onClose()}>
            {
                loaded
                    ? renderContent()
                    : <StyleLoad />
            }
        </Modal>
    );
};
