import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.header`
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    z-index: 3;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0px 24px;
    font-weight: bold;
    font-size: 12px;
    background-color: ${color('white')};
    box-shadow: 0 12px 12px -12px ${color('modal')};
`;
