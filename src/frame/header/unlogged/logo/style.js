import styled from 'styled-components';
import Img from 'webmotors-react-pj/img';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.a`
    width: 118px;
    line-height: 50%;
`;

export const StyleImg = styled(Img)`
    .st0{
        fill: ${color('#232323')};
    }
    .st1{
        fill: ${color('primary')};
    }
`;
