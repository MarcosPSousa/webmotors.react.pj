
import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { scrollSmooth } from 'webmotors-react-pj/utils';
import { StyleWrapper, StyleImg } from 'webmotors-react-pj/frame/header/unlogged/logo/style';

export default () => {
    const handleClick = () => scrollSmooth(0);

    return (
        <StyleWrapper onClick={() => handleClick()} href="/">
            <StyleImg src={`${UrlCockpit}/assets/img/brands/cockpit.svg`} aria-label="Logo do Cockpit" />
        </StyleWrapper>
    );
};
