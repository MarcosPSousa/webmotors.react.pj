import React, { useRef, useEffect, useState } from 'react';
import { scrollSmooth } from 'webmotors-react-pj/utils';
import Submenu from 'webmotors-react-pj/frame/header/unlogged/menu/submenu';
import {
    StyleWrapper,
    StyleItem,
    StyleTabLink,
    StyleTabItem,
} from 'webmotors-react-pj/frame/header/unlogged/menu/items/style';

export default ({ moveLine }) => {
    const firstTab = useRef();
    const [open, setOpen] = useState(false);

    const LineMove = (tag) => {
        const rect = tag.getBoundingClientRect();
        const {
            left,
            width,
            top,
            height,
        } = rect;
        moveLine({ left, width, top: top + height });
    };

    const MouseEnter = tag => LineMove(tag);

    useEffect(() => LineMove(firstTab.current), []);

    return (
        <StyleWrapper>
            <StyleItem ref={firstTab}>
                <StyleTabLink href="/" onClick={() => scrollSmooth(0)} onMouseEnter={e => MouseEnter(e.target)}>Login</StyleTabLink>
            </StyleItem>
            <StyleItem onMouseLeave={() => setOpen(false)}>
                <StyleTabItem
                    onMouseEnter={(e) => {
                        MouseEnter(e.target);
                        setOpen(true);
                    }}
                >
                    Soluções
                </StyleTabItem>
                <Submenu onOpen={open} />
            </StyleItem>
        </StyleWrapper>
    );
};
