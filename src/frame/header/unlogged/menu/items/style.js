import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.ul`
    display: flex;
`;

export const StyleItem = styled.li`
    position: relative;
`;

export const StyleTabLink = styled.a`
    display: block;
    padding: 22px 32px;
    cursor: pointer;
    transition: color 0.3s;
`;

export const StyleTabItem = styled.span`
    display: block;
    padding: 22px 32px;
    cursor: pointer;
    transition: color 0.3s;
`;

export const StyleSubMenu = styled.ul`
    position: absolute;
    right: 0;
    top: 100%;
    min-width: 100%;
    border-top: 12px solid transparent;
    box-shadow: 8px 0 12px -20px rgba(35, 35, 35, 0.6), -12px 0 12px -20px rgba(35, 35, 35, 0.6);
    &:before{
        content: '';
        width: 19px;
        height: 19px;
        transform: rotate(45deg) translate(10px, 10px);
        border-radius: 2px;
        background: ${color('white')};
        position: absolute;
        right: 24px;
        top: -20px;
    }
`;
