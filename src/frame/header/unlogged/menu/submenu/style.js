import React from 'react';
import styled, { keyframes, css } from 'styled-components';
import { scrollSmooth } from 'webmotors-react-pj/utils';
import color from 'webmotors-react-pj/tokens/color';

const ElementSubLinkKeyframes = keyframes`
    0% {opacity: 0;}
    100% {opacity: 1;}
`;

const ElementSubLinkAnimation = ({ data }) => (
    data.active
        ? css`${ElementSubLinkKeyframes} ${data.index * 0.2}s forwards`
        : ''
);

const ElementSubItem = styled.li`
    opacity: 1;
    transition: opacity 0.3s;
    &:first-child a{
        border-radius: 8px 8px 0 0;
    }
    &:last-child a{
        border-radius: 0 0 8px 8px;
    }
`;

const ElementSubLink = styled.a`
    background-color: ${color('white')};
    display: block;
    position: relative;
    z-index: 1;
    font-weight: 500;
    padding: 18px;
    min-width: 220px;
    border-bottom: 1px solid ${color('gray-4')};
    animation: ${ElementSubLinkAnimation};
    transition: color 0.3s, transform 0.3s 1s;
    &:hover{
        color: ${color('#43bccd')};
    }
`;

export const StyleSubMenu = styled.ul`
    position: absolute;
    right: 0;
    top: 100%;
    min-width: 100%;
    border-top: 12px solid transparent;
    box-shadow: 8px 0 12px -20px rgba(35, 35, 35, 0.6), -12px 0 12px -20px rgba(35, 35, 35, 0.6);
    pointer-events: ${({ active }) => (active ? 'all' : 'none')};
    opacity: ${({ active }) => (active ? 1 : 0)};
    &:before{
        content: '';
        width: 19px;
        height: 19px;
        transform: ${({ active }) => (active ? 'rotate(45deg) translate(10px, 10px)' : 'rotate(45deg) translate(0px, 0px)')};
        border-radius: 2px;
        background: ${color('white')};
        position: absolute;
        right: 24px;
        top: -20px;
        opacity: ${({ active }) => (active ? 1 : 0)};
        transition: opacity 0.3s, transform 0.3s;
    }
`;

export const StyleSubItem = ({ data, href, children }) => (
    <ElementSubItem>
        <ElementSubLink
            data={data}
            onClick={() => scrollSmooth(0)}
            href={href}
        >
            {children}
        </ElementSubLink>
    </ElementSubItem>
);
