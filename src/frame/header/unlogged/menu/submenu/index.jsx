import React from 'react';
import {
    StyleSubMenu,
    StyleSubItem,
} from 'webmotors-react-pj/frame/header/unlogged/menu/submenu/style';

export default props => (
    <StyleSubMenu active={props.onOpen}>
        <StyleSubItem data={{ index: 0, active: props.onOpen }} href="/solucoes/crm">CRM</StyleSubItem>
        <StyleSubItem data={{ index: 1, active: props.onOpen }} href="/solucoes/estoque">Estoque</StyleSubItem>
        <StyleSubItem data={{ index: 2, active: props.onOpen }} href="/solucoes/autoguru">Autoguru</StyleSubItem>
        <StyleSubItem data={{ index: 3, active: props.onOpen }} href="/solucoes/universidade">Universidade</StyleSubItem>
        <StyleSubItem data={{ index: 4, active: props.onOpen }} href="/solucoes/feiroes">Feirões</StyleSubItem>
    </StyleSubMenu>
);
