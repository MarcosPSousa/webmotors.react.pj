import React, { useState } from 'react';
import StyleWrapper from 'webmotors-react-pj/frame/header/unlogged/menu/style';
import Line from 'webmotors-react-pj/frame/header/unlogged/menu/line';
import Items from 'webmotors-react-pj/frame/header/unlogged/menu/items';

export default () => {
    const [line, setLine] = useState();
    return (
        <StyleWrapper>
            <Line line={line} />
            <Items moveLine={e => setLine(e)} />
        </StyleWrapper>
    );
};
