import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.i`
    position: absolute;
    bottom: 0;
    left: 0;
    height: ${({ height }) => `${height}px`};
    background-color: ${color('primary')};
    will-change: transform, width;
    transition: transform 0.3s, width 0.3s;
`;
