
import React from 'react';
import StyleLine from 'webmotors-react-pj/frame/header/unlogged/menu/line/style';

export default ({ line }) => {
    const height = 3;
    if (line) {
        return (
            <StyleLine
                height={height}
                style={{
                    transform: `translateX(${line.left}px)`,
                    top: line.top - height + 1,
                    width: line.width,
                }}
            />
        );
    }
    return null;
};
