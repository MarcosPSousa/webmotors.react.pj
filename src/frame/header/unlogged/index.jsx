
import React from 'react';
import StyleHeader from 'webmotors-react-pj/frame/header/unlogged/style';
import Logo from 'webmotors-react-pj/frame/header/unlogged/logo';
import Menu from 'webmotors-react-pj/frame/header/unlogged/menu';

export default () => (
    <StyleHeader>
        <Logo />
        <Menu />
    </StyleHeader>
);
