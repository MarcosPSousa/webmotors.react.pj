import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { StyleLink, StyleLogo } from 'webmotors-react-pj/frame/header/logged/logo/style';

export default () => (
    <StyleLink href={UrlCockpit}>
        <StyleLogo src={`${UrlCockpit}/assets/img/brands/cockpit.svg`} alt="Logo Cockpit" />
    </StyleLink>
);
