import styled from 'styled-components';

export const StyleLink = styled.a`
    width: 250px;
    height: 86px;
    line-height: 50%;
    box-shadow: 0 0px 8px 0 rgba(0, 0, 0, 0.1);
`;

export const StyleLogo = styled.img`
    width: 200px;
    margin: 20px auto;
    display: block;
`;
