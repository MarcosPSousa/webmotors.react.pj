import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.header`
    display: flex;
    align-items: center;
    color: ${color('gray-2')};
    background-color: ${color('white')};
    position: relative;
    box-shadow: 0 0 12px ${color('gray-3')};
    z-index: ${({ isOpen }) => (isOpen ? '4' : '0')};
`;
