import React, { useState } from 'react';
import StyleHeader from 'webmotors-react-pj/frame/header/logged/style';
import Logo from 'webmotors-react-pj/frame/header/logged/logo';
import Menu from 'webmotors-react-pj/frame/header/logged/items';

export default () => {
    const show = /crm\.webmotors/.test(window.location.hostname);
    return (
        show
            ? (
                'Menu'
            )
            : <div data-search />
    );
}