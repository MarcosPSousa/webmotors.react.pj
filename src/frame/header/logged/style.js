import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.header`
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    color: ${color('gray-2')};
    background-color: ${color('white')};
    position: relative;
    box-shadow: 0 0 12px ${color('gray-3')};
    z-index: 0;
    &[data-header-popover]{
        z-index: 5;
    }
    [data-icon-click]{
        cursor: pointer;
    }
    [data-icon-click] > *{
        pointer-events: none;
    }
`;
