import styled from 'styled-components';

export const StyleWrapper = styled.div`
    position: relative;
`;

export const StyleImg = styled.img`
    width: 24px;
    height: 24px;
    transform: translate(4px, 4px);
    opacity: 0.9;
    transition: opacity 0.3s;
    pointer-events: none;
    &:hover{
        opacity: 1;
    }
`;

export const StyleIcon = styled.div`
    position: relative;
    cursor: pointer;
`;
