import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const background = action => ({
    default: 'transparent',
    success: color('primary'),
    decline: 'transparent',
}[action || 'default']);

const textColor = action => ({
    default: color('gray-2'),
    success: color('white'),
    decline: color('gray-2'),
}[action || 'default']);

const borderColor = action => ({
    default: 'transparent',
    success: color('primary'),
    decline: color('gray-2'),
}[action || 'default']);

export const StyleButton = styled.button`
    display: inline-block;
    margin-right: 24px;
    background-color: ${({ action }) => background(action)};
    color: ${({ action }) => textColor(action)};
    border-width: 1px;
    border-radius: 4px;
    border-color: ${({ action }) => borderColor(action)};
    border-style: solid;
    padding: 8px 18px;
    font-size: 1.4rem;
    font-weight: 500;
    transition: opacity 0.2s, box-shadow 0.3s;
    &[disabled] {
        opacity: 0.7;
    }
`;

export const StyleLink = styled.a`
    &[action]{
        display: inline-block;
        margin-right: 24px;
        background-color: ${({ action }) => background(action)};
        color: ${({ action }) => textColor(action)};
        border-width: 1px;
        border-radius: 4px;
        border-color: ${({ action }) => borderColor(action)};
        border-style: solid;
        padding: 8px 18px;
        font-size: 1.4rem;
        font-weight: 500;
        transition: opacity 0.2s, box-shadow 0.3s;
        &[disabled] {
            opacity: 0.7;
        }
    }
`;
