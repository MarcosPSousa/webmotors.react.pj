import React from 'react';
import {
    StyleWrapper,
    StyleText,
    StyleActions,
    StyleError,
} from 'webmotors-react-pj/frame/header/logged/items/notification/modal/step2/style';
import StyleButton from 'webmotors-react-pj/frame/header/logged/items/notification/modal/buttons/style';

export default ({ onAccept }) => {
    return (
        <StyleWrapper>
            <StyleText>
                Seu interesse na renovação do produto Feirões para o ano de
                <br />
                2020 foi enviada ao seu gerente de relacionamento,
                <br />
                que entrará em contato em breve.
            </StyleText>
            <StyleActions>
                <StyleButton action="success" type="button" onClick={() => onAccept()}>Continuar</StyleButton>
            </StyleActions>
        </StyleWrapper>
    )
};
