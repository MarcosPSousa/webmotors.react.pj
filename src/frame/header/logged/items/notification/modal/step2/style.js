import styled from 'styled-components';

export const StyleWrapper = styled.div`
    text-align: center;
`;

export const StyleActions = styled.div`
    margin-top: 32px;
`;

export const StyleText = styled.div`
    font-size: 1.6rem;
    line-height: 1.50em;
`;
