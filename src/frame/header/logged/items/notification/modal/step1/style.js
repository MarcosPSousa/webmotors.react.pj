import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    display: flex;
    align-items: flex-start;
`;

export const StyleImg = styled.img`
    width: 96px;
    height: auto;
    border-radius: 4px;
`;

export const StyleContent = styled.div`
    flex: 1;
    padding-left: 16px;
    &[disabled] {
        opacity: 0.7;
    }
`;

export const StyleTitle = styled.div`
    font-size: 1.4rem;
    font-weight: bold;
`;

export const StylePhrase = styled.div`
    font-size: 1.2rem;
`;

export const StyleMessage = styled.div`
    margin-top: 32px;
    padding: 32px;
    background-color: ${color('#f9f9f9')};
    font-size: 1.2rem;
    border-radius: 4px;
    
    > p {
        font-style: italic;
        + p{
            margin-top: 12px;
        }
    }
`;

export const StyleMessageImg = styled.img`
    width: 100%;
    height: auto;
    margin: 12px 0 8px -6px;
`;

export const StyleActions = styled.div`
    margin-top: 32px;
`;

export const StyleButtonLink = styled.a`
    font-size: 1.2rem;
    &.link{
        color: ${color('primary')};
        text-decoration: none;
        cursor: pointer;
        background-color: transparent;
        &:before{
            border-bottom-color: ${color('primary')};
        }
    }
`;
