import React, { useState, Fragment, useEffect } from 'react';
import { UrlCockpit, UrlCRM } from 'webmotors-react-pj/config';
import {
    StyleWrapper,
    StyleImg,
    StyleContent,
    StyleTitle,
    StylePhrase,
    StyleMessage,
    StyleMessageImg,
    StyleActions,
    StyleButtonLink,
} from 'webmotors-react-pj/frame/header/logged/items/notification/modal/step1/style';
import { StyleButton, StyleLink } from 'webmotors-react-pj/frame/header/logged/items/notification/modal/buttons/style';
import TagAnalytics from 'webmotors-react-pj/tag-analytics';

export default ({ onAccept, onDecline, disabled, error }) => {
    useEffect(() => {
        const consoleEvent = (e) => console.log({ event: e.type });
        window.objDataLayer = TagAnalytics.objDatalayer;
        const { objDataLayer } = window;
        objDataLayer.site.country = 'brasil',
        objDataLayer.site.subEnvironment = 'homepage',
        objDataLayer.page.flowType = 'cockpit-homepage',
        objDataLayer.page.pageType = 'modal-car-delivery',
        objDataLayer.page.pageName = '/webmotors/cockpit/homepage/car-delivery',
        objDataLayer.page['pageName.tier1'] = 'cockpit',
        objDataLayer.page['pageName.tier2'] = 'homepage',
        objDataLayer.page['pageName.tier3'] = 'car-delivery',
        document.addEventListener('customPageView', consoleEvent);
        document.dispatchEvent(new CustomEvent('customPageView', { detail: objDataLayer }));
        setTimeout(() => document.removeEventListener('customPageView', consoleEvent), 300);
    }, []);

    return (
        <Fragment>
            <StyleWrapper>
                <StyleImg src={`${UrlCockpit}/assets/img/icons/placeholder-cardelivery2.svg`} alt="" />
                <StyleContent>
                    <StyleTitle>Venda 100% online com o CarDelivery!</StyleTitle>
                    <StylePhrase>
                        Sua loja cuida de toda a venda e seu cliente recebe o carro em casa, com mais praticidade, conforto e segurança.
                    </StylePhrase>
                    <StyleMessage>
                        <p>Veja como é fácil ativar o CarDelivery:</p>
                        <StyleMessageImg src={`${UrlCockpit}/assets/img/static/notifications-cardelivery.png`} />
                        <p>Basta ativar a opção em CRM &gt; Configurações.</p>
                        <StyleActions>
                            <StyleLink disabled={disabled} action="success" href={`${UrlCRM}/configuration/?idcmpint=t1:c17:m07:modal-cardeliverypj-crm:cardelivery::quero-oferecer`} onClick={() => onAccept()}>Quero ativar</StyleLink>
                            <StyleButton disabled={disabled} action="decline" type="button" onClick={() => onDecline()}>Agora não</StyleButton>
                            <StyleButtonLink href="https://www.webmotors.com.br/cardelivery/lojas/?idcmpint=t1:c17:m07:modal-lp-cardeliverypj:cardelivery::detalhes-cardelivery" target="_blank" className="link">Mais detalhes</StyleButtonLink>
                        </StyleActions>
                    </StyleMessage>
                </StyleContent>
            </StyleWrapper>
        </Fragment>
    );
};
