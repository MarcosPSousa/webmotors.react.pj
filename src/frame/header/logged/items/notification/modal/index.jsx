import React, { useState, useEffect } from 'react';
import Modal from 'webmotors-react-pj/modal';
import Step1 from 'webmotors-react-pj/frame/header/logged/items/notification/modal/step1';
import Step2 from 'webmotors-react-pj/frame/header/logged/items/notification/modal/step2';
import { ApiCockpit } from 'webmotors-react-pj/config';
import { Ajax, jwtGet } from 'webmotors-react-pj/utils';
import TagAnalytics from 'webmotors-react-pj/tag-analytics';

export default ({ onClose }) => {
    const [step, setStep] = useState(1);
    const [disabled, setDisabled] = useState(false);
    const [error, setError] = useState('');

    const modalClose = (dispatchAnalytics) => {
        onClose(dispatchAnalytics);
    };

    return (
        <Modal onClose={() => modalClose(true)}>
            <style>
                {`
                    [data-modal-notification] .modal__box{max-width: 776px;}
                    [data-modal-notification] .modal__wrap{margin-top: -60px;padding: 64px 32px 40px 40px;}
                `}
            </style>
            <Step1
                disabled={disabled}
                error={error}
                onAccept={() => modalClose(false)}
                onDecline={() => modalClose(true)}
            />
        </Modal>
    );
};
