import React, { useState, useEffect, useRef } from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { cookieGet, cookieSet, jwtGet } from 'webmotors-react-pj/utils';
import TagAnalytics from 'webmotors-react-pj/tag-analytics';
import { StyleImg, StyleIcon, StyleWrapper } from 'webmotors-react-pj/frame/header/logged/items/notification/style';
import NotificationModal from 'webmotors-react-pj/frame/header/logged/items/notification/modal';
import NotificationPopover from 'webmotors-react-pj/frame/header/logged/items/notification/popover';
import DotNotice from 'webmotors-react-pj/frame/header/logged/items/dot-notice';

export default () => {
    const [notice, setNotice] = useState('');
    const [modal, setModal] = useState(false);
    const elementWrapper = useRef();
    const cookieName = 'CockpitNotifications';
    const {
        sid: id,
        role,
        utilizaCrmTerceiro,
    } = jwtGet();
    const userAdmin = role ? (role.indexOf('2') !== -1 && utilizaCrmTerceiro.indexOf('0') !== -1) : false;
    const sid = Number(id);

    const noticeCookieGet = () => {
        const cookie = cookieGet(cookieName) || '[]';
        return JSON.parse(cookie);
    };

    const handleCloseModal = (dispatchAnalytics) => {
        setModal(false);
        document.body.click();

        if (dispatchAnalytics) {
            const consoleEvent = (e) => console.log({ event: e.type });
            window.objDataLayer = TagAnalytics.objDatalayer;
            const { objDataLayer } = window;
            objDataLayer.page.flowType = 'cockpit',
            objDataLayer.page.pageType = 'cockpit homepage',
            objDataLayer.page.pageName = '/webmotors/cockpit/homepage',
            document.addEventListener('customPageView', consoleEvent);
            document.dispatchEvent(new CustomEvent('customPageView', { detail: objDataLayer }));
            setTimeout(() => document.removeEventListener('customPageView', consoleEvent), 300);
        }
    };

    const handleClosePopover = () => {
        setModal(true);
    };

    const handlePopoverOpen = (value) => {
        const noticeUserHas = noticeCookieGet().includes(sid);
        if (!noticeUserHas) {
            const noticeCookie = noticeCookieGet();
            noticeCookie.push(sid);
            cookieSet(cookieName, JSON.stringify(noticeCookie));
        }
        const consoleEvent = () => console.log({ event: value });
        window.objDataLayer = TagAnalytics.objDatalayer;
        const { objDataLayer } = window;
        const isOpen = Boolean(document.querySelector('[data-popover-open]'));
        if (!isOpen) {
            document.addEventListener(value, consoleEvent);
            document.dispatchEvent(new CustomEvent(value, { detail: objDataLayer }));
        }
        setTimeout(() => document.removeEventListener(value, consoleEvent), 300);
        setNotice('');
    };

    useEffect(() => {
        const noticeUserHas = noticeCookieGet().includes(sid);
        if (!noticeUserHas) {
            setNotice('1');
        }
    }, []);
    
    return (
        userAdmin
            ? (
                <StyleWrapper data-icon="" data-modal-notification>
                    <StyleIcon onClick={() => handlePopoverOpen('notificacaoCockpit')} data-icon-click>
                        <DotNotice token="success">{notice}</DotNotice>
                        <StyleImg alt="" aria-hidden="true" src={`${UrlCockpit}/assets/img/icons/letter.svg`} />
                    </StyleIcon>
                    <NotificationPopover onOpenModal={() => handleClosePopover()} />
                    {
                        modal && (
                            <NotificationModal onClose={e => handleCloseModal(e)} />
                        )
                    }
                </StyleWrapper>
            )
            : null
    );
};
