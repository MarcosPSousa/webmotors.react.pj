import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    position: absolute;
    background-color: ${color('white')};
    border: 1px solid ${color('gray-4')};
    width: 400px;
    right: -20px;
    top: 100%;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px 0px;
    opacity: 0;
    pointer-events: none;
    transition: transform 0.3s, opacity 0.3s;
    transform: translateY(-32px);
    &:before{
        content: '';
        position: absolute;
        top: -8px;
        right: 27px;
        width: 16px;
        height: 16px;
        background-color: ${color('white')};
        transform: rotate(45deg);
        box-shadow: -1px -1px 1px ${color('gray-4')};
    }
    &[data-popover-open]{
        pointer-events: all;
        opacity: 1;
        transform: translateY(14px);
    }
`;

export const StyleBigTitle = styled.div`
    font-size: 1.4rem;
    font-weight: bold;
    padding: 12px 16px;
    border-bottom: 1px solid ${color('gray-4')};
`;

export const StyleContent = styled.div`
    padding: 16px;
    cursor: pointer;
    opacity: 0.9;
    &:hover{
        opacity: 1;
    }
`;

export const StyleProduct = styled.div`
    display: flex;
    justify-content: space-between;
`;

export const StyleLogo = styled.img`
    width: 60px;
`;

export const StyleTime = styled.div`
    font-size: 1rem;
    font-weight: normal;
    color: ${color('gray-2')};
`;

export const StyleImg = styled.img`
    width: 100%;
    height: auto;
    border-radius: 4px;
`;

export const StyleThumb = styled.div`
    width: 76px;
    padding-right: 16px;
`;

export const StyleMessage = styled.div`
    flex: 1;
`;

export const StyleInfo = styled.div`
    padding: 16px 24px 0 0;
    display: flex;
    font-size: 1.2rem;
    line-height: 1.167em;
    position: relative;
    &:before{
        content: '';
        position: absolute;
        right: 0;
        top: 16px;
        border-radius: 100%;
        width: 6px;
        height: 6px;
        background-color: ${color('primary')};
    }
`;

export const StyleText = styled.div`
    line-height: 1.5em;
`;

export const StyleTitle = styled.div`
    font-weight: bold;
    padding-bottom: 4px;
`;

export const StyleButton = styled.div`
    margin-top: 16px;
    color: ${color('primary')};
    font-weight: bold;
`;
