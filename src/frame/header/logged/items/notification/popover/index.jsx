import React, { useState, useEffect, useRef } from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import TagAnalytics from 'webmotors-react-pj/tag-analytics';
import {
    StyleWrapper,
    StyleBigTitle,
    StyleContent,
    StyleProduct,
    StyleLogo,
    StyleTime,
    StyleInfo,
    StyleThumb,
    StyleImg,
    StyleMessage,
    StyleTitle,
    StyleText,
    StyleButton,
} from 'webmotors-react-pj/frame/header/logged/items/notification/popover/style';

export default ({ onOpenModal }) => {
    const handleClick = (value) => {
        onOpenModal();
    };

    return (
        <StyleWrapper>
            <StyleBigTitle>Mensagens</StyleBigTitle>
            <StyleContent onClick={() => handleClick('popover')}>
                <StyleProduct>
                    <StyleLogo src={`${UrlCockpit}/assets/img/brands/cockpit.svg`} alt="Logo Cockpit" />
                    {/* <StyleTime>3 minutos</StyleTime> */}
                </StyleProduct>
                <StyleInfo>
                    <StyleThumb>
                        <StyleImg src={`${UrlCockpit}/assets/img/icons/placeholder-cardelivery2.svg`} alt="" />
                    </StyleThumb>
                    <StyleMessage>
                        <StyleTitle>Ofereça o CarDelivery!</StyleTitle>
                        <StyleText>Seu cliente compra online, sua loja cuida de tudo e você turbina suas vendas!</StyleText>
                        <StyleButton>Saiba mais</StyleButton>
                    </StyleMessage>
                </StyleInfo>
            </StyleContent>
        </StyleWrapper>
    );
};
