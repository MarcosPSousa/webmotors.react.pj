import React, { useState } from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { jwtGet, cookieGet } from 'webmotors-react-pj/utils';
import ModalStore from 'webmotors-react-pj/frame/modal/store';
import {
    StyleWrapper,
    StyleButton,
    StyleIcon,
    StyleInfo,
} from 'webmotors-react-pj/frame/header/logged/items/group/style';

export default () => {
    const [modalGroup, setModalGroup] = useState(false);
    const { nomeGrupo: groupName, idGrupo, role } = jwtGet();
    const groupIs = (idGrupo && idGrupo !== '0') && (role && role.indexOf('1') !== -1);
    const storeName = cookieGet('CockpitStoreName') || 'Todas as lojas';
    return (
        groupIs
            ? (
                <StyleWrapper role="button">
                    <StyleButton data-icon-click onClick={() => setModalGroup(true)}>
                        <StyleIcon src={`${UrlCockpit}/assets/img/icons/pin-map.svg`} alt="" />
                        <StyleInfo>
                            <div>{groupName}</div>
                            <div>{storeName}</div>
                        </StyleInfo>
                    </StyleButton>
                    {
                        modalGroup && <ModalStore onClose={() => setModalGroup(false)} />
                    }
                </StyleWrapper>
            )
            : false
    );
};
