import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleInfo = styled.div`
    flex: 1;
    padding-left: 12px;
`;

export const StyleButton = styled.div`
    display: flex;
    line-height: 1.4em;
    transition: color 0.3s;
    font-size: 1.4rem;
    position: relative;
    &:after{
        content: '';
        position: absolute;
        right: -24px;
        top: 50%;
        width: 10px;
        height: 10px;
        border-style: solid;
        border-color: ${color('gray')};
        border-width: 0 1px 1px 0;
        transition: border-color 0.4s;
        transform: rotate(45deg) translateY(-50%);
    }
`;

export const StyleWrapper = styled.div`
    
    margin-left: 32px;
    padding-right: 40px;
    &:hover{
        color: ${color('#43bccd')};
        .${StyleInfo.styledComponentId}:after{
            border-color: ${color('#43bccd')};
        }
    }
`;

export const StyleIcon = styled.img`
    width: 26px;
    height: 26px;
    transform: translateY(2px);
`;
