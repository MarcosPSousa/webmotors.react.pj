import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const sizeInit = 32;

export default styled.div`
    width: ${({ size = sizeInit }) => `${size}px`};
    height: ${({ size = sizeInit }) => `${size}px`};
    line-height: ${({ size = sizeInit }) => `${size / 10}rem`};
    border-radius: ${({ size = sizeInit }) => `${size}px`};
    text-align: center;
    text-transform: uppercase;
    color: ${color('white')};
    background-color: ${color('gray-2')};
    border: 2px solid transparent;
    box-shadow: 0 0 1px 1px ${color('white')} inset;
    font-size: ${({ size = sizeInit }) => `${size / 20}rem`};;
    font-weight: 500;
    cursor: ${({ onClick }) => (onClick ? 'pointer' : 'default')};
    opacity: ${({ open }) => (open ? 1 : 0.9)};
    &:hover, &[data-opem]{
        opacity: 1;
    }
`;