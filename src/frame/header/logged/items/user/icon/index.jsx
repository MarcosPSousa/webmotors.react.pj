import React from 'react';
import { jwtGet } from 'webmotors-react-pj/utils';
import StyleIcon from 'webmotors-react-pj/frame/header/logged/items/user/icon/style';

export default props => <StyleIcon {...props}>{props.children}</StyleIcon>;
