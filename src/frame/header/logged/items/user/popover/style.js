import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.div`
    position: absolute;
    top: 100%;
    right: -16px;
    background-color: ${color('white')};
    padding: 16px 24px 8px;
    box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.2), 0 6px 8px 0 rgba(0, 0, 0, 0.2);
    border-radius: 4px;
    width: 280px;
    font-size: 1.4rem;
    pointer-events: none;
    transition: transform 0.3s, opacity 0.3s;
    will-change: transform, opacity;
    opacity: 0;
    transform: translateY(-32px);
    &:before{
        content: '';
        position: absolute;
        right: 12px;
        top: 0px;
        box-shadow: rgba(0, 0, 0, 0.5) -6px -6px 12px -3px;
        transform: rotate(45deg) translate(-13px, 5px);
        background-color: ${color('white')};
        width: 14px;
        height: 14px;
    }
    &[data-popover-open]{
        pointer-events: all;
        opacity: 1;
        transform: translateY(12px);
    }
`;
