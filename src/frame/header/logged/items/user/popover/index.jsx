import React from 'react';
import StyleWrapper from 'webmotors-react-pj/frame/header/logged/items/user/popover/style';
import Info from 'webmotors-react-pj/frame/header/logged/items/user/info';
import Menu from 'webmotors-react-pj/frame/header/logged/items/user/menu';

export default () => (
    <StyleWrapper>
        <Info />
        <Menu />
    </StyleWrapper>
)
