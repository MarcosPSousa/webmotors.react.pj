import React from 'react';
import styled from 'styled-components';
import { UrlCockpit } from 'webmotors-react-pj/config';

const Link = styled.a`
    padding: 8px 0;
    opacity: 0.7;
    transition: opacity 0.3s;
    display: block;
    line-height: 1em;
    &:hover{
        opacity: 1;
    }
`;

const Img = styled.img`
    width: 22px;
    height: 22px;
    margin-right: 12px;
    vertical-align: middle;
    transform: translateY(-2px);
`;

export default ({
    children,
    icon,
    href,
    target,
}) => (
    <li>
        <Link target={target} rel={target ? 'noopener noreferrer' : ''} href={href}>
            <Img alt="" aria-hidden="true" src={`${UrlCockpit}/assets/img/icons/${icon}`} />
            {children}
        </Link>
    </li>
);
