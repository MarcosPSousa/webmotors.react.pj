import React from 'react';
import { jwtGet } from 'webmotors-react-pj/utils';
import { UrlCockpit, UrlEstoquePlataforma } from 'webmotors-react-pj/config';
import StyleItem from 'webmotors-react-pj/frame/header/logged/items/user/menu/style';

export default () => {
    const jwt = jwtGet();

    return (
        <ul>
            <StyleItem icon="name-tag.svg" href={`${UrlCockpit}/usuario?id=${jwt.sid}`}>Meu perfil</StyleItem>
            <StyleItem icon="marketshare.svg" href={`${UrlEstoquePlataforma}/perfil/dados-revenda`} target="_blank">Dados cadastrais</StyleItem>
            <StyleItem icon="name-tag.svg" href={`${UrlCockpit}/logout`}>Sair</StyleItem>
        </ul>
    );
};
