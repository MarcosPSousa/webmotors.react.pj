import React from 'react';
import { jwtGet } from 'webmotors-react-pj/utils';
import {
    StyleWrapper,
    StyleUser,
    StyleName,
    StyleEmail,
} from 'webmotors-react-pj/frame/header/logged/items/user/info/style';
import Icon from 'webmotors-react-pj/frame/header/logged/items/user/icon';

export default () => {
    const jwt = jwtGet();
    return (
        <StyleWrapper>
            <div>
                <Icon open size={64}>{(jwt.nameid && jwt.nameid[0]) || '🛱'}</Icon>
            </div>
            <StyleUser>
                <StyleName>{jwt.nameid}</StyleName>
                <StyleEmail>{jwt.email}</StyleEmail>
            </StyleUser>
        </StyleWrapper>
    );
};
