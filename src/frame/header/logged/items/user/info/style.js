import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    display: flex;
    align-items: center;
    padding-bottom: 16px;
    border-bottom: 1px solid ${color('gray-4')};
    margin-bottom: 8px;
`;
export const StyleThumb = styled.div`
    position: relative;
`;
export const StyleUser = styled.div`
    padding-left: 12px;
    flex: 1;
    overflow: hidden;
`;
export const StyleName = styled.div`
    font-size: 1.6rem;
`;
export const StyleEmail = styled.div`
    font-size: 1.2rem;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;
