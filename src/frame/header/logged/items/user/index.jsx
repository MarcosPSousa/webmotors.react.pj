import React, { useEffect, useState, useRef } from 'react';
import { jwtGet } from 'webmotors-react-pj/utils';
import StyleWrapper from 'webmotors-react-pj/frame/header/logged/items/user/style';
import Icon from 'webmotors-react-pj/frame/header/logged/items/user/icon';
import Popover from 'webmotors-react-pj/frame/header/logged/items/user/popover';

export default () => {
    const jwt = jwtGet();
    return (
        <StyleWrapper>
            <Icon data-icon-click>{(jwt.nameid && jwt.nameid[0]) || '🛱'}</Icon>
            <Popover />
        </StyleWrapper>
    );
};
