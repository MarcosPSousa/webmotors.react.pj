import React from 'react';
import Intercom from 'webmotors-react-pj/frame/header/logged/items/intercom';
import User from 'webmotors-react-pj/frame/header/logged/items/user';
import Group from 'webmotors-react-pj/frame/header/logged/items/group';
import Icon from 'webmotors-react-pj/frame/header/logged/items/icon';
import {
    StyleWrapper,
    StyleMenu,
} from 'webmotors-react-pj/frame/header/logged/items/style';
import Search from 'webmotors-react-pj/frame/header/logged/search';
import translate from 'webmotors-react-pj/translate';

export default () => (
    <StyleWrapper>
        <Search />
        <StyleMenu>
            <Icon>
                {translate.header.notification}
            </Icon>
            <Icon>
                <Intercom />
            </Icon>
            <Group />
            <Icon>
                <User />
            </Icon>
        </StyleMenu>
    </StyleWrapper>
);
