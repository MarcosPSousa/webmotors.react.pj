import React, { useEffect, useRef } from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { jwtGet } from 'webmotors-react-pj/utils';
import { StyleWrapper, StyleImg } from 'webmotors-react-pj/frame/header/logged/items/intercom/style';
import DotNotice from 'webmotors-react-pj/frame/header/logged/items/dot-notice';

export default () => {
    const refWrapper = useRef();

    useEffect(() => {
        setTimeout(() => {
            const classNameIntercom = [...refWrapper.current.classList][0];
            const jwt = jwtGet();
            const appId = 'skfddf3u';
            const { email, nameid: name, sid } = jwt;
            const w = window;
            const ic = w.Intercom;
            w.intercomSettings = {
                name,
                email,
                user_id: sid,
                app_id: appId,
                custom_launcher_selector: `.${classNameIntercom}`,
                alignment: 'right',
                created_at: Date.now(),
                horizontal_padding: 0,
                vertical_padding: 0,
            };
            if (typeof ic === 'function') {
                ic('reattach_activator');
                ic('update', w.intercomSettings);
            } else {
                const d = document;
                // eslint-disable-next-line no-undef
                const i = () => i.c(arguments);
                const l = () => {
                    const s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = `https://widget.intercom.io/widget/${appId}`;
                    const x = d.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                };
                i.q = [];
                i.c = args => i.q.push(args);
                w.Intercom = i;
                if (document.readyState === 'complete') {
                    l();
                } else if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
            w.Intercom('boot', {
                app_id: appId,
                email,
                user_id: sid,
                created_at: Date.now(),
            });
            w.intercomTimeout = w.setTimeout(() => w.clearInterval(w.intercomInterval), 30000);
            w.intercomInterval = w.setInterval(() => {
                if (!!w.Intercom && w.Intercom.booted) {
                    clearInterval(w.intercomInterval);
                    clearTimeout(w.intercomTimeout);
                    w.Intercom('onUnreadCountChange', (unreadCount) => {
                        const count = unreadCount <= 9 ? unreadCount : '9+';
                        const notify = window.document.getElementById('intercom-notify');
                        if (count && notify) {
                            notify.innerText = count;
                        }
                    });
                }
            }, 100);
        }, 100);
    }, []);

    return (
        <StyleWrapper ref={refWrapper}>
            <DotNotice id="intercom-notify" />
            <StyleImg alt="" aria-hidden="true" src={`${UrlCockpit}/assets/img/icons/bell.svg`} />
        </StyleWrapper>
    );
};
