import React from 'react';
import StyleIcon from 'webmotors-react-pj/frame/header/logged/items/icon/style';

export default ({ children }) => <StyleIcon role="button">{children}</StyleIcon>;
