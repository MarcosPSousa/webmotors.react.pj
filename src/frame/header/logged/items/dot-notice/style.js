import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.div`
    position: absolute;
    right: -10px;
    top: -10px;
    width: 26px;
    height: 26px;
    border-radius: 100%;
    border: 3px solid ${color('white')};
    transform: scale(1);
    transition: transform 0.3s;
    text-align: center;
    z-index: 1;
    font-size: 1rem;
    line-height: 2em;
    color: ${color('white')};
    background-color: ${({ token }) => color(token)};
    pointer-events: none;
    &:empty{
        transform: scale(0);
    }
`;
