import React from 'react';
import StyleDot from 'webmotors-react-pj/frame/header/logged/items/dot-notice/style';

export default props => <StyleDot {...props}>{props.children}</StyleDot>;
