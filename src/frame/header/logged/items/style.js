import styled from 'styled-components';

export const StyleWrapper = styled.div`
    flex: 1;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 32px;
`;

export const StyleMenu = styled.div`
    display: flex;
`;
