import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    background-color: ${color('primary')};
    color: ${color('white')};
    font-size: 1.2rem;
    width: 100%;
    padding: 12px;
`;
