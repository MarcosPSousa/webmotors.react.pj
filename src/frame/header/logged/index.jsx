import React, { useState, useEffect } from 'react';
import StyleHeader from 'webmotors-react-pj/frame/header/logged/style';
import Logo from 'webmotors-react-pj/frame/header/logged/logo';
import Items from 'webmotors-react-pj/frame/header/logged/items';

export default () => {
    if (!window.clickHeader) {
        window.clickHeader = true;
        window.addEventListener('click', (e) => {
            const { target } = e;
            const headerPopoverAttribute = 'data-header-popover';
            const popoverAttribute = 'data-popover-open';
            const header = document.querySelector('[data-header]');
            const iconClicked = target.hasAttribute('data-icon-click');
            const iconNextElement = target.nextElementSibling;
            const removePopover = () => document.querySelectorAll(`[${popoverAttribute}]`).forEach(item => item.removeAttribute(popoverAttribute));

            if (!target.closest('.modal')) {
                if (
                    (iconClicked && iconNextElement && !iconNextElement.hasAttribute(popoverAttribute))
                    || target.closest(`[${popoverAttribute}]`)
                ) {
                    if (!target.closest(`[${popoverAttribute}]`)) {
                        removePopover();
                    }
                    header.setAttribute(headerPopoverAttribute, '');
                    iconNextElement && iconNextElement.setAttribute(popoverAttribute, '');
                } else {
                    removePopover();
                    header.removeAttribute(headerPopoverAttribute);
                }
            }
        });
    }

    return (
        <StyleHeader data-header>
            <Logo />
            <Items />
        </StyleHeader>
    )
};
