import styled, { keyframes } from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const rotate = keyframes`
    from {transform: rotate(0deg);}
    to {transform: rotate(359deg);}
`;

export default styled.div`
    position: relative;
    width: 24px;
    height: 24px;
    margin: 0 auto;
    padding: 32px 0;
    &:before{
        content: '';
        width: inherit;
        height: inherit;
        animation: ${rotate} 1s infinite;
        border-radius: 100%;
        border: 3px solid transparent;
        border-left-color: ${color('gray-2')};
        border-right-color: ${color('gray-2')};
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left: -15px;
        margin-top: -15px;
    }
`;
