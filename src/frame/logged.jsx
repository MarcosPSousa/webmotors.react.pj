import Header from 'webmotors-react-pj/frame/header/logged';
import Footer from 'webmotors-react-pj/frame/footer/logged';
import Menu from 'webmotors-react-pj/frame/menu';

export {
    Header,
    Footer,
    Menu,
};
