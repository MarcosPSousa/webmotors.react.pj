import styled from 'styled-components';
import Img from 'webmotors-react-pj/img';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    display: flex;
    align-items: center;
`;

export const StyleImage = styled(Img)`
    width: 16px;
    fill: ${color('white')};
    &:hover{
        filter: drop-shadow(0 0 10px ${color('white')});
    }
`;

export const StyleLink = styled.a`
    margin-left: 22px;
`;
