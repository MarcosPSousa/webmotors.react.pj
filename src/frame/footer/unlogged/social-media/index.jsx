import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { StyleWrapper, StyleImage, StyleLink } from 'webmotors-react-pj/frame/footer/unlogged/social-media/style';

export default () => (
    <StyleWrapper>
        <div>Siga a #Webmotors</div>
        <StyleLink target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/webmotors">
            <StyleImage src={`${UrlCockpit}/assets/img/icons/instagram.svg`} />
        </StyleLink>
        <StyleLink target="_blank" rel="noopener noreferrer" href="https://twitter.com/webmotors">
            <StyleImage src={`${UrlCockpit}/assets/img/icons/twitter.svg`} />
        </StyleLink>
        <StyleLink target="_blank" rel="noopener noreferrer" href="https://facebook.com/webmotors">
            <StyleImage src={`${UrlCockpit}/assets/img/icons/facebook-like.svg`} />
        </StyleLink>
    </StyleWrapper>
);
