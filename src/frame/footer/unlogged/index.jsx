import React from 'react';
import {
    Footer,
    Container,
    Row1,
    Row2,
} from 'webmotors-react-pj/frame/footer/unlogged/style';
import AppStore from 'webmotors-react-pj/frame/footer/unlogged/app-store';
import SocialMedia from 'webmotors-react-pj/frame/footer/unlogged/social-media';
import BrandsWebmotors from 'webmotors-react-pj/frame/footer/unlogged/brands-webmotors';
import Brands from 'webmotors-react-pj/frame/footer/unlogged/brands';

export default () => (
    <Footer>
        <Container>
            <Row1>
                <AppStore />
                <SocialMedia />
            </Row1>
            <Row2>
                <BrandsWebmotors />
                <Brands />
            </Row2>
        </Container>
    </Footer>
);
