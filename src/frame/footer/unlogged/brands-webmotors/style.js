import styled from 'styled-components';
import Img from 'webmotors-react-pj/img';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    display: flex;
    align-items: center;
    transform: translateX(-8px);
`;

export const StyleImageWebmotors = styled(Img)`
    height: 34px;
    .st0, .st1{
        fill: ${color('white')};
    }
    .st2{
        fill: ${color('primary')};
    }
`;

export const StyleImageSantander = styled(Img)`
    height: 18px;
    margin-left: 24px;
    fill: ${color('white')};
    transform: translateY(-2px);
`;
