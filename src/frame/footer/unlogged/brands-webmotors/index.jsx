
import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { StyleWrapper, StyleImageWebmotors, StyleImageSantander } from 'webmotors-react-pj/frame/footer/unlogged/brands-webmotors/style';

export default () => (
    <StyleWrapper>
        <StyleImageWebmotors src={`${UrlCockpit}/assets/img/brands/webmotors-2-fill.svg`} />
        <StyleImageSantander src={`${UrlCockpit}/assets/img/brands/santander-financiamento.svg`} />
    </StyleWrapper>
);
