import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { StyleWrapper, StyleTitle, StyleImage } from 'webmotors-react-pj/frame/footer/unlogged/brands/style';

export default () => (
    <StyleWrapper>
        <StyleTitle>Marcas Webmotors:</StyleTitle>
        <StyleImage src={`${UrlCockpit}/assets/img/brands/compreauto.svg`} />
        <StyleImage src={`${UrlCockpit}/assets/img/brands/meucarango.svg`} size="big" />
        <StyleImage src={`${UrlCockpit}/assets/img/brands/blucarros.svg`} />
        <StyleImage src={`${UrlCockpit}/assets/img/brands/poacarros.svg`} />
        <StyleImage src={`${UrlCockpit}/assets/img/brands/joinvillecarros.svg`} />
        <StyleImage src={`${UrlCockpit}/assets/img/brands/floripacarros.svg`} />
        <StyleImage src={`${UrlCockpit}/assets/img/brands/loop.svg`} />
    </StyleWrapper>
);
