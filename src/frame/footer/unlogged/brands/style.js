import styled from 'styled-components';
import Img from 'webmotors-react-pj/img';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    display: flex;
    align-items: center;
`;

export const StyleTitle = styled.div`
    opacity: 0.3;
    font-weight: 100;
`;

export const StyleImage = styled(Img)`
    margin-left: 28px;
    fill: ${color('white')};
    opacity: 0.2;
    height: ${props => (props.size === 'big' ? '30px' : '20px')}
`;
