import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const Footer = styled.footer`
    background-color: ${color('gray')};
    padding: 32px 22px;
    font-size: 16px;
    font-weight: 400;
    color: ${color('white')};
`;

export const Container = styled.div`
    max-width: 1280px;
    width: 100%;
    margin-left: auto;
    margin-right: auto;
`;

export const Row1 = styled.div`
    margin-bottom: 32px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const Row2 = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
