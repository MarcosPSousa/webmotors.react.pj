
import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { StyleWrapper, StyleTitle, StyleImage } from 'webmotors-react-pj/frame/footer/unlogged/app-store/style';


export default () => (
    <StyleWrapper>
        <StyleTitle>Baixe nosso app:</StyleTitle>
        <a target="_blank" rel="noopener noreferrer" href="https://apps.apple.com/br/app/revendedor-webmotors/id1069917512">
            <StyleImage src={`${UrlCockpit}/assets/img/static/store-app-store.png`} alt="App Store" />
        </a>
        <a target="_blank" rel="noopener noreferrer" href="https://apps.apple.com/br/app/revendedor-webmotors/id1069917512">
            <StyleImage src={`${UrlCockpit}/assets/img/static/store-google-play.png`} alt="Google Play" />
        </a>
    </StyleWrapper>
);
