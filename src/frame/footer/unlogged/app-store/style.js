import styled from 'styled-components';
import Img from 'webmotors-react-pj/img';

export const StyleWrapper = styled.div`
    display: flex;
    align-items: center;
`;

export const StyleTitle = styled.div`
    margin-right: 40px;
`;

export const StyleImage = styled(Img)`
    height: 38px;
    width: auto;
    margin-right: 16px;
`;
