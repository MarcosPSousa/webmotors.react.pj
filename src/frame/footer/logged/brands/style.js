import styled from 'styled-components';

export const StyleSign = styled.div`
    display: flex;
    align-items: center;
`;

export const StyleImgWebmotors = styled.img`
    height: 32px;
    transform: translateY(4px);
`;

export const StyleImgSantander = styled.img`
    height: 25px;
    margin-left: 16px;
`;
