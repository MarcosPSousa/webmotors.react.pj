import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { StyleSign, StyleImgWebmotors, StyleImgSantander } from 'webmotors-react-pj/frame/footer/logged/brands/style';

export default () => (
    <StyleSign>
        <StyleImgWebmotors aria-hidden="true" alt="" src={`${UrlCockpit}/assets/img/brands/webmotors-2.svg`} />
        <StyleImgSantander aria-hidden="true" alt="" src={`${UrlCockpit}/assets/img/brands/santander-financiamento.svg`} />
    </StyleSign>
);
