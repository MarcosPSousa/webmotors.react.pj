import React from 'react';

export default () => <div>{`© 1995-${new Date().getFullYear()} Webmotors S.A. Todos os direitos reservados.`}</div>;
