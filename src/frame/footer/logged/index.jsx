import React from 'react';
import { StyleFooter, StyleContainer } from 'webmotors-react-pj/frame/footer/logged/style';
import Date from 'webmotors-react-pj/frame/footer/logged/date';
import Brands from 'webmotors-react-pj/frame/footer/logged/brands';

export default () => (
    <StyleFooter>
        <StyleContainer>
            <Date />
            <Brands />
        </StyleContainer>
    </StyleFooter>
);
