import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleFooter = styled.footer`
    position: relative;
    border-top: 1px solid ${color('gray-4')};
    padding: 16px 32px;
    background-color: ${color('white')};
    font-size: 14px;
`;

export const StyleContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
