import React from 'react';
import List from 'webmotors-react-pj/frame/menu/list';
import StyleWrapper from 'webmotors-react-pj/frame/menu/style';

export default () => (
    <StyleWrapper>
        <List />
    </StyleWrapper>
);
