import React from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';
import { jwtGet } from 'webmotors-react-pj/utils';
import { StyleLink, StyleImg, StyleSublist } from 'webmotors-react-pj/frame/menu/items/style';

export default ({
    children,
    line,
    needSelectStore,
    onModal,
    onRedirect,
}) => {
    const modalLinkBlock = [
        '/termos/pendentes',
        '/meu-plano/fatura',
    ];
    const { idGrupo, role, unique_name: uniqueName, v2 } = jwtGet();
    const Href = ({ sub, url }) => (sub ? '#' : url.replace('$hashv2', v2));
    const Rel = url => (/cockpit\./.test(url) ? '' : 'noopener noreferrer');
    const Target = url => (
        /(autoguru)\.webmotors|zendesk\.com|\/universidade$/.test(url)
        || (
            !/(betaestoque|hkestoque)/.test(window.location.origin)
            && /(\/h?estoque)/.test(url)
        )
            ? '_blank'
            : '_self'
    );

    const Icon = param => (param && <StyleImg src={`${UrlCockpit}/assets/img/icons${param}`} alt="" aria-hidden="true" />);

    const Sub = sub => (
        Boolean(sub) && (
            <StyleSublist>
                {
                    sub.map(item => (
                        <MenuItem key={item.name}>
                            {item}
                        </MenuItem>
                    ))
                }
            </StyleSublist>
        )
    );

    const handleClick = (event) => {
        const { target } = event;
        const { nextElementSibling } = target;
        const groupIs = (idGrupo && idGrupo !== '0') && (role && role.indexOf('1') !== -1);
        const path = target.href.replace(/http(s?):\/\/(h|azul|)(local|cockpit).webmotors.com.br(:(\d+))?/, '');
        const modalShow = uniqueName === '0' && modalLinkBlock.includes(path) && groupIs;
        if (modalShow) {
            onRedirect(target.href);
            event.preventDefault();
            onModal();
        }
        if (nextElementSibling) {
            event.preventDefault();
            const classOpen = 'cockpit-menu-open';
            const classAnimation = 'cockpit-menu-animation';
            const submenuClose = (item) => {
                const element = item;
                element.style.height = '0px';
                element.classList.remove(classOpen);
                element.addEventListener('transitionend', () => {
                    element.classList.remove(classAnimation);
                    element.removeAttribute('style');
                }, { once: true });
            };
            const isOpen = nextElementSibling.classList.contains(classOpen);
            [...document.querySelectorAll(`.${classOpen}`)].forEach(item => submenuClose(item));
            if (!isOpen) {
                nextElementSibling.classList.add(classOpen);
                const { clientHeight } = nextElementSibling;
                nextElementSibling.classList.remove(classOpen);
                nextElementSibling.classList.add(classAnimation);
                setTimeout(() => {
                    nextElementSibling.style.height = `${clientHeight}px`;
                    nextElementSibling.classList.add(classOpen);
                }, 10);
            }
        }
    };

    const handleMouseEnter = (e) => {
        const lineElement = document.querySelector(`.${line.type.styledComponentId}`);
        const { target } = e;
        const { top, height } = target.getBoundingClientRect();
        lineElement.setAttribute('style', `transform: translateY(${top + window.scrollY}px);height: ${height}px;`);
    };

    const MenuItem = ({ children: child }) => {
        const {
            sub,
            url,
            icon,
            name,
        } = child;
        return (
            <li key={name}>
                <StyleLink
                    onClick={e => handleClick(e)}
                    onMouseEnter={e => handleMouseEnter(e)}
                    href={Href({ sub, url })}
                    target={Target(url)}
                    rel={Rel(url)}
                >
                    {Icon(icon)}
                    {name}
                </StyleLink>
                {Sub(sub)}
            </li>
        );
    };

    return <MenuItem>{children}</MenuItem>;
};
