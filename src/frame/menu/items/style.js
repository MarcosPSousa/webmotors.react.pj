import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleLink = styled.a`
    line-height: 40px;
    padding: 0 32px;
    display: block;
    color: ${color('white')};
    opacity: 0.7;
    transition: opacity 0.3s;
    &:hover{
        opacity: 1;
    }
`;

export const StyleImg = styled.img`
    vertical-align: middle;
    width: 20px;
    height: 20px;
    margin-right: 12px;
    transform: translateY(-2px);
`;

export const StyleSublist = styled.ul`
    padding: 0;
    background-color: ${color('gray-2')};
    overflow: hidden;
    text-transform: none;
    height: 0;
    &.cockpit-menu-animation{
        transition: height 0.3s, padding 0.3s;
    }
    &.cockpit-menu-open{
        height: auto;
        padding: 8px 0;
    }
`;
