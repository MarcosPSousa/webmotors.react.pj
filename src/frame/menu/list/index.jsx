/* eslint-disable import/no-dynamic-require */
import React, { useState, Fragment } from 'react';
import { jwtGet } from 'webmotors-react-pj/utils';
import ModalStore from 'webmotors-react-pj/frame/modal/store';
import Items from 'webmotors-react-pj/frame/menu/items';
import {
    StyleList,
    StyleLine,
} from 'webmotors-react-pj/frame/menu/list/style';

export default () => {
    let env = 'production';
    const { hostname } = window.location;
    const { unique_name: uniqueName, idGrupo } = jwtGet();
    const userGroup = uniqueName === '0' && idGrupo !== '0';
    const line = <StyleLine />;

    const [openModal, setOpenModal] = useState(false);
    const [redirect, setRedirect] = useState(false);

    if (/^local\./.test(hostname)) {
        env = 'development';
    }
    if (/^hk?(cockpit|crm|gb|estoque|comprarveiculos|autoguru)\./.test(hostname)) {
        env = 'homologation';
    }
    if (/^azul/.test(hostname)) {
        env = 'blue';
    }

    const menu = require(`../env/${env}`);

    const menuProducts = menu.filter(item => item.order === 1);
    const menuInternal = menu.filter(item => item.order === 2);

    const renderMenu = obj => (
        obj.map((item) => (
            <Items
                key={JSON.stringify(item)}
                line={line}
                onModal={() => setOpenModal(true)}
                onRedirect={url => setRedirect(url)}
            >
                {item}
            </Items>
        ))
    );
    return (
        <Fragment>
            {line}
            <StyleList>
                {renderMenu(menuProducts)}
            </StyleList>
            <StyleList>
                {renderMenu(menuInternal)}
            </StyleList>
            {
                openModal && (
                    <ModalStore
                        onClose={() => setOpenModal(false)}
                        onRedirect={redirect}
                    />
                )
            }
        </Fragment>
    );
};
