import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleList = styled.ul`
    padding: 0 0 32px;
    font-size: 1.2rem;
    text-transform: uppercase;
    + ul {
        padding-top: 32px;
        font-size: 1.4rem;
        text-transform: none;
        ul a{
            padding: 0 32px;
        }
        &:before{
            content: '';
            display: block;
            border-top: 1px solid ${color('gray-2')};
            margin: 0 32px;
            transform: translateY(-32px);
        }
    }
    ul a{
        padding: 0 32px 0 64px;
    }
`;

export const StyleLine = styled.i`
    position: absolute;
    left: 0;
    top: 0;
    width: 9px;
    background-color: ${color('primary')};
    transition: transform 0.2s;
`;

export const StyleError = styled.div`
    padding: 0 32px;
    display: flex;
    align-items: center;
    font-size: 1.2rem;
    cursor: ${({ onClick }) => (onClick ? 'pointer' : 'default')};
`;

export const StyleErrorImg = styled.img`
    width: 24px;
    height: 24px;
    margin-right: 16px;
`;
