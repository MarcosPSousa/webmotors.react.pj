import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.nav`
    background-color: ${color('gray')};
    color: ${color('white')};
    padding: 24px 0 64px;
    min-width: 250px;
    max-width: 250px;
    min-height: 100%;
`;
