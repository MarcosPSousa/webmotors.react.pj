import React, { useState } from 'react';
import {
    StyleWrapper,
    StyleItem,
    StyleCurrent,
    StyleItemLink,
} from 'webmotors-react-pj/pagination/style'

export default (props) => {
    const {
        page: pageInitial = 1,
        totalPages = 1,
        maxItems = 5,
        queryPage = 'page',
        onClickItem = () => {},
    } = props;
    const [page, setPage] = useState(pageInitial);

    const urlClear = (value) => {
        const { search, pathname } = window.location;
        const urlParamRegex = new RegExp(`((&|\\?)${queryPage})=(\\d+)`);
        const replace = search.replace(urlParamRegex, `$1=${value}`);
        return `${pathname}${replace}`;
    };
    
    const handleClick = (event) => {
        event.preventDefault();
        const pageCurrent = Number(event.target.textContent);
        setPage(pageCurrent);
        onClickItem({ page: pageCurrent, event });
    };

    const renderFirstPages = (itemsBefore, itemsAfter) => {
        for (let i = 1; i < page; i++) {
            itemsBefore.unshift(<StyleItemLink onClick={e => handleClick(e)} to={urlClear(`${page - i}`)} key={i}>{page - i}</StyleItemLink>);
        }
        for (let i = (page + 1); i < Math.min(maxItems, totalPages); i++) {
            itemsAfter.push(<StyleItemLink onClick={e => handleClick(e)} to={urlClear(`${i}`)} key={i}>{i}</StyleItemLink>);
        }
        if (totalPages > maxItems) {
            itemsAfter.push(<StyleItem key="...">...</StyleItem>);
        }
        if (page < totalPages) {
            itemsAfter.push(<StyleItemLink onClick={e => handleClick(e)} to={urlClear(`${totalPages}`)} key="last">{totalPages}</StyleItemLink>);
        }
    };

    const renderBetweenPages = (itemsBefore, itemsAfter) => {
        for (let i = page; i > page - 1; i--) {
            itemsBefore.unshift(<StyleItemLink onClick={e => handleClick(e)} to={urlClear(`${i - 1}`)} key={i}>{i - 1}</StyleItemLink>);
        }
        for (let i = page; i < page + 1; i++) {
            itemsAfter.push(<StyleItemLink onClick={e => handleClick(e)} to={urlClear(`${i + 1}`)} key={i}>{i + 1}</StyleItemLink>);
        }
        itemsBefore.unshift(<StyleItemLink onClick={e => handleClick(e)} to={urlClear('1')} key="last">1</StyleItemLink>, <StyleItem key="...">...</StyleItem>);
        itemsAfter.push(<StyleItem key="...">...</StyleItem>, <StyleItemLink onClick={e => handleClick(e)} to={urlClear(`${totalPages}`)} key="last">{totalPages}</StyleItemLink>);
    };

    const renderLastPages = (itemsBefore, itemsAfter) => {
        for (let i = page; i < totalPages; i++) {
            itemsAfter.push(<StyleItemLink onClick={e => handleClick(e)} to={urlClear(`${i + 1}`)} key={i}>{i + 1}</StyleItemLink>);
        }
        for (let i = 1; i < (maxItems - itemsAfter.length - 1); i++) {
            itemsBefore.unshift(<StyleItemLink onClick={e => handleClick(e)} to={urlClear(`${page - i}`)} key={i}>{page - i}</StyleItemLink>);
        }
        itemsBefore.unshift(<StyleItemLink onClick={e => handleClick(e)} to={urlClear('1')} key="last">1</StyleItemLink>, <StyleItem key="...">...</StyleItem>);
    };

    const renderPagination = () => {
        const itemsBefore = [];
        const itemsAfter = [];
        if (page <= Math.ceil(maxItems / 2)) {
            renderFirstPages(itemsBefore, itemsAfter);
        } else {
            if (page >= totalPages - Math.ceil(maxItems / 2)) {
                renderLastPages(itemsBefore, itemsAfter);
            } else {
                renderBetweenPages(itemsBefore, itemsAfter);
            }
        }
        return (
            <StyleWrapper>
                {itemsBefore}
                <StyleCurrent>{page}</StyleCurrent>
                {itemsAfter}
            </StyleWrapper>
        )
    };

    return (
        (totalPages > 1) 
            ? renderPagination()
            : false
    );
};
