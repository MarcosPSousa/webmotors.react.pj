import styled from 'styled-components';
import { Link } from 'react-router-dom';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    display: flex;
    justify-content: center;
    white-space: nowrap;
    padding-top: 32px;
    font-size: 1.2rem;
    font-weight: bold;
    color: ${color('gray-3')};
`;

export const StyleItem = styled.div`
    padding: 0 4px;
    cursor: default;
`;

export const StyleCurrent = styled(StyleItem)`
    color: ${color('gray')};
`;

export const StyleItemLink = styled(Link)`
    padding: 0 4px; 
`;
