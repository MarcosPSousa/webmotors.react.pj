import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.div`
    background-color: ${color('white')};
    color: ${color('gray-2')};
    box-shadow: 0px 2px 2px -2px rgba(0, 0, 0, 0.16);
    border-radius: ${({ type }) => `${(type === 'radius' ? 8 : 0)}px`};
    padding: ${({ padding }) => `${padding}px`};
`;
