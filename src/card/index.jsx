import React from 'react';
import StyleCard from 'webmotors-react-pj/card/style.js';

export default ({ children, type = 'radius', padding = 32, className }) => (
    <StyleCard className={className} type={type} padding={padding}>{children}</StyleCard>
);
