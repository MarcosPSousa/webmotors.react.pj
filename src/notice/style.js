import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const backgroundColor = (type) => ({
    error: color('error'),
    success: color('success'),
    danger: color('danger'),
    warning: color('warning'),
    wait: color('gray-2'),
}[type || 'error']);

export const StyleWrapper = styled.div`
    padding: ${({ open }) => (open ? '12px 32px' : '0px 32px')};
    font-size: 1.2rem;
    line-height: 1.5em;
    font-weight: 500;
    color: ${color('white')};
    display: flex;
    justify-content: space-between;
    border-radius: 4px;
    background-color: ${({ type }) => backgroundColor(type)};
    overflow: hidden;
    margin-bottom: ${({ open }) => (open ? '20px' : '0px')};
    transition: padding 0.3s, margin-bottom 0.3s;
`;

export const StyleText = styled.div`
    margin-top: ${({ open }) => (open ? '0px' : '-100px')};
    transition: margin-top 0.3s;
    display: flex;
    align-items: center;
`;

export const StyleIcon = styled.i`
    width: 24px;
    height: 24px;
    margin-right: 16px;
    fill: ${color('white')};
`;

export const StyleClose = styled.button`
    width: 24px;
    height: 24px;
    fill: ${color('white')};
    background-color: transparent;
    margin-top: ${({ open }) => (open ? '0px' : '-90px')};
    transition: margin-top 0.3s;
`;
