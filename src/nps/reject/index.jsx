import React from 'react';
import { Reject } from 'webmotors-react-pj/nps/db/update';
import RejectStyle from 'webmotors-react-pj/nps/reject/style';

export default ({ children, onClick }) => {
    const handleClick = () => {
        Reject();
        onClick();
    };
    return <div><RejectStyle onClick={() => handleClick()}>{children}</RejectStyle></div>;
};
