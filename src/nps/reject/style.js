import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.button`
    background-color: transparent;
    font-size: 12px;
    color: ${color('#43bccd')};
`;
