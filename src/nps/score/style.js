import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const backgroundColor = props => ({
    0: '#B72025',
    1: '#D62027',
    2: '#F05223',
    3: '#F36F21',
    4: '#FAA823',
    5: '#FFCA27',
    6: '#ECDB12',
    7: '#E8E73D',
    8: '#C5D92D',
    9: '#AFD136',
    10: '#64B64D',
}[props.children]);
const opacity = props => (props.active ? 1 : 0.4);

export const Score = styled.div`
    display: flex;
    justify-content: center;
    position: relative;
    padding-bottom: 24px;
    margin-bottom: 16px;
    &:before{
        content: '';
        position: absolute;
        left: -16px;
        right: -16px;
        border-bottom: 1px solid ${color('#f9f9f9')};
        bottom: 0;
    }
`;

export const ScoreLabel = styled.label`
    margin: 0 3px;
    cursor: pointer;
`;

export const ScoreRadio = styled.input`
    opacity: 0;
    position: absolute;
    z-index: -1;
`;

export const ScoreSquare = styled.span`
    display: block;
    width: 18px;
    height: 18px;
    line-height: 20px;
    font-size: 10px;
    border-radius: 4px;
    color: ${color('white')};
    opacity: ${props => opacity(props)};
    background-color: ${props => backgroundColor(props)};
    transition: opacity 0.3s;
`;
