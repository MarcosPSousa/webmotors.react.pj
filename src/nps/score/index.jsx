import React, { useState } from 'react';
import {
    Score,
    ScoreLabel,
    ScoreRadio,
    ScoreSquare,
} from 'webmotors-react-pj/nps/score/style';

export default ({ onUnlock, onScore }) => {
    const score = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const [scoreActive, setScoreActive] = useState([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

    const setScore = (point) => {
        setScoreActive([point]);
        onScore(point);
        onUnlock();
    };

    return (
        <Score>
            {
                score.map(item => (
                    <ScoreLabel key={item}>
                        <ScoreRadio type="radio" name="nota" onClick={() => setScore(item)} />
                        <ScoreSquare active={scoreActive.includes(item)}>{item}</ScoreSquare>
                    </ScoreLabel>
                ))
            }
        </Score>
    );
};
