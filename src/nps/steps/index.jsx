/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect } from 'react';
import { ScrollStyle, ScrollContentStyle, ScrollChildStyle } from 'webmotors-react-pj/nps/steps/style';
import Step1 from 'webmotors-react-pj/nps/steps/step1';
import Step2 from 'webmotors-react-pj/nps/steps/step2';

export default ({
    handleOpen,
    handleShow,
}) => {
    const [step, setStep] = useState(0);
    const [widthContent, setWidthContent] = useState(0);
    const [widthChild, setWidthChild] = useState(0);

    const child = [
        <Step1
            handleShow={handleShow}
            handleOpen={handleOpen}
            onNextStep={() => {
                setStep(step + 1);
            }}
        />,
        <Step2 handleShow={handleShow} />,
    ];

    useEffect(() => {
        setWidthChild(100 / child.length);
        setWidthContent(100 * child.length);
    }, []);

    useEffect(() => {
        const stepSelector = document.querySelector(`.step-${(step + 1)}`);
        if (stepSelector) {
            document.querySelector('.step-scroll').style.height = `${stepSelector.clientHeight}px`;
        }
    });

    return (
        <ScrollStyle>
            <ScrollContentStyle className="step-scroll" width={widthContent} left={step * (100 / child.length)}>
                {
                    child.map((item, i) => (
                        <ScrollChildStyle width={widthChild} key={i}>{item}</ScrollChildStyle>
                    ))
                }
            </ScrollContentStyle>
        </ScrollStyle>
    );
};
