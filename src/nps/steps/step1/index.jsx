import React, { useState } from 'react';
import Text from 'webmotors-react-pj/nps/text';
import Score from 'webmotors-react-pj/nps/score';
import Description from 'webmotors-react-pj/nps/description';
import Textarea from 'webmotors-react-pj/nps/textarea';
import Button from 'webmotors-react-pj/nps/button';
import Reject from 'webmotors-react-pj/nps/reject';
import { Send } from 'webmotors-react-pj/nps/db/update';

export default ({
    handleShow,
    onNextStep,
}) => {
    const [unlock, setUnlock] = useState(false);
    const [score, setScore] = useState(0);
    const [comments, setComments] = useState('');
    const handleSend = () => {
        onNextStep();
        Send({ score, comments });
    };
    return (
        <div className="step-1">
            <Text>
                Com base na sua experiência com a Webmotors,
                como você classifica os serviços prestados, em uma escala de 0 a 10?
            </Text>
            <Score onUnlock={() => setUnlock(true)} onScore={e => setScore(e)} />
            <Description sendHas={unlock}>
                Explique o que te motivou a atribuir esta nota
            </Description>
            <Textarea sendHas={unlock} onComments={e => setComments(e)} />
            <Button onClick={() => handleSend()} sendHas={unlock}>Enviar</Button>
            <Reject onClick={() => handleShow(false)}>Agora não</Reject>
        </div>
    );
};
