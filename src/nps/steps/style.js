import styled from 'styled-components';

const ScrollStyle = styled.div`
    overflow: hidden;
`;

const ScrollContentStyle = styled.div`
    display: flex;
    align-items: flex-start;
    transform: ${props => `translateX(-${((props.left))}%)`};
    width: ${props => `${props.width}%`};
    transition: transform 0.3s, height 0.3s;
`;

const ScrollChildStyle = styled.div`
    width: ${props => `${props.width}%`};
`;

export {
    ScrollStyle,
    ScrollContentStyle,
    ScrollChildStyle,
};
