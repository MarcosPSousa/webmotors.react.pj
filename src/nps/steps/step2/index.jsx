import React from 'react';
import Text from 'webmotors-react-pj/nps/text';
import Button from 'webmotors-react-pj/nps/button';

export default ({ handleShow }) => (
    <div className="step-2">
        <Text>Agradecemos a sua resposta</Text>
        <Button sendHas onClick={() => handleShow(false)}>Continuar</Button>
    </div>
);
