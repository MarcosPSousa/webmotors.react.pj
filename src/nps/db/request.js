import { ApiCockpit } from 'webmotors-react-pj/config';
import { Ajax } from 'webmotors-react-pj/utils';
import { showAgainDays } from 'webmotors-react-pj/nps/db/update';

export default async () => {
    const request = await Ajax({
        url: `${ApiCockpit}/Nps`,
        method: 'GET',
    }).then((e) => {
        const { data, success } = e;
        let response = false;
        if (success) {
            const dateShow = new Date(`${data.lastAnswer}T00:00`);
            const dateSave = new Date(dateShow.getFullYear(), dateShow.getMonth() + 3, 1).getTime();
            response = data.permissionNps;
            if (!response) {
                showAgainDays(dateSave);
            }
        }
        return response;
    });
    return request;
};
