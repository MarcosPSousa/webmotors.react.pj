import Dexie from 'dexie';
import { ApiCockpit } from 'webmotors-react-pj/config';
import { Ajax, jwtGet } from 'webmotors-react-pj/utils';

const showAgainDays = (timestamp) => {
    const { sid } = jwtGet();
    const db = new Dexie('cockpit');
    db.version(4).stores({ Nps: 'id,dateShow' });
    const { Nps } = db;
    db.transaction('rw', Nps, () => {
        const id = Number(sid);
        Nps.get({ id }).then((user) => {
            if (!user) {
                Nps.add({ id, dateShow: timestamp });
            } else {
                Nps.update(id, { dateShow: timestamp });
            }
        });
    });
};

const Send = ({
    score,
    comments,
}) => {
    Ajax({
        url: `${ApiCockpit}/Nps?${Math.random()*100}`,
        method: 'POST',
        data: {
            nota: score,
            comentario: comments,
        },
    }).then((e) => {
        if (e.success) {
            const days = 90;
            const date = new Date(Date.now() + (1000 * 60 * 60 * 24 * days));
            const firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getTime();
            showAgainDays(firstDay);
        }
    });
};

const Reject = () => {
    const days = 7;
    const date = new Date(Date.now() + (1000 * 60 * 60 * 24 * days)).getTime();
    showAgainDays(date);
};

export {
    Send,
    Reject,
    showAgainDays,
};
