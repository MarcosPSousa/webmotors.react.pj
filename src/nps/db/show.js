
import Dexie from 'dexie';
import { jwtGet } from 'webmotors-react-pj/utils';
import RequestDB from 'webmotors-react-pj/nps/db/request';

export default ({ onRender }) => {
    const { sid } = jwtGet();
    const db = new Dexie('cockpit');
    db.version(4).stores({ Nps: 'id,dateShow' });
    const { Nps } = db;
    return db.transaction('rw', Nps, async () => {
        const id = Number(sid);
        const dateNow = Date.now();
        const get = await Nps.get({ id }).then(async (user) => {
            if (!user) {
                const request = await RequestDB();
                return request;
            }
            const { dateShow } = user;
            if (dateNow > dateShow) {
                const request = await RequestDB();
                return request;
            }
            return false;
        });
        onRender(get);
        return get;
    });
};
