import React from 'react';
import WrapperStyle from 'webmotors-react-pj/nps/wrapper/style';

export default ({ children, render }) => (
    render
        ? <WrapperStyle>{children}</WrapperStyle>
        : false
);
