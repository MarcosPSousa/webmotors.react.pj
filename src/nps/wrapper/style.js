import styled from 'styled-components';

export default styled.div`
    overflow: hidden;
    position: fixed;
    z-index: 4;
    left: 0;
    bottom: 0;
    width: 100%;
    max-width: 450px;
    height: 100%;
    max-height: 460px;
    pointer-events: none;
`;
