import React from 'react';
import FormStyle from 'webmotors-react-pj/nps/form/style';

export default ({ children, open, show }) => (
    <FormStyle open={open} onSubmit={e => e.preventDefault()} show={show}>
        {children}
    </FormStyle>
);
