import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const open = props => (props.open ? 'translateY(0px)' : 'translateY(calc(100% - 16px))');
const show = props => (props.show ? open(props) : 'translateY(150%)');

export default styled.form`
    max-width: 310px;
    width: 90%;
    padding: 16px;
    border-radius: 4px 4px 0 0;
    box-shadow: 0 2px 80px 0 rgba(0, 0, 0, 0.5);
    position: absolute;
    left: 32px;
    bottom: 0;
    z-index: 4;
    text-align: center;
    background-color: ${color('white')};
    transform: ${props => show(props)};
    transition: transform 0.3s;
    pointer-events: all;
`;
