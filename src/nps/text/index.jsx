import React from 'react';
import Text from 'webmotors-react-pj/nps/text/style';

export default ({ children }) => (
    <Text>
        {children}
    </Text>
);
