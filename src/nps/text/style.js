import styled from 'styled-components';

export default styled.div`
    font-size: 12px;
    line-height: 1.333em;
    padding-bottom: 24px;
`;
