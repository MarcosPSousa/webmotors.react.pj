import React, { useState, useEffect } from 'react';
import NpsShow from 'webmotors-react-pj/nps/db/show';
import Steps from 'webmotors-react-pj/nps/steps';
import Wrapper from 'webmotors-react-pj/nps/wrapper';
import Form from 'webmotors-react-pj/nps/form';
import Toggle from 'webmotors-react-pj/nps/toggle';

export default () => {
    const [open, setOpen] = useState(false);
    const [show, setShow] = useState(false);
    const [render, setRender] = useState(true);

    useEffect(() => {
        NpsShow({ onRender: setRender }).then((e) => {
            setShow(e);
            setOpen(e);
        }).catch(() => {
            setOpen(true);
            setShow(true);
        });
    }, []);

    return (
        <Wrapper render={render}>
            <Form open={open} show={show}>
                <Toggle
                    open={open}
                    onClick={() => setOpen(!open)}
                />
                <Steps handleShow={e => setShow(e)} />
            </Form>
        </Wrapper>
    );
};
