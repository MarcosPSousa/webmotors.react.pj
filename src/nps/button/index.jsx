import React from 'react';
import Button from 'webmotors-react-pj/nps/button/style';

export default ({ children, sendHas, onClick }) => (
    <Button onClick={() => onClick()} type="button" disabled={!sendHas}>
        {children}
    </Button>
);
