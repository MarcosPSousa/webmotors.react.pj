import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const disabled = props => (props.disabled ? 'not-allowed' : 'pointer');
const opacityDisabled = (props, value) => (props.disabled ? 0.2 : value);

export default styled.button`
    display: inline-block;
    padding: 8px 18px;
    border-radius: 3px;
    border-style: none;
    font-family: inherit;
    font-size: 1.4rem;
    font-weight: 500;
    color: ${color('white')};
    background-color: ${color('primary')};
    cursor: ${props => disabled(props)};
    opacity: ${props => opacityDisabled(props, 1)};
    transition: opacity 0.2s, box-shadow 0.3s;
    margin-bottom: 12px;
    &:hover{
        opacity: ${props => opacityDisabled(props, 0.7)};
    }
`;
