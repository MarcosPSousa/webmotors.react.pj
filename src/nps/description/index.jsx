import React from 'react';
import Description from 'webmotors-react-pj/nps/description/style';

export default ({ children, sendHas }) => (
    <Description sendHas={sendHas}>
        {children}
    </Description>
);
