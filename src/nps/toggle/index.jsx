import React from 'react';
import ToggleStyle from 'webmotors-react-pj/nps/toggle/style';

export default ({ open, onClick }) => <ToggleStyle open={open} onClick={() => onClick()} />;
