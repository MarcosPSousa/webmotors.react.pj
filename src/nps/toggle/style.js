import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const open = props => (props.open ? 'rotate(45deg)' : 'rotate(-135deg)');

export default styled.button`
    position: absolute;
    right: 12px;
    top: 0;
    padding: 6px 12px 4px 10px;
    box-shadow: 0 -10px 80px -10px rgba(0, 0, 0, 0.5);
    transform: translateY(-80%);
    background-color: ${color('white')};
    border-radius: 4px 4px 0 0;
    &:before{
        content: '';
        border: 2px solid ${color('gray-2')};
        width: 8px;
        height: 8px;
        border-left-style: none;
        border-top-style: none;
        display: block;
        transform: ${props => open(props)};
        transform-origin: 6px 6px;
        transition: transform 0.3s;
    }
`;
