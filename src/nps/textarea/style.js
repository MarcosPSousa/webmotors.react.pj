import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export default styled.textarea`
    width: 100%;
    border: 1px solid ${color('gray-3')};
    border-radius: 8px;
    margin-bottom: 16px;
    padding: 12px;
    font-family: inherit;
    background-color: ${color('white')};
    opacity: ${props => (props.disabled ? 0.3 : 1)}
`;
