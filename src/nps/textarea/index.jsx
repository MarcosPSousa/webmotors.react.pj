import React from 'react';
import Textarea from 'webmotors-react-pj/nps/textarea/style';

export default ({ sendHas, onComments }) => <Textarea maxlength="512" onInput={e => onComments(e.target.value)} disabled={!sendHas} />;
