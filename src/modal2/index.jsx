import React, { useEffect, useState } from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';

export default function Modal(props) {
    const [close, setClose] = useState(false);

    useEffect(() => {
        document.querySelector('body').classList.add('overflow-hidden');
        return () => document.querySelector('body').classList.remove('overflow-hidden');
    });

    function handleClose() {
        if (!props.locked) {
            setClose(true);
            props.onClose();
        }
    }

    return (
        <div className={`modal-stock ${props.forceClose || close ? 'modal-stock--close' : ''}`}>
            <button type="button" className="modal-stock__bg" onClick={!props.noCloseButton ? () => handleClose() : ''} />
            <div className="modal-stock__box" style={props.maxWidth ? { maxWidth: props.maxWidth } : null}>
                {!props.noCloseButton ? <img src={`${UrlCockpit}/assets/img/icons/icon-close-black.svg`} className="modal-stock__close" onClick={() => handleClose()} /> : ''}
                <div className="modal-stock__content modal-stock__content--flex">
                    {props.children}
                </div>
            </div>
        </div>
    );
}
