import React, { Component } from 'react';

class PageTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.key = 0;
    }

    componentDidMount() {
        setTimeout(() => this.setState({ title: document.title }), 10);
    }

    render() {
        return (
            <header className="pagetitle">
                <div className="pagetitle__content">
                    <h1 className="pagetitle__title">{ this.props.title }</h1>
                    <div className="pagetitle__aside">{ this.props.children }</div>
                </div>
                <div className="pagetitle__tablist" role="tablist" aria-label={`Listagem de Links da página ${this.state.title}`}>
                    {
                        this.props.tabs && Object.entries(this.props.tabs).map(item => (
                            item[0] === window.location.pathname
                                ? <span key={++this.key} className="pagetitle__tab pagetitle__tab--active" role="tab">{item[1]}</span>
                                : (
                                    <a
                                        key={++this.key}
                                        href={item[0]}
                                        className="pagetitle__tab"
                                        type="button"
                                        role="tab"
                                        onClick={this.props.onClick}
                                    >
                                        {item[1]}
                                    </a>
                                )
                        ))
                    }
                </div>
            </header>
        );
    }
}

export default PageTitle;
