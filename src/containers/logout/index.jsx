import React, { Component } from 'react';
import { cookieRemoveAll } from 'webmotors-react-pj/utils';
import { ApiSisense } from 'webmotors-react-pj/config';
import Load from 'webmotors-react-pj/load';

class Logout extends Component {
    constructor(props) {
        super(props);
        [
            'AppTokenSSO',
            'CockpitChannelsLeads',
            'CockpitFilter',
            'storeProductHire',
        ].forEach(item => window.localStorage.removeItem(item));
        cookieRemoveAll(['CockpitNotifications']);
    }

    componentDidMount() {
        setTimeout(() => window.location.href = '/', 2000);
    }

    render() {
        return (
            <div role="main" className="content">
                <iframe title="Logout Sisense" src={`${ApiSisense}auth/logout`} style={{ width: 0, height: 0, border: 'none' }} frameBorder="0" />
                <Load text="Aguarde enquando deslogamos com segurança" />
            </div>
        );
    }
}

export default Logout;
