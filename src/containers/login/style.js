import styled, { createGlobalStyle } from 'styled-components';

export const StyleFormLoginset = styled.form`
    position: absolute;
    left: 0;
    top: 0;
    opacity: 0;
    pointer-events: none;
`;

export const StyleButtonIcon = styled.button`
    background-color: transparent;
    position: absolute;
    right: 16px;
    top: 16px;
    width: 24px;
    height: 24px;
    span {
        pointer-events: none;
    }
`;

export const StyleModal = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 9;
`;

export const StyleModalLoad = styled.div`
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
`;

export const StyleModalBg = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.8);
    backdrop-filter: blur(4px);
`;

export const StyleModalContent = styled.div`
    position: relative;
    z-index: 1;
    margin: 16px auto;
    max-width: 1320px;
    width: 90%;
    top: 50%;
    transform: translateY(-50%);
    overflow: hidden;
    background-color: #000;
    min-height: 70vh;
    iframe {
        position: relative;
        z-index: 1;
        min-height: inherit;
    }
`;

export const StyleModalClose = styled.div`
    position: absolute;
    left: 36px;
    top: 36px;
    width: 20px;
    height: 20px;
    cursor: pointer;
    &:before, &:after{
        content: '';
        position: absolute;
        left: 9px;
        top: -2px;
        width: 2px;
        height: 24px;
        background-color: #fff;
    }
    &:before{
        transform: rotate(45deg);
    }
    &:after{
        transform: rotate(-45deg);
    }
    &:hover{
        &:before, &:after{
            box-shadow: 0 0 10px #fff;
        }
    }
`;

export const StyleLoginContainer = createGlobalStyle`
    .mt-8{margin-top:8px}.mt-16{margin-top:16px}.mt-24{margin-top:24px}.mt-32{margin-top:32px}.mt-40{margin-top:40px}.mt-48{margin-top:48px}.mt-56{margin-top:56px}.mt-64{margin-top:64px}.mb-8{margin-bottom:8px}.mb-16{margin-bottom:16px}.mb-24{margin-bottom:24px}.mb-32{margin-bottom:32px}.mb-40{margin-bottom:40px}.mb-48{margin-bottom:48px}.mb-56{margin-bottom:56px}.mb-64{margin-bottom:64px}.ml-8{margin-left:8px}.ml-16{margin-left:16px}.ml-24{margin-left:24px}.ml-32{margin-left:32px}.ml-40{margin-left:40px}.ml-48{margin-left:48px}.ml-56{margin-left:56px}.ml-64{margin-left:64px}.mr-8{margin-right:8px}.mr-16{margin-right:16px}.mr-24{margin-right:24px}.mr-32{margin-right:32px}.mr-40{margin-right:40px}.mr-48{margin-right:48px}.mr-56{margin-right:56px}.mr-64{margin-right:64px}.pt-8{padding-top:8px}.pt-16{padding-top:16px}.pt-24{padding-top:24px}.pt-32{padding-top:32px}.pt-40{padding-top:40px}.pt-48{padding-top:48px}.pt-56{padding-top:56px}.pt-64{padding-top:64px}.pb-8{padding-bottom:8px}.pb-16{padding-bottom:16px}.pb-24{padding-bottom:24px}.pb-32{padding-bottom:32px}.pb-40{padding-bottom:40px}.pb-48{padding-bottom:48px}.pb-56{padding-bottom:56px}.pb-64{padding-bottom:64px}.pl-8{padding-left:8px}.pl-16{padding-left:16px}.pl-24{padding-left:24px}.pl-32{padding-left:32px}.pl-40{padding-left:40px}.pl-48{padding-left:48px}.pl-56{padding-left:56px}.pl-64{padding-left:64px}.pr-8{padding-right:8px}.pr-16{padding-right:16px}.pr-24{padding-right:24px}.pr-32{padding-right:32px}.pr-40{padding-right:40px}.pr-48{padding-right:48px}.pr-56{padding-right:56px}.pr-64{padding-right:64px}.fz-8{font-size:.8rem}.fz-10{font-size:1rem}.fz-11{font-size:1.1rem}.fz-12{font-size:1.2rem}.fz-14{font-size:1.4rem}.fz-16{font-size:1.6rem}.fz-18{font-size:1.8rem}.fz-20{font-size:2rem}.fz-24{font-size:2.4rem}.fz-32{font-size:3.2rem}.fz-36{font-size:3.6rem}.bg-1{background-color:#f3123c}.bg-2{background-color:#8826bb}.bg-3{background-color:#ff8327}.bg-4{background-color:#43bccd}.bg-5{background-color:#00d6a6}.bg-6{background-color:#ffce35}.bg-gray-1{background-color:#fff}.bg-gray-2{background-color:#f3f5f8}.bg-gray-4{background-color:#c5c6d0}.bg-gray-5{background-color:#8b8c99}.bg-gray-6{background-color:#696977}.bg-gray-7{background-color:#4d4c59}.bg-gray-8{background-color:#2e2d37}.bg-gray-9{background-color:#000}.color-1{color:#f3123c}.color-2{color:#8826bb}.color-3{color:#ff8327}.color-4{color:#43bccd}.color-5{color:#00d6a6}.color-6{color:#ffce35}.color-gray-1{color:#fff}.color-gray-2{color:#f3f5f8}.color-gray-4{color:#c5c6d0}.color-gray-5{color:#8b8c99}.color-gray-6{color:#696977}.color-gray-7{color:#4d4c59}.color-gray-8{color:#2e2d37}.color-gray-9{color:#000}
    .page{
        background-color: #ECEDF2;
        strong{
            font-weight: 500;
        }
        &__title{
            font-size: 40px;
            font-weight: bold;
            line-height: 1.5em;
        }
        &__subtitle{
            font-size: 24px;
            font-weight: bold;
            line-height: 1.5em;
        }
        &__text{
            font-size: 16px;
            line-height: 1.5em;
        }
        &__text-min{
            font-size: 12px;
            line-height: 1.5em;
        }
        &__division{
            border-top: 1px solid #eee;
        }
        &__brands{
            svg{
                height: 36px;
            }
            &--white{
                .page{
                    &__brands-webmotors{
                        &:before{
                            background-color: #fff;
                        }
                        .st0{
                            fill: #fff;
                        }
                        .st1{
                            fill: #fff;
                        }
                        .st2{
                            fill: #ec0928;
                        }
                    }
                    &__brands-financiamento{
                        fill: #fff;
                        height: 22px;
                    }
                }
            }
        }
        &__brands-webmotors{
            position: relative;
            &:before{
                content: '';
                position: absolute;
                left: 100%;
                top: 0;
                transform: translate(14px, 16px);
                width: 4px;
                height: 4px;
                background-color: #171717;
                border-radius: 100%;
            }
            .st0{
                fill: #171717;
            }
            .st1{
                fill: transparent;
            }
            .st2{
                fill: #ec0928;
            }
        }
        &__brands-financiamento{
            transform: translateY(3px);
            fill: #F72805;
        }
        &__card{
            padding: 0 8px;
        }
        &__card-icon{
            width: 52px;
            height: 52px;
        }
        &__cover{
            height: 420px;
            background-image: radial-gradient(#383B3D, #232425);
            background-size: cover;
            background-position: center;
            padding: 88px 24px;
            position: relative;
            &--big{
                height: 480px;
            }
            &--crm{
                background-image: url('/assets/img/static/destaque-crm@2x.jpg');
            }
            &--autoguru{
                background-image: url('/assets/img/static/banner-autoguru.jpg');
            }
            &--universidade{
                background-image: url('/assets/img/static/destaque-universidade@2x.jpg');
            }
            &--feiroes{
                background-image: url('/assets/img/static/destaque-feiroes@3x.jpg');
            }
            &--estoque{
                background-image: url('/assets/img/static/destaque-estoque@2x.jpg');
            }
        }
        &__cover-bg{
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            max-width: 100%;
            overflow: hidden;
        }
        &__cover-content{
            position: relative;
            z-index: 1;
            .inputcheckbox__check{
                border: 2px solid #2E2D37;
                border-radius: 4px;
                &:before{
                    left: 4px;
                    top: 2px;
                    width: 6px;
                    height: 9px;
                }
            }
        }
        &__cover-circle{
            position: absolute;
            left: 50%;
            height: 100%;
            width: 1280px;
            transform: translateX(-50%);
            &:before{
                content: '';
                width: 762px;
                height: 762px;
                position: absolute;
                right: -90px;
                top: -30px;
                border: 30px solid #F3123C;
                border-radius: 100%;
                animation: zoomInBgForm 0.6s forwards;
            }
        }
        &__cover-form{
            padding: 42px 52px 52px;
            border-radius: 8px;
            box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
            font-size: 12px;
            font-weight: bold;
            position: relative;
            z-index: 1;
        }
        &__cover-inputwrap{
            position: relative;
        }
        &__cover-input{
            border: 2px solid #ECEDF2;
            border-radius: 8px;
            padding: 16px 24px;
            width: 100%;
            font-weight: 500;
        }
        &__cover-inputbutton{
            background-color: transparent;
            position: absolute;
            right: 16px;
            top: 16px;
            width: 24px;
            height: 24px;
            &:hover{
                opacity: 0.9;
            }
            svg{
                fill: #2E2D37;
            }
            .svg, svg{
                pointer-events: none;
            }
            &--show{
                svg{
                    fill: #8B8C99;
                }
            }
        }
        &__products{
            padding: 92px 24px 0;
        }
        &__products-circle{
            width: 886px;
            height: 886px;
            background-image: radial-gradient(transparent 0%, transparent 55%, #ECEDF2 100%), radial-gradient(#fff 0%, #fff 10%, transparent 75%);
            background-position: center, 50% 40px;
            background-repeat: repeat, no-repeat;
            box-shadow: 0 0 200px #FFFFFF;
            border-radius: 100%;
            margin: 186px auto 170px;
            position: relative;
            &:before{
                content: '';
                position: absolute;
                left: -100px;
                right: -100px;
                bottom: 286px;
                height: 136px;
                background-image: linear-gradient(to top, #ecedf2 32%, transparent);
            }
        }
        &__products-list{
            svg{
                position: absolute;
            }
        }
        &__products-item-1{
            height: 30px;
            left: -144px;
            top: 280px;
            .st0{
                fill: #2E2D37;
            }
            .st1{
                fill: #F3123C;
            }
        }
        &__products-item-2{
            height: 34px;
            left: -134px;
            top: 136px;
            .st0{
                fill: #2E2D37;
            }
            .st1{
                fill: #F3123C;
            }
        }
        &__products-item-3{
            height: 20px;
            left: -6px;
            top: 24px;
            .st0{
                fill: #F3123C;
            }
            .st1{
                fill: #2E2D37;
            }
        }
        &__products-item-4{
            height: 34px;
            left: 192px;
            top: -80px;
            .st0{
                fill: #F3123C;
            }
            .st1{
                fill: #2E2D37;
            }
        }
        &__products-item-5{
            height: 34px;
            right: 92px;
            top: -80px;
            .st0{
                fill: #2E2D37;
            }
            .st1{
                fill: #F3123C;
            }
        }
        &__products-item-6{
            height: 26px;
            right: -22px;
            top: 24px;
            .st0{
                fill: #2E2D37;
            }
            .st1{
                fill: #F3123C;
            }
        }
        &__products-item-7{
            height: 38px;
            right: -145px;
            top: 136px;
            .st0{
                fill: #2E2D37;
            }
            .st1{
                fill: #F3123C;
            }
        }
        &__products-item-8{
            height: 32px;
            right: -194px;
            top: 280px;
            .st0{
                fill: #2E2D37;
            }
            .st1{
                fill: #F3123C;
            }
        }
        &__products-infos{
            .page{
                &__text{
                    position: absolute;
                    width: 180px;
                }
            }
        }
        &__products-text-1, &__products-text-4{
            top: 304px;
        }
        &__products-text-2, &__products-text-3{
            top: 142px;
        }
        &__products-text-1{
            left: 54px;
        }
        &__products-text-2{
            left: 188px;
        }
        &__products-text-3{
            right: 188px;
        }
        &__products-text-4{
            right: 54px;
        }
        &__products-logo{
            transform: translateY(294px);
            max-width: 256px;
            .st0{
                fill: #2E2D37;
            }
            .st1{
                fill: #F3123C;
            }
        }
        &__products-button{
            padding-top: 405px;
            position: relative;
            z-index: 1;
            svg{
                width: 28px;
                height: 28px;
                vertical-align: middle;
                margin-right: 8px;
                fill: #fff;
            }
            .button{
                font-size: 18px;
                font-weight: 600;
                padding: 12px 20px;
            }
        }
        &__mobile{
            padding: 102px 22px 0;
            margin-top: -456px;
            position: relative;
            .page{
                &__text{
                    max-width: 386px;
                }
            }
        }
        &__mobile-shadow{
            overflow: hidden;
            height: 600px;
            position: relative;
            &:before{
                content: '';
                position: absolute;
                right: 20px;
                bottom: 0;
                width: 260px;
                height: 470px;
                background-color: #222;
                filter: blur(22px);
                transform: rotate(-30deg) translate(-345px, 50px);
            }
        }
        &__mobile-photo{
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
            &:before{
                content: url('/assets/img/login/mobile.png');
                position: absolute;
                right: 22px;
                bottom: -32px;
            }
        }
        &__comments{
            padding: 150px 22px 40px;
            box-shadow: 0px 4px 4px rgba(46, 45, 55, 0.1);
        }
        &__join{
            padding: 110px 22px;
        }
        &__join-logo{
            width: 266px;
            .st0{
                fill: #2E2D37;
            }
            .st1{
                fill: #F3123C;
            }
        }
        &__crm{
            .page{
                &__cover-webmotors{
                    &:before{
                        background-color: #2E2D37;
                    }
                    .st0{
                        fill: #2E2D37;
                    }
                }
                &__cover-financiamento{
                    fill: #F72805;
                }
            }
        }
        &__crm-img{
            background-image: url('/assets/img/static/crm@2x.png'), url('/assets/img/static/arrows.svg');
            background-size: contain, 90% 90%;
            background-repeat: no-repeat;
            background-position: 50% 0, calc(50% + 16px) 120px;
            padding-bottom: 92px;
        }
        &__crm-box{
            padding: 24px;
            border-radius: 8px;
            height: 100%;
        }
        &__crm-boxtitle{
            font-size: 17px;
            line-height: 1.5em;
            font-weight: bold;
        }
        &__crm-boxtext{
            font-size: 14px;
            height: 42px;
        }
        &__crm-item{
            font-size: 12px;
            line-height: 2em;
            padding-left: 24px;
            &--active{
                position: relative;
                &:before{
                    content: '';
                    background-image: url('/assets/img/static/check.svg');
                    background-size: contain;
                    background-repeat: no-repeat;
                    width: 10px;
                    height: 10px;
                    position: absolute;
                    left: 0;
                    top: 8px;
                }
            }
        }
        &__autoguru-decision, &__autoguru-mobile{
            .page{
                &__subtitle{
                    font-weight: 500;
                }
            }
        }
        &__autoguru-cel{
            max-width: 480px;
            transform: translateX(-104px);
        }
        &__autoguru-mobile{
            .page{
                &__subtitle{
                    &:before{
                        content: '';
                        display: block;
                        width: 22px;
                        height: 22px;
                        background-image: url('/assets/img/static/check.svg');
                        background-repeat: no-repeat;
                        background-size: contain;
                        margin-bottom: 8px;
                    }
                }
            }
        }
        &__universidade-card{
            svg{
                width: 22px;
                height: 22px;
            }
        }
        &__universidade-reasons{
            height: 950px;
            background-size: 1300px auto, 850px auto;
            background-image: url('/assets/img/static/reasons.svg'), url('/assets/img/static/universidade@2x.jpg');
            background-repeat: no-repeat;
            background-position: bottom center, center 480px;
        }
        &__feiroes-offer{
            overflow: hidden;
            .page{
                &__subtitle{
                    font-weight: normal;
                }
            }
        }
        &__feiroes-screen{
            height: 520px;
        }
        &__feiroes-benefits{
            svg{
                width: 22px;
                fill: #F3123C;
                margin-bottom: 8px;
            }
            .page{
                &__subtitle{
                    font-weight: normal;
                }
            }
        }
        &__estoque{
            .page{
                &__text{
                    font-size: 18px;
                }
            }
        }
        &__estoque-product{
            background-image: url('/assets/img/static/bg-stock.jpg');
            background-size: cover;
            background-position: center top;
            padding: 170px 0px 60px;
        }
        &__estoque-features{
            .page{
                &__subtitle{
                    font-size: 18px;
                    font-weight: 500;
                    &:after{
                        content: 'Em breve!';
                        padding: 0px 4px;
                        background: #f3123c;
                        border-radius: 4px;
                        font-size: 9px;
                        transform: translate(10px, -2px);
                        display: inline-block;
                        line-height: 17px;
                    }
                }
                &__text{
                    font-size: 16px;
                    &:before{
                        content: '';
                        display: block;
                        width: 22px;
                        height: 22px;
                        background-image: url('/assets/img/static/check.svg');
                        background-repeat: no-repeat;
                        background-size: contain;
                        margin-bottom: 8px;
                    }
                }
            }
        }
        @media (max-width: 1200px) {
            &__page{
                margin-top: 90px;
                padding: 24px;
            }
            &__cover--big{
                padding: 88px 0 64px;
                .pc-col-5{
                    order: 1;
                    text-align: center;
                    padding-top: 24px;
                }
                .pc-col-5, .pc-col-6{
                    padding: 0px;
                    margin: auto;
                }
            }
            &__cover-form{
                padding: 16px;
            }
            &__crm-brands, &__brands, &__join .container, .tl-ta-c{
                justify-content: center;
            }
            &__subtitle, &__text, &__text-min, &__title{
                text-align: center;
            }
            &__crm-img{
                background-image: none;
            }
            &__join{
                .page{
                    &__title{
                        margin-bottom: 24px;
                    }
                }
            }
            &__cover{
                height: auto;
            }
            &__card{
                display: flex;
                align-items: center;
                margin-bottom: 16px;
                .svg{
                    margin-right: 24px;
                }
                .page{
                    &__subtitle{
                        width: 240px;
                        text-align: right;
                        padding: 0 16px;
                    }
                    &__text{
                        flex: 1;
                    }
                }
            }
            &__autoguru-cel{
                display: none;
            }
            &__autoguru-mobile{
                .pc-col-6{
                    width: 50%
                }
                .page{
                    &__subtitle{
                        &:before{
                            content: none;
                        }
                    }
                }
            }
            &__universidade-card{
                flex: 1;
                padding: 8px 24px;
            }
            &__universidade-reasons{
                background-position: bottom center, center bottom;
                background-size: 100%, 70%;
            }
            &__feiroes-screen{
                height: 0;
                margin-bottom: 64px;
            }
            &__feiroes-benefits{
                .svg{
                    display: none;
                }
                .page{
                    &__title{
                        margin-bottom: 32px;
                    }
                }
            }
            &__estoque-product{
                background-color: #ebeef3;
                background-image: none;
                padding: 32px 0;
            }
        }
        &__estoque-features{
            .page{
                &__text{
                    font-size: 16px;
                    text-align: left;
                    margin-bottom: 26px;
                    padding: 0;
                    &:before{
                        content: '';
                        display: block;
                        width: 22px;
                        height: 22px;
                        background-image: url('/assets/img/static/check.svg');
                        background-repeat: no-repeat;
                        background-size: contain;
                        margin-bottom: 8px;
                    }
                }
            }
        }
    }
`;
