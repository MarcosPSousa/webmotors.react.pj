import React, { Component, createRef } from 'react';
import SVG from 'webmotors-svg';

class WebInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            readOnly: true,
            passwordView: true,
            description: this.props.description,
            passwordClass: '',
        };
        this.input = createRef();
        this.wrapper = createRef();
    }

    componentDidMount() {
        this.getId();
        this.wrapperClassAdd('inputtext--disabled', this.props.disabled);
        this.wrapperClassAdd('inputtext--opened', this.props.value && this.props.value !== '');
        this.handlerAutoMask();
    }

    componentDidUpdate() {
        this.wrapperClassRemove('inputtext--disabled', !this.props.disabled);
        this.wrapperClassAdd('inputtext--opened', this.props.value);
        this.handlerUpdateMask();
        this.handlerUpdateChecked();
    }

    getId() {
        const id = this.props.id ? this.props.id : `WebInput_${Date.now()}_${(this.props.placeholder || this.props.label).replace(/\W/g, '').toLowerCase()}`;
        this.setState({ id });
    }

    wrapperClassAdd(className, test) {
        setTimeout(() => {
            const { current } = this.wrapper;
            if (test && current) {
                current.classList.add(className);
            }
        }, 10);
    }

    wrapperClassRemove(className, test) {
        const { current } = this.wrapper;
        if (test && current) {
            setTimeout(() => current.classList.remove(className), 10);
        }
    }

    handlerUpdateMask() {
        if (this.state.mask && this.input.current.getAttribute('value')) {
            this.renderMask(this.input.current);
        }
    }

    handlerUpdateChecked() {
        if (this.props.type === 'radio' && this.props.checked) {
            this.input.current.click();
        }
    }

    handlerAutoMask() {
        const fn = {
            tel: () => this.setState({ mask: '(99) 9999-99999' }),
            'tel-home': () => this.setState({ mask: '(99) 9999-9999' }),
            cpf: () => this.setState({ mask: '999.999.999-99' }),
            cnpj: () => this.setState({ mask: '999.999.999/9999-99' }),
            cep: () => this.setState({ mask: '99999-999' }),
            plate: () => /* this.setState({ mask: '(99) 9999-99999' }) */ console.info('mask plate not found'),
            date: () => this.setState({ mask: '99/99/9999' }),
            'date-short': () => this.setState({ mask: '99/9999' }),
            renavam: () => this.setState({ mask: '99999999999' }),
            price: () => this.setState({ mask: '999.999.999.999,99', maskReverse: true }),
            time: () => this.setState({ mask: '99:99' }),
        };
        const type = fn[this.props.type];
        if (type) {
            type();
        }
    }

    handlerHasValue(input) {
        if (input.value !== '') {
            this.wrapperClassAdd('inputtext--opened', true);
        } else {
            this.wrapperClassRemove('inputtext--opened', true);
        }
    }

    handlerFocus(e) {
        this.setState({ readOnly: false });
        this.handlerHasValue(e.target);
        this.wrapperClassAdd('inputtext--active', true);
    }

    handlerBlur(e) {
        this.handlerHasValue(e.target);
        this.handlerValidate(e.target);
        this.wrapperClassRemove('inputtext--active', true);
    }

    handlerInput(e) {
        const { target } = e;
        this.wrapperClassAdd('inputtext--opened', target.value !== '');
        this.renderMask(target);
        if (this.props.type === 'email' || this.props.type === 'email-login') {
            target.value = target.value.toLowerCase();
        }
    }

    handlerError(error) {
        if (error) {
            this.wrapperClassAdd('inputtext--error', true);
        } else {
            this.setState({ description: this.props.description });
            this.wrapperClassRemove('inputtext--error', true);
        }
    }

    handlerCustomEvent(event, object) {
        this.props[event](object);
    }

    handlerValidate(input) {
        const { value } = input;
        const { props } = this;
        const { type, min, max } = props;
        let error;
        switch (type) {
            case 'email': {
                error = !/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
                this.setState({ description: 'Digite um e-mail válido' });
                break;
            }
            case 'email-login': {
                error = !/^([a-zA-Z0-9_\-.]+)@([a-zA-Z-0-9\-.])+\.[a-z]{1,10}(\.[a-z]{1,3})?$/.test(value);
                this.setState({ description: 'Digite um e-mail válido' });
                break;
            }
            case 'tel': {
                error = !/^\(\d{2}\)\s?(\d{4}-\d{4,5}|\d{4,5}-\d{4})$/.test(value);
                this.setState({ description: 'Digite um telefone válido' });
                break;
            }
            case 'tel-home': {
                error = !/^\(\d{2}\)\s?\d{4}-\d{4}$/.test(value);
                this.setState({ description: 'Digite um telefone fixo válido' });
                break;
            }
            case 'cpf': {
                error = (() => {
                    const cpf = value.replace(/[^\d]+/g,'');
                    let add;
                    let rev;
                    let i;
                    if (cpf === '') {
                        return true;
                    }
                    if (/0{11}|1{11}|2{11}|3{11}|4{11}|5{11}|6{11}|7{11}|8{11}|9{11}/.test(cpf)) {
                        return true;
                    }
                    // Valida 1o digito
                    add = 0;
                    for (i = 0; i < 9; i++) {
                        add += parseInt(cpf.charAt(i), 10) * (10 - i);
                    }
                    rev = 11 - (add % 11);
                    if (rev === 10 || rev === 11) {
                        rev = 0;
                    }
                    if (rev !== parseInt(cpf.charAt(9), 10)) {
                        return true;
                    }
                    // Valida 2o digito
                    add = 0;
                    for (i = 0; i < 10; i++) {
                        add += parseInt(cpf.charAt(i), 10) * (11 - i);
                    }
                    rev = 11 - (add % 11);
                    if (rev === 10 || rev === 11) {
                        rev = 0;
                    }
                    if (rev !== parseInt(cpf.charAt(10), 10)) {
                        return true;
                    }
                    return false;
                })();
                this.setState({ description: 'Digite um CPF válido' });
                break;
            }
            case 'cnpj': {
                error = (() => {
                    const cnpj = value.replace(/\D/g, '');
                    let i;
                    let tamanho;
                    let numeros;
                    let soma;
                    let pos;
                    let resultado;
                    if (cnpj === '' || cnpj.length !== 14 || /0{14}|1{14}|2{14}|3{14}|4{14}|5{14}|6{14}|7{14}|8{14}|9{14}/.test(cnpj)) {
                        return true;
                    }
                    // Valida DVs
                    tamanho = cnpj.length - 2;
                    numeros = cnpj.substring(0, tamanho);
                    const digitos = cnpj.substring(tamanho);
                    soma = 0;
                    pos = tamanho - 7;
                    for (i = tamanho; i >= 1; i--) {
                        soma += numeros.charAt(tamanho - i) * pos--;
                        if (pos < 2) {
                            pos = 9;
                        }
                    }
                    resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);
                    if (resultado !== parseInt(digitos.charAt(0), 10)) {
                        return true;
                    }
                    numeros = cnpj.substring(0, ++tamanho);
                    soma = 0;
                    pos = tamanho - 7;
                    for (i = tamanho; i >= 1; i--) {
                        soma += numeros.charAt(tamanho - i) * pos--;
                        if (pos < 2) {
                            pos = 9;
                        }
                    }
                    resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);
                    if (resultado !== parseInt(digitos.charAt(1), 10)) {
                        return true;
                    }
                    return false;
                })();
                this.setState({ description: 'Digite um CNPJ válido' });
                break;
            }
            case 'name-surname': {
                error = !/^\D{2,}\s\D{2,}$/.test(value);
                this.setState({ description: 'Digite um nome e sobrenome válido' });
                break;
            }
            case 'cep': {
                error = !/^\w{5}-\w{3}$/.test(value);
                this.setState({ description: 'Digite um CEP válido' });
                break;
            }
            case 'char-between': {
                error = (value.length < (min || 1) || value.length > (max || 8));
                this.setState({ description: `Digite uma palavra com mais de ${min} caracteres e menos de ${max}` });
                break;
            }
            case 'plate': {
                error = !/^\[A-Z]{3}-\d{4}$/.test(value) || !/^\[A-Z]{3}\d[A-J]\d{2}$/.test(value);
                this.setState({ description: 'Digite uma placa de veículo válida' });
                break;
            }
            case 'date': {
                error = !/^\w{2}\/\w{2}\/\w{4}$/.test(value);
                this.setState({ description: 'Digite uma data válida' });
                break;
            }
            case 'date-short': {
                error = !/^\d{2}\/\d{2,4}$/.test(value);
                this.setState({ description: 'Digite uma data simplificada válida' });
                break;
            }
            case 'renavam': {
                error = (() => {
                    const d = value.split('');
                    let soma = 0;
                    let valor = 0;
                    let digito = 0;
                    let x = 0;
                    for (let i = 5; i >= 2; i--) {
                        soma += d[x] * i;
                        x++;
                    }
                    valor = soma % 11;
                    if (valor === 11 || valor === 0 || valor >= 10) {
                        digito = 0;
                    } else {
                        digito = valor;
                    }
                    return digito === d[4];
                })();
                this.setState({ description: 'Digite um renavam válido' });
                break;
            }
            case 'password': {
                if (!this.props.noForce) {
                    error = !/(?=^.{8,}$)((?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*\d{1,})(?=.*\W{1,}))^.*$/.test(value);
                    this.setState({ description: 'Digite todos critérios de senha válidos' });
                }
                break;
            }
            case 'url': {
                error = !/^https?:\/\/(\w|-)+\.(\w|-)+((\.|\/)[a-z]+(\.[a-z]+(\/\w+)?)?)?.+$/.test(value);
                this.setState({ description: 'Digite uma url válida (ex.: http://www.webmotors.com.br)' });
                break;
            }
            case 'time': {
                error = !/^[0-2]\d:[0-5]\d$/.test(value);
                this.setState({ description: 'Digite um horário válido' });
                break;
            }
            default: {
                break;
            }
        }
        if ((max || min) && type !== 'char-between') {
            const valueNumber = Number(value);
            const notNumber = /\D/.test(value);
            if (notNumber) {
                error = true;
                this.setState({ description: 'Digite um número válido' });
            }
            if (props.max && max < valueNumber) {
                error = true;
                this.setState({ description: `Digite um número menor que ${max}` });
            }
            if (props.min && min > valueNumber) {
                error = true;
                this.setState({ description: `Digite um número maior que ${min}` });
            }
        }
        if (props.equal) {
            const inputEqual = document.querySelector(props.equal);
            if (inputEqual && inputEqual.value !== value) {
                error = true;
                const inputName = inputEqual.previousElementSibling;
                this.setState({ description: `Digite um valor igual ao campo "${inputName.textContent}"` });
            }
        }
        if (value.length === 0 && props.required) {
            error = true;
            this.setState({ description: 'Preencha o campo obrigatório' });
        }
        if (value.length === 0 && !props.required) {
            error = false;
            this.setState({ description: props.description });
        }
        this.handlerError(error);
    }

    handlerPasswordToggle() {
        if (this.state.passwordView) {
            this.setState({ passwordClass: 'inputtext__button--view', passwordView: false });
            this.input.current.type = 'text';
        } else {
            this.setState({ passwordClass: '', passwordView: true });
            this.input.current.type = 'password';
        }
    }

    renderMask(tag) {
        const mask = this.props.mask || this.state.mask;
        if (mask) {
            const input = tag;
            let valueCount = 0;
            const valueArray = input.value.replace(/\D/g, '').split('');
            const valueFinal = [];
            const maskArray = mask.split('');
            if (this.state.maskReverse) {
                const maskArrayReverse = maskArray;
                const valueArrayReverse = valueArray;
                maskArrayReverse.reverse();
                valueArrayReverse.reverse();
                maskArrayReverse.forEach((item, i) => {
                    if (valueArrayReverse[i - valueCount]) {
                        if (/\D/.test(item)) {
                            valueFinal.push(item);
                            valueCount++;
                        } else {
                            valueFinal.push(valueArrayReverse[i - valueCount]);
                        }
                    }
                });
                valueFinal.reverse();
            } else {
                maskArray.forEach((item) => {
                    if (valueCount < valueArray.length) {
                        if (/\D/.test(item)) {
                            valueFinal.push(item);
                        } else {
                            valueFinal.push(valueArray[valueCount]);
                            valueCount++;
                        }
                    }
                });
            }
            input[input.value ? 'value' : 'textContent'] = valueFinal.join('');
        }
    }

    renderPlaceholder() {
        return <span className="inputtext__placeholder">{this.props.placeholder}</span>;
    }

    renderDescription() {
        return <div className="inputtext__description">{this.props.description || this.state.description}</div>;
    }

    renderType() {
        const { props } = this;
        const value = (this.input.current && this.input.current.value) || props.value;
        switch (props.type) {
            case 'checkbox': {
                return (
                    <label className={`inputcheckbox ${props.disabled ? 'inputcheckbox--disabled' : ''}`} htmlFor={this.state.id}>
                        <input
                            type="checkbox"
                            className="inputcheckbox__input"
                            ref={this.input}
                            id={this.state.id}
                            name={props.name}
                            defaultValue={value}
                            defaultChecked={props.checked}
                            disabled={props.disabled}
                            onChange={(e) => {
                                if (props.onChange && !props.disabled) this.handlerCustomEvent('onChange', e);
                            }}
                            data-type={this.props['data-type']}
                        />
                        <span className="inputcheckbox__check" />
                        <div className="inputcheckbox__label">{props.label}</div>
                    </label>
                );
            }
            case 'radio': {
                return (
                    <label className={`inputradio ${props.disabled ? 'inputradio--disabled' : ''}`} htmlFor={this.state.id}>
                        <input
                            type="radio"
                            className="inputradio__input"
                            ref={this.input}
                            id={this.state.id}
                            name={props.name}
                            defaultValue={value}
                            defaultChecked={props.checked}
                            disabled={props.disabled}
                            onChange={(e) => {
                                if (props.onChange) this.handlerCustomEvent('onChange', e);
                            }}
                            data-type={this.props['data-type']}
                        />
                        <span className="inputradio__check" />
                        <div className="inputradio__label">{props.label}</div>
                    </label>
                );
            }
            case 'select': {
                return (
                    props.disabled
                        ? (
                            <div ref={this.wrapper} className={`inputtext ${props.error ? 'inputtext--error' : ''} ${value ? 'inputtext--opened' : ''}`}>
                                { this.renderPlaceholder() }
                                <div className="inputtext__input" id={this.state.id}>{value}</div>
                                { this.renderDescription() }
                            </div>
                        )
                        : (
                            <div ref={this.wrapper} className={`inputtext ${props.error ? 'inputtext--error' : ''} ${value ? 'inputtext--opened' : ''}`}>
                                <span className="inputtext__select">
                                    { this.renderPlaceholder() }
                                </span>
                                <select
                                    className="inputtext__input"
                                    ref={this.input}
                                    id={this.state.id}
                                    name={props.name}
                                    defaultValue={value}
                                    onChange={(e) => {
                                        this.wrapperClassAdd('inputtext--opened', e.target.value !== '');
                                        this.wrapperClassRemove('inputtext--opened', e.target.value === '');
                                        if (props.onChange) this.handlerCustomEvent('onChange', e);
                                    }}
                                    onFocus={(e) => {
                                        this.handlerFocus(e);
                                        if (props.onFocus) this.handlerCustomEvent('onFocus', e);
                                    }}
                                    onBlur={(e) => {
                                        this.handlerBlur(e);
                                        if (props.onBlur) this.handlerCustomEvent('onBlur', e);
                                    }}
                                    data-type={this.props['data-type']}
                                >
                                    {value === '' && <option value="">{' '}</option>}
                                    {props.children}
                                </select>
                                { this.renderDescription() }
                            </div>
                        )
                );
            }
            case 'switch': {
                return (
                    <label className={`inputswitch ${props.disabled ? 'inputswitch--disabled' : ''}`} htmlFor={this.state.id}>
                        <input
                            className="inputswitch__input"
                            type="checkbox"
                            ref={this.input}
                            name={props.name}
                            defaultValue={value}
                            id={this.state.id}
                            defaultChecked={props.checked}
                            disabled={props.disabled}
                            onChange={(e) => {
                                if (props.onChange) this.handlerCustomEvent('onChange', e);
                            }}
                            tabIndex="-1"
                            data-type={this.props['data-type']}
                        />
                        <div className="inputswitch__scroll" />
                        <div className="inputswitch__label">{props.label}</div>
                    </label>
                );
            }
            default: {
                const inputMaxLength = () => {
                    if (props.type === 'email' || props.type === 'email-login') {
                        return 150;
                    }
                    if (props.type === 'password') {
                        return 40;
                    }
                    return props.maxlength;
                };
                return (
                    props.disabled
                        ? (
                            <div ref={this.wrapper} className={`inputtext ${props.error ? 'inputtext--error' : ''} ${value ? 'inputtext--opened' : ''}`}>
                                { this.renderPlaceholder() }
                                <div className="inputtext__input" ref={this.input} id={this.state.id}>{value}</div>
                                { this.renderDescription() }
                            </div>
                        )
                        : (
                            <label
                                ref={this.wrapper}
                                className={`inputtext ${props.error ? 'inputtext--error' : ''} ${value ? 'inputtext--opened' : ''}`}
                                htmlFor={this.state.id}
                            >
                                { this.renderPlaceholder() }
                                <input
                                    type={props.type === 'password' ? 'password' : 'text'}
                                    className="inputtext__input"
                                    ref={this.input}
                                    id={this.state.id}
                                    name={props.name}
                                    defaultValue={value}
                                    readOnly={this.state.readOnly}
                                    tabIndex={props.tabIndex ? props.tabIndex : '0'}
                                    maxLength={inputMaxLength()}
                                    onFocus={(e) => {
                                        this.handlerFocus(e);
                                        if (props.onFocus) this.handlerCustomEvent('onFocus', e);
                                    }}
                                    onBlur={(e) => {
                                        this.handlerBlur(e);
                                        if (props.onBlur) this.handlerCustomEvent('onBlur', e);
                                    }}
                                    onInput={(e) => {
                                        this.handlerInput(e);
                                        if (props.onInput) this.handlerCustomEvent('onInput', e);
                                    }}
                                    data-type={this.props['data-type']}
                                />
                                {
                                    props.type === 'password' && (
                                        <button tabIndex="-1" aria-hidden="true" className={`inputtext__button ${this.state.passwordClass}`} type="button" onClick={() => this.handlerPasswordToggle()}>
                                            <SVG className="inputtext__password--show" src="/assets/img/icons/eye-view.svg" />
                                            <SVG className="inputtext__password--hide" src="/assets/img/icons/eye-hide.svg" />
                                        </button>
                                    )
                                }
                                { this.renderDescription() }
                            </label>
                        )
                );
            }
        }
    }

    render() {
        return this.renderType();
    }
}

export default WebInput;
