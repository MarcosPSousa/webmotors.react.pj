import React, { Component } from 'react';

class Notice extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.text = this.props.text;
    }

    render() {
        const { text } = this.props;
        const type = this.props.type === undefined ? 'error' : this.props.type;
        return (
            <div role="alert" className={`notice notice--${type} ${text ? 'notice--active' : ''}`}>
                <div className="notice__content">{text}</div>
            </div>
        );
    }
}

export default Notice;
