import React, { Component } from 'react';

class Button extends Component {
    renderLink() {
        return (
            this.props.target === '_blank'
                ? (
                    <a target="_blank" rel="noopener noreferrer" href={this.props.url} className={this.renderClassName()}>
                        {this.props.children}
                    </a>
                )
                : (
                    <a href={this.props.url} className={this.renderClassName()}>
                        {this.props.children}
                    </a>
                )
        );
    }

    renderClassName() {
        const name = ['button'];
        name.push(this.props.mode ? `button__${this.props.mode}` : 'button__default');
        if (this.props.outline) {
            name.push('button--outline');
        }
        if (this.props.shadow) {
            name.push('button--shadow');
        }
        if (this.props.disabled) {
            name.push('button--disabled');
        }
        if (this.props.className) {
            name.push(this.props.className);
        }
        return name.join(' ');
    }

    render() {
        const button = this.props.type || 'button';
        return (
            this.props.url
                ? this.renderLink()
                : (
                    <button onClick={() => this.props.onClick && this.props.onClick()} type={button} disabled={this.props.disabled} className={this.renderClassName()}>
                        {this.props.children}
                    </button>
                )
        );
    }
}

export default Button;
