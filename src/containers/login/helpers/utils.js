import React from 'react';
import { UrlEstoque } from 'webmotors-react-pj/config';
import {
    formatDate,
    formatMoney,
    WINDOW,
    cookieGet,
} from 'webmotors-react-pj/utils';
import Axios from 'axios';
import { ENUM } from './constants';

const formNotice = (that, text, type = 'error', timer = 8000) => {
    const t = that;
    t.setState({
        noticeShow: true,
        noticeText: text,
        noticeType: type,
    });
    clearTimeout(t.timerClose);
    t.timerClose = setTimeout(() => t.setState({ noticeText: '' }), timer);
};

const scrollSmooth = (to, duration = 300) => {
    if (WINDOW) {
        clearTimeout(window.scrollSmoothTimerCache);
        if (duration <= 0) {
            return;
        }
        const difference = to - window.scrollY - 100;
        const perTick = (difference / duration * 10);
        window.scrollSmoothTimerCache = setTimeout(() => {
            if (!Number.isNaN(parseInt(perTick, 10))) {
                window.scrollTo(0, window.scrollY + perTick);
                scrollSmooth(to, duration - 10);
            }
        }, 10);
    }
};

const accordionAnimate = (testOpen, item) => {
    const tag = item;
    const getPadding = padding => parseFloat(getComputedStyle(tag)[padding]);
    tag.classList.add('accordion--transition');
    if (testOpen) {
        tag.style.height = 'auto';
        const { clientHeight } = tag;
        const height = (clientHeight + getPadding('paddingTop') + getPadding('paddingBottom'));
        tag.style.height = '0px';
        setTimeout(() => {
            tag.style.height = `${height}px`;
        }, 1);
    } else {
        tag.style.height = '0px';
        tag.addEventListener('transitionEnd', () => tag.classList.remove('accordion--transition'));
    }
};

const listboxItemAnimation = () => {
    setTimeout(() => [...document.querySelectorAll('.listbox__item')].forEach((item, i) => setTimeout(() => item.classList.add('listbox__item--active'), Math.min(i * 60, 2000))), 10);
};

const invoiceFormat = obj => (
    obj.map((item) => {
        const getTicketStatus = (status) => {
            const statusReturn = {
                [ENUM.invoice.Emitida]: { className: 'color-error', text: 'Emitida' },
                [ENUM.invoice.AguardandoPagamento]: { className: 'color-error', text: 'Aguardando pagamento' },
                [ENUM.invoice.EmAtraso]: { className: 'color-error', text: 'Em atraso' },
                [ENUM.invoice.Expirada]: { className: 'color-error', text: 'Expirada' },
                [ENUM.invoice.Reagendada]: { className: 'color-error', text: 'Reagendada' },
                [ENUM.invoice.Quitada]: { className: 'color-success', text: 'Quitada' },
                [ENUM.invoice.Vencida]: { className: 'color-error', text: 'Vencida' },
                [ENUM.invoice.ErroNaCobranca]: { className: 'color-error', text: 'Erro na cobrança' },
                [ENUM.invoice.Prorrogada]: { className: 'color-error', text: 'Prorrogada' },
                [ENUM.invoice.EmAcordo]: { className: 'color-error', text: 'Em acordo' },
            };
            if (statusReturn[status]) {
                return statusReturn[status];
            }
            return { className: 'color-error', text: '-' };
        };
        const getLinkFormat = (product) => {
            if (product.LinkBoleto) {
                return <a className="link" href={product.LinkBoleto} target="_blank" rel="noopener noreferrer">Boleto</a>;
            }
            return '-';
        };
        const getDateValid = date => /0001-01-01/.test(date);
        const response = {};
        const invoiceList = item.Lista;
        const invoiceListFirst = invoiceList[0];
        const invoiceProducts = item.produtos;
        const dateIssue = new Date(invoiceListFirst.DataEmissao);
        const invoiceProductsFilter = [];
        const invoiceProductsList = [];
        let invoiceProductsValue = 0;
        response.outsideTitle = 'Mês';
        response.outsideTotal = 'Valor total da fatura';
        response.outsideNfe = item.notasFiscais.map(nfe => <a key={`nfe_${nfe.Codigo}_${nfe.CodigoRPS}`} className="listbox__title--space link" href={nfe.Link} target="_blank" rel="noopener noreferrer">{nfe.Codigo || nfe.CodigoRPS}</a>);
        response.infoMonth = new Date(invoiceListFirst.DataEmissao).toLocaleDateString('pt-BR', { month: 'long' });
        response.printButton = `/meu-plano/imprimir/${dateIssue.getFullYear()}/${dateIssue.getMonth() + 1}`;
        response.exportButton = `${UrlEstoque}/meu-plano/fatura?exportar=1&ano=${dateIssue.getFullYear()}&mes=${dateIssue.getMonth() + 1}&buscarPorVencimento=false`;
        response.tickets = {
            header: ['Status', 'Nº da Fatura', 'Via', 'Emissão', 'Vencimento', 'Quitação', 'Cobrança', 'Imprimir boleto'],
            body: invoiceList.map(list => ([
                getTicketStatus(list.StatusCobranca),
                { className: '', text: list.CobrancaId },
                { className: '', text: list.Via },
                { className: '', text: getDateValid(list.DataEmissao) ? '-' : formatDate(list.DataEmissao) },
                { className: '', text: getDateValid(list.DataVencimento) ? '-' : formatDate(list.DataVencimento) },
                { className: '', text: getDateValid(list.DataPagamento) ? '-' : formatDate(list.DataPagamento) },
                { className: '', text: getDateValid(list.Valor) ? '-' : formatMoney(list.Valor) },
                { className: 'listbox__table-cell--big', text: getLinkFormat(list) },
            ])),
        };
        Object.values(invoiceProducts).forEach((product) => {
            if (product) {
                invoiceProductsFilter.push(product);
            }
        });
        invoiceProductsFilter.forEach(list => list.forEach((product) => {
            invoiceProductsValue = product.PrecoFatura + invoiceProductsValue;
            invoiceProductsList.push([
                { className: 'listbox__table-cell--strong ta-l', text: product.Produto },
                { className: '', text: product.Quantidade },
                { className: '', text: product.DataVigencia },
                { className: '', text: product.PrecoTotalFormatado },
                { className: '', text: product.PrecoDescontoFormatado },
                { className: '', text: product.PrecoFaturaFormatado },
            ]);
        }));
        response.infoTotal = invoiceProductsValue.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
        response.products = {
            header: ['Produto', 'Quantidade', 'Período de cobrança', 'Valor bruto', 'Desconto comercial', 'Valor líquido'],
            body: invoiceProductsList,
        };
        return response;
    })
);

const ajax = (url, data, method = 'POST', dispatch) => (
    Axios({
        method,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${cookieGet('CockpitLogged')}`,
        },
        url,
        data,
        params: method === 'POST' ? '' : data,
    }).then((res) => {
        const resData = res && (res.data.data || res.data.Result);
        const resStatus = res && res.status;
        const returnObj = {
            type: `AJAX_${url}`,
            payload: {
                success: true,
                data: resData,
                status: resStatus,
            },
        };
        if (dispatch) {
            return dispatch(returnObj);
        }
        return returnObj;
    }).catch((rej) => {
        let resData;
        let resStatus;
        const resErrorText = {
            400: 'Dados inválidos',
            401: 'Usuário não autorizado para esta ação',
            403: 'Usuário não autorizado para esta ação. Verifique com o administrador da loja',
            500: 'Erro interno da aplicação, tente novamente mais tarde',
            503: 'Servidor fora do ar, tente novamente mais tarde',
        };
        const resErrorTextUndefined = 'Erro não parametrizado pelo servidor';
        console.error(rej);
        if (rej && rej.response) {
            const errorData = rej.response.data;
            resStatus = rej.status || rej.response.status;
            resData = (
                errorData.errors
                || (errorData.Messages && errorData.Messages[0])
                || errorData
                || (() => {
                    resData = resErrorText[resStatus];
                    if (!resData) {
                        resData = resErrorTextUndefined;
                    }
                    return resData;
                })(resStatus)
            );
        } else {
            resStatus = /(\d+|\d+\n)/.exec(rej) ? /(\d+|\d+\n)/.exec(rej)[0].trim() : false;
            resData = resErrorText[resStatus];
            if (!resData) {
                resData = resErrorTextUndefined;
            }
        }
        const returnObj = {
            type: `AJAX_${url}_FAILURE`,
            payload: {
                success: false,
                errors: resData,
                status: Number(resStatus),
            },
        };
        if (dispatch) {
            return dispatch(returnObj);
        }
        return returnObj;
    })
);

export {
    WINDOW,
    formNotice,
    scrollSmooth,
    accordionAnimate,
    listboxItemAnimation,
    invoiceFormat,
    ajax,
};
export default WINDOW;
