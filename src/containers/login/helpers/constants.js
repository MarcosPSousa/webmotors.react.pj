const ENUM = Object.freeze({
    user: {
        filter: {
            ativo: 1,
            inativo: 2,
            ausente: 3,
        },
        perfil: {
            administrador: 1,
            gerente: 2,
            marketing: 3,
            vendedor: 4,
            estoquista: 5,
        },
        perfilNome: {
            1: 'Administrador',
            2: 'Gerente de Loja',
            3: 'Marketing',
            4: 'Vendedor',
            5: 'Estoquista',
        },
    },
    profiles: {
        administradorGrupo: 1,
        administradorRevenda: 2,
        gerenteVendas: 3,
        vendedor: 4,
        integradorApi: 5,
        atendenteWebGrupo: 6,
        administradorCockpit: 7,
        funcionarioCockpit: 8,
        perfilCustomizado: 9,
        administradorGestorEstoque: 10,
        gerenteCrm: 11,
        maisFidelidade: 12,
        autoguru: 13,
        universidade: 14,
        gearbox: 15,
    },
    graphicType: {
        Barra: 1,
        Linhas: 2,
        Indicador: 3,
        Funil: 5,
        Pizza: 6,
        Velocimetro: 7,
        BarraLinha: 8,
    },
    invoice: {
        Emitida: 69,
        AguardandoPagamento: 77,
        EmAtraso: 65,
        Expirada: 80,
        Reagendada: 82,
        Quitada: 81,
        Vencida: 86,
        ErroNaCobranca: 78,
        Prorrogada: 71,
        EmAcordo: 79,
    },
    leads: {
        Tipo: {
            Usado: 78,
            Novo: 85,
        },
    },
    leadsExtractMonths: [
        {
            id: 0,
            title: 'Atual',
        },
        {
            id: 1,
            title: '03/2019',
        },
        {
            id: 2,
            title: '04/2019',
        },
        {
            id: 3,
            title: '05/2019',
        },
    ],
    leadsExtractOrigin: [
        {
            id: 0,
            title: 'Todos',
        },
        {
            id: 1,
            title: 'Monitoramento de ligações',
        },
        {
            id: 2,
            title: 'Formulário de proposta',
        },
        {
            id: 3,
            title: 'Proposta de ZeroKM',
        },
        {
            id: 4,
            title: 'Proposta de Moto',
        },
        {
            id: 5,
            title: 'Formulário de Financiamento',
        },
    ],
    leadsExtractStatus: [
        {
            id: 0,
            title: 'Todos',
        },
        {
            id: 1,
            title: 'Bonificado',
        },
        {
            id: 2,
            title: 'Cobrável',
        },
        {
            id: 3,
            title: 'Contestado',
        },
        {
            id: 4,
            title: 'Duplicado',
        },
        {
            id: 5,
            title: 'Recusado',
        },
    ],
    product: {
        crm: 1,
        estoque: 2,
        maisFidelidade: 3,
        cockpit: 4,
        planosWebmotors: 5,
        site: 6,
        gearbox: 7,
        universidade: 8,
        autoguru: 9,
        planoControle: 10,
        planoWeb: 11,
        planoPerformance: 12,
        gestorEstoque: 13,
    },
    dateWeek: {
        semanas: [
            {
                dia: 1,
                nomeDia: 'Segunda-feira',
            },
            {
                dia: 2,
                nomeDia: 'Terça-feira',
            },
            {
                dia: 3,
                nomeDia: 'Quarta-feira',
            },
            {
                dia: 4,
                nomeDia: 'Quinta-feira',
            },
            {
                dia: 5,
                nomeDia: 'Sexta-feira',
            },
        ],
        fimDeSemana: [
            {
                dia: 6,
                nomeDia: 'Sábado',
            },
            {
                dia: 0,
                nomeDia: 'Domingo',
            },
            {
                dia: 7,
                nomeDia: 'Feriados',
            },
        ],
    },
});

const PRODUCT = 'COCKPIT';

const INITIAL_STATE = {
    fetched: false,
    result: [],
    error: null,
};

export {
    ENUM,
    INITIAL_STATE,
    PRODUCT,
};

export default '';
