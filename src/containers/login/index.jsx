import React, { Component, createRef } from 'react';
import Helmet from 'react-helmet';
import {
    cookieSet,
    jwtGet,
    paramURL,
    Ajax,
} from 'webmotors-react-pj/utils';
import { ApiLogin, UrlCockpit, ApiLoginWebmotors } from 'webmotors-react-pj/config';
import Form from 'webmotors-react-pj/form';
import translate from 'webmotors-react-pj/translate';
import InputText from 'webmotors-react-pj/webinput/text';
import Img from 'webmotors-react-pj/img';
import WebInputCheckbox from './components/WebInput';
import Notice from './components/Notice';
import Button from './components/Button';
import { formNotice } from './helpers/utils';
import {
    StyleButtonIcon,
    StyleModal,
    StyleModalLoad,
    StyleModalBg,
    StyleModalClose,
    StyleModalContent,
    StyleLoginContainer,
    StyleFormLoginset,
} from './style';

const ModalNoFrame = ({ children, open, onClose }) => (
    open ? (
        <StyleModal>
            <StyleModalBg />
            <StyleModalClose aria-hidden="true" onClick={() => onClose()} />
            <StyleModalContent>{children}</StyleModalContent>
        </StyleModal>
    ) : false
);

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            backoffice: paramURL('bokey'),
            logincookie: paramURL('cookie'),
        };
        this.email = createRef();
        this.password = createRef();
        this.refButtonLoginset = createRef();
    }

    componentDidMount() {
        if (this.state.backoffice) {
            this.getBOKey();
        }
    }

    getBOKey() {
        Ajax({
            url: `${ApiLogin}/access/sso/loginhub`,
            data: { Bokey: this.state.backoffice },
            method: 'POST',
        }).then((e) => {
            if (e.success) {
                cookieSet('CockpitLogged', e.data.jwt);
                const jwt = jwtGet();
                let secounds = 5;
                const mensageSuccess = () => {
                    if (secounds >= 0) {
                        formNotice(this, (
                            <div>
                                {translate.login.loginFrom}
                                <strong>{` ${jwt.status}`}</strong>
                                <br />
                                <br />
                                {translate.login.redirectIn}
                                {` ${secounds--}`}
                                {translate.login.seconds}
                            </div>
                        ), 'success');
                    } else {
                        window.clearInterval(window.LoginInterval);
                        window.location.href = '/';
                    }
                };
                mensageSuccess();
                window.LoginInterval = window.setInterval(() => {
                    mensageSuccess();
                }, 1000);
            } else {
                formNotice(this, e.data);
            }
        }).then(() => {
            this.setState({ loading: false });
        });
    }

    getEmail() {
        return 'false';
    }

    getPassword() {
        return 'false';
    }

    handleChangeEmail(e) {
        const { value: email } = e.target;
        this.setState({
            email,
        }, () => {
            window.localStorage.setItem('CokpitLoginError', email);
        });
    }

    handleChangePassword(e) {
        const { value: password } = e.target;
        this.setState({
            password,
        });
    }

    handlePassword(e) {
        const { target } = e;
        const { previousElementSibling: input } = target;
        const { type } = input;
        target.classList.toggle('page__cover-inputbutton--show');
        input.type = type === 'text' ? 'password' : 'text';
    }

    handleSuccess(e) {
        const { data } = e;
        const { jwt } = data;
        const jwtPayload = jwt.replace(/_/g, '/').replace(/-/g, '+');

        const b64DecodeUnicode = str => decodeURIComponent(
            Array.prototype.map.call(atob(str), c =>
                '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
            ).join('')
        );
        const parseJwt = token => JSON.parse(
            b64DecodeUnicode(token.split('.')[1].replace('-', '+').replace('_', '/'))
        );
        const setCookieLogged = () => {
            cookieSet('CockpitLogged', jwt)
        };
        const result = parseJwt(jwt);
        if (result.role.indexOf('5') !== -1) {
            formNotice(this, 'Usuário e/ou senha inválidos');
        } else {
            switch (data.status) {
                case ('Nao Migrado'): {
                    formNotice(this, 'Usuário e/ou senha inválidos');
                    break;
                } case ('Pendente'): {
                    setCookieLogged();
                    window.location.href = '/usuario/introducao-cockpit';
                    break;
                } case ('senhaExpirada'): {
                    setCookieLogged();
                    window.location.href = '/redefinir-senha/troca-de-seguranca';
                    break;
                } default: {
                    const logged = () => {
                        setCookieLogged();
                    };
                    if (this.state.logincookie) {
                        this.setState({
                            jwt,
                        }, () => {
                            logged();
                            this.refButtonLoginset.current.click();
                        });
                    } else {
                        logged();
                        window.location.reload();
                    }
                }
            }
        }
    }

    handleBeforeSend() {
        formNotice(this, '');
        this.setState({ noticeText: '' }, () => {
            const [email, senha] = [this.email.current.value, this.password.current.value];
            this.setState({ formBeforeSend: (email.trim() !== '' && senha.trim() !== '') });
        });
    }

    handleError(e) {
        formNotice(this, e.data);
    }

    handleComplete() {
        if (!this.state.formBeforeSend) {
            formNotice(this, 'Preencha o campo de e-mail e senha');
        }
    }

    render() {
        return (
            <div role="main" className="page mt-64">
                <Helmet>
                    <title>Cockpit</title>
                </Helmet>
                <StyleLoginContainer />
                <div className="page__cover page__cover--big">
                    <div className="container page__cover-content d-fx">
                        <div className="pc-col-5 tl-col-12">
                            <h1 className="page__title page__cover-title mb-40 color-gray-1">{translate.login.title}</h1>
                            <p className="page__text page__cover-text mb-40">{translate.login.subTitle}</p>
                            <div className="page__brands page__brands--white mt-40 d-fx">
                                <div className="page__brands-webmotors mr-32">
                                    <Img src="/assets/img/brands/webmotors-2-fill.svg" />
                                </div>
                                <Img className="page__brands-financiamento" src="/assets/img/brands/santander-financiamento.svg" removeClass />
                            </div>
                        </div>
                        <div className="pc-col-6 -pc-col-1 tl-col-10 -tl-col-1">
                            <StyleFormLoginset method="POST" action={`${ApiLoginWebmotors}/access/sso/loginset`}>
                                <input type="hidden" name="Email" value={this.state.email} />
                                <input type="hidden" name="Senha" value={this.state.password} />
                                <input type="hidden" name="jwt" value={this.state.jwt} />
                                <input type="hidden" name="returnPage" value={window.location.href} />
                                <button ref={this.refButtonLoginset} type="submit">Enviar</button>
                            </StyleFormLoginset>
                            <Form
                                action={this.state.formBeforeSend && `${ApiLogin}/access/sso/login`}
                                method="POST"
                                className="page__cover-form bg-gray-1 color-gray-8"
                                onBeforeSend={() => this.handleBeforeSend()}
                                onSuccess={e => this.handleSuccess(e)}
                                onError={e => this.handleError(e)}
                                onComplete={() => this.handleComplete()}
                                autoComplete="on"
                            >
                                <Notice
                                    text={this.state.noticeText}
                                    type={this.state.noticeType}
                                />
                                <h3 className="page__title page__cover-subtitle pb-32">{translate.login.joinNow}</h3>
                                <InputText ref={this.email} placeholder={translate.login.email} required type="email" name="Email" id="Email" onInput={e => this.handleChangeEmail(e)} />
                                <InputText ref={this.password} placeholder={translate.login.password} required type="password-weakened" name="Senha" id="Senha" onInput={e => this.handleChangePassword(e)} />
                                <div className="ta-r mb-32">
                                    <a className="page__cover-recovery link" href={`${UrlCockpit}/redefinir-senha`}>{translate.login.recoverPass}</a>
                                </div>
                                <div className="page__cover-tools d-fx ta-j va-m">
                                    <div className="page__cover-logged">
                                        <WebInputCheckbox type="checkbox" checked value="true" name="ManterConectado" id="ManterConectado" label={translate.login.keepLogged} />
                                    </div>
                                    <Button type="submit" mode="danger" disabled={this.state.loading}>{translate.login.loginButton}</Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                    <div className="container page__cover-bg">
                        <div className="page__cover-circle" />
                    </div>
                </div>
                <div className="page__products">
                    <div className="container">
                        <h2 className="page__title pc-col-5 color-gray-8">{translate.login.secondTitle}</h2>
                        <div className="page__products-circle ta-c">
                            <div className="page__products-list">
                                <Img className="page__products-item page__products-item-1" src="/assets/img/brands/crm.svg" />
                                <Img className="page__products-item page__products-item-2" src="/assets/img/brands/gearbox.svg" />
                                <Img className="page__products-item page__products-item-3" src={translate.login.brandFidelity} />
                                <Img className="page__products-item page__products-item-4" src={translate.login.brandStock} />
                                <Img className="page__products-item page__products-item-5" src={translate.login.brandUniversity} />
                                <Img className="page__products-item page__products-item-6" src="/assets/img/brands/floorplan.svg" />
                                <Img className="page__products-item page__products-item-7" src="/assets/img/brands/autoguru.svg" />
                                <Img className="page__products-item page__products-item-8" src={translate.login.brandHotdeals} />
                            </div>
                            <div className="page__products-infos">
                                <div className="page__text page__products-text-1">{translate.login.productText1}</div>
                                <div className="page__text page__products-text-2">{translate.login.productText2}</div>
                                <div className="page__text page__products-text-3">{translate.login.productText3}</div>
                                <div className="page__text page__products-text-4">{translate.login.productText4}</div>
                            </div>
                            <Img className="page__products-logo" src="/assets/img/brands/cockpit.svg" />
                            <div className="page__products-button">
                                <Button type="button" mode="danger" onClick={() => this.setState({ modalOpen: true })}>
                                    <Img src="/assets/img/icons/play-2.svg" />
                                    {translate.login.playVideo}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="page__mobile bg-gray-8">
                    <div className="container page__mobile-shadow d-fx">
                        <div className="page__mobile-info pc-col-6">
                            <h2 className="page__title color-gray-1 mb-64">{translate.login.thirdTitle}</h2>
                            <p className="page__text mb-64 pb-8">{translate.login.thirdSubTitle}</p>
                            <div className="page__mobile-store">
                                <a target="_blank" rel="noopener noreferrer" href={translate.login.urlAppApple}>
                                    <Img className="page__mobile-app page__mobile-app-1 mr-24" src="/assets/img/static/store-app-store.png" alt="App Store" />
                                </a>
                                <a target="_blank" rel="noopener noreferrer" href={translate.login.urlAppAndroid}>
                                    <Img className="page__mobile-app page__mobile-app-2" src="/assets/img/static/store-google-play.png" alt="Google Play" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="container page__mobile-photo" />
                </div>
                <div className="page__join">
                    <div className="container d-fx va-m">
                        <div className="pc-col-8">
                            <h2 className="page__title">
                                <span className="color-1">{translate.login.joinText1}</span>
                                <br />
                                <span className="color-gray-8">{translate.login.joinText2}</span>
                            </h2>
                        </div>
                        <div className="pc-col-4 ta-r">
                            <div className="d-b">
                                <Img className="page__join-logo" src="/assets/img/brands/cockpit.svg" />
                            </div>
                            <p className="page__text page__join-text">{translate.login.joinText3}</p>
                        </div>
                    </div>
                </div>
                <ModalNoFrame
                    open={this.state.modalOpen}
                    onClose={() => this.setState({ modalOpen: false })}
                >
                    <StyleModalLoad>
                        <div className="load load--white" />
                    </StyleModalLoad>
                    <iframe
                        width="100%"
                        height="315"
                        className="login__video-iframe"
                        src="https://www.youtube-nocookie.com/embed/ieMwYid6aAc?rel=0"
                        title="Vídeo Cockpit"
                        frameBorder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                    />
                </ModalNoFrame>
            </div>
        );
    }
}

export default Login;
