const config = {
    ApiLogin: {
        development: '//localhost/WebMotors.API/login',
        homologation: 'https://hcockpit.webmotors.com.br/api/login',
        blue: 'https://azulcockpit.webmotors.com.br/api/login',
        production: 'https://cockpit.webmotors.com.br/api/login',
    },
    ApiCockpit: {
        development: 'https://localhost:44351/api',
        homologation: 'https://hcockpit.webmotors.com.br/api/cockpit',
        blue: 'https://azulcockpit.webmotors.com.br/api/cockpit',
        production: 'https://cockpit.webmotors.com.br/api/cockpit',
    },
    ApiCockpitLambda: {
        development: 'https://localhost:44365/api/cockpit',
        homologation: 'https://hcockpit.webmotors.com.br/api/cockpit',
        blue: 'https://azulcockpit.webmotors.com.br/api/cockpit',
        production: 'https://cockpit.webmotors.com.br/api/cockpit',
    },
    ApiSisense: {
        development: 'https://sisense.webmotors.com.br/api/',
        homologation: 'https://sisense.webmotors.com.br/api/',
        blue: 'https://sisense.webmotors.com.br/api/',
        production: 'https://sisense.webmotors.com.br/api/',
    },
    ApiStore: {
        development: 'https://localhost:44365/api/cockpit/shopping',
        homologation: 'https://hcockpit.webmotors.com.br/api/cockpit/shopping',
        blue: 'https://azulcockpit.webmotors.com.br/api/cockpit/shopping',
        production: 'https://cockpit.webmotors.com.br/api/cockpit/shopping',
    },
    ApiMaisFidelidade: {
        development: 'https://localhost/webmotors.com.br/cockpit.maisfidelidade',
        homologation: 'https://hcockpit.webmotors.com.br/api/cockpit/maisfidelidade',
        blue: 'https://azulcockpit.webmotors.com.br/api/cockpit/maisfidelidade',
        production: 'https://cockpit.webmotors.com.br/api/cockpit/maisfidelidade',
    },
    ApiCRM: {
        development: 'http://local.webmotors.com.br:81/webmotors.vmotors.api.pj/v3',
        homologation: 'https://hkcrm.webmotors.com.br/v3',
        blue: 'https://azulcrm.webmotors.com.br/v3',
        production: 'https://crm.webmotors.com.br/v3',
    },
    ApiEstoque: {
        development: 'https://local.webmotors.com.br:44343',
        homologation: 'https://hkestoque.webmotors.com.br',
        blue: 'https://azulestoque.webmotors.com.br',
        production: 'https://betaestoque.webmotors.com.br',
    },
    ApiEstoquePlataforma: {
        development: 'https://local.webmotors.com.br:44343',
        homologation: 'https://hkestoque.webmotors.com.br',
        blue: 'https://azulestoque.webmotors.com.br',
        production: 'https://betaestoque.webmotors.com.br',
    },
    UrlAutoguru: {
        development: 'http://local.webmotors.com.br:8080',
        homologation: 'https://hkautoguru.webmotors.com.br',
        blue: 'https://azulautoguru.webmotors.com.br',
        production: 'https://autoguru.webmotors.com.br',
    },
    UrlCockpit: {
        development: 'http://local.webmotors.com.br:8000',
        homologation: 'https://hcockpit.webmotors.com.br',
        blue: 'https://azulcockpit.webmotors.com.br',
        production: 'https://cockpit.webmotors.com.br',
    },
    UrlCockpitI18n: {
        development: 'http://local.webmotors.com.br:8000',
        homologation: 'https://hml.cockpit.com.br',
        blue: 'https://azl.cockpit.com.br',
        production: 'https://cockpit.webmotors.com.br',
    },
    UrlComprarVeiculos: {
        development: 'https://local.webmotors.com.br:8080',
        homologation: 'https://hkcomprarveiculos.webmotors.com.br',
        blue: 'https://azulcomprarveiculos.webmotors.com.br',
        production: 'https://comprarveiculos.webmotors.com.br',
    },
    UrlCRM: {
        development: 'http://local.webmotors.com.br:81/plataformarevendedor',
        homologation: 'https://hkcrm.webmotors.com.br',
        blue: 'https://azulcrm.webmotors.com.br',
        production: 'https://crm.webmotors.com.br',
    },
    UrlEstoque: {
        development: 'http://local.webmotors.com.br:8080',
        homologation: 'https://hkestoque.webmotors.com.br',
        blue: 'https://azulestoque.webmotors.com.br',
        production: 'https://betaestoque.webmotors.com.br',
    },
    UrlEstoquePlataforma: {
        development: 'http://local.webmotors.com.br:8080',
        homologation: 'https://hestoque.webmotors.com.br',
        blue: 'https://azulestoque.webmotors.com.br',
        production: 'https://estoque.webmotors.com.br',
    },
    UrlGearbox: {
        development: 'http://local.webmotors.com.br:3000',
        homologation: 'https://hgb.webmotors.com.br',
        blue: 'https://azulgb.webmotors.com.br',
        production: 'https://gb.webmotors.com.br',
    },
    UrlMaisFidelidade: {
        development: 'http://local.webmotors.com.br:8080',
        homologation: 'https://hkmaisfidelidade.webmotors.com.br',
        blue: 'https://azulmaisfidelidade.webmotors.com.br',
        production: 'https://maisfidelidade.webmotors.com.br',
    },
    UrlUniversidade: {
        development: 'http://local.webmotors.com.br:8080',
        homologation: 'https://hkuniversidade.webmotors.com.br',
        blue: 'https://azuluniversidade.webmotors.com.br',
        production: 'https://universidade.webmotors.com.br',
    },
};

module.exports = config;
