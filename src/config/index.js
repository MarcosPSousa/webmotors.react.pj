/* eslint-disable import/no-dynamic-require */
const env = require(process.env.NODE_ENV ? `./env/${process.env.NODE_ENV}` : './env/development');

module.exports = env;
