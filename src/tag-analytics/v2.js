import { jwtGet } from 'webmotors-react-pj/utils';

const AdobeTrackingDefault = {
    site: {
        domain: undefined,
        country: undefined,
        server: undefined,
        environment: undefined,
        subEnvironment: undefined,
        microEnvironment: undefined,
        tmsVersion: undefined,
        clientType: undefined,
        platform: undefined,
    },
    page: {
        flowType: undefined,
        pageType: undefined,
        pageName: undefined,
        'pageName.tier1': undefined,
        'pageName.tier2': undefined,
        'pageName.tier3': undefined,
        error: undefined,
        loadTime: undefined,
        url: undefined,
        trackingCodeExternal: undefined,
        trackingCodeInternal: undefined,
    },
    user: {
        loginStatus: undefined,
        userID: undefined,
        loginID: undefined,
        cockpitUserID: undefined,
        cdCliente: undefined,
        type: undefined,
        clientType: undefined,
        clientCode: undefined,
        nmCidade: undefined,
        nmEstado: undefined,
        nmFantasia: undefined,
        nmRede: undefined,
        cdRede: undefined,
        nmZona: undefined,
        cdZona: undefined,
        nmFilial: undefined,
        cdFilial: undefined,
        pacoteProduto: undefined,
        cdPacoteProduto: undefined,
    },
};

const dispatchEventTagManager = e => console.log({ event: e.type });

export default ({ dataLayer = {}, event = 'customPageView' }) => {
    const { objDataLayer } = window;
    const AdobeTracking = (objDataLayer ? { ...objDataLayer } : { ...AdobeTrackingDefault });

    Object.entries(dataLayer).map(item => {
        const [key, value] = item;
        AdobeTracking[key] = { ...AdobeTracking[key], ...value };
    });

    document.addEventListener(event, dispatchEventTagManager);
    document.dispatchEvent(new CustomEvent(event, { detail: AdobeTracking }));
    document.removeEventListener(event, dispatchEventTagManager);
};
