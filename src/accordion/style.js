import styled from 'styled-components';

export default styled.div`
    overflow: hidden;
    transition: height 0.3s;
    &.accordion-open{
        overflow: visible;
    }
`;
