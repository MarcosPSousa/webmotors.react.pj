import React, { useEffect, useRef } from 'react';
import StyleWrapper from 'webmotors-react-pj/accordion/style'; 

export default ({ children, open }) => {
    const accordion = useRef();

    useEffect(() => {
        const { current } = accordion;
        const { style } = current;
        current.addEventListener('transitionstart', () => {
            const openState = (current.dataset.accordion === 'true');
            if (!openState) {
                current.classList.remove('accordion-open');
            }
        });
        current.addEventListener('transitionend', () => {
            const openState = (current.dataset.accordion === 'true');
            if (openState) {
                current.classList.add('accordion-open');
            }
        });
    }, []);

    useEffect(() => {
        const { current } = accordion;
        const { style } = current;
        if (open) {
            style.height = 'auto';
            const { clientHeight } = current;
            style.height = '0px';
            setTimeout(() => {
                style.height = `${clientHeight}px`;
            }, 10);
        } else {
            style.height = '0px';
        }
    }, [open]);

    return <StyleWrapper ref={accordion} data-accordion={open}>{children}</StyleWrapper>
}