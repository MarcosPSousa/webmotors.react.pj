import React, { Component, createRef } from 'react';
import SVG from 'webmotors-svg';

class WebModal extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.modal = createRef();
        this.wrap = createRef();
        this.content = createRef();
    }

    componentDidMount() {
        this.handleZIndex();
        if (this.props.onOpen) {
            this.props.onOpen();
        }
    }

    handleZIndex() {
        if (this.props.classZIndex) {
            document.querySelector(this.props.classZIndex).classList[this.state.close ? 'remove' : 'add']('modal__zindex');
        }
    }

    handleClose() {
        this.setState({
            close: true,
        }, () => this.modal.current.addEventListener('animationend', () => {
            if (this.props.onClose) {
                this.props.onClose();
            } else {
                const { _reactInternalFiber: internal } = this;
                const { _debugOwner: parent } = internal;
                if (parent) {
                    parent.stateNode.setState({ [(this.props.stateToggle || 'modal')]: false });
                }
            }
            this.handleZIndex();
        }));
    }

    handleScrollEnter() {
        const { current } = this.content;
        const { offsetHeight } = current;
        const { scrollHeight } = current;
        current.classList[offsetHeight === scrollHeight ? 'add' : 'remove']('modal__content--noscroll');
    }

    handleScrollLeave() {
        const { current } = this.content;
        current.classList.remove('modal__content--noscroll');
    }

    render() {
        return (
            <div ref={this.modal} className={`modal ${this.state.close ? 'modal--close' : ''}`}>
                <div aria-hidden="true" className="modal__bg" onClick={() => this.handleClose()} />
                <div className={`modal__box ${this.props.size ? `modal__box--${this.props.size}` : ''}`}>
                    {this.props.icon && <div className="ta-c"><SVG className="modal__icon" src={this.props.icon} /></div>}
                    {this.props.title && <h2 className="modal__bigtitle ta-c">{this.props.title}</h2>}
                    <div aria-hidden="true" className="modal__close" onClick={() => this.handleClose()}>
                        <SVG src="/assets/img/icons/times.svg" />
                    </div>
                    {
                        this.props.children && (
                            <div className={`modal__wrap ${this.props.shadow ? 'modal__wrap--shadow' : ''}`}>
                                <div className="modal__content" ref={this.content} onMouseEnter={() => this.handleScrollEnter()} onMouseLeave={() => this.handleScrollLeave()}>
                                    {this.props.children}
                                </div>
                            </div>
                        )
                    }
                    {
                        this.props.button && (
                            <div className="modal__buttons">
                                {this.props.button}
                            </div>
                        )
                    }
                </div>
            </div>
        );
    }
}

export default WebModal;
