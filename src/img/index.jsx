import React, { useState, useEffect } from 'react';
import Dexie from 'dexie';

export default (props) => {
    const {
        src,
        v = 1,
        maxSize = 1024 * 300,
        onClick,
    } = props;
    const [img, setImg] = useState('');
    const [imgError, setImgError] = useState();
    const idb = new Dexie('img-store');

    const propsImg = () => (
        Object.entries(props).map((entries) => {
            const [attr, value] = entries;
            if (!/^(on)|(maxSize|v|src)$/.test(attr)) {
                return `${attr === 'className' ? 'class' : attr}="${value}" `;
            }
            return '';
        }).join('').trim()
    );

    if (!src) {
        console.error('Component <Img />: attribute src undefined');
        return false;
    }

    const blobToBitmap = blob => setImg(`<img src="${URL.createObjectURL(blob)}" ${propsImg()} />`);

    const blobToSvg = e => (
        e.text().then((f) => {
            const data = f
                .replace(/(<\?(\n|.)+?\?>|<!--(\n|.)+?-->|\s(version|x|y|style|(xml:|xmlns:).+?)=".+?")/g, '')
                .replace(/(\s|\n\s)+/g, ' ')
                .replace(/>\s+</g, '><')
                .replace(/\s+<\//g, '</')
                .replace(/<style(\n|.)+?<\/style>/g, '')
                .replace(/<svg/, `<svg ${propsImg()}`);
            setImg(data);
        })
    );

    const imgDetected = blob => (
        (blob.type === 'image/svg+xml')
            ? blobToSvg(blob)
            : blobToBitmap(blob)
    );

    const imgFetch = ({ table, action }) => {
        const url = src.replace(/((\?|#).+|(\?|#|\/)$)/g, '');
        window.fetch(url)
            .then(e => e.blob())
            .then((blob) => {
                const { type, size } = blob;
                if (!/^image/.test(type)) {
                    return setImgError(`file "${url}" isn't image/*`);
                }
                if (size > maxSize) {
                    return setImgError(`file size is more then ${maxSize / 1024}kb`);
                }
                imgDetected(blob);
                setTimeout(() => {
                    const tableActions = {
                        add: () => table.add({ src: url, v, blob }),
                        update: () => table.update(url, { v, blob }),
                    };
                    if (tableActions[action]) {
                        tableActions[action]();
                    }
                }, 10);
                return blob;
            })
            .catch(e => setImgError(`file "${url}" error: "${e.message}"`));
    };

    const imgGet = ({ table, data }) => (
        (data.v !== v)
            ? imgFetch({ table, action: 'update' })
            : imgDetected(data.blob)
    );

    const imgRender = () => {
        idb.version(1).stores({ images: 'src,blob,v' });
        const { images } = idb;
        idb.transaction('rw', images, () => (
            images.get({ src }).then(data => (
                !data
                    ? imgFetch({ table: images, action: 'add' })
                    : imgGet({ table: images, data })
            )).catch(data => console.error({ data }))
        ));
    };

    useEffect(() => imgRender(), []);

    return (
        <span
            data-src={src}
            data-error={imgError}
            data-version={v === 1 ? undefined : v}
            dangerouslySetInnerHTML={{ __html: img }}
            onClick={onClick}
        />
    );
};
