import styled, { createGlobalStyle } from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const statusColor = (status) => ({
    default: color('gray-3'),
    success: color('primary'),
    error: color('error'),
}[status || 'default']);

const textColor = (status) => {

};

export const StyleWrapper = styled.div`
    display: flex;
`;

export const StyleItem = styled.div`
    width: 40px;
    margin-right: 8px;
    border-bottom: 2px solid;
    font-size: 1.2rem;
    line-height: 1.5em;
    text-align: center;
    transition: border-bottom-color 0.3s, color 0.3s;
    border-bottom-color: ${({ status }) => statusColor(status)};
    color: ${({ status }) => statusColor(status)};
`;

export const StyleCheck = styled.div`
    width: 24px;
    height: 24px;
    fill: ${({ checked }) => checked ? color('success') : 'transparent'};
    transition: fill 0.3s;
`;
