import React, { useRef, useEffect, useState } from 'react';
import { StyleWrapper, StyleItem, StyleCheck } from 'webmotors-react-pj/password-criteria/style';

export default ({ input }) => {
    const [stateLowerCase, setStateLowerCase] = useState('default');
    const [stateUpperCase, setStateUpperCase] = useState('default');
    const [stateNumber, setStateNumber] = useState('default');
    const [stateSpecial, setStateSpecial] = useState('default');
    const [stateMoreThen, setStateMoreThen] = useState('default');
    const [stateChecked, setStateChecked] = useState(false);

    const handlerInput = () => {
        setTimeout(() => {
            const inputPassword = document.querySelector(input);
            const stateChangeSuccess = (fn, condition) => fn(condition ? 'success' : 'default');
            const stateChangeError = (fn, condition) => fn(condition ? 'success' : 'error');
            if (inputPassword) {
                inputPassword.addEventListener('input', (e) => {
                    const { value } = e.target;
                    stateChangeSuccess(setStateLowerCase, (/[a-z]/g.test(value)));
                    stateChangeSuccess(setStateUpperCase, (/[A-Z]/g.test(value)));
                    stateChangeSuccess(setStateNumber, (/\d/g.test(value)));
                    stateChangeSuccess(setStateSpecial, (/(\W|_)/g.test(value)));
                    stateChangeSuccess(setStateMoreThen, (value.length >= 8));
                });
                inputPassword.addEventListener('blur', (e) => {
                    const { value } = e.target;
                    stateChangeError(setStateLowerCase, (/[a-z]/g.test(value)));
                    stateChangeError(setStateUpperCase, (/[A-Z]/g.test(value)));
                    stateChangeError(setStateNumber, (/\d/g.test(value)));
                    stateChangeError(setStateSpecial, (/(\W|_)/g.test(value)));
                    stateChangeError(setStateMoreThen, (value.length >= 8));
                });
            } else {
                console.error(`[password-criteria] selector ${input} not found`);
            }
        }, 10);
    }

    useEffect(() => handlerInput(), []);

    useEffect(() => {
        const stateSuccessComplete = [stateLowerCase, stateUpperCase, stateNumber, stateSpecial, stateMoreThen].includes('default');
        const stateErrorComplete = [stateLowerCase, stateUpperCase, stateNumber, stateSpecial, stateMoreThen].includes('error');

        setStateChecked(!stateSuccessComplete && !stateErrorComplete);
    }, [stateLowerCase, stateUpperCase, stateNumber, stateSpecial, stateMoreThen]);
    
    return (
        <StyleWrapper>
            <StyleItem status={stateLowerCase}>abc</StyleItem>
            <StyleItem status={stateUpperCase}>ABC</StyleItem>
            <StyleItem status={stateNumber}>123</StyleItem>
            <StyleItem status={stateSpecial}>!@#</StyleItem>
            <StyleItem status={stateMoreThen}>8+</StyleItem>
            <StyleCheck checked={stateChecked}>
                <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.2595 15.7403L7.2925 13.7733C6.9025 13.3833 6.9025 12.7502 7.2925 12.3593C7.6835 11.9693 8.3165 11.9693 8.7065 12.3593L9.9665 13.6192L15.2925 8.29325C15.6835 7.90225 16.3165 7.90225 16.7065 8.29325C17.0975 8.68325 17.0975 9.31625 16.7065 9.70725L10.6735 15.7403C10.4785 15.9353 10.2225 16.0332 9.9665 16.0332C9.7105 16.0332 9.4545 15.9353 9.2595 15.7403Z" />
                </svg>
            </StyleCheck>
        </StyleWrapper>
    );
};
