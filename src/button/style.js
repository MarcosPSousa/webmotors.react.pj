import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const borderColor = (token) => ({
    primary: color('primary'),
    secondary: color('primary-3'),
    tertiary: color('primary'),
    ghost: 'transparent',
    inactive: color('gray-3'),
    cancel: color('gray-2'),
    danger: color('danger'),
}[token || 'primary']);

const backgroundColor = (token) => ({
    primary: color('primary'),
    secondary: color('primary-3'),
    tertiary: 'transparent',
    ghost: 'transparent',
    inactive: color('gray-3'),
    cancel: 'transparent',
    danger: color('danger'),
}[token || 'primary']);

const textColor = (token) => ({
    primary: color('white'),
    secondary: color('primary'),
    tertiary: color('primary'),
    ghost: color('primary'),
    inactive: color('white'),
    cancel: color('gray-2'),
    danger: color('gray-1'),
}[token || 'primary']);

export const StyleElement = ({ tag, token, size }) => styled(tag)`
    border: 1px solid;
    border-color: ${borderColor(token)};
    background-color: ${backgroundColor(token)};
    color: ${textColor(token)};
    font-weight: bold;
    font-size: ${({ size }) => (size === 'big' ? '1.6rem' : '1.2rem')};
    padding: ${({ size }) => (size === 'big' ? '16px 24px' : '12px 20px')};
    border-radius: 8px;
`;

export const StyleIcon = styled.i`
    display: none;
`;
