import React, { Fragment, createElement } from 'react';
import { StyleElement, StyleIcon } from 'webmotors-react-pj/button/style';
import { customAttributes } from 'webmotors-react-pj/utils';

export default (props) => {
    const {
        tag = 'button',
        type = 'submit',
        size = 'small',
        token = 'primary',
        icon,
        children,
    } = props;
    const propsCustom = customAttributes(props, 'tag|token|icon|size');
    return createElement(
        StyleElement({ tag, token, size }),
        { ...propsCustom },
        <Fragment>
            { children }
            <StyleIcon>{ icon }</StyleIcon>
        </Fragment>
    );
};
