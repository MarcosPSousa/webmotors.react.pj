import React, { Component, createRef } from 'react';
import { scrollSmooth, Ajax } from 'webmotors-react-pj/utils';

class Form extends Component {
    constructor(props) {
        super(props);
        this.form = createRef();
    }

    getError() {
        const inputsError = Boolean(document.querySelector('.inputtext--error')) || Boolean(document.querySelector('[data-input-error="true"]'));
        return inputsError;
    }

    handlerSubmit(e) {
        const { target } = e;
        const {
            action: url,
            method,
            onSuccess = () => '',
            onError = () => '',
        } = this.props;
        if (!url) {
            console.error('[Component] Form: url undefined');
            if (this.props.onComplete) {
                this.handleFormEvent(this.props.onComplete, e);
            }
            return false;
        }
        if (!this.getError()) {
            const data = {};
            const button = target.querySelector('[type="submit"]');
            const inputs = [...target.querySelectorAll('input, select, textarea')];
            const fomatObject = (objCurrent, path, value) => {
                const current = objCurrent;
                const limiter = /\[|\./.exec(path) || (path && /\s$/.exec(`${path} `));
                if (limiter) {
                    const key = path.substring(0, limiter.index);
                    const isArray = limiter[0] === '[';
                    if (isArray) {
                        const pathNext = path.replace(new RegExp(`^${key}.|.$`, 'g'), '');
                        if (!current[key]) {
                            current[key] = [];
                        }
                        current[key].push(!pathNext ? value : { [pathNext]: value });
                    } else {
                        const pathNext = path.replace(new RegExp(`${key}?.`), '');
                        if (!current[key]) {
                            current[key] = !pathNext ? value : {};
                        }
                        fomatObject(current[key], pathNext, value);
                    }
                }
            };
            const formatNumber = v => Number(v);
            const formatString = v => (v).toString();
            const formatBoolean = v => v === 'true';
            const formatJSON = v => JSON.parse(v);
            inputs.forEach((input) => {
                const { type, value: val, key } = input.dataset;
                const value = val || input.value;
                const name = key || input.name;
                const { type: typeInput} = input;
                let valueFinal = null;
                if (!type) {
                    if (value === 'true' || value === 'false') {
                        valueFinal = formatBoolean(value);
                    } else {
                        const valueToNumber = Number(value);
                        valueFinal = window.Number.isNaN(valueToNumber)
                            ? formatString(value)
                            : formatNumber(value);
                    }
                } else {
                    switch (type) {
                        case 'boolean': {
                            valueFinal = formatBoolean(value);
                            break;
                        }
                        case 'number': {
                            valueFinal = formatNumber(value);
                            break;
                        }
                        case 'json': {
                            valueFinal = formatJSON(value);
                            break;
                        }
                        default: {
                            valueFinal = formatString(value);
                        }
                    }
                }
                if ((typeInput === 'radio' && input.checked) || typeInput !== 'radio') {
                    fomatObject(data, name, valueFinal);
                }
            });
            const inputLock = (state) => {
                inputs.forEach((input) => {
                    input[state ? 'setAttribute' : 'removeAttribute']('readonly', '');
                    input.parentNode.classList[state ? 'add' : 'remove']('input-lock');
                });
            };
            const buttonLock = (element) => {
                const btn = element;
                btn.disabled = true;
                btn.setAttribute('data-button', btn.innerHTML);
                btn.textContent = 'Aguarde...';
            };
            const buttonUnlock = (element) => {
                const btn = element;
                btn.removeAttribute('disabled');
                btn.innerHTML = btn.getAttribute('data-button').replace('<script', '');
            };
            buttonLock(button);
            inputLock(true);
            Ajax({
                url,
                method,
                data,
                target,
            })
                .then(f => {
                    this.handleFormEvent(f.success ? onSuccess : onError, f);
                    return f;
                })
                .then((f) => {
                    buttonUnlock(button);
                    inputLock(false);
                    if (this.props.onComplete) {
                        this.handleFormEvent(this.props.onComplete, f);
                    }
                });
        }
        return true;
    }

    handleBeforeSend(e) {
        const { target } = e;
        const inputAll = [...target.querySelectorAll('input, select, textarea')];
        const { scrollX, scrollY } = window;
        inputAll.forEach((item) => {
            item.focus();
            item.blur();
        });
        window.scrollTo(scrollX, scrollY);
        if (this.getError()) {
            const error = target.querySelector('.inputtext--error') || document.querySelector('[data-input-error="true"]');
            const rect = error.getBoundingClientRect();
            const top = rect.top + window.pageYOffset - 4;
            scrollSmooth(top);
        }
    }

    handleFormEvent(fn, e) {
        fn(e);
    }

    render() {
        const {
            onBeforeSend,
            className,
        } = this.props;
        return (
            <form
                className={`form ${!className ? '' : className}`}
                ref={this.form}
                autoComplete={this.props.autoComplete || 'off'}
                onSubmit={(e) => {
                    e.preventDefault();
                    e.persist();
                    const before = onBeforeSend && onBeforeSend();
                    if (before || before === undefined) {
                        this.handleBeforeSend(e);
                        setTimeout(() => this.handlerSubmit(e), 10);
                    }
                }}
            >
                {this.props.children}
            </form>
        );
    }
}

export default Form;
