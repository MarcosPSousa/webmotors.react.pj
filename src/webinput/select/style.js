import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleWrapper = styled.div`
    position: relative;
    font-size: 1.2rem;
    line-height: 1.5em;
    *::-webkit-scrollbar {
        width: 10px;
    }
    *::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: ${color('gray-3')};
        border: 2px solid ${color('gray-4')};
    }
    *:hover::-webkit-scrollbar-track {
        border-radius: 10px;
        background-color: #ddd;
    }
    *:hover::-webkit-scrollbar-thumb {
        border-color: #ddd;
    }
`;

export const StyleSelect = styled.select`
    position: absolute;
    opacity: 0;
    width: 1px;
    height: 1px;
`;
export const StyleContent = styled.div`
    border: 2px solid;
    padding: 7px 16px 5px;
    border-color: ${color('gray-4')};
    border-radius: 8px;
    background-color: ${color('white')};
    color: ${color('gray-3')};
    cursor: default;
    font-size: 1.2rem;
    line-height: 1.5em;
    position: relative;
    width: ${({ width }) => width};
    white-space: nowrap;
`;

export const StylePlaceholder = styled.span`
    position: absolute;
    left: 0;
    top: 50%;
    transform: translate(5px,-50%);
    margin-top: 1px;
    padding: 0 12px;
    background-color: ${color('white')};
    color: ${color('gray-3')};
    transition: top 0.3s, margin-top 0.3s, transform 0.3s,color 0.3s;
    will-change: top, margin-top, transform;
`;

export const StyleItemCurrent = styled.div`
    color: ${color('gray-3')};
    transition: color 0.3s;
    display: flex;
    justify-content: space-between;
    align-items: center;
    transform: translateY(1px);
`;

export const StyleItemName = styled.div`
    flex: 1;
    overflow: hidden;
    text-overflow: ellipsis;
`;

export const StyleItemIcon = styled.svg`
    width: 24px;
    height: 24px;
    fill: ${color('gray-3')};
    transform: translateY(-1px);
    margin-left: 16px;
`;
export const StyleOptions = styled.div`
    position: absolute;
    left: -2px;
    top: -2px;
    min-width: calc(100% + 4px);
    background-color: ${color('gray-4')};
    padding: 12px;
    border-radius: 0 0 8px 8px;
    transform-origin: top;
    transform: ${({ open }) => open ? 'rotateX(0deg)' : 'rotateX(-90deg)'};
    pointer-events: ${({ open }) => open ? 'all' : 'none'};
    opacity: ${({ open }) => open ? 1 : 0 };
    will-change: transform, opacity;
    transition: transform 0.3s, opacity 0.3s;
    z-index: 2;
`;

export const StyleOptionsContent = styled.div`
    overflow: auto;
    max-height: ${({ maxItems }) => `${(2.8 * maxItems) + 0.2}em`};
`;

export const StyleOptionsItem = styled.div`
    color: ${color('gray')};
    border-radius: 8px;
    padding: 8px 12px;
    font-weight: 500;
    transition: background-color 0.3s;
    &:hover{
        background-color: ${color('white')}
    }
`;
