import React, {
    useState,
    useEffect,
    useRef,
    forwardRef,
} from 'react';
import {
    StyleWrapper,
    StyleSelect,
    StyleContent,
    StylePlaceholder,
    StyleItemCurrent,
    StyleItemName,
    StyleItemIcon,
    StyleOptions,
    StyleOptionsContent,
    StyleOptionsItem,
} from 'webmotors-react-pj/webinput/select/style';
import CustomAttributes from 'webmotors-react-pj/webinput/custom-attributes';

export default forwardRef((props, ref) => {
    const {
        children = [],
        onChange,
        value,
        placeholder,
        maxItems = Math.min(props.children && props.children.length, 5),
        id = `webinput_select_${Math.floor(Math.random() * 100000)}`,
        width = '200px',
    } = props;
    const [open, setOpen] = useState(false);
    const [selectText, setSelectText] = useState();
    const [selectValue, setSelectValue] = useState(value);
    const [selectItems, setSelectItems] = useState(children);
    const selectInput = (useRef() || ref);
    const propsCustom = CustomAttributes(props, 'options|maxItems|value');

    const handleOpen = () => {
        setOpen(true);
    };

    const handleChange = (e) => {
        setSelectValue(e.target.value);
        onChange && onChange(e);
    };

    const handleSelectItem = (e) => {
        const { target } = e;

        setSelectValue(target.dataset.value || '');
        setSelectText(target.textContent);
        setOpen(false);
    };

    const forceClose = () => {
        window.addEventListener('click', (e) => {
            if (!e.target.closest('[data-inputselect]')) {
                setOpen(false);
            }
        });
    };

    const renderPlaceholder = () => placeholder && <StylePlaceholder>{placeholder}</StylePlaceholder>;

    useEffect(() => {
        forceClose();
        const child = [];
        children.forEach((item) => {
            if (item && item.props) {
                const { value: optionValue } = item.props;
                const { children: optionContent } = item.props;
                child.push(
                    <StyleOptionsItem
                        key={optionValue || Math.floor(Math.random() * 100000)}
                        data-value={optionValue}
                        onClick={(e) => handleSelectItem(e)}>
                        {optionContent}
                    </StyleOptionsItem>
                );
            }
        });
        const { selectedOptions } = selectInput.current;
        const [selectedOptionsText] = selectedOptions;
        setSelectText(selectedOptionsText && selectedOptionsText.textContent);
        setSelectItems(child);
    }, [children]);

    useEffect(() => {
        // console.log({ value });
        setTimeout(() => setSelectValue(value), 10);
    }, [value]);

    useEffect(() => {
        const { current } = selectInput;
        Object.getOwnPropertyDescriptor(window.HTMLSelectElement.prototype, 'value').get.call(current);
        const event = new Event('change', { bubbles: true });
        current.dispatchEvent(event);
        setTimeout(() => {
            const { selectedOptions } = current;
            let textContent = '';
            if (selectedOptions && selectedOptions[0]) {
                textContent = selectedOptions[0].textContent;
            }
            setSelectText(textContent);
        }, 10);
    }, [selectValue]);

    return (
        <StyleWrapper data-inputselect>
            <StyleSelect
                {...propsCustom}
                ref={selectInput}
                value={selectValue}
                onChange={e => handleChange(e)}
            >
                {children}
            </StyleSelect>
            <StyleContent width={width} data-value={selectValue}>
                { renderPlaceholder() }
                <StyleItemCurrent onClick={() => handleOpen()}>
                    <StyleItemName>{selectText}</StyleItemName>
                    <StyleItemIcon viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21.0004 8.00012C21.0004 8.25612 20.9024 8.51212 20.7074 8.70712L12.7074 16.7071C12.3164 17.0981 11.6844 17.0981 11.2934 16.7071L3.29337 8.70712C2.90237 8.31612 2.90237 7.68412 3.29337 7.29312C3.68437 6.90212 4.31637 6.90212 4.70737 7.29312L12.0004 14.5861L19.2934 7.29312C19.6844 6.90212 20.3164 6.90212 20.7074 7.29312C20.9024 7.48812 21.0004 7.74412 21.0004 8.00012Z"/>
                    </StyleItemIcon>
                </StyleItemCurrent>
                <StyleOptions open={open}>
                    <StyleOptionsContent maxItems={maxItems}>
                        {selectItems}
                    </StyleOptionsContent>
                </StyleOptions>
            </StyleContent>
        </StyleWrapper>
    )
});
