export default (props, except) => {
    const propsCustom = {};
    Object.entries(props).forEach((item) => {
        const [propKey, propValue] = item;
        const regex = new RegExp(`^(?!(${except})).+`);
        if (regex.test(propKey)) {
            propsCustom[propKey] = propValue;
        }
    });
    return propsCustom;
};
