import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleLabel = styled.label`
    display: flex;
    align-items: center;
    position: relative;
    padding-right: 24px;
`;
export const StyleInput = styled.input`
    position: absolute;
    left: 0;
    top: 0;
    opacity: 0;
    &:checked{
        + div{
            border-color: transparent;
            background-color: ${color('primary')};
            svg{
                fill: ${color('white')};
            }
        }
    }
`;
export const StyleSquare = styled.div`
    width: 18px;
    height: 18px;
    border: 2px solid ${color('gray')};
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 100%;
    transition: border-color 0.3s, background-color 0.3s;
    svg{
        flex: 1;
        fill: transparent;
        transition: fill 0.3s;
    }
`;
export const StylePlaceholder = styled.div`
    padding-left: 12px;
    font-size: 1.6rem;
    color: ${color('gray')};
    font-weight: 500;
`;
