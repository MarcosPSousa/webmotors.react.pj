import React, { useState, useEffect, forwardRef, useRef } from 'react';
import {
    StyleLabel,
    StyleInput,
    StyleToggle,
    StylePlaceholder,
} from 'webmotors-react-pj/webinput/switch/style';
import CustomAttributes from 'webmotors-react-pj/webinput/custom-attributes';

export default forwardRef((props, ref) => {
    const {
        id = `webinput_switch_${Math.floor(Math.random() * 100000)}`,
        name,
        value,
        checked,
        onChange,
        placeholder,
    } = props;
    const [itemChecked, setItemChecked] = useState(checked);
    const input = (ref || useRef());
    const propsCustom = CustomAttributes(props, 'onChange|checked');

    const handleChange = (e) => {
        setItemChecked(prevState => !prevState);
        onChange && onChange(e);
    }

    useEffect(() => {
        setItemChecked(checked);
        input.current.checked = checked;
    }, [checked]);

    return (
        <StyleLabel>
            <StyleInput
                ref={input}
                {...propsCustom}
                type="checkbox"
                defaultChecked={itemChecked}
                id={id}
                onChange={e => handleChange(e)}
            />
            <StyleToggle />
            <StylePlaceholder>{placeholder}</StylePlaceholder>
        </StyleLabel>
    )
});
