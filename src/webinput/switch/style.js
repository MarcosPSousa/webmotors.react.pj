import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

export const StyleLabel = styled.label`
    display: inline-flex;
    align-items: center;
    position: relative;
    margin-top: 2px;
`;

export const StyleToggle = styled.div`
    width: 48px;
    height: 20px;
    border-radius: 20px;
    background-color: ${color('gray-3')};
    position: relative;
    &:before{
        content: '';
        width: 24px;
        height: 24px;
        position: absolute;
        left: 0;
        top: 50%;
        margin-top: -12px;
        background-color: ${color('gray-2')};
        transform: translateX(0px);
        border-radius: 100%;
        transition: background-color 0.3s, transform 0.3s;
        will-change: background-color, transform;
    }
`;

export const StyleInput = styled.input`
    position: absolute;
    left: 0;
    top: 0;
    opacity: 0;
    &:checked{
        + div {
            background-color: ${color('primary-3')};
            transition: background-color 0.3s;
            &:before{
                transform: translateX(24px);
                height: 24px;
                background-color: ${color('primary')};
            }
        }
    }
`;


export const StylePlaceholder = styled.div`
    font-size: 1.6rem;
    padding-left: 16px;
`;

