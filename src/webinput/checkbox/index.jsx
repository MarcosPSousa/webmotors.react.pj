import React, { useState, forwardRef } from 'react';
import {
    StyleLabel,
    StyleInput,
    StyleSquare,
    StylePlaceholder,
} from 'webmotors-react-pj/webinput/checkbox/style';
import CustomAttributes from 'webmotors-react-pj/webinput/custom-attributes';

export default forwardRef((props, ref) => {
    const {
        checked,
        placeholder,
        onChange,
    } = props;
    const [itemChecked, setItemChecked] = useState(checked);
    const propsCustom = CustomAttributes(props, 'onChange|type');

    const handleChange = (e) => {
        setItemChecked(prevState => !prevState);
        onChange && onChange(e);
    }

    return (
        <StyleLabel>
            <StyleInput
                ref={ref}
                {...propsCustom}
                type="checkbox"
                defaultChecked={itemChecked}
                onChange={e => handleChange(e)}
            />
            <StyleSquare>
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 18 18">
                    <path d="M12.7,5.3l-5.3,5.3L5.3,8.5c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4l2.8,2.8C6.9,12.9,7.2,13,7.4,13s0.5-0.1,0.7-0.3l6-6 c0.4-0.4,0.4-1,0-1.4C13.7,4.9,13.1,4.9,12.7,5.3z"/>
                </svg>
            </StyleSquare>
            <StylePlaceholder>{placeholder}</StylePlaceholder>
        </StyleLabel>
    )
});
