import React, {
    useState,
    useEffect,
    useRef,
    forwardRef,
} from 'react';
import Img from 'webmotors-react-pj/img';
import {
    StyleLabel,
    StylePlaceholder,
    StyleInput,
    StyleDescript,
} from 'webmotors-react-pj/webinput/text/style';
import validation from 'webmotors-react-pj/webinput/text/validation';
import { getMask, setMask } from 'webmotors-react-pj/webinput/text/mask';
import CustomAttributes from 'webmotors-react-pj/webinput/custom-attributes';

export default forwardRef((props, ref) => {
    const {
        type = 'text',
        description = '',
        placeholder = '',
        onInput,
        onFocus,
        onBlur,
        messageError,
        messageSuccess,
        value: valueInput = '',
        required,
        iconLeft,
        iconLeftEvent,
        iconRight,
        iconRightEvent,
        equal,
        min,
        max,
        maxLength,
        minLength,
        innerRef,
    } = props;

    const inputRef = (ref || useRef(innerRef));
    const iconPasswordToggle = '/assets/img/icons/eye.svg';
    const inputIsPassword = (type === 'password' || type === 'password-weakened');
    const [active, setActive] = useState(false);
    const [focus, setFocus] = useState(false);
    const [error, setError] = useState(false);
    const [value, setValue] = useState(valueInput);
    const [readonly, setReadonly] = useState(true);
    const [iconSrcLeft, setIconSrcLeft] = useState(iconLeft);
    const [iconSrcRight, setIconSrcRight] = useState(inputIsPassword ? iconPasswordToggle : iconRight);
    const [descript, setDescript] = useState(description);
    const [inputType, setInputType] = useState(inputIsPassword ? 'password' : 'text');
    const propsCustom = CustomAttributes(props, 'onBlur|onFocus|onInput|placeholder|value|required');

    const handleMask = () => {
        const typeMask = getMask(type);
        if (typeMask) {
            setMask({
                input: inputRef.current,
                mask: typeMask,
                initialValue: value,
            });
        }
    };

    const handleFormat = () => {
        if (type === 'email') {
            inputRef.current.value = value.toLowerCase();
        }
        if (value.length > maxLength) {
            inputRef.current.value = value.substring(0, maxLength);
        }
    }

    const handleInput = (e) => {
        const { value } = e.target;
        let valueFormat
        if (type === 'email') {
            const { current: input } = inputRef
            const { value } = input;
            input.value = value.toLowerCase();
        } else {

        }
        setValue(e.target.value);
        onInput && onInput(e);
    };

    const handleFocus = (e) => {
        setActive(true);
        setFocus(true);
        setReadonly(false);
        onFocus && onFocus(e);
    };

    const handleBlur = (e) => {
        e.persist();
        const valueEmpty = value.length === 0;
        setActive(!valueEmpty);
        setFocus(false);
        validation({
            value,
            type,
            min,
            max,
            required,
            equal,
        }).then((f) => {
            const { success, data } = f;
            const dataError = !success && (messageError || data);
            const dataSuccess = success && (messageSuccess || description);
            renderIconRightType(success);
            setError(!success);
            setDescript(!success ? dataError : dataSuccess);
            onBlur && onBlur(e);
        });
    };

    const handleClickIconLeft = () => {
        iconLeftEvent && iconLeftEvent();
    };

    const handleClickIconRight = () => {
        setInputType(prevType => {
            if (inputIsPassword){
                return prevType === 'password' ? 'text' : 'password'
            }
            return 'text';
        });
        iconRightEvent && iconRightEvent();
    };

    const renderIconError = (success) => {
        setIconSrcRight(!success ? '/assets/img/icons/alert-2.svg' : iconRight);
    };

    const renderIconRightType = (success) => {
        (inputIsPassword)
            ? setIconSrcRight(iconPasswordToggle)
            : renderIconError(success);
    };

    const renderIconLeft = () => (
        iconSrcLeft && <Img className="icon icon-left" src={iconSrcLeft} onClick={() => handleClickIconLeft()} />
    );

    const renderIconRight = () => (
        iconSrcRight && <Img className="icon icon-right" src={iconSrcRight} onClick={() => handleClickIconRight()} />
    );

    const handleActive = () => {
        if (value && value.length !== 0) {
            setActive(true);
        }
    };

    useEffect(() => {
        handleActive();
        setTimeout(() =>  {
            handleMask();
        }, 100);
    }, []);

    useEffect(() => {
        handleActive();
        handleFormat();
        setTimeout(() =>  {
            handleMask();
        }, 100);
    }, [value]);

    useEffect(() => {
        setDescript(messageError);
        setError(messageError && messageError.length);
    }, [messageError]);

    useEffect(() => {
        setDescript(messageSuccess);
    }, [messageSuccess]);

    return (
        <div>
            <StyleLabel active={active} focus={focus} error={error} data-input-error={error}>
                {renderIconLeft()}
                <StylePlaceholder active={active} focus={focus} error={error} iconLeft={iconSrcLeft}>{placeholder}</StylePlaceholder>
                <StyleInput
                    {...propsCustom}
                    error={error}
                    active={active}
                    type={inputType}
                    defaultValue={value}
                    ref={inputRef}
                    readOnly={readonly}
                    onInput={e => handleInput(e)}
                    onFocus={e => handleFocus(e)}
                    onBlur={e => handleBlur(e)}
                />
                {renderIconRight()}
            </StyleLabel>
            <StyleDescript>{descript}</StyleDescript>
        </div>
    );
});
