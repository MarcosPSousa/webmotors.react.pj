import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';
import { borderColor, placeholderColor, inputColor } from 'webmotors-react-pj/webinput/text/style/colors';

export const StyleLabel = styled.label`
    position: relative;
    display: block;
    border: 2px solid;
    display: flex;
    align-items: center;
    border-color: ${props => borderColor(props)};
    border-radius: 8px;
    background-color: ${color('white')};
    transition: border-color 0.3s;
    &.input-lock{
        opacity: 0.6;
        pointer-events: none;
    }
    > [data-src]{
        width: 24px;
        height: 24px;
        fill: ${props => inputColor(props)};
        position: relative;
        .icon{
            width: 100%;
            height: 100%;
        }
        &:nth-child(1){
            margin-left: 16px;
        }
        &:last-child{
            margin-right: 8px;
        }
    }
`;

export const StylePlaceholder = styled.span`
    position: absolute;
    left: 0;
    top: ${({ active }) => (active ? '0%' : '50%')};
    transform: ${({ iconLeft }) => (iconLeft ? 'translate(41px, -50%)' : 'translate(5px, -50%)')};
    margin-top: ${({ active }) => (active ? '0px' : '1px')};
    padding: 0 12px;
    font-size: 1.2rem;
    line-height: 1.5em;
    background-color: ${color('white')};
    color: ${props => placeholderColor(props)}
    transition: top 0.3s, margin-top 0.3s, transform 0.3s, color 0.3s;
    will-change: top, margin-top, transform;
    cursor: text;
`;

export const StyleInput = styled.input`
    font-size: 1.2rem;
    font-weight: 500;
    line-height: 1.5em;
    padding: 10px 16px 8px;
    border-style: none;
    outline-style: none;
    background-color: transparent;
    width: 100%;
    color: ${props => inputColor(props)}
    transition: color 0.3s, width 0.3s;
`;

export const StyleDescript = styled.div`
    min-height: 20px;
    font-size: 1rem;
    line-height: 1.5em;
    margin-bottom: 12px;
    font-weight: 500;
    padding-top: 4px;
`;
