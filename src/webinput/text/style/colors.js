import color from 'webmotors-react-pj/tokens/color';

export const borderColor = ({ error, focus }) => {
    if (error) {
        return color('error')
    }
    return focus ? color('gray') : color('gray-4');
};

export const placeholderColor = ({ error, focus }) => {
    if (error) {
        return color('error')
    }
    return focus ? color('gray') : color('gray-3');
};

export const inputColor = ({ error, active }) => {
    if (error) {
        return color('error')
    }
    return active ? color('gray') : color('gray-3');
};
