import rules from 'webmotors-react-pj/webinput/text/validation/rules';

export default ({
    type = 'default',
    min,
    max,
    required,
    value,
    equal,
}) => {
    const rulesApply = [];
    const validationByAttributes = { required, equal, min, max };
    const rulesType = rules({
        type,
        min,
        max,
        value,
        equal,
    });

    Object.entries(validationByAttributes).forEach((item) => {
        const [key, rule] = item;
        if (rule) {
            rulesApply.push(key);
        }
    });

    if (rulesType[type]) {
        rulesApply.push(type);
    }

    const validationErrors = rulesApply.filter(item => rulesType[item].fn())[0];
    const validationRule = rulesType[validationErrors];
    const validationRequired = validationErrors === 'required';
    const validationShowFn = validationRule && validationRule.fn();
    const validationShow = (
        (
            (!validationRequired && value.length >= 1)
            || (required && validationShowFn)
        )
        && validationShowFn
    );
    return Promise.resolve({
        data: validationShow ? validationRule.message : '',
        success: !Boolean(validationShow),
    });
};
