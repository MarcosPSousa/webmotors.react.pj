export const getMask = (type) => ({
    default: undefined,
    tel: '(99) 9999-99999',
    date: '99/99/9999',
    'date-short': '99/9999',
    time: '99:99',
    cep: '99999-999',
    plate: '',
    'new-plate': '',
    cpf: '999.999.999-99',
    cnpj: '99.999.999/9999-99',
    renavam: '99999999999',
}[type || 'default']);

export const setMask = ({
    input,
    mask,
    initialValue,
    minSize = false,
    reverse = false,
    letters = false,
    after = null,
}) => {
    const event = () => {
        if (window.navigator.msPointerEnabled) {
            const eventInput = document.createEvent('Event');
            eventInput.initEvent('input', false, true);
            input.dispatchEvent(eventInput);
        } else {
            input.dispatchEvent(new Event('input'));
        }
    }
    const formatAfter = () => {
        (e) => after(e);
    }
    const format = () => {
        let valueCount = 0;
        const valueArray = input.value.replace(/\D/g, '').split('');
        const valueFinal = [];
        const maskArray = mask.split('');
        if (reverse) {
            const maskArrayReverse = maskArray;
            const valueArrayReverse = valueArray;
            maskArrayReverse.reverse();
            valueArrayReverse.reverse();
            maskArrayReverse.forEach((item, i) => {
                if (valueArrayReverse[i - valueCount]) {
                    if (/\D/.test(item)) {
                        valueFinal.push(item);
                        valueCount++;
                    } else {
                        valueFinal.push(valueArrayReverse[i - valueCount]);
                    }
                }
            });
            valueFinal.reverse();
        } else {
            maskArray.forEach((item) => {
                if (valueCount < valueArray.length) {
                    if (/\D/.test(item)) {
                        valueFinal.push(item);
                    } else {
                        valueFinal.push(valueArray[valueCount]);
                        valueCount++;
                    }
                }
            });
        }
        input.value = valueFinal.join('');
    }

    const init = () => {
        input.addEventListener('input', () => format());
        const _value = input.value.trim();
        if (_value !== '' || initialValue) {
            input.value = (input.value || input.dataset.maskfyValue || initialValue);
        }
        event();
    }

    if (!input) {
        return console.error(new Error('Maskfy: Insert tag selector required. Ex.: Maskfy({tag: "[data-maskfy]"})'));
    }
    
    init();
};
