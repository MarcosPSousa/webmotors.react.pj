import React, { Component } from 'react';
import { UrlCockpit } from 'webmotors-react-pj/config';

class Load extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.setState({ className: this.props.className });
    }

    render() {
        return (
            <div className={`cockpit-load ${this.state.className}`}>
                <img
                    className="cockpit-load__icon"
                    src={this.props.icon ? this.props.icon : `${UrlCockpit}/assets/img/brands/webmotors-2-icon.svg`}
                    alt=""
                    aria-hidden="true"
                />
                {this.props.title && <div className="cockpit-load__title">{this.props.title}</div>}
                <div className="cockpit-load__text">{this.props.text || 'Carregando...'}</div>
            </div>
        );
    }
}

export default Load;