import React, { Component } from 'react';
import SVG from 'webmotors-svg';

class ScoreStar extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { score } = this.props;
        const starArray = new Array(5).fill(false);
        const star = starArray.map((item, i) => (i + 1) <= (score || 0));
        return (
            <div className="scorestar">
                { star.map((item, i) => <SVG key={`star_${i}`} className={`scorestar__icon ${item ? 'scorestar__icon--fill' : ''}`} src="/shopping/assets/img/icons/star-rounded.svg" />) }
            </div>
        );
    }
}

export default ScoreStar;
