import React, { createElement } from 'react';
import styled from 'styled-components';
import color from 'webmotors-react-pj/tokens/color';

const StyleHeading = (tag) => styled(tag)`
    color: ${color('gray-2')};
    font-weight: 600;
    font-size: 2rem;
    margin-bottom: 22px;
`;

export default ({ tag = 'h1', children, className }) => createElement(StyleHeading(tag), { className }, children);
